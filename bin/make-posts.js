const fs = require('fs'),
      path = require('path'),
      parseMD = require('parse-md').default,
      stdout = process.stdout;

var docs = [];

fs.readdir('content/posts', function (err, files) {
  if (err) {
    console.error("Could not list the directory!", err);
    process.exit(1);
  }

  files.forEach(function (file, index) {
    const mdFile = path.join('content/posts', file);
    if (path.extname(file) !== '.md') {
      return;
    }

    const slug = path.basename(file, '.md');

    const fileContents = fs.readFileSync(mdFile, 'utf8');
    const { metadata, content } = parseMD(fileContents);

    const doc = {
      id: slug,
      title: metadata.title,
      url: '/posts/' + slug + '/'
    };

    docs.push(doc);
  });

  stdout.write(JSON.stringify(docs));
});

