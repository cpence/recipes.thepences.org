const lunr = require('lunr'),
      fs = require('fs'),
      path = require('path'),
      parseMD = require('parse-md').default,
      stdout = process.stdout;

var builder = new lunr.Builder
builder.pipeline.add(lunr.trimmer,
                     lunr.stopWordFilter,
                     lunr.stemmer);
builder.searchPipeline.add(lunr.stemmer);

builder.ref('id');
builder.field('title');
builder.field('categories');
builder.field('tags');
builder.field('content');

function cleanTag(tag) {
  return tag.replace(' ', '-');
}

fs.readdir('content/posts', function (err, files) {
  if (err) {
    console.error("Could not list the directory!", err);
    process.exit(1);
  }

  files.forEach(function (file, index) {
    const mdFile = path.join('content/posts', file);
    if (path.extname(file) !== '.md') {
      return;
    }

    const slug = path.basename(file, '.md');

    const fileContents = fs.readFileSync(mdFile, 'utf8');
    const { metadata, content } = parseMD(fileContents);

    const doc = {
      id: slug,
      title: metadata.title,
      categories: metadata.categories ? metadata.categories.map(cleanTag) : null,
      tags: metadata.tags ? metadata.tags.map(cleanTag) : null,
      content: content
    };

    builder.add(doc);
  });

  const idx = builder.build();
  stdout.write(JSON.stringify(idx));
});

