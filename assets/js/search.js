import * as params from "@params";

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
      args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

function getGzipString(url, func) {
  var oReq = new XMLHttpRequest();
  oReq.open("GET", url, true);
  oReq.responseType = "arraybuffer";

  oReq.onload = function (_) {
    var arrayBuffer = oReq.response;
    var byteArray = new Uint8Array(arrayBuffer);

    var gunzip = new Zlib.Gunzip(byteArray);
    var bytes = gunzip.decompress();

    var plain = new TextDecoder("utf-8").decode(bytes);

    func(plain);
  };

  oReq.send();
}

function clearSearch() {
  document.querySelector("#search-results").innerHTML = "";
}

function doSearch(query) {
  // make any single-word query a wildcard
  if (!query.includes(" "))
    query += "*";
  
  var result = window.searchIndex.search(query);
  var docs = result.map(function (r) {
    return window.postList.find(function (d) {
      return d.id == r.ref;
    });
  });

  var html = '<table class="table table-striped"><tbody>';
  docs.slice(0, 25).forEach(function (doc) {
    html +=
      '<tr><td><a href="' +
      params.baseurl +
      doc.url +
      '">' +
      doc.title +
      "</a></td></tr>";
  });
  html += "</tbody></table>";

  document.querySelector("#search-results").innerHTML = html;
}

var onInput = debounce(function () {
  var value = document.querySelector("#search-box").value;

  if (value.length <= 3) {
    clearSearch();
  } else {
    doSearch(value);
  }
}, 250);

function onKeyDown(event) {
  var key = event.key || event.code;

  if (key === "Enter") {
    doSearch(document.querySelector("#search-box").value);
    event.preventDefault();
  }
}

function loadSearch() {
  var sb = document.querySelector("#search-box");
  sb.addEventListener("input", onInput);
  sb.addEventListener("keydown", onKeyDown);
  
  getGzipString(params.baseurl + "/search-index.json.gz", function (data) {
    var idx = lunr.Index.load(JSON.parse(data));
    window.searchIndex = idx;

    getGzipString(params.baseurl + "/posts.json.gz", function (data) {
      window.postList = JSON.parse(data);

      document.querySelector("#search-box").removeAttribute("disabled");
    });
  });
}

if (document.readyState !== "loading") {
  loadSearch();
} else {
  document.addEventListener('DOMContentLoaded', loadSearch);
}
