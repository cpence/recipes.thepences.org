---
title: Tea Brewing Times
author: pencechp
url: /tea/
---

### Taiwanese Small Pot

- **Oolong, Ti Kuan Yin, etc.**
  - Water: 95C
  - Amount: 1/4-1/5 pot
  - Brew Times: 40sec, 20sec, 40sec, 1min, 1min30
- **Pu-erh**
  - Water: 95C
  - Amount: 1 brick
  - Rinse first for 5-30sec, pour out
  - Brew times: 10sec, 10sec, 10sec, 15sec, 20sec, …, final brew 2min
- **Black tea**
  - Water: 90C
  - Amount: 2/5 pot
  - Brew times: 20sec, 20sec, 25sec, 30sec, 35sec
- **Green tea**
  - Water: 85C
  - Amount: 1/2 pot
  - Brew times: 40sec, 20sec, 40sec, 1min, 1min30
