---
title: "Marinated Thai-Style Pork Spareribs"
author: "pencechp"
date: "2012-07-11"
categories:
  - main course
tags:
  - pork
  - thai
  - vietnamese
---

## Ingredients

*   1 cup sliced shallots
*   10 scallions, coarsely chopped
*   One 3-inch piece fresh ginger, sliced
*   8 large cloves garlic, peeled
*   1 cup coarsely chopped fresh cilantro including thin stems (and roots, if possible)
*   6 tablespoons soy sauce
*   2 tablespoons Thai or Vietnamese fish sauce (nam pla or nuoc mam)
*   1 teaspoon kosher salt
*   1 teaspoon fresh coarsely ground black pepper
*   2 tablespoons sugar
*   4 pounds pork spare ribs, cut by your butcher across the bone into 2- to 3-inch "racks," each rack cut between the bones into individual 2- to 3-inch-long riblets
*   [Thai Chile-Herb Dipping Sauce]( {{< relref "thai-chile-herb-dipping-sauce" >}} )

## Preparation

1\. Put the shallots, scallions, ginger, garlic, cilantro, soy sauce, fish sauce, salt, pepper, and sugar in the bowl of a food processor. Process to a loose, finely chopped paste, scraping down the sides of the bowl once or twice.

2\. Place pork ribs in a large bowl or a pair of heavy resealable plastic bags. Thoroughly coat the ribs with the marinade, massaging the paste into the flesh for a minute or so. Cover and marinate at room temperature for 2 hours or up to 5 hours in the refrigerator, tossing the ribs once or twice during this time.

3\. Preheat oven to 350°F. Spread the ribs out, bone-side down, on two large, parchment-lined baking sheets and bake until ribs are deeply colored and very tender but not yet falling from the bone, about 11/2 hours, occasionally rotating the pans to encourage even cooking. Remove from the oven and serve with small bowls of Thai Chile-Herb Dipping Sauce.

_Lobel's Meat Bible, via Epicurious_
