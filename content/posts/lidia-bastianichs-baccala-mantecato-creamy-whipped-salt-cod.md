---
title: "Lidia Bastianich's Baccala Mantecato (Creamy Whipped Salt Cod)"
author: "pencechp"
date: "2014-01-04"
categories:
  - side dish
tags:
  - fish
  - italian
---

## Ingredients

*   1 pound(s) salt cod, soaked overnight
*   1 medium russet potato
*   2 clove(s) garlic, finely minced
*   1 cup(s) extra-virgin olive oil
*   1/2 cup(s) light cream
*   1/2 cup(s) poaching water from baccala

## Preparation

Soak the salt cod overnight in a large bowl of cold water, changing the water often, to remove the excess salt. Cut the cod into 6-inch pieces and put them in a saucepan with water to cover by 1 inch. Bring the water to a boil and cook the salt cod for about 20 minutes, until it just begins to flake. Do not let the fish break apart. **Reserve** 1/2 c. of the poaching liquid from the fish. Remove the cod to a colander to drain and cool.

Meanwhile, rinse the russet potato, but leave it whole and unpeeled. Cook the potato in its skin until it is cooked through and easily pierced with a knife. Let it cool, and then peel the skin away with the back side of a knife.

Set up a food processor with steel knife blade. Put in the cooked salt cod and pulse it a few times to break up the fish. Add the minced garlic and pulse a few more times. Add the potato and light cream and turn the food processor on. Puree the mixture while adding the olive oil in a thin stream. If the mixture is too dense, thin it with some of the cod poaching liquid. Season with freshly ground pepper. The baccala is best served warm as a spread for grilled country bread or crostini, or even as a dip for crudités.

_Delish_
