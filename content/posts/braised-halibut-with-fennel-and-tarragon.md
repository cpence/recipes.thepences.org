---
title: "Braised Halibut with Fennel and Tarragon"
author: "pencechp"
date: "2016-06-08"
categories:
  - main course
tags:
  - fennel
  - fish
  - untested
---

## Ingredients

*   4 (6-8 oz) halibut fillets, ¾ to 1 inch thick
*   Salt and pepper
*   6 tbsp. unsalted butter
*   2 fennel bulbs, halved, cored, and sliced thin
*   4 shallots, halved and sliced thin
*   ¾ c. white wine
*   1 tsp. lemon juice, plus lemon wedges for serving
*   1 tbsp. minced fresh tarragon

## Preparation

1\. Sprinkle fish with 1/2 teaspoon salt. Melt butter in 12-inch skillet over low heat. Place fish in skillet, skinned side up, increase heat to medium, and cook, shaking pan occasionally, until butter begins to brown (fish should not brown), 3 to 4 minutes. Using spatula, carefully transfer fish to large plate, raw side down.

2\. Add fennel, shallots, and 1/2 teaspoon salt to skillet and cook, stirring frequently, until vegetables begin to soften, 2 to 4 minutes. Add wine and bring to gentle simmer. Place fish, raw side down, on top of vegetables. Cover skillet and cook, adjusting heat to maintain gentle simmer, until fish registers 135 to 140 degrees, 10 to 14 minutes. Remove skillet from heat and, using 2 spatulas, transfer fish and vegetables to serving platter or individual plates. Tent loosely with aluminum foil.

3\. Return skillet to high heat and cook until sauce is thickened, 2 to 3 minutes. Remove pan from heat, stir in lemon juice, and season with salt and pepper to taste. Spoon sauce over fish and sprinkle with tarragon. Serve immediately with lemon wedges.

_CI_
