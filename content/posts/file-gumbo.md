---
title: "File Gumbo"
author: "pencechp"
date: "2010-04-02"
categories:
  - main course
tags:
  - cajun
  - pence
  - stew
---

## Ingredients

*   2 chicken breasts or 4 duck breasts
*   1 slice fresh (uncooked) ham, cubed or 1 pkg. andouille sausage
*   1 bunch celery, chopped (including leaves and stalks)
*   1 tbsp Worcestershire sauce
*   1 tsp Tabasco sauce
*   2 cloves garlic, chopped
*   1 bay leaf
*   1 lg. onion, chopped
*   1 28oz. can tomatoes
*   1 to 2 doz. whole okra
*   ½ lb. uncooked shrimp, shelled
*   ½ lb. lump crab meat (optional)
*   ½ lb. fillet of white fish (any variety)
*   ½ lb. fresh oysters, shucked
*   5/8 oz. file
*   White rice, for serving
*   Salt and pepper, to taste

## Preparation

1\. Boil two whole chicken breasts and ham (if desired) until the chicken is cooked through. Remove chicken breasts, bone, skin, and cube, and return chicken and skin to the pot. If using andouille instead of ham, slice and sautee, and add to the pot now.

2\. Add celery, Worcestershire sauce, Tabasco sauce, garlic, bay leaf, onion, and pepper. Cook for around one hour on low.

3\. Add tomatoes, okra, and shrimp, and cook for around 15 minutes.

4\. Add crab, fish, and oysters. Cook just long enough for the fish to flake.

5\. Turn off and let sit for a while.

6\. Warm again and add file. Check for taste, and also for proper thickening. Add salt if necessary.

7\. Serve over white rice.
