---
title: "Italian Sausage with Grapes and Balsamic Vinegar"
author: "pencechp"
date: "2015-11-22"
categories:
  - main course
tags:
  - italian
  - sausage
---

## Ingredients

*   1 tbsp. vegetable oil
*   1½ pounds sweet Italian sausage
*   1 lb. red grapes, halved lengthwise (3 cups)
*   1 onion, halved and sliced thin
*   ¼ c. water
*   ¼ tsp. pepper
*   ⅛ tsp. salt
*   ¼ c. dry white wine
*   1 tbsp. chopped fresh oregano
*   2 tsp. balsamic vinegar
*   2 tsp. chopped fresh mint

## Preparation

Heat oil in 12-inch skillet over medium heat until shimmering. Arrange sausages in pan and cook, turning once, until browned on 2 sides, about 5 minutes. Tilt skillet and carefully remove excess fat with paper towel (no, really, you want a dry skillet at this point). Distribute grapes and onion over and around sausages. Add water and immediately cover. Cook, turning sausages once, until they register between 160 and 165 degrees and onions and grapes have softened, about 10 minutes.

Transfer sausages to paper towel–lined plate and tent with aluminum foil. Return skillet to medium-high heat and stir pepper and salt into grape-onion mixture. Spread grape-onion mixture in even layer in skillet and cook without stirring until browned, 3 to 5 minutes. Stir and continue to cook, stirring frequently, until mixture is well browned and grapes are soft but still retain their shape, 3 to 5 minutes longer. Reduce heat to medium, stir in wine and oregano, and cook, scraping up any browned bits, until wine is reduced by half, 30 to 60 seconds. Remove pan from heat and stir in vinegar.

Arrange sausages on serving platter and spoon grape-onion mixture over top. Sprinkle with mint and serve with crusty bread or over polenta.
