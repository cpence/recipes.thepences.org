---
title: "Salmon Cakes"
author: "pencechp"
date: "2015-11-01"
categories:
  - main course
tags:
  - fish
  - untested
---

## Ingredients

*   3 tbsp. plus ¾ c. panko bread crumbs
*   2 tbsp. minced parsley
*   2 tbsp. mayonnaise
*   4 tsp. lemon juice
*   1 scallion, sliced thin 
*  1 small shallot, minced
*  1 tsp. Dijon mustard
*  ¾ tsp. salt
*  ¼ tsp. pepper
*  pinch cayenne pepper
*  1 (1 ¼ pound) skinless salmon fillet, cut into 1-inch pieces
*  ½ c. vegetable oil

## Preparation

1\. Combine 3 tablespoons panko, parsley, mayonnaise, lemon juice, scallion, shallot, mustard, salt, pepper, and cayenne in bowl. Working in 3 batches, pulse salmon in food processor until coarsely chopped into 1/4-inch pieces, about 2 pulses, transferring each batch to bowl with panko mixture. Gently mix until uniformly combined.

2\. Place remaining 3/4 cup panko in pie plate. Using 1/3-cup measure, scoop level amount of salmon mixture and transfer to baking sheet; repeat to make 8 cakes. Carefully coat each cake in bread crumbs, gently patting into disk measuring 2 3/4 inches in diameter and 1 inch high. Return coated cakes to baking sheet.

3\. Heat oil in 12-inch skillet over medium-high heat until shimmering. Place salmon cakes in skillet and cook without moving until golden brown, about 2 minutes. Carefully flip cakes and cook until second side is golden brown, 2 to 3 minutes. Transfer cakes to paper towel–lined plate to drain 1 minute. Serve.
