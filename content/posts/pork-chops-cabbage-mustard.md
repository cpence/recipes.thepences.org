---
title: "Pork Chops and Cabbage with Mustard Cream Sauce"
date: 2020-11-10T14:59:16+01:00
categories:
    - main course
tags:
    - german
    - pork
    - cabbage
---

## Ingredients

*   4 thin center-cut boneless pork chops (about 1 pound total)
*   1/2 teaspoon kosher salt
*   1/2 teaspoon freshly ground black pepper
*   2 tablespoons extra-virgin olive oil, divided
*   1/2 cup finely diced onion (about 1/2 large onion)
*   4 cups rough chopped green cabbage (about 1 pound)
*   1 tablespoon butter (optional)
*   3/4 cup heavy whipping cream
*   2 tablespoons Dijon mustard
*   2 tablespoons fresh lemon juice
*   1/4 teaspoon white pepper, or to taste
*   Chopped fresh parsley, for garnish

## Preparation

Pat the pork chops dry and season with salt and pepper.

In a large skillet over medium-high heat, heat the oil until shimmering. Add the
chops and cook until browned on one side, about 4 minutes. Turn the chops and
cook until browned on the other side, about 3 minutes. (If the chops are
browning too quickly, lower the heat.) Transfer the chops to a plate and cover
to keep warm.

Add the onion to the skillet and stir, cooking until the onion is softened and
nearly translucent, 6 to 8 minutes. Stir in the cabbage and cook until the
cabbage is tender, 5 to 8 minutes. Add the butter, if using, and toss to coat
the cabbage. (If the chops are lean, butter will enhance the flavor.) Transfer
the cabbage mixture to a serving platter and cover to keep warm.

Lower the heat to medium. Add the cream and mustard to the skillet and stir
until the mustard is fully incorporated, about 1 minute. Add the lemon juice and
white pepper; stir to combine.

Add the chops back to the skillet. Lower the heat to medium-low and simmer until
the sauce thickens slightly, about 5 minutes, basting the chops with the sauce.

Place the chops on top of the cabbage on the serving platter. Drizzle the chops
and cabbage with the remaining sauce, garnish with the parsley and serve.

*WaPo*

