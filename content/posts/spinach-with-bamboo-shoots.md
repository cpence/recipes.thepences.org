---
title: "Spinach with Bamboo Shoots"
author: "pencechp"
date: "2011-03-21"
categories:
  - side dish
tags:
  - chinese
  - spinach
---

## Ingredients

*   1 lb. fresh spinach
*   1/2 cup peanut, vegetable, or corn oil
*   1/4 cup finely shredded bamboo shoots
*   1 teaspoon salt
*   1 teaspoon sugar

## Preparation

1. Wash spinach leaves thoroughly under cold running water, drain well.

2. Heat the oil in a wok or skillet. Using a medium-high flame, cook the bamboo
   shoots in the oil approximately 45 seconds, stirring constantly.

3. Add spinach and stir until wilted.

4. Add salt and sugar, and cook, stirring, about 1 1/2 to 2 minutes longer.

5. Transfer to a hot platter, but do not add the liquid from the pan.

_Epicurious, January 2001_
