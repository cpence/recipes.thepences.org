---
title: "Polenta"
author: "pencechp"
date: "2010-05-08"
categories:
  - side dish
tags:
---

## Ingredients

*   2 1/2 c. milk
*   1 1/4 c. water
*   1 c. yellow cornmeal
*   2 cloves minced garlic (optional)
*   1 tsp. salt
*   1/2 c. parmesan
*   3 tbsp. butter

## Preparation

Combine first 5 ingredients.  Bring to a simmer over medium heat, whisking often.  Cook until done, about 12 minutes.  Stir in parmesan, butter and salt and pepper to taste.
