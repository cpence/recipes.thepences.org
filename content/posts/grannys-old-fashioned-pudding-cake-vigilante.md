---
title: "Granny's Old Fashioned Pudding Cake Vigilante"
author: "pencechp"
date: "2012-07-19"
categories:
  - dessert
tags:
  - cake
  - lemon
---

## Ingredients

*Cake:*

*   1 box vanilla cake mix
*   1 small box vanilla pudding mix
*   1 cup buttermilk
*   1/2 cup vegetable oil
*   3 eggs
*   Zest of 1 lemon
*   Juice of 1/2 lemon [CAP: 1 lemon]
*   Juice of 1/2 lime [CAP: 1 lime]

*Glaze:*

*   2 cups powdered sugar
*   1 stick unsalted butter
*   1/4 cup buttermilk
*   Zest of 1 lemon
*   Zest of 1 lime
*   Juice of 1/2 lemon [CAP: 1 lemon + another half lime]

## Preparation

Preheat the oven to 350 degrees F.

For the cake: In a large bowl, combine the cake mix, pudding mix, buttermilk,
oil, eggs, lemon zest and lemon and lime juices. Whisk well, or beat with a hand
mixer, until the batter is combined and smooth.

Pour the batter into a 9-by-13-inch nonstick baking pan, or use a regular pan
coated with cooking spray. Bake until the cake is baked through, 25 to 30
minutes. Remove the cake from the oven.

For the glaze: Combine the powdered sugar, butter, buttermilk, lemon and lime
zests and lemon juice in a saucepan and whisk well to combine. Bring the mixture
to a simmer over low heat. Remove from the heat.

Slice the cake into even pieces and drizzle the glaze over the top.

_Coolio \[CP: yeah, that Coolio\], via Food Network_
