---
title: "Tangerine-Teriyaki Shrimp"
author: "juliaphilip"
date: "2010-04-03"
categories:
  - main course
tags:
  - japanese
  - shrimp
  - untested
---

## Ingredients

*   3/4 c mirin
*   3/4 c tangerine or orange juice
*   2/3 c lower-sodium soy sauce
*   3 tbl sugar
*   1 tbl dark sesame oil
*   1 tbl grated tangerine or orange peel
*   1 3 in piece of fresh ginger, peeled and sliced 1/8 in thick
*   1/4 to 1/2 tsp chipolte chili powder
*   1 tbl cornstarch
*   1 1/2 lb shelled, deveined uncooked med shrimp
*   1/3 c sliced green onions
*   3 tbl toasted sesame seeds

## Preparation

Simmer mirin, tangerine juice, soy sauce, sugar, sesame oil, tangerine peel, ginger and chili powder in large skillet over medium-high heat for 3 min.

Combine 3 tbl water and cornstarch, stir into sauce. Simmer 1 to 2 min until thick and glossy. Add shrimp, cook 3 to 5 min or until shrimp turn pink. Sprinkle with green onions and sesame seeds.
