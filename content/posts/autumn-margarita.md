---
title: "Autumn Margarita"
author: "juliaphilip"
date: "2012-10-14"
categories:
  - cocktails
tags:
  - apple
  - tequila
---

## Ingredients

*   2 oz. good white tequila
*   juice of 1/2 a lime
*   3 oz. fresh-pressed cider

## Preparation

Put all ingredients in a cocktail shaker with ice and shake vigorously. Pour into cocktail glass with a cinnamon-sugar rim. Garnish with a slice of fresh apple.

_Mario Batali, New York Times Magazine_
