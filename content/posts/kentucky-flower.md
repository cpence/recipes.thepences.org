---
title: "Kentucky Flower"
author: "pencechp"
date: "2013-03-05"
categories:
  - cocktails
tags:
  - stgermain
  - whiskey
---

## Ingredients

*   1 1/2 oz. bourbon
*   1/2 oz. St. Germain
*   1 oz. juice \[good: cranberry cocktail, better: Simply Grapefruit, worse: 100% cranberry\]
*   1 egg white
*   1 splash fresh lemon juice

## Preparation

Combine in a cocktail shaker.  Shake vigorously without ice until frothy.  Add ice and shake again, then double-strain into a cocktail glass.  Garnish with a lemon wheel.

_David Mangiantine, Bar Manager, Farmstead, via Spirit Magazine, Feb. 2013_
