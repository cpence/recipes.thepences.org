---
title: "Lobster Vol au vent with Orange Cognac Sauce"
date: 2023-01-29T12:22:07+01:00
categories:
  - main course
tags:
  - lobster
  - french
  - belgian
  - untested
---

## Ingredients

_for the sauce:_

- 1 cup fresh squeezed orange juice
- 1 tablespoon sugar
- Salt and pepper
- 1 cup chicken stock
- 1/2 cup heavy cream
- 2 tablespoons cognac

_for the dish:_

- 2 tablespoons olive oil
- 8 ounces lobster meat, diced
- 4 ounces shiitake mushrooms, diced
- 1 tablespoon chopped shallots
- 1 teaspoon chopped fresh tarragon leaves
- White pepper
- 4 prepared vol au vent shells
- Fresh flat-leaf parsley, for garnish

## Preparation

1. For the Orange Cognac Sauce: In a medium saucepan over high heat combine the
   orange juice, sugar, salt, and pepper. Bring to a boil then reduce heat until
   nearly evaporated. Add the stock, heavy cream, and cognac. Boil for minute,
   remove from the heat, and set aside.

2. For the Lobster Vol au Vent: Preheat a saucepan over high heat until very
   hot. Add the olive oil, then the lobster meat, mushrooms, shallots, tarragon,
   and white pepper, to taste, while stirring to combine ingredients. Add 1/2
   cup of the Orange Cognac Sauce and bring to a boil for 1 minute. Reduce to a
   simmer.

3. Spoon reduced mixture into prepared shells, drizzle with remaining orange
   cognac sauce, and garnish with parsley leaves.

_Food Network_
