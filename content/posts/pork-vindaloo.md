---
title: "Pork Vindaloo"
date: 2021-07-10T11:04:49+02:00
categories:
  - main course
tags:
  - indian
  - pork
  - curry
  - untested
---

## Ingredients

- 750 g pork shoulder, cut into 1" pieces
- 4 tbsp. vegetable or coconut oil
- 1 tsp black mustard seeds
- 500g red onions, finely sliced
- 5 ripe tomatoes, chopped
- 2 small green chiles, stalks removed
- 10 curry leaves
- 1 tbsp. sugar
- 1 1/2 tsp. salt
- 2 tbsp. tamarind pulp
- 6 small sweet pickled onions, each cut into 2
- 20g fresh cilantro, finely chopped

_for the paste:_

- 5cm cinnamon stick
- 6 crushed cardamom pods
- 1 tsp. black peppercorns
- 10 cloves
- 1 tsp. cumin seeds
- 1 tsp. coriander seeds
- 2 tbsp. chili powder
- 1/2 tsp. ground turmeric
- 10 cloves garlic
- 5 cm piece peeled ginger
- 60ml malt vinegar

## Preparation

Grind together the paste ingredients, starting with the largest spices first,
then adding the cumin and coriander seeds, and blend to a fine consistency. Add
the chilli and turmeric powders and mix well. Add the garlic and ginger and
grind, adding the vinegar to make a paste.

Rub the paste into the diced pork and leave to sit for at least an hour or
overnight in a refrigerator.

Heat the oil in a wide, pan (one that has a lid) over a medium-low flame, add
the mustard seeds and let crackle for 30 seconds. Add the onions and fry until
soft and golden. Add the tomatoes, chillies and curry leaves, and cook until the
tomatoes start to break down.

Add the pork and turn the heat up to medium-high. Cook for 10-12 minutes
stirring continually until the pork browns. Add 250ml of water, stir well, add
the jaggery, salt and tamarind and mix through. Bring to a simmer, cover
tightly, turn the heat right down and cook gently for 45-60 minutes.

Check the amount of liquid in the pan and, if needed, cook for another 15
minutes, or until the meat is very tender and the sauce has thickened.

Scatter the pickled onions and chopped coriander over the dish and serve hot.

_Vivek Singh / BBC_
