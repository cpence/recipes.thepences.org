---
title: "New Mexican Red Chile"
author: "pencechp"
date: "2010-09-11"
categories:
  - main course
tags:
  - chile
  - newmexican
  - pence
  - pork
  - stew
---

## Ingredients

*   1 medium-sized pork shoulder roast
*   2 tbsp. flour
*   4 c. (or more) chicken broth
*   1 lg. onion, chopped
*   3 cloves garlic, diced
*   1/4 c. (or more) chile powder (pref. New Mexico; _see below_ for alternate with Bueno red chile)
*   cumin (to taste)
*   mexican oregano (to taste)
*   salt (to taste)
*   corn tortillas (for serving)
*   fried eggs (for serving) cheddar cheese (for serving)

## Preparation

Brown the 2 tbsp. of flour in a large dutch oven.  Add a bit of chicken broth to turn into a roux, making sure to avoid lumps.  Cut the pork shoulder into small cubes, and add it to the pot.  Add enough broth to cover.  Add the onion and garlic.  Cook for a few hours, until the meat is nearly tender.

Add a few tbsp. of the cooking stock to the chile powder in a small bowl, and stir to form a paste.  Add the paste to the pot, along with the cumin, mexican oregano, and salt to taste.  You may need to thicken by adding either more chile powder or a thickener like flour.  Cook the meat with the chile a little longer, until it's fork-tender.

Limp your corn tortillas in the stock, and serve flat-stack style with cheddar cheese and a fried egg on top.

**Another Option:** You can replace the 1/4 c. of red chile powder with quite a lot more (maybe as much as half of the standard container, 1-2 cups) liquid Bueno mild red chile.  Add some mixture of masa and water (or just flour and water) to the chile at the end to thicken things back up since you're missing the chile powder.
