---
title: "Oven-Roasted Prawns with Romesco Sauce"
author: "juliaphilip"
date: "2009-12-06"
categories:
  - main course
tags:
  - italian
  - shrimp
  - tomato
  - untested
---

## Ingredients

*   3 tomatoes, halved
*   10 garlic cloves
*   2 slices crusty bread
*   1/2 cup whole almonds, with skin
*   1/2 cup hazelnuts or pine nuts
*   1 pimento or roasted red pepper
*   1/2 cup red wine vinegar
*   3/4 cup olive oil
*   1 teaspoon paprika
*   2 teaspoons kosher salt
*   3 tablespoons olive oil
*   2 pounds prawns, shelled, tails removed, heads on

## Preparation

Preheat oven to 450 degrees F.

Arrange the tomatoes, garlic, bread, and nuts on a baking sheet; roast for 10 to 15 minutes.

Transfer to a food processor and pulse to roughly break up. Add the pimentos, vinegar, oil, and paprika. Pulse again until well combined, add salt. This sauce is best if allowed to rest so the flavors can meld (may be made a day in advance).

Heat oil to almost smoking in a large skillet or roasting pan. Toss the prawns in half the romesco sauce to coat, and pan sear the shrimp quickly in the oil just until barely opaque. Then roast for 10 to 12 minutes in a 450 degree F oven. Serve the remaining sauce on the side for dipping.

_Tyler Florence_
