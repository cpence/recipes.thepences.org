---
title: "Horseradish Sauce"
author: "juliaphilip"
date: "2016-05-02"
categories:
  - miscellaneous
tags:
  - sauce
---

## Ingredients

*   3/4 cup mayonnaise
*   3/4 cup sour cream
*   1/4 cup plus 2 tablespoons jarred grated horseradish (with liquid)
*   1/2 teaspoon grated lemon zest
*   2 teaspoons kosher salt
*   Freshly ground black pepper

## Preparation

In a small bowl, mix together the mayonnaise, sour cream, horseradish, zest, and 2 teaspoons salt. Season generously with pepper to taste. Refrigerate the horseradish sauce for at least 30 minutes before serving.

_Food Network_
