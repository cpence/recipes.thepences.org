---
title: "Olive Oil Poached Halibut Nuggets with Garlic and Mint"
author: "juliaphilip"
date: "2009-12-06"
categories:
  - main course
tags:
  - fish
---

## Ingredients

*   1 pound halibut fillet, cut into 1 1/4-inch cubes
*   1/4 teaspoon fine sea salt, more to taste
*   1/4 teaspoon freshly ground black pepper, more to taste
*   4 tablespoons extra virgin olive oil
*   1 small rosemary sprig
*   1/2 teaspoon dried mint
*   2 garlic cloves, minced
*   Fresh lemon juice, to taste (optional)
*   Chopped fresh mint, for garnish

## Preparation

Season halibut all over with a generous pinch of salt and pepper. In a medium-size skillet just large enough to hold fish cubes in a single layer, heat oil over low heat. Add fish, rosemary sprig and dried mint, and let cook slowly until fish begins to turn opaque, about 3 minutes.

Stir in garlic and 1/4 teaspoon each of salt and pepper, and cook until garlic is fragrant and fish is just cooked through, another 3 minutes or so (heat should be low enough so as not to brown the garlic or fish but high enough to gently cook everything; the cooking time will vary widely with your stove).

Taste and add more salt and pepper and a few drops of lemon juice if desired. Stir in the fresh mint and serve, using a slotted spoon if you want to leave the poaching oil in pan; it is delicious over couscous or potatoes.

_Melissa Clark, New York Times_
