---
title: "Red Lentils (Misir Wot)"
author: "pencechp"
date: "2012-12-02"
categories:
  - main course
tags:
  - ethiopian
  - lentil
  - vegetarian
---

## Ingredients

*   1 lg. red onion, chopped
*   1/4 c. olive oil
*   2 1/2-3 c. water, divided
*   1-2 tbsp. cayenne (1 tbsp. is hot, 2 is blazing)
*   1/4 tsp. dried ginger
*   1 sm. clove garlic, diced
*   1 c. red lentils, washed
*   1 tbsp. butter or [qibe]( {{< relref "qibe-spiced-butter" >}} )
*   1/4 tsp. salt, to taste
*   1/4 tsp. black pepper, to taste

## Preparation

Using a medium pot with a lid, start by sauteeing the onions until light brown, about 6-8 minutes over medium heat.  Don't worry if they stick a bit.  Add the oil.  Once it is simmering, add 1/4 c. water.  Then immediately add the cayenne and the ginger and garlic.  It will be a rather thick mix, but keep stirring as it may burn if you don't.  Add the qibe or butter, and about 1 cup water, 1/4 cup at a time, constantly stirring.  It is important that you add the water gradually because the cayenne has to cook and blend with the broth.  Once you have enough volume in your pot to submerge the lentils, add them.  Stir the lentils for about 2 minutes.  Then add about a cup of water to the pot and close the lid to let it come to a boil.  Once the volume of the pot has considerably come down, 10 minutes or so, start stirring to see if the lentils have cooked.  If they are tender and crush easily, it's time to take your pot off the stove.  If not, keep them cooking, checking every 3 minutes, adding a few tbsp. of water so they don't dry out.  For red lentils, the recipe should take around 12 minutes, for green lentils, 20.  Let cool a bit, add salt and pepper, and serve warm with bread for dipping.

_Penzey's Spices_
