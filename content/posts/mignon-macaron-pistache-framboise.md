---
title: "Mignon macaron pistache framboise"
author: "pencechp"
date: "2018-07-20"
categories:
  - dessert
tags:
  - french
  - untested
---

## Ingrédients

*   400gr pâte d'amande 50%
*   40gr pâte de pistache
*   75gr d'oeufs
*   50gr de beurre fondu froid
*   20gr de farine
*   framboises fraîches

## Préparation

Mélanger au batteur avec la palette la pâte d'amande et la pâte de pistache. Ajouter progressivement les oeufs puis le beurre fondu froid, stopper et ajouter la farine tamisée à la spatule. Garnir les moules (mini-moules pour canelés) à la moitié à l'aide d'une poche, dépose une framboise et recouvrir avec l'appareil. Cuisson 13min à 175C. Démouler a foid. _Finition:_ une framboise posée à l'envers sur le mignon, une pistache entière à l'intérieur.

_Elastomoule MINI CANELÉS_
