---
title: "Elephant Ears"
author: "juliaphilip"
date: "2010-12-29"
categories:
  - breakfast
tags:
  - danish
  - pastry
---

## Ingredients

*   1/2 recipe [Danish pastry dough]( {{< relref "danish-pastry-dough" >}} )
*   Cinnamon-Pecan filling (recipe follows)
*   1 egg, slightly beaten
*   Sugar
*   Coarsely chopped pecans

## Preparation

1\. Roll Danish pastry dough out on a lightly floured pastry board or cloth to a 12-inch square.  Spread cinnamon-pecan filling evenly over the pastry; roll up, jelly-roll fashion.  With a sharp knife, cut into 1-inch slices, then carefully cut each slice almost in half, but not all the way through.  Gently spread out the two halves, leaving them attached in the center.

2\. Place pastries, 2 inches apart, on a greased cooky sheet.  Let rise in a warm place, away from draft, until double in bulk, 30 to 45 minutes.  Brush with egg; sprinkle with sugar and pecans.

3\. Place in hot oven (400 degrees); reduce the heat immediately to 350.  Bake for 24 minutes, or until puffed and golden.  Remove to wire rack; cool.  Makes 12 pastries.  \[CP: My mother has an annotation here in the margin which simply reads "icing drizzle."  That can't be too bad of an idea.\]

## Cinnamon-Pecan Filling

Beat 1/2 cup sugar, 1/4 cup (1/2 stick) softened butter, 1/2 tsp. ground cinnamon and 1/2 tsp. ground cardamom in a small bowl until smooth.  Stir in 1/2 cup coarsely chopped pecans, 1/4 cup dried currants.  Makes 1 cup.

_Family Circle Cookbook_
