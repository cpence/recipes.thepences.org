---
title: "Melomakarona"
author: "pencechp"
date: "2011-12-30"
categories:
  - dessert
tags:
  - cookie
  - greek
---

## Ingredients

_For the cookies:_

*   1 cup olive oil
*   1 cup vegetable oil
*   3/4 cup sugar
*   Zest of one orange
*   3/4 cup orange juice
*   1/4 cup brandy
*   2 tsp. baking powder
*   1 tsp. baking soda
*   Pinch of salt
*   7 1/2 cups all-purpose flour
*   3/4 cup walnuts, ground coarsely
*   Ground cinnamon for sprinkling

_For the syrup:_

*   1 cup honey
*   1 cup sugar
*   1 1/2 cups water
*   1 cinnamon stick
*   3-4 whole cloves
*   1-2-inch piece lemon rind
*   1 tsp. lemon juice

## Preparation

Preheat the oven to 350 degrees.

In a small bowl, using your fingers, combine the orange zest with the sugar – rubbing the grains as if you were playing with sand to release the orange oils into the sugar.

Using an electric mixer, beat the oil with the orange sugar until well mixed. In a separate bowl, sift the flour with the baking powder, baking soda and salt.

Add the orange juice and brandy to mixer and mix well.

Slowly incorporate the flour cup by cup until the mixture forms a dough that is not too loose but not quite firm either. It will be dense and wet but not sticky. Once the flour is incorporated fully stop mixing.

To roll cookies, pinch a portion of dough off about the size of a walnut. Shape in your palms into a smooth oblong shape, almost like a small egg. Place on an ungreased cookie sheet. Shape and roll cookies until the sheet is filled.

Press the tines of a large fork in a crosshatch pattern in the center of each cookie. This will flatten them slightly in the center. The cookies should resemble lightly flattened ovals when they go in the oven.

Bake in a preheated 350-degree oven for 25 – 30 minutes until lightly browned. (The cookies will darken when submerged in syrup.)

While the cookies are baking, prepare the syrup.

In a saucepan, combine the honey, sugar, water, cinnamon, cloves, and lemon rind. Bring the mixture to a boil then lower the heat and simmer uncovered for about 10 to 15 minutes. Remove the cinnamon, cloves, and lemon rind and stir in lemon juice.

Place the ground walnuts in a shallow plate or bowl next to the stove top. When the cookies come out of the oven and while they are still very warm, carefully float the cookies in the syrup and allow the cookies to absorb syrup on both sides.

Using a fork or small spatula, remove the cookie from the syrup and place on a platter or plate. Press ground walnuts lightly into the tops of the cookies (syrup will help it adhere) and sprinkle lightly with ground cinnamon.

Do not refrigerate Melomakarona as they will harden. Store in an airtight container at room temperature.

_Lynn Livanos Athan, About.com_
