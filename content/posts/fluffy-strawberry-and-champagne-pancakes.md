---
title: "Fluffy Strawberry and Champagne Pancakes"
author: "pencechp"
date: "2015-01-21"
categories:
  - breakfast
tags:
  - champagne
  - pancake
  - strawberry
  - untested
---

## Ingredients

_Fluffy Champagne Pancakes_

*   2 large eggs
*   1 cup lowfat buttermilk
*   2 tablespoon canola oil
*   1 1/2 cups unbleached all-purpose flour
*   1/2 teaspoon fine sea salt
*   2 teaspoons baking powder
*   2 tablespoon granulated sugar
*   1/4 cup champagne (I used Brut)

_Smashed Strawberry Syrup_

*   1 pound strawberries, hulled and halved
*   3/4 cup granulated sugar
*   1/4 teaspoon orange zest
*   1/2 tablespoon fresh-squeezed orange juice
*   1 teaspoon cornstarch
*   3 tablespoons champagne

## Preparation

Whisk together the egg, milk, and oil. Set aside.

Sift together the flour, salt, baking powder, and sugar.

Gradually stir the milk/egg mixture into the dry ingredients just until you no longer see any dry flour. It will be a little lumpy - thats okay. Set aside for 15 minutes.

Preheat a griddle to 375 degrees.

For the syrup, combine strawberries, sugar, and orange zest , juice, and cornstarch in a medium saucepan. Smash strawberries and turn heat to medium. Bring to a boil, reduce to simmer for 10 minutes without a cover. Remove from heat and stir in the champagne.

As the sauce is simmering, cook the pancakes. Gently fold the champagne into the batter until it's just incorporated. Drop 1/4 cup of batter onto a greased griddle. Bake on one side until bubbles begin to form, and the edges start to firm. Flip the pancakes and cook until golden (another couple minutes). Repeat with remaining batter. The recipe will make 9 pancakes.

Top pancakes with warm strawberry syrup, and serve with a mimosa (if you like).

_Veggie and the Beast_
