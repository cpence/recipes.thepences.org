---
title: "Squash Gratin with Poblanos and Cream"
author: "pencechp"
date: "2017-09-17"
categories:
  - main course
  - side dish
tags:
  - casserole
  - chile
  - squash
  - vegetarian
---

\[CP: This recipe is really flexible. I made it with zucchini and summer squash (sauteeing rather than baking), and substituting the farmer's cheese for crumbled feta, and it came out great.\]

## Ingredients

*   6 large poblanos (about 1 1/2 pounds)
*   2 large butternut squash (4 pounds total)—peeled, halved, seeded and sliced 1/2 inch thick
*   1/2 cup plus 1 tablespoon extra-virgin olive oil
*   1 1/2 teaspoons coarsely chopped thyme
*   Salt and freshly ground black pepper
*   1 large white onion, thinly sliced
*   3 large garlic cloves, thinly sliced
*   1 teaspoon coarsely chopped oregano
*   1/2 cup heavy cream
*   3/4 cup crème fraîche or sour cream
*   8 ounces Monterey Jack cheese, shredded
*   8 ounces farmer cheese (crumbling, dry cottage cheese style cheese)
*   Toasted pumpkin seeds, for serving

## Preparation

Preheat the oven to 400°. Roast the poblanos directly over a gas flame or under the broiler, turning, until they are charred all over. Transfer the chiles to a bowl, cover tightly with plastic wrap and let them cool. Peel, stem and seed the chiles, then cut them into thin strips.

Brush the butternut squash with 6 tablespoons of the olive oil and spread it on 2 large rimmed baking sheets. Sprinkle with 1 teaspoon of the thyme and season with salt and pepper. Roast for about 25 minutes, until the squash is tender, shifting the pans from top to bottom and front to back halfway through baking. Increase the oven temperature to 425°.

Meanwhile, in a large, deep skillet, heat the remaining 3 tablespoons of olive oil. Add the sliced onion, garlic, oregano and the remaining 1/2 teaspoon of thyme and cook over moderate heat, stirring occasionally, until the onion is softened and fragrant, about 8 minutes. Add the poblano strips and cook until they are very tender, about 5 minutes. Add the heavy cream and simmer until thickened, about 5 minutes. Remove from the heat. Stir in the crème fraîche and season the poblano mixture with salt and pepper.

Spoon half of the poblano mixture into a large baking dish and top with half of the butternut squash and half of the Monterey Jack and farmer cheeses. Repeat with the remaining poblano mixture, butternut squash and both cheeses. Bake in the center of the oven for about 30 minutes, until the gratin is golden and bubbling. Let the gratin rest for 10 minutes. Garnish with the pumpkin seeds and serve.

_Food & Wine_
