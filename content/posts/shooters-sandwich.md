---
title: "Shooter's Sandwich"
author: "pencechp"
date: "2011-05-21"
categories:
  - main course
tags:
  - sandwich
  - steak
---

## Ingredients

*   1 giant, round crusty loaf
*   2 good steaks (boneless)
*   5-6 tbsp. butter
*   1 lb. mushrooms, sliced
*   7 oz. shallots, diced
*   salt and pepper
*   some garlic
*   some brandy
*   Worcestershire sauce
*   horseradish
*   Dijon mustard

## Preparation

Slice the top off of your crusty loaf, and hollow out the middle.  Melt the butter, and cook the shallots and mushrooms.  Add garlic, brandy, salt and pepper, and Worcestershire sauce to taste.  Sautee your steaks in some butter or oil of their own, seasoned w/ salt and pepper.  Place one of the steaks into the loaf.  Add all the mushroom mixture.  Top with the other steak.  Spread horseradish and Dijon, to taste, over the top.  Take the whole loaf, wrap in parchment paper, string, and tin foil, and weight down with a cutting board and some nice heavy books, overnight (preferably in your cool basement, _not_ in the refrigerator).  The next day, slice into wedges as you'd cut a pie (just cut through the paper/foil), and enjoy.

_The Grauniad_
