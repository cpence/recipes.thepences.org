---
title: "Generic Vegetable Soup"
author: "pencechp"
date: "2015-11-22"
categories:
  - main course
tags:
  - soup
---

## Ingredients

*   1 to 2 pounds vegetables
*   Aromatics, such as onion, garlic, or leeks
*   Olive oil or unsalted butter
*   Salt and pepper
*   4 to 6 cups stock

## Preparation

Choose and weigh 1 to 2 pounds of vegetables. Cut up the vegetables and aromatics, and heat 1 tablespoon of olive oil (or butter, ghee, or the fat from rendering some bacon, chicken, etc.) in a Dutch oven over medium heat. (Also, you could add ginger or chili peppers right here.)

Sautee the aromatics until fragrant and soft (five minutes), brown the vegetables, and season them with salt, pepper, and spices (dried herbs like mint, sage, or oregano, curry powder, cumin, chili powder, cinnamon, smoked paprika, and smoked or truffled salt).

Add 4 to 6 cups of broth and bring to a simmer. Add a sprig of fresh herbs now if desired. Turn the heat down to low and cover the pot. Let cook for about 30 minutes, then check the soup. Are the vegetables as soft as you would like? If you want to leave the vegetables intact, take the soup off the heat now. If you want the vegetables very soft for pureeing, keep cooking until they are falling apart.

Whether you are leaving the vegetables intact or pureeing the soup, make sure to taste the soup as it finishes cooking. If it seems flat, add some vinegar or lemon juice. If it is too salty, thin out with some extra broth or dairy. You can also add leftover cooked pasta, a few crumbles of cooked ground turkey or beef, cooked strips of chicken breast on top of each bowl, 1/4 cup of rice, quinoa, or another grain (simmering until done), or a can of beans, chickpeas, or tomatoes, and simmer until warmed through.

Once the vegetables are very soft, you can puree the soup in a blender or with a stick blender if you like. Rewarm gently after blending. When pureeing the soup, you can add flavor and creaminess by adding beans, tofu, coconut milk, yogurt, or other dairy such as cream, mascarpone, or even cream cheese. Finish the soup with something acid like lemon juice, balsamic vinegar, or fruit vinegar. Or drizzle on a little oil like chili oil, smoked olive oil, or something else a little special.

_The Kitchn_
