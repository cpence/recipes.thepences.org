---
title: "Ceylon Curry of Oysters"
author: "pencechp"
date: "2015-03-13"
categories:
  - main course
tags:
  - curry
  - indian
  - oyster
  - untested
---

## Ingredients

*   2 tablespoons butter or coconut oil
*   4 small shallots, finely chopped
*   1 clove garlic, minced
*   ½ serrano or Thai chili, seeded and minced
*   1 tablespoon curry powder
*   1 large pinch turmeric
*   1 cinnamon stick
*   3 cloves
*   1 bay leaf
*   1 cup coconut milk
*   Salt
*   12 oysters, shucked, liquor reserved
*   Juice of 1/2 lemon

## Preparation

In a medium saucepan over medium heat, melt the butter. Add the shallots, garlic and chili, and sauté until softened and starting to brown, 2 to 3 minutes. Stir in the curry powder, turmeric, cinnamon stick, cloves and bay leaf and cook for 1 minute.

Reduce heat to low and add the coconut milk and 1/2 teaspoon salt. Simmer for 3 minutes. Add the oysters and their liquor; simmer until the oysters are just firm, 3 to 4 minutes. Take the pan off the heat and add lemon juice and salt to taste. Serve over rice -- the rice in the fish recipe that follows would go well -- or on hoppers (see the recipe at nytimes.com/magazine).

_New York Times_
