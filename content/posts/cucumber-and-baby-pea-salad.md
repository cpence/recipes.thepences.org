---
title: "Cucumber and Baby Pea Salad"
author: "pencechp"
date: "2017-06-20"
categories:
  - side dish
tags:
  - cucumber
  - peas
  - salad
  - untested
---

## Ingredients

*   1 cup plain whole-milk Greek yogurt
*   3 tablespoons fresh lemon juice
*   1/4 cup extra-virgin olive oil
*   1 cup flat-leaf parsley leaves
*   1/4 cup finely shredded basil leaves
*   Salt and freshly ground pepper
*   1 pound frozen baby peas, thawed
*   3 large seedless cucumbers (about 1 pound each)—peeled, halved lengthwise, seeded and sliced crosswise 1/2 inch thick

## Preparation

In a large bowl, whisk the yogurt with the lemon juice and olive oil. Add the parsley and basil and season with salt and pepper. Stir in the peas and cucumbers and serve.

_Food & Wine_
