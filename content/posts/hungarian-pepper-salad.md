---
title: "Hungarian Pepper Salad"
author: "pencechp"
date: "2011-02-02"
categories:
  - side dish
tags:
  - bellpepper
  - hungarian
  - pence
  - salad
---

## Ingredients

*   3-5 red bell peppers, roasted and peeled
*   2-4 cloves garlic
*   vinegar (sherry or balsamic work well)
*   olive oil

## Preparation

Seed the peppers and slice into strips, roughly 2"x4".  Spread in an even layer on a plate or low baking dish.  Mince garlic and sprinkle over the top.  Sprinkle with a healthy amount of vinegar and olive oil.
