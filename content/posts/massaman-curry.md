---
title: "Massaman Curry"
date: 2021-07-10T11:30:07+02:00
categories:
  - main course
tags:
  - thai
  - curry
  - goat
  - lamb
  - untested
---

## Ingredients

_for the paste:_

- 2 shallots
- 4 cloves garlic
- 1 coriander root
- 12 long dried chiles
- 1 cinnamon stick
- 1 tbsp. cumin seeds
- 1 tbsp. coriander seeds
- 1 pinch sea salt
- 2 cloves
- seeds of 1 cardamom pod
- 1/4 tsp. black peppercorns
- 2 sticks lemongrass, bashed and finely chopped
- 1/4 nutmeg, grated
- 1 heaped tbsp. kapi (shrimp paste)

_for the curry:_

- 2 tbsp. ghee
- 400g goat meat, cubed
- 2 tbsp. vegetable oil
- 2 shallots, peeled and halved
- 4 new potatoes, halved
- 2 tbsp. coconut cream
- 400ml coconut milk
- handful raisins (optional)
- 200ml water
- 2 tbsp. tamarind paste
- 6 cardamom pods, broken open
- 1 tbsp. sugar
- 1-2 tbsp. fish sauce
- 1-2 tbsp. roasted peanuts

## Preparation

To make the paste, preheat the oven to 180C fan/gas mark 6.

Wrap the shallots, garlic and coriander root tightly in tin foil and bake in the
oven for about 20 minutes, or until soft.

Meanwhile, in a dry frying pan, toast the dried chillies until they are crispy,
shaking them in the pan to ensure they don’t burn. Set aside to cool, then snip
them up into small pieces with scissors, discarding the stalks and the seeds.
Soak the pieces in warm water for at least 20 minutes. Dry them thoroughly with
paper towels.

Toast the cinnamon stick, cumin seeds and coriander seeds in the dry pan until
they’re fragrant. Then grind the paste, starting with the dried chillies and the
salt, followed by the toasted spices, the remaining dry spices, and the
lemongrass. Peel the shallots and garlic, cut the coriander root into small
pieces, and pound them into the paste, followed by the grated nutmeg and the
kapi.

Keep grinding until the paste is as smooth as possible, and everything is
thoroughly incorporated.

To make the curry, melt the ghee in a large frying pan, and gently brown the
meat. You will need to do this in batches.

Heat the vegetable oil in a large wok or saucepan, then add the paste, and fry
until it’s very fragrant. Add the meat, shallots, potatoes and the coconut
cream, and stir them thoroughly into the paste. Then add the coconut milk,
raisins (if using) and water, bring up to the boil, and simmer for 30 minutes.

Now add the tamarind paste, cardamom, palm sugar and nam pla and gently simmer,
partially covered, for another 30-40 minutes, until the meat is tender. About 10
minutes before you finish cooking, add the peanuts.

Finally, taste the curry and adjust the seasoning. You’re looking for a sour
start to its taste, which then develops in the mouth to become sweet and
savoury.

_Kay Plunkett-Hogge / BBC_
