---
title: "Vodka-Spiked Cherry Tomatoes"
author: "pencechp"
date: "2017-09-17"
categories:
  - appetizer
tags:
  - tomato
  - untested
---

## Ingredients

*   3 pints firm small mixed cherry and grape tomatoes
*   1/2 cup vodka
*   3 tablespoons white-wine vinegar
*   1 tablespoon superfine granulated sugar
*   1 teaspoon grated lemon zest
*   3 tablespoons kosher salt
*   1 1/2 tablespoons coarsely ground black pepper

## Preparation

Cut a small X in bottom of each tomato. Blanch tomatoes, 5 at a time, in a saucepan of boiling water 3 seconds. Immediately transfer with a slotted spoon to an ice bath to stop cooking. Drain and peel, transferring to a large shallow dish. Stir together vodka, vinegar, sugar, and zest until sugar has dissolved, then pour over tomatoes, gently tossing to coat. Marinate, chilled, at least 30 minutes and up to 1 hour. Stir together salt and pepper and serve with tomatoes for dipping.

_Gourmet, August 2009_
