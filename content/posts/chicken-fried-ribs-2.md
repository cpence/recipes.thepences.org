---
title: "Chicken-Fried Ribs"
author: "pencechp"
date: "2017-05-02"
categories:
  - main course
tags:
  - ribs
---

## Ingredients

_For ribs:_

*   About 6 cups vegetable oil
*   2 racks baby back ribs (about 1 pound each), cut into ribs
*   1/3 cup all-purpose flour
*   3 large eggs, lightly beaten
*   1 1/2 to 1 3/4 cups panko

_For mustard sauce:_

*   1/2 cup sour cream
*   2 tablespoons mayonnaise
*   1 Kirby cucumber, coarsely grated
*   2 tablespoons Dijon mustard

## Preparation

_Make ribs:_

Preheat oven to 200°F. Heat 3/4 inch oil to 325°F in a deep 12-inch skillet over medium heat. Meanwhile, season ribs with 3/4 teaspoon salt and 1/2 teaspoon pepper. Put flour, eggs, and panko in 3 separate shallow bowls. Dredge ribs in flour, then coat with egg, letting excess drip off, and panko.

Fry ribs in batches, turning once, until golden brown and cooked through, 7 to 8 minutes per batch. Transfer first batch to a paper-towel-lined baking sheet and keep warm in oven. (Return oil to 325°F between batches.)

_Make sauce while ribs fry:_ Stir together sauce ingredients. Serve ribs with sauce.

_Gourmet, October 2008_
