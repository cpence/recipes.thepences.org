---
title: "Hot Glazed Bonuts"
author: "pencechp"
date: "2016-09-16"
categories:
  - breakfast
tags:
  - donut
  - untested
---

## Ingredients

_for the glaze_

*   1/4 cup whole milk
*   1 teaspoon vanilla extract
*   8 ounces powdered sugar, about 2 cups

_for the bonut_

*   2 quarts peanut oil
*   12 ounces all-purpose flour, plus an additional 1/2 cup for dusting
*   4 teaspoons baking powder
*   1/4 teaspoon baking soda
*   3/4 teaspoon kosher salt
*   1 ounce unsalted butter, chilled
*   2 ounces shortening, chilled
*   1 cup low-fat buttermilk, chilled

## Preparation

Heat the peanut oil over high heat in a large Dutch oven fitted with a fry/candy thermometer. Bring the oil to 350 degrees F while you prepare the biscuit dough. Keep an eye on it though as it’s easy to shoot through the target temp. When I get to about 300 degrees F, I back off on the flame a bit.

Whisk the flour, baking powder, baking soda and salt together in a large mixing bowl.

Using your fingertips, rub the butter and shortening into the dry goods until the mixture resembles coarse crumbs. You don’t want the fat to melt so work fast and only with your fingertips.

Make a well in the middle of this mixture and pour in the buttermilk. Stir with a large spoon or rubber spatula until the dough just comes together. Then hand-knead in the bowl until all the flour has been taken up.

Turn the dough out onto a lightly floured surface, gently flatten it then fold over book-style, repeating 8-10 times, until the dough is soft and smooth.

Press the dough into a 1-inch-thick round. Cut out dough using a 2 1/2-inch doughnut cutter or pastry ring and using a 1-inch ring for the center whole. Make your cuts as close together as possible to limit waste. Re-roll and cut as many donuts as possible. Whatever scrap is left should be cut and formed to match the "holes," which is why in the end you’ll have more holes than bonuts.

Fry the donuts 3 to 4 at a time, for 1-2 minutes per side. When the cold dough hits the fat, the temperature is going to fall quickly so you’ll want to boost the heat and keep an eye on the thermometer.

Remove the golden brown rings-of-wonder to a cooling rack inverted over a paper towel lined half sheet pan and cool for 2 minutes.

Microwave the milk in a large heat-proof bowl for 15 seconds. Whisk in the vanilla and the powdered sugar until smooth.

Gently dip one side of each "bonut" into the glaze, give it a twist then lift straight out. Allow the excess to drain off then flip glaze side up on the cooling rig. To glaze the holes (that little center thing you cut out and would never waste), I usually drop them in then lift them out with a dinner fork. Or…just go bobbing for them.

_Alton Brown_
