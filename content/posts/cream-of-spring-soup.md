---
title: "Cream of Spring Soup"
author: "pencechp"
date: "2010-05-08"
categories:
  - side dish
tags:
  - asparagus
  - soup
  - vegetarian
---

## Ingredients

*   1 1/2 lb. asparagus
*   4 cups chicken broth
*   coarse salt
*   4 tbsp. unsalted butter
*   1/4 c. flour
*   white pepper
*   2 tsp. each chopped chive, chervil, and dill
*   2 tbsp. heavy cream
*   lemon

## Preparation

1\. Prep: Snap the woody bottom off each asparagus stalk and discard.  Clean stalks with a vegetable peeler.  Trim off tips and set aside.

2\. Simmer: Settle stalks in a large saucepan.  Pour in broth.  Bring to a boil, reduce heat, cover and simmer until very soft, 40-45 minutes.  Don't be alarmed by long cooking time.  It's necessary to a smooth soup.

3\. Boil: Meanwhile, drop asparagus tips into fast-boiling, lightly salted water and cook until bright green and just tender, 5 minutes.  Drain and set aside.

4\. Blend: Using a food processor, immersion blender or the standard sort, swirl smooth broth and stalks.  Keep warm.

5\. Thicken: In a large heavy saucepan melt butter.  Add flour and whisk, without browning, 2 minutes.  Pour in pureed stock all at once and bring to a boil.  Cook over low heat until slightly thickened, 5 minutes.

6\. Spike: Season soup with salt, pepper, and a squeeze of lemon.  Stir in herbs, cream and reserved asparagus tips.  Enjoy fully dressed, or not.
