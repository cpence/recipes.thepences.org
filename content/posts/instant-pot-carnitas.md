---
title: "Instant Pot Carnitas"
author: "pencechp"
date: "2018-01-01"
categories:
  - main course
tags:
  - instantpot
  - mexican
  - pork
---

## Ingredients

_for the carnitas:_

*   1 (4-5 pound) lean boneless pork roast, excess fat trimmed, cut into 2-inch chunks
*   salt and pepper
*   1 tablespoon olive oil

_for the sauce:_

*   1 cup beer (or chicken stock)
*   1 head of garlic, cloves separated, peeled and minced
*   1/2 cup fresh orange juice
*   1/4 cup fresh lime juice
*   1 teaspoon dried oregano
*   1 teaspoon ground cumin
*   1 teaspoons salt
*   1/2 teaspoon freshly-cracked black pepper

## Preparation

In a medium mixing bowl, mix all sauce ingredients together.

Season pork chunks on all sides with salt and pepper.

Click the “Saute” setting on the Instant Pot.  Add oil and half of the pork, and sear — turning every 45-60 seconds or so — until the pork is browned on all sides.  Transfer pork to a separate clean plate, and repeat with the other half of the pork, searing until browned on all sides.  Press “Cancel” to turn off the heat. Pour in the mojo sauce, and toss briefly to combine. Close lid securely and set vent to “Sealing”. Press “Meat”, then press “Pressure” until the light on “High Pressure” lights up, then adjust the up/down arrows until time reads 30 minutes.  Cook.  Then let the pressure release naturally, about 15 minutes.  Carefully turn the vent to “Venting”, just to release any extra pressure that might still be in there.  Then remove the lid.

Shred the pork with two forks.  Then transfer it with a slotted spoon to a large baking sheet.  Spoon about a third of the leftover juices evenly on top of the pork.  Then broil for 4-5 minutes, or until the edges of the pork begin browning and crisping up. Remove the sheet from the oven, then ladle a remaining third of the juices from the Instant Pot evenly over the pork, and then give it a good toss with some tongs.  Broil for an additional 5 minutes to get the meat even more crispy. Then remove and ladle the final third of the juices over the pork, and toss to combine.

_Gimme Some Oven_
