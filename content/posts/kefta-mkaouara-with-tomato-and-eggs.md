---
title: "Kefta Mkaouara with Tomato and Eggs"
author: "pencechp"
date: "2010-05-28"
categories:
  - main course
tags:
  - lamb
  - moroccan
  - tomato
---

## Ingredients

*Kefta:*

*   1 lb. (about 1/2 kg) ground beef or lamb (or a combination of the two)
*   1 medium onion, grated
*   2 teaspoons paprika
*   1 teaspoon cumin
*   1 teaspoon salt
*   1/2 teaspoon ground cinnamon (optional)
*   1/4 teaspoon pepper
*   1/4 teaspoon hot paprika (or 1/8 teaspoon ground hot pepper)
*   1/4 cup chopped fresh parsley
*   1/4 cup chopped fresh coriander (cilantro)

*Tomato Sauce:*

*   2 lbs. (about 1 kg) fresh, ripe tomatoes
*   1 medium onion, grated (optional)
*   1 1/2 teaspoons paprika
*   1 1/2 teaspoons cumin
*   1 1/2 teaspoons salt
*   1/2 teaspoon hot paprika or 1/4 teaspoon ground hot pepper
*   3 tablespoons finely chopped fresh parsley
*   3 tablespoons finely chopped fresh coriander (cilantro)
*   3 cloves garlic, pressed
*   1/3 cup olive oil

*   3 or 4 eggs (optional)

## Preparation

Start Cooking the Tomato Sauce

Cut the tomatoes in half, seed them and grate them.

Mix the tomatoes, onions (if using) and the rest of the sauce ingredients in the base of a tagine or in a large, deep skillet. Cover, and bring to a simmer over medium heat. (Note: If using a tagine, place a diffuser between the tagine and burner, and allow 10 to 15 minutes for the tomato sauce to reach a simmer.)

Once simmering, reduce the heat to medium-low, just enough heat to maintain the simmer but low enough to avoid scorching. Allow the sauce to cook for 15 to 20 minutes before adding the meatballs.

Make the Kefta Meatballs

Combine all of the kefta ingredients, using your hands to knead in the spices and herbs. Shape the kefta mixture into very small meatballs the size of large cherries – about 3/4 inch in diameter.

Add the meatballs to the tomato sauce, along with a little water – 1/4 cup (60 ml) is usually sufficient – and cover. Cook for about 40 minutes, or until the sauce is thick.

Break the eggs over the top of the meatballs, and cover. Cook for an additional 7 to 10 minutes, until the egg whites are solid and the yolks are partially set. Serve immediately.

Kefta Mkaouara is traditionally served from the same dish in which it was prepared, with each person using [crusty Moroccan bread]( {{< relref "moroccan-wheat-bread" >}} ) for scooping up the meatballs from his own side of the dish.

_Christine Benlafquih, About.com_
