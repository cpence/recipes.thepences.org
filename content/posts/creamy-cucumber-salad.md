---
title: "Creamy Cucumber Salad"
date: 2021-07-10T11:09:25+02:00
categories:
  - side dish
tags:
  - salad
  - cucumber
  - untested
---

## Ingredients

- 1 English cucumber, sliced into rounds
- 3 tablespoons plain thick yogurt
- 2 tablespoons mayonnaise
- 1 tablespoon apple cider vinegar (or 1 teaspoon white vinegar)
- ¼ cup minced red onion
- ½ teaspoon black pepper 
- ¼ teaspoon sea salt

## Preparation

Slice cucumber into rounds, toss with remaining ingredients.

_Endless Meal_
