---
title: "Sekanjabin (Iranian Mint Vinegar Syrup)"
date: 2021-08-09T17:55:23+02:00
categories:
  - cocktails
tags:
  - iranian
  - untested
---

## Ingredients

- 8 cups orange blossom honey
- 5 cups water
- 2 cups white wine vinegar
- 12 large sprigs fresh mint

## Preparation

Stir honey and water together in a pot; bring to a boil and stir constantly until
honey dissolves. Add vinegar, reduce heat to low, and simmer until syrup flavors
combine, about 20 minutes. Remove from heat.

Submerge mint in hot syrup and cool to room temperature. Remove and discard
mint. Chill in the refrigerator.

To serve, mix 4-5 tbsp. with iced water and garnish with mint.

_allrecipes_
