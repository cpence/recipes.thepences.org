---
title: "Indian Vegetable Curry"
author: "pencechp"
date: "2017-05-02"
categories:
  - main course
tags:
  - cauliflower
  - indian
  - potato
  - vegan
  - vegetarian
---

## Ingredients

*   2 tablespoons curry powder
*   1 ½ teaspoons garam masala
*   ¼ cup vegetable oil
*   2 c. chopped onion
*   3/4 lb. potatoes, cut into 1/2" pieces
*   3 cloves garlic, pressed
*   1 tbsp. grated fresh ginger
*   1 - 1 ½ serrano chiles, minced
*   1 tbsp. tomato paste
*   ½ head cauliflower, florets (4 c.)
*   1 (14.5oz) can diced tomatoes, blended until nearly smooth
*   1 ¼ cups water
*   1 (15oz) can chickpeas, drained and rinsed
*   8 oz. frozen peas (about 1 1/2 cups)
*   ¼ cup heavy cream or coconut milk

## Preparation

Toast curry powder and garam masala in small skillet over medium-high heat, stirring constantly, until spices darken slightly and become fragrant, about 1 minute. Remove spices from skillet and set aside.

Heat 3 tbsp. oil in pot over medium-high heat. Add onions and potatoes and cook, stirring occasionally, until onions are caramelized and potatoes start to brown. Reduce heat to medium. Add remaining tablespoon oil, garlic, ginger, chile, and tomato paste; cook 30 seconds. Add toasted spices and cook 1 minute longer. Add cauliflower cook until well coated, about 2 minutes.

Add tomatoes, water, chickpeas, and 1 tsp. salt; increase heat to medium-high and bring mixture to boil. Cover and simmer briskly, stirring occasionally, until vegetables are tender, 10 to 15 minutes. Stir in peas and cream or coconut milk; continue to cook until heated through, about 2 minutes longer.
