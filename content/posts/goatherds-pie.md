---
title: "Goatherd's Pie"
author: "pencechp"
date: "2014-10-03"
categories:
  - main course
tags:
  - casserole
  - cornbread
  - goat
  - untested
---

## Ingredients

_Filling_

*   1 lb. ground goat
*   2 tbsp. vegetable oil
*   1 lg. onion, minced
*   2 cloves garlic, minced
*   1/4 tsp. ground cumin
*   1 tbsp. chili powder
*   1/4 tsp. Mexican oregano
*   1/8 tsp. cinnamon
*   1/4 tsp. cayenne pepper
*   2 c. chopped tomatoes (2-3 fresh, 14.5 oz can)
*   1/4-1/2 tsp. salt
*   1/4-1/2 tsp. pepper

_Crust_

*   1 c. coarse blue cornmeal
*   1-2 tbsp. flour
*   1/2 tsp. salt
*   2 tsp. baking powder
*   1 egg
*   1/2-3/4 c. plain yogurt
*   1 tbsp. vegetable oil

_Topping_

*   1 c. plain yogurt
*   1/4 tsp. salt
*   1/8 tsp. smoked chili powder
*   1/4 tsp. Mexican oregano

## Preparation

Preheat oven to 450. Heat the oil in a skillet over medium-high heat. Add the meat and cook until it starts browning. Add the onion and garlic and spices and continue cooking until the meat is thoroughly browned. Add the tomatoes, reduce the heat to medium and simmer for 10 minutes. Taste and add more spices if necessary. Add salt and pepper to taste. The mixture should be moist; neither dry nor soupy.

While the filling simmers, prepare the crust. Combine cornmeal, 1 tbsp. flour, salt, baking powder, yogurt, and vegetable oil. Pour the wet ingredients into the dry and stir until just combined. It should be a very thick consistency batter, just pourable. The extra 1/4 c. of yogurt or 1 tbsp. flour can be added to adjust either way.

Pour the meat into an 8x8" baking dish. Pour the crust on top of it. Bake at 450 until the top is crusted over and a bit browned, about 20-25 minutes. Serve with topping.

_Penzey's Spices_
