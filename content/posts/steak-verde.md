---
title: "Steak Verde"
author: "pencechp"
date: "2012-04-01"
categories:
  - main course
tags:
  - greenchile
  - newmexican
  - pence
  - steak
---

## Ingredients

*   8 TBSP butter
*   8 oz. sliced mushrooms
*   1 med. onion, chopped
*   3 cloves garlic, ditto
*   1 tsp. salt
*   1/2 tsp. pepper
*   4 C. chopped green chile
*   8 slices (calls for Swiss) queso quesadilla
*   4 NY strip steaks

## Preparation

Heat oven to 350.  Saute onion, mushrooms and garlic in butter.  Add chile and S&P.  Simmer 10-15 minutes.  Broil, grill or pan fry seasoned steaks.  Top with chile mixture and cheese and bake until cheese melts.
