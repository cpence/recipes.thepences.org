---
title: "Tartare Aux Asperges"
date: 2020-04-09T12:25:23+02:00
categories:
    - side dish
    - appetizer
tags:
    - asparagus
    - french
---

## Ingredients

*Pour le tartare:*

*   3 asperges blanches [white asparagus]
*   2 asperges vertes [green asparagus]
*   1 échalote hachée [shallot, minced]
*   ciboulette ciselée [chives, finely chopped]
*   1 càc de vinaigre balsamique blanc [1 tsp. white balsamic vinegar]
*   sel et poivre [salt and pepper]
*   quelques rosaces de Tête de Moine [several rosettes of Tête de Moine cheese; or shaved Parmesan]

*Pour la crème de Meaux:*

*   150 mL crème à 40% [150 mL of heavy whipping cream]
*   1 càc de moutarde de Meaux ou à l'ancienne [1 tsp. of Meaux mustard]
*   sel et poivre [salt and pepper]

## Preparation

*Le tartare :*

Cuire les asperges blanches dans de l'eau bouillante pendant 10 à 12 minutes. Vérifier la cuisson avec la pointe du couteau. Ensuite plongez-les dans de l'eau froide pour stopper la cuisson. Faites de même pour les asperges vertes mais pour une cuisson de 5 à 8 minutes. [Cook the white asparagus in boiling water for 10 to 12 minutes. Check the cooking with a knife point, then transfer to a cold water bath to stop cooking. Do the same for the green asparagus for 5 to 8 minutes.]

Couper les asperges en petits dés en gardant les têtes entières pour la décoration. Ajouter l'échalote, la ciboulette, le vinaigre balsamique blanc ; saler et poivrer. [Cut the asparagus into a small dice, keeping the heads whole for decoration. Add the shallot, chive, vinegar, and season with salt and pepper.]

*La crème à la moutarde de Meaux :*

Monter les ingrédients de la crème au batteur comme une chantilly. [Add all ingredients and beat to a Chantilly consistency, like soft whipped cream.]

Pour la décoration, utiliser les têtes d'asperge et quelques rosaces de Tête de Moine. [Decorate with the aspargagus heads and cheese.]

*Martin's Hotels*

