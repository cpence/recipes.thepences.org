---
title: "Shrimp Garlic Pasta"
author: "pencechp"
date: "2015-05-06"
categories:
  - main course
tags:
  - pasta
  - shrimp
---

## Ingredients

*   5 medium garlic cloves, pressed, divided (3+2), plus 4 medium cloves,
    smashed
*   1 pound large shrimp (21-25), peeled, deveined, each shrimp cut into 3
    pieces
*   3 tablespoons olive oil
*   Table salt
*   1/4-1/2 teaspoon red pepper flakes
*   1 pound pasta in short, tubular shapes
*   2 teaspoons unbleached all-purpose flour
*   1/2 cup dry vermouth or white wine
*   3/4 cup clam juice
*   1/2 cup chopped fresh parsley
*   3 tablespoons unsalted butter
*   1 teaspoon lemon juice plus 1 lemon, cut into wedges
*   Ground black pepper

## Preparation

Toss 2 cloves pressed garlic, shrimp, 1 tablespoon oil, and 1/4 teaspoon salt in
medium bowl. Let shrimp marinate at room temperature 20 minutes.

Heat 4 smashed garlic cloves and remaining 2 tablespoons oil in 12-inch skillet
over medium-low heat, stirring occasionally, until garlic is light golden brown,
4 to 7 minutes. Remove skillet from heat and use slotted spoon to remove garlic
from skillet; discard garlic. Set skillet aside.

Bring 4 quarts water to boil in large Dutch oven over high heat. Add 1
tablespoon salt and pasta. Cook until just al dente, then drain pasta, reserving
1/4 cup cooking water, and transfer pasta back to Dutch oven.

While pasta cooks, return skillet with oil to medium heat; add shrimp with
marinade to skillet in single layer. Cook shrimp, undisturbed, until oil starts
to bubble gently, 1 to 2 minutes. Stir shrimp and continue to cook until almost
cooked through, about 1 minute longer. Using slotted spoon, transfer shrimp to
medium bowl. Add remaining 3 cloves pressed garlic and pepper flakes to skillet
and cook until fragrant, about 1 minute. Add flour and cook, stirring
constantly, for 1 minute; stir in vermouth and cook for 1 minute. Add clam juice
and parsley; cook until mixture starts to thicken, 1 to 2 minutes. Off heat,
whisk in butter and lemon juice. Add shrimp and sauce to pasta, adding reserved
cooking water if sauce is too thick. Season with black pepper. Serve, passing
lemon wedges separately.

_CI_
