---
title: "Corned Beef and Cabbage"
author: "juliaphilip"
date: "2016-05-02"
categories:
  - main course
tags:
  - beef
  - cabbage
  - irish
---

## Ingredients

*   One 3-pound corned beef brisket (uncooked), in brine
*   16 cups cold water
*   2 bay leaves
*   2 teaspoons black peppercorns
*   4 whole allspice berries
*   2 whole cloves
*   1/2 large head green cabbage (about 2 pounds), cut into 8 thick wedges
*   8 small new potatoes (about 1 1/4 pounds), halved
*   Freshly ground black pepper to taste
*   Horseradish sauce

## Preparation

Preheat the oven to 300 degrees F. Place the corned beef in a colander in the sink and rinse well under cold running water. Place the corned beef in a large Dutch oven with a tight-fitting lid, add the water, bay leaves, peppercorns, allspice and cloves. Bring to a boil, uncovered, and skim off any scum that rises to the surface. Cover and transfer pan to the oven, and braise until very tender, about 3 hours and 45 minutes. Transfer the corned beef to a cutting board and cover tightly with foil to keep warm. Add the cabbage and potatoes to the cooking liquid and bring to a boil. Lower the heat and simmer until the vegetables are tender, about 20 minutes. Using a slotted spoon, transfer the cabbage to a large platter. Slice the corned beef across the grain of the meat into thin slices. Lay the slices over the cabbage and surround it with the potatoes. Ladle some of the hot cooking liquid over the corned beef and season with pepper. Serve immediately with the mustard or horseradish sauce.

_Food Network_
