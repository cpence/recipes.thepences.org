---
title: "Nonnettes"
date: 2021-11-28T12:12:40+01:00
categories:
  - dessert
tags:
  - french
---

## Ingredients

- 1/2 c. honey
- 1/2 c. dark brown sugar, packed
- 3/4 c. water
- 6 tbsp. butter
- 3 tbsp. vegetable oil
- 1 1/2 c. (200 g) all-purpose flour
- 3/4 c. (80 g) rye flour
- 1 tbsp. pain d'épices spice blend
- 1 tsp. ground cinnamon
- 2 tsp. baking powder
- 1 tsp. baking soda
- 1/4 tsp. salt
- 1 tbsp. rum
- 1 tsp. vanilla extract
- 2 tsp. orange zest
- 1 egg yolk
- black currant jam (or orange marmalade)
- 3/4 c. powdered sugar
- 3 tbsp. lemon juice

## Preparation

Preheat the oven to 375 F.

In a small saucepan over medium heat, add butter, oil, water, honey, and brown
sugar, and stir until melted and smooth. Remove from heat and let cool while
preparing dry ingredients.

In a mixing bowl, combine flours, baking soda, baking powder, salt, cinnamon,
and spice blend. Stir. Pour the honey mixture into the flour and stir to
combine. Add the rum, vanilla, orange zest, and egg yolk, and stir.

Butter the bottoms of a muffin tin. Fill each with batter 1/2 to 2/3 full. Make
an indentation and put a teaspoon of jam inside. (Alternatively, half-fill the
molds, add the jam, and then add more batter.)

Bake for 12-15 minutes until golden brown and the dough springs back when
pressed.

While baking, prepare the glaze by combining powdered sugar and lemon juice in a
small bowl. While the nonnettes are still warm and in the molds, spoon the glaze
over them, letting the glaze drip down into the molds. Remove when cool.

_Daring Gourmet_
