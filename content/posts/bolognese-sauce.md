---
title: "Bolognese Sauce"
author: "juliaphilip"
date: "2010-02-06"
categories:
  - main course
tags:
  - beef
  - italian
  - untested
---

## Ingredients

*   3 tbs. extra-virgin olive oil
*   1 oz. pancetta, finely chopped
*   1 large carrot, peeled and minced
*   2 small celery ribs, minced
*   1/2 med. onion, minced
*   1 1/3 lb. coarsely ground beef skirt steak or ground chuck
*   3/4 c. chicken or beef stock
*   2/3 c. dry white wine
*   2 tbl. tomato paste
*   1 1/2 c whole milk

## Preparation

Heat olive oil and pancetta in a large saucepan over medium-low heat and cook until pancetta releases its fat but is not browned, about 8 minutes. Increase the heat to medium and add carrot, celery and onion. Cook, stirring until the onions are translucent, about 5 minutes. Add ground beef and brown. Stir in stock, wine, and tomato paste. Reduce heat to low and simmer gently, partially covered and skimming occasionally and add milk 2 tbl. at a time from time to time as the sauce simmers. Cook until the sauce is the consistency of a thick soup, about 2 hours. Remove from the heat and let cool. Cover and refrigerate for up to 24 h. Skim the fat off the top before reheating. Serve over wide noodles.
