---
title: "Citrus Olive Oil Cake"
author: "pencechp"
date: "2017-05-02"
categories:
  - dessert
tags:
  - cake
  - lemon
  - orange
---

## Ingredients

_For Cake_

*   1 1/2 cups whole wheat flour
*   2 teaspoons baking powder
*   1/4 teaspoon salt
*   2 teaspoons citrus zest (any type)
*   1/4 cup fresh citrus juice
*   1 cup sugar
*   3/4 cup plain yogurt
*   3 eggs
*   1/2 teaspoon pure vanilla extract
*   1/2 cup extra-virgin olive oil

_For Glaze_

*   1/3 cup powdered sugar
*   2 to 3 teaspoons fresh citrus juice

## Preparation

Preheat the oven to 350 degrees Fahrenheit. Generously butter and flour an 8½ by 4¼-inch loaf pan. In a medium mixing bowl, whisk together the flour, baking powder and salt.

Pour the sugar into another medium-sized mixing bowl. Use a Microplane to grate 2 teaspoons zest from your fresh citrus fruits and add it to the bowl. Rub the zest into the sugar until the sugar takes on the color and fragrance of the citrus.

In a liquid measuring cup, measure out ¾ cup yogurt and squeeze in about ¼ cup citrus juice to yield 1 cup total liquid. Whisk the yogurt and juice, eggs and vanilla into the sugar mixture until well blended.

Gently whisk the dry ingredients into the wet ingredients, just until incorporated. Switch to a spatula and fold in the oil, making sure it’s all incorporated. The batter will be shiny. Pour the batter into the prepared loaf pan and smooth the top.

Bake the cake for 50 to 55 minutes, or until the top is golden and the sides start to pull away from the sides of the pan; a toothpick inserted into the center should come out clean.

Let the cake cool in the pan for 10 minutes, then run a knife between the cake and the sides of the pan to loosen. Unmold the cake by placing a large plate upside down over the loaf pan and carefully turning them over. Carefully flip the cake back over and let it cool to room temperature.

To make the glaze: Measure the powdered sugar into a small bowl. Whisk in just enough blood orange juice to make a thick glaze that drizzles easily off your whisk when lifted. Drizzle the glaze back and forth over the cake. The glaze will need about 15 minutes to set, then it’s ready to slice and serve.

_Cookie and Kate_
