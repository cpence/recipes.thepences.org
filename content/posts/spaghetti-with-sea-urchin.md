---
title: "Spaghetti with Sea Urchin"
author: "pencechp"
date: "2014-01-04"
categories:
  - main course
  - side dish
tags:
  - italian
  - uni
---

## Ingredients

*   1 lb. spaghetti, prepared per package directions
*   1/4 to 1/2 lb. sea urchin roe (uni)
*   4 tbsp. olive oil
*   2 cloves crushed garlic
*   1 bunch chives, minced
*   1/4 c. fresh parsley, minced
*   2 tbsp. fresh lemon juice
*   salt and pepper

## Preparation

Whisk sea urchin roe, chives, parsley, lemon juice, salt and pepper (to taste), and half the olive oil together. Set aside.

Heat remaining olive oil and garlic in a large skillet for a few minutes. Remove garlic. Toss with spaghetti. Transfer to a bowl and toss with sea urchin mixture. Serve.

_from Weird Combinations, edited_
