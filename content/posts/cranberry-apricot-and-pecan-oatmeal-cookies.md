---
title: "Cranberry, Apricot and Pecan Oatmeal Cookies"
author: "juliaphilip"
date: "2009-12-06"
categories:
  - dessert
tags:
  - apricot
  - cookie
  - cranberry
  - pecan
  - untested
---

## Ingredients

*   1/2 cup packed dark brown sugar
*   1/4 cup sugar
*   1 stick butter, softened
*   1 large egg
*   1 tsp vanilla
*   3/4 cup flour
*   1/4 tsp salt
*   1/2 tsp baking soda
*   1/2 tsp ground coriander
*   1/4 tsp cinnamon
*   1/4 tsp allspice
*   1/8 tsp mace
*   1/2 cup chopped pecans, lightly toasted
*   1/4 cup dried apricots, diced
*   1/4 cup dried cranberries
*   1 1/2 cup old-fashioned oatmeal

## Preparation

Preheat oven to 350º. Cream together the sugars and butter. Add the egg and vanilla and mix well. In a small bowl, mix together the flour, baking soda, salt, and spices. Add the flour mixture to the butter mixture a bit at a time, mixing well after each addition. Mix together the pecans, apricots and cranberries in the bowl that used to hold the flour. Alternate adding the oatmeal and the fruit/nut mix to the batter, mixing well after each addition. Form walnut-sized balls of dough and place on ungreased cookie sheets. Flatten the cookies slightly with your hands. Bake for 6 minutes and the flip the cookie sheets from front to back and top to bottom if baking with two sheets at once. Bake and additional 6 minutes. Let cool on a tray. Keep the dough in the refrigerator between batches.

_Penzeys_
