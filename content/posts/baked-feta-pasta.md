---
title: "Baked Feta Pasta"
date: 2021-03-07T12:37:13+01:00
categories:
  - main course
tags:
  - pasta
---

## Ingredients

- 2 pt. cherry or grape tomatoes
- 1 shallot, quartered
- 3 cloves garlic, smashed (optional)
- 1/2 c. extra virgin olive oil, divided
- Kosher salt
- Crushed red pepper flakes
- 1 (8-oz., 225g) block feta
- 3 sprigs thyme
- 10 oz. pasta
- Zest of 1 lemon (optional)
- fresh basil (optional)

## Preparation

1. Preheat oven to 400°/205C. In a large ovenproof skillet or medium baking dish, combine tomatoes, shallot, garlic, and most of the olive oil. Season with salt and red pepper flakes and toss to combine.
2. Place feta into center of tomato mixture and drizzle top with remaining olive oil. Scatter thyme over tomatoes. Bake for 40 to 45 minutes, until tomatoes are bursting and feta is golden on top.
3. Meanwhile, in a large pot of salted boiling water, cook pasta according to package instructions. Reserve ½ cup pasta water before draining.
4. To skillet with tomatoes and feta, add cooked pasta, reserved pasta water (if the sauce needs it), and lemon zest (if using) to skillet and stir until completely combined. Garnish with basil before serving.

_TikTok, via Delish_
