---
title: "Shrimp and Bulgur Salad"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - main course
tags:
  - bulgur
  - shrimp
  - untested
---

## Ingredients

*   1/2 cup bulgur
*   1 cup frozen, thawed cooked baby shrimp, well drained (about 4 ounces)
*   2 tablespoons sliced pitted green olives
*   4 large grape tomatoes, sliced
*   1 scallion, trimmed and chopped
*   2 tablespoons minced Italian parsley
*   2 teaspoons white wine vinegar
*   2 teaspoons olive oil
*   1/4 teaspoon ground cumin
*   Salt and pepper to taste

## Preparation

Bring 1/2 cup water to boiling in a small pot. Stir in bulgur. Cover, remove from heat and let stand 15 minutes or until water is absorbed and bulgur is chewy, not hard. Spoon into a large salad bowl and set aside to cool. Add shrimp, olives, tomatoes, scallion and parsley to bulgur. Stir well. Combine vinegar, oil and cumin in a small cup. Pour over salad. Toss gently but well. Season with salt and pepper. Set aside 10 minutes for flavors to blend.

_Chicago Tribune_
