---
title: "Bacon Scallion Hoecakes"
author: "pencechp"
date: "2015-01-01"
categories:
  - side dish
tags:
  - bacon
  - pancake
  - untested
---

## Ingredients

*   1 c. self-rising flour
*   1 c. fine yellow cornmeal
*   2 tsp. baking powder
*   1/2 tsp. salt
*   1/2 tbsp. sugar
*   2/3 c. buttermilk
*   1/3 c. water
*   scant 1/4 c. bacon fat
*   1/3 c. creme fraiche or sour cream
*   2 eggs
*   1/3 c. scallions, thinly sliced
*   1/4 c. crisp bacon, finely chopped
*   Canola oil, combined with some bacon fat if desired

## Preparation

In a medium bowl, whisk together flour, cornmeal, baking powder, salt, and sugar.

In another small bowl, combine buttermilk, water, bacon fat, and creme fraiche or sour cream. Blend well. Add eggs and mix until just combined.

Pour liquid into dry ingredients, and mix just until a thick batter has formed. Stir in scallions and bacon. To keep the hoecakes tender, avoid overmixing.

Heat oil and bacon fat in a cast-iron or other heavy skillet over medium to medium-high heat. Use about an eighth of a cup of batter for smaller cakes, or a quarter cup for larger ones. Cook them as you would pancakes, allowing them to fry on one side for about three minutes or until crisp and golden brown. Flip and cook for another couple of minutes. Replenish oil as needed, to maintain a generous film in the pan.

Batter will keep for a couple of days tightly covered in the refrigerator. If it seems too thick after storing, stir in a bit more buttermilk or water.

_Garden & Gun, April/May 2013_
