---
title: "Fried Chinese Five-Spice Chicken Wings"
author: "pencechp"
date: "2013-05-28"
categories:
  - appetizer
  - main course
tags:
  - chicken
  - chinese
  - untested
---

## Ingredients

*   3 pounds chicken wings (12 to 14)
*   1 onion
*   a 1 1/2-inch piece peeled fresh ginger root
*   2 tablespoons soy sauce (preferably Kikkoman)
*   1 tbsp. medium-dry Sherry
*   1 tablespoon Chinese five-spice powder
*   1 tablespoon sugar
*   2 tablespoons plus 1 teaspoon coarse salt
*   2 teaspoons freshly ground black pepper, or to taste
*   1/2 cup cornstarch
*   6 cups vegetable oil

## Preparation

Cut off wing tips, reserving for another use, and halve wings at joint. Coarsely chop onion and finely chop ginger root. In a large sealable plastic bag combine onion, ginger root, soy sauce, Sherry, five-spice powder, sugar, and 1 teaspoon salt. Add wings and seal bag, pressing out excess air. Turn bag until wings are completely coated. Marinate chicken wings in bag in a large bowl, chilled, turning bag once, 2 hours.

Preheat oven to 350°F.

Transfer wings and marinade to a roasting pan or large shallow baking pan and bake, covered with foil, in middle of oven 1 hour. Cool wings until they can be handled and drain in a colander, discarding marinade. Wings may be prepared up to this point 1 day ahead and chilled, covered.

Reduce temperature to 250°F.

In a small serving bowl combine remaining 2 tablespoons salt and pepper. Put cornstarch in a small bowl and dredge each wing, knocking off excess cornstarch and transferring to a shallow pan. In a 5- to 6-quart heavy kettle heat oil until a deep-fat thermometer registers 370°F. Working in batches of 6 or 7, fry wings in oil until golden brown, about 3 minutes, and with a slotted spoon transfer to paper towels to drain. Keep wings warm on a baking sheet in oven. Fry remaining wings in same manner, returning oil to 370°F. between batches.

Serve wings with salt and pepper mixture.

_Serves 6 as an appetizer; 4 as a main course._

_Gourmet, July 1999_
