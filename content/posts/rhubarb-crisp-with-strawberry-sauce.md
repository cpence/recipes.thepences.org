---
title: "Rhubarb Crisp with Strawberry Sauce"
author: "pencechp"
date: "2010-05-08"
categories:
  - dessert
tags:
  - rhubarb
  - strawberry
  - untested
---

## Strawberry sauce

*   2 cups strawberries
*   1 tablespoon sugar, or to taste

1\. In a food processor or food mill, purée the berries until smooth. Add the sugar, a teaspoon at a time, making sure the sauce is not too sweet.

2\. Strain the sauce through a fine mesh strainer to remove the seeds. Cover and chill. This makes 1 1/2 cups sauce.

## Rhubarb crisp and assembly

*   1/2 cup sugar
*   1 1/2 tablespoons cornstarch
*   Finely shredded zest of 1 large orange
*   1 1/2 pounds (10 to 12 inch-wide stalks) rhubarb, trimmed
*   1 cup flour
*   1/2 cup firmly packed light brown sugar
*   1/2 teaspoon finely ground white pepper
*   1/4 teaspoon nutmeg
*   1/4 teaspoon ground ginger
*   Good pinch of ground cloves
*   1/2 cup (1 stick) butter
*   3 tablespoons orange juice
*   3 cups vanilla ice cream

1\. Heat the oven to 375 degrees. Combine the sugar, cornstarch and orange zest in a large mixing bowl. Cut the rhubarb into 1-inch pieces (you should have about 5 1/2 cups), add to the bowl and mix with your hands until the rhubarb is evenly covered.

2\. In a food processor or mixing bowl, blend the flour, brown sugar, pepper, nutmeg, ginger and cloves. Cut the butter into thin chips, then pulse or work the mixture lightly with your fingertips until it is the texture of raw oats.

3\. Arrange the rhubarb mixture evenly in a 9 1/2 -inch glass pie plate, making sure all the sugar doesn't fall to the bottom (if it does, spoon it over the rhubarb again). Sprinkle with the orange juice, then spoon the flour and spice mixture evenly over the dish.

4\. Bake until bubbly and golden brown, 50 to 55 minutes, rotating the pan halfway through for even cooking. Serve warm, topped with a small scoop of ice cream and a splash of the bright red sauce.

_Chicago Tribune_
