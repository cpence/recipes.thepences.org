---
title: "Cauliflower Pizza Dough"
author: "pencechp"
date: "2014-03-18"
categories:
  - main course
tags:
  - italian
  - pizza
  - untested
---

## Ingredients

*   1 head of cauliflower
*   1 egg
*   A dollop of goat cheese
*   1/2 cup grated parm (or more)
*   Salt, pepper, and any other seasoning or herb you want in the dough
*   Garlic or garlic powder
*   Fresh mozzarella
*   Sliced tomatoes (or tomato sauce)
*   Basil
*   More parm, to sprinkle on top (or whatever toppings instead of these last four)

## Preparation

First, grind up the cauliflower in your food processor. Don't have a food processor? That's fine, you can use a box grater, though it will take more time.

Next, cook the cauliflower—you can heat it up in your microwave for 5 minutes, or cook it (for that same amount of time) with a little bit of water in a pot or pan. After it cools (you can put it in the fridge to speed along this process) place it into a piece of cheesecloth and wring every last drop of water out of it. This is important!

Now you have a nice ball of dry cauliflower "flour," which you will mix with one egg, a dollop of goat cheese, a half a cup or so of grated parm, and any seasoning you want. Honestly, once you have the egg and cauliflower in there, you don't have to be exact in what type or how much cheese to add.

Once that's done, form it into a pizza crust and bake at 425ºF for about 10 minutes (until it starts to get golden). Do this on a parchment paper-lined baking sheet, and WITHOUT any toppings.

Remove your dough from the oven and add your toppings (we recommend putting a little garlic powder on the dough before you start topping it—cauliflower has a pretty bland taste, so don't be afraid to season). Then return it to the oven to cook for another 10 minutes, until the cheese is melted.

_Gothamist_
