---
title: "Pão de Milho (Portuguese Corn Bread)"
author: "pencechp"
date: "2014-11-18"
categories:
  - bread
tags:
  - portuguese
  - untested
---

## Ingredients

*   1 1/4 cups fine white cornmeal
*   2 teaspoons salt
*   1 1/4 cups boiling water
*   1 teaspoon granulated sugar
*   1 cup lukewarm water, divided
*   1 tablespoon active dry yeast (or 1 15 mL pkg)
*   3 cups all-purpose flour
*   1/2 cup white cornflour (approx. measure) or 1/2 cup yellow cornflour (approx. measure)

## Preparation

1) Please note that the corn flour in this recipe is NOT cornstarch; you should be able to find corn flour at your grocery store in either the aisle where flour is sold, or in the aisle where ethnic foods are sold.

2) In a large mixing bowl, mix the cornmeal and salt together, then add the boiling water and stir until smooth.

3) Let this cool until mixture is lukewarm (should take about 10 minutes).

4) Meanwhile, in a measuring cup, dissolve the sugar in 1/2 cup of the lukewarm water and sprinkle in the yeast; let stand for 10 minutes.

5) Now rapidly whisk the yeast mixture with a fork and then stir into the cornmeal mixture.

6) A bit at a time, mix in the all-purpose flour, stopping a few times to slowly add in the remaining 1/2 cup lukewarm water; blend this mixture well, until completely combined.

7) Turn this mixture out onto a well-floured surface and knead until elastic and all ingredients are well blended; this will likely take about 10 minutes.

8) If you need to — and only if you need to — add a bit more flour; you will likely need to if your kitchen is at all humid, as that can keep the dough a little sticky.

9) When smooth and elastic, gather the dough into a ball.

10) Lightly grease a fairly large mixing bowl and put the dough into bowl, turning dough so it is greased all over.

11) Cover bowl with a clean tea towel, place in a draft-free area (try the top of your fridge, or inside your oven — not turned on of course) and let dough rise until doubled in bulk, which should take about 90 minutes.

12) Now punch down the dough and shape it into either one round loaf or two small ones; have ready a well greased baking sheet or a well greased pie plate.

13) Roll the loaf (loaves) in corn flour until well covered, then, if you’ve made the two small loaves, place on baking sheet, or if you’ve prepared just the one large loaf, place on pie plate.

14) Cover with clean tea towel and let rise in a draft-free place for 45 minutes, or until doubled in size.

15) Sprinkle with additional corn flour just before baking.

16) Bake bread in preheated 450F oven for 30 to 40 minutes, or until loaves sound hollow when tapped on bottom; they should be golden brown and crusty on top.

17) Transfer to wire racks and wait impatiently for them to cool.

_Easy Portuguese Recipes_
