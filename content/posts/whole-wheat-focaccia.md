---
title: "Whole Wheat Focaccia"
author: "pencechp"
date: "2012-08-16"
categories:
  - bread
tags:
  - italian
---

## Ingredients

*   3 cups whole wheat flour
*   1 packet active dried yeast
*   1 1/4 cups warm water
*   1 tsp cumin seeds
*   6 cloves fresh garlic, finely chopped
*   1 tsp salt
*   3 tbs olive oil

Toppings: cumin seeds, thinly sliced onions

## Preparation

Combine warm water and yeast. Stir in olive oil, cumin seeds, chopped garlic, salt and flour and mix to form a dough. Add more flour if the dough is too sticky.

Transfer the dough to a floured surface and knead for a few minutes in a folding motion until the dough is smooth.

Grease a large bowl with olive oil and plop the dough into it. Roll around a few times to coat the dough in the oil. Cover the bowl with a warm cloth. Place the bowl in a warm spot and let the dough rise for about an hour.

After the rise, punch the dough once and place it on a floured surface and knead gently for a few minutes. Roll out the dough to about 1" thickness.

Place dough on a greased baking sheet. Poke holes all over the surface with your finger.

Brush the top of the dough generously with olive oil. Top with cumin seeds and sliced onions.

Cover with the towel and let rise again for 20 minutes, in a warm spot.

Bake the bread in a preheated 350 degree F oven for about 30-35 minutes or until you see a light brown crust. Check the bread for firmness with a toothpick. Cool for a few minutes before slicing.

_Healthy Happy Life, via Tasty Treats_
