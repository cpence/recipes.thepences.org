---
title: "Black Bean-Chorizo Stew"
author: "pencechp"
date: "2015-03-13"
categories:
  - main course
tags:
  - bean
  - mexican
  - sausage
  - untested
---

## Ingredients

*   2 tablespoons extra-virgin olive oil
*   1 large white onion, diced
*   ½ batch (12 ounces) [homemade green chorizo,]( {{< relref "green-chorizo" >}} ) or use another spicy fresh sausage
*   ¼ cup chopped cilantro stems, leaves reserved for serving
*   7 cups cooked black beans (from 4 cans or 1 pound dried beans), drained
*   1 (28-ounce) can diced plum tomatoes with their juices
*   2 teaspoons kosher salt, more as needed
*   Diced avocado, for serving
*   Sliced scallion, for serving
*   Lime wedges, for serving

## Preparation

Heat oil over medium heat in a large Dutch oven or heavy-bottom pot. Add onion and cook until softened, 5 to 10 minutes. Stir in chorizo and cilantro stems and cook 5 minutes over high heat, or until much of the liquid has evaporated.

Stir in beans, tomatoes and their liquid, and 1 cup water. Bring mixture to a boil over high heat; reduce to medium.

Partly cover pot and simmer until tomatoes have fallen apart, about 1 hour to 1 hour 15 minutes. Season with salt. Serve topped with avocado, scallion, cilantro leaves and lime wedges.

_New York Times_
