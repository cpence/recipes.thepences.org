---
title: "Dark Chocolate Brownies"
author: "pencechp"
date: "2013-11-04"
categories:
  - dessert
tags:
  - brownie
  - chocolate
  - untested
---

## Ingredients

*   7 ounces unsweetened chocolate, coarsely chopped
*   3/4 cup butter
*   1/4 cup water
*   1 cup granulated sugar
*   3/4 cup packed brown sugar
*   2 eggs
*   1 teaspoon vanilla
*   1 1/3 cups all-purpose flour
*   1/8 teaspoon salt
*   1/8 teaspoon ground cinnamon
*   Unsweetened cocoa powder and/or powdered sugar

## Preparation

1\. Preheat oven to 350 degrees F. Lightly grease a 9-inch square by 2-inch deep baking pan; set aside. In a medium saucepan, combine chocolate, butter, and the water; cook and stir over low heat until chocolate is melted. Transfer to a large bowl.

2\. Add granulated sugar and brown sugar to chocolate mixture; beat with an electric mixer on low to medium speed until combined. Add eggs and vanilla; beat on medium speed for 2 minutes. Add flour, salt, and cinnamon. Beat on low speed until combined. Spread batter in prepared pan.

3\. Bake for 25 to 30 minutes or until a toothpick inserted near center comes out clean. Cool in pan on a wire rack. Cut into bars. Sprinkle with cocoa powder and/or powdered sugar. Makes 20 to 25 brownies.

_Better Homes & Gardens_
