---
title: "Fifty-Fifty (Classic Martini)"
author: "pencechp"
date: "2010-05-09"
categories:
  - cocktails
tags:
  - gin
---

## Ingredients

*   Cracked ice
*   1 1/2 oz. London dry gin, such as Tanqueray (high-proof, full-bodied)
*   1 1/2 oz. dry vermouth, preferably Noilly Prat
*   2 dashes orange bitters
*   Small piece lemon peel

## Preparation

In mixing glass or cocktail shaker, combine gin, vermouth, and bitters.  Stir well, about 20 seconds, then strain into martini glass or shallow cocktail glass.  Twist lemon peel directly over drink to release essential oils, and serve.

_Epicurious, March 2007_
