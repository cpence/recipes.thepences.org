---
title: "Bitter Orange Juice"
author: "pencechp"
date: "2010-05-08"
categories:
  - miscellaneous
tags:
  - mexican
  - orange
  - sauce
---

## Ingredients

*   1/2 c. grapefruit juice
*   1/4 c. orange juice
*   3 tbsp. lime juice

## Preparation

Combine all ingredients and let sit for two hours at room temperature.
