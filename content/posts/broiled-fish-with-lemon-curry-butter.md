---
title: "Broiled Fish with Lemon Curry Butter"
author: "pencechp"
date: "2017-10-30"
categories:
  - main course
tags:
  - curry
  - fish
  - indian
---

## Ingredients

*   4 tablespoons unsalted butter
*   4 garlic cloves, finely grated or minced
*   1 ½ tablespoons minced thyme leaves
*   1 ½ teaspoons curry powder
*   1 ½ teaspoons grated ginger
*   ¼ teaspoon fine sea salt, more as needed
*   ¾ teaspoon finely grated lemon zest
*   Ground black pepper, to taste
*   4 (6-ounce) blackfish, flounder or hake fillets
*   Fresh lemon juice, for serving
*   Dill fronds or fresh parsley, for serving

## Preparation

Heat the broiler. In a small saucepan over medium heat, melt butter. Stir in garlic, thyme, curry powder, ginger and 1/4 teaspoon salt; heat until fragrant, about 1 minute. Stir in lemon zest.

Season fish with salt and pepper and place on a rimmed baking sheet. Pour sauce over fish and broil until fish is flaky and cooked through, about 5 minutes. Top with a squeeze of lemon juice and fresh dill, and serve.

_New York Times_
