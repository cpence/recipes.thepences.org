---
title: "Chips and Fish [Chile Rellenos]"
author: "pencechp"
date: "2010-01-08"
categories:
  - main course
tags:
  - chile
  - fish
  - greenchile
---

\[N.B.: This is the chile relleno recipe as well; just make the batter, omitting the cayenne pepper and Old Bay.\]

## Ingredients

*For the fries:*

*   1 gallon safflower oil
*   4 large Russet potatoes
*   Kosher salt

*For the batter:*

*   2 cups flour
*   1 tablespoon baking powder
*   1 teaspoon kosher salt
*   1/4 teaspoon cayenne pepper
*   Dash Old Bay Seasoning
*   1 bottle brown beer, cold
*   1 1/2 pounds firm-fleshed whitefish (tilapia, pollock, cod), cut into 1-ounce strips
*   Cornstarch, for dredging

## Preparation

Heat oven to 200 degrees F.

Heat the safflower oil in a 5-quart Dutch oven over high heat until it reaches 320 degrees.

Using a V-slicer with a wide blade, slice the potatoes with the skin on. Place in a large bowl with cold water.

In a bowl, whisk together the flour, baking powder, salt, cayenne pepper, and Old Bay seasoning. Whisk in the beer until the batter is completely smooth and free of any lumps. Refrigerate for 15 minutes. Note: The batter can be made up to 1 hour ahead of time.

Drain potatoes thoroughly, removing any excess water. When oil reaches 320 degrees, submerge the potatoes in the oil. Working in small batches, fry for 2 to 3 minutes until they are pale and floppy. Remove from oil, drain, and cool to room temperature.

Increase the temperature of the oil to 375 degrees. Re-immerse fries and cook until crisp and golden brown, about 2 to 3 minutes. Remove and drain on roasting rack. Season with kosher salt while hot and hold in the oven.

Allow oil to return to 350 degrees. Lightly dredge fish strips in cornstarch. Working in small batches, dip the fish into batter and immerse into hot oil. When the batter is set, turn the pieces of fish over and cook until golden brown, about 2 minutes. Drain the fish on the roasting rack. Serve with malt vinegar.

_Alton Brown, FoodNetwork.com_
