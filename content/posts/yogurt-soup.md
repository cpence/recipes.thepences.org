---
title: "Yogurt Soup"
author: "juliaphilip"
date: "2010-04-03"
categories:
  - main course
tags:
  - lamb
  - mediterranean
  - soup
---

## Ingredients

*   8 c water
*   3/4 lb ground lamb or left-over leg of lamb cut into 3/4 in pieces
*   2 onions, finely chopped
*   1/2 c yellow split peas
*   1/2 lb spinach, chopped
*   10 scallions, chopped
*   1 tsp salt freshly ground black pepper
*   1 c raw rice
*   2 c yogurt
*   1 tbl olive oil
*   2 tbl fresh chopped mint or 2 tsp dried mint

## Preparation

Place the water, lamb, onions, split peas, spinach, scallions, salt and pepper in a saucepan. Bring to a boil, cover and simmer 40 min. Stir in the rice and cook over moderate heat for about 25 min, stirring occasionally. Add the yogurt. Stir 1 minute without boiling and remove from heat. Heat the oil in a frying pan, add the mint and stir for 1 min. Add to the soup and serve.
