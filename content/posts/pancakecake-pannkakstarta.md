---
title: "Pancakecake (Pannkakstårta)"
author: "pencechp"
date: "2013-03-18"
categories:
  - breakfast
  - dessert
tags:
  - cake
  - pancake
  - swedish
  - untested
---

## Ingredients

_For the pancake batter:_

*   3 eggs
*   2 c. flour
*   4 c. milk
*   1 tsp. vanilla
*   1 tbsp. sugar
*   pinch salt
*   50 g melted butter

_For the filling/garnish:_

*   berries
*   about 2 1/2 c. whipping cream, sweetened

## Preparation

Melt the butter in your pancake skillet (should be big, around 12" at least).  Beat the eggs and whisk in the flour until all lumps are gone.  Add the sugar, vanilla, sugar, and salt.  Pour in a little milk at a time until the mixture becomes a nice paste.  (These will be thin pancakes, somewhere between American pancakes and crepes.)  Pour the melted butter into the batter.  Mix well every time you take a scoop of batter to cook.  Cook all the pancakes in butter and let them cool.  Whip the cream and sweeten it (you can add some vanilla, too).  When the pancakes have cooled, put a pancake on a plate, add a little cream and berries, and repeat until you run out of stuff.

_Nadjas Kitchen, and Google Translate_
