---
title: "Almond Cake"
author: "pencechp"
date: "2012-12-22"
categories:
  - dessert
tags:
  - almond
  - cake
  - untested
---

## Ingredients

*   1 small to medium orange
*   1 lemon
*   6 oz. raw almonds
*   1 c. all-purpose flour
*   1 tbsp. baking powder
*   4 eggs
*   1/2 tbsp. salt
*   1 1/2 c. sugar
*   2/3 c. olive oil
*   powdered sugar

## Preparation

1. Place the orange and the lemon in a saucepan, and cover with water.  Bring to a boil over medium-high heat; then reduce the heat and simmer for 30 minutes.  Drain and cool.
2. Preheat the oven to 325 degrees, and set a rack in the middle.  Bake the almonds 10 to 15 minutes.  Set aside to cool completely.  When the almonds are cool, pulse in a food processor until ground.
3. Set oven to 350 degrees, and grease a 9-inch springform pan.
4. When the citrus is cool, cut the lemon in half and discard the pulp and seeds.  Cut the orange in half and discard the seeds.  Put the fruits in the food processor and process almost to a paste.
5. In a small bowl, whisk the flour and baking powder.  Combine eggs and salt.  Beat until foamy.  Beat in the sugar.  Fold in the flour mixture.  Add the citrus, almonds, and olive oil, and beat on low speed until incorporated.  Pour the batter into the pan, and bake for about 1 hours.  Remove and dust with powdered sugar.

_New York Times Magazine, Adapted from Molly Wizenberg_
