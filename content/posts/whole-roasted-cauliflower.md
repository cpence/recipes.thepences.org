---
title: "Whole Roasted Cauliflower"
date: 2022-11-28T13:14:00+01:00
categories:
  - main course
  - side dish
tags:
  - cauliflower 
  - vegetarian
---

## Ingredients

- 1/4 cup extra-virgin olive oil
- 1 teaspoon dried thyme
- 1 teaspoon dried rubbed sage
- 1/2 teaspoon garlic powder
- 1/2 teaspoon fine salt
- 1/2 teaspoon freshly ground black pepper
- One large (2 1/2- to 3-pound) head cauliflower
- 4 tablespoons unsalted butter
- 1/4 cup sliced almonds

## Preparation

1. Position a rack in the middle of the oven and preheat to 400 degrees. In a small bowl, whisk together the oil, thyme, sage, garlic powder, salt and pepper to combine. Trim all the leaves from the base of the cauliflower. Trim the thick part of the stalk as much as needed so the cauliflower can sit flat.

2. Place the cauliflower bottom side up in a medium cast-iron skillet, Dutch oven or other similarly sized dish. Brush the bottom with some of the olive oil mixture. Flip the cauliflower right side up and brush with the remaining olive oil mixture until the cauliflower is evenly coated.

3. Cover tightly with aluminum foil or a lid and roast for 30 minutes. Uncover, baste with the oil from the pan, and continue roasting for 1 hour more, basting every 20 to 30 minutes, if desired, until a knife can be inserted into the cauliflower with little to no resistance and the cauliflower is nicely browned. Transfer to a platter for serving.

4. Place a medium, preferably stainless steel (so you can easily see the color of the butter) skillet over medium heat. Add the butter and cook, stirring and swirling the skillet regularly, until the butter turns a light golden brown, about 3 minutes. Add the almonds, reduce the heat to low, and continue to cook, stirring and swirling the skillet regularly, until the almonds are toasted and the butter is nicely browned, 2 to 3 minutes more. Pour the almond brown butter over the cauliflower, then carve it into thick wedges or slices, and serve warm.

_WaPo_
