---
title: "Dill Chicken"
date: 2022-09-04T19:10:57+02:00
categories:
  - main course
tags:
  - chicken
  - pence
---

## Ingredients

- one whole chicken
- 2 c. rice
- 3/8 c. (0.9 oz) dried dill
- 8 oz. container sour cream

## Preparation

Preheat oven to 350F.

Boil the chicken in salted water, to which you've added around 1/4 c. of dried dill. When done, remove the chicken, bone it, shred the meat, and reserve.

Cook the rice in 3 c. of the broth that remains, remembering to stir in all of the dill and chicken bits that sank to the bottom. (Reserve the remaining broth.) Put the rice in a buttered 13"x9" casserole dish. Add the chicken in a single layer.

Take the sour cream and thin it with a bit of the broth. Add the remaining 2 tbsp. of dried dill. Cover the chicken with the sour cream mixture

Bake until the top layer starts to get a few golden-brown spots.
