---
title: "Yogurt and Apricot Pie"
date: 2020-09-28T12:20:06+02:00
categories:
    - dessert
tags:
    - pie
    - apricot
---

## Ingredients

*   1 cup all-purpose flour
*   1/3 cup sugar
*   1/4 cup sliced almonds, crushed
*   1/4 cup rolled oats
*   Pinch of salt
*   2 tablespoons unsalted butter
*   1/4 cup canola oil
*   1 cup low-fat plain Greek yogurt
*   2 large eggs, lightly beaten
*   1/4 cup sugar
*   3 tablespoons fresh lemon juice
*   1 teaspoon pure vanilla extract
*   1/2 cup warmed apricot preserves

## Preparation

Preheat the oven to 350°. In a large bowl, combine the flour, sugar, almonds,
oats and salt. In a large skillet, melt the butter in the oil. Add the granola
mixture and cook over moderate heat, stirring constantly, until golden, 5
minutes; transfer to a 9-inch glass pie plate and let cool slightly.

Using a flat-bottomed glass, gently press the granola evenly over the bottom and
side of the pie plate to form a 1/2-inch-thick crust. Freeze the crust for about
10 minutes, until completely cooled.

Meanwhile, in a bowl, whisk the yogurt with the eggs, sugar, lemon juice and
vanilla; whisk until smooth. Pour the filling into the pie shell and bake for 25
minutes, until the filling is set but still slightly jiggly in the center. Let
stand at room temperature for 5 minutes. Pour the warm apricot preserves on top
of the yogurt and gently spread in an even layer. Refrigerate until chilled, at
least 2 hours. Using a warm knife, cut the pie into wedges and serve.

*Food & Wine*

