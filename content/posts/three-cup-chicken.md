---
title: "Three Cup Chicken"
author: "pencechp"
date: "2013-03-21"
categories:
  - main course
tags:
  - chicken
  - chinese
  - untested
---

## Ingredients

*   3 whole star anise
*   ¼ cup soy sauce
*   ¼ cup rice vinegar
*   ¼ cup sugar
*   2 tablespoons Shaoxing rice wine (or sherry)
*   ½ teaspoon vegetable oil
*   4 chicken legs, skin on
*   3 medium garlic cloves, smashed
*   6 slices fresh ginger
*   1 teaspoon Sichuan peppercorns, lightly toasted and ground
*   ½ cup chicken stock

## Preparation

Whisk together star anise, soy sauce, rice vinegar, sugar, and rice wine in a medium-sized bowl. Set aside.

Place a large wok over high heat. When it starts to smoke, pour in oil. Swirl oil around, and then add chicken legs skin side down. Adjust heat to medium-high and cook undisturbed until they are browned, 3 to 4 minutes. Flip the chicken legs, and add garlic and ginger. Continue cooking until the other side is browned, 3 to 4 minutes.

Add Sichuan peppercorns, soy sauce mixture, and chicken stock. Toss chicken pieces with sauce. Turn heat to high and bring to a boil. Then cover the work, reduce heat to low, and cook until juices run clear in the legs, about 10 minutes. Remove legs and set aside on a plate.

Turn heat to medium-high, and reduce sauce until it lightly coats the back of a spoon, stirring often. Serve chicken legs with sauce. Pair with white rice or broccoli.

_Nick Kindlesperger, Serious Eats_
