---
title: "Southern-Style Pork Vindaloo with Cardamom Cornbread and Green Beans"
author: "pencechp"
date: "2015-01-01"
categories:
  - main course
tags:
  - cornbread
  - curry
  - greenbean
  - pork
  - untested
---

## Ingredients for Pork

*   12 cloves garlic
*   1 tbsp. paprika
*   2 tsp. sugar
*   1 tsp. salt
*   1 tbsp. tomato paste
*   1/2 c. white vinegar
*   1/4 c. water
*   1/4 c. vegetable oil
*   1 medium red onion, chopped
*   2 lbs. pork tenderloin, cut into medium dice
*   1/4 tsp. red pepper flake

## Preparation

In a blender, combine garlic, paprika, sugar, salt, tomato paste, vinegar, and
water, and blend to a smooth paste. In a large skillet, heat oil over
medium-high heat. Add onions, and stir until light golden. Add meat and paste,
stir well to coat and reduce heat to medium. Stir in red pepper flakes. Cover
and cook for about 35 minutes, stirring occasionally. Uncover and check
consistency of sauce. If it seems thin, cook over high heat for three to five
minutes to thicken.

## Ingredients for Green Bean and Thyme Verakka

* 2 tbsp. coconut oil
* 1 tsp. mustard seeds
* 1 tsp. cumin seeds
* 1 lb. green beans, sliced into 1/4" thick coins
* 10 sprigs of thyme, chopped
* 1 tsp. salt

## Preparation

Heat oil in a pan over medium-high heat. Add mustard seeds. When they start to
pop, add cumin seeds. Stir in beans, thyme, and salt. Stir-fry for about four
minutes.

## Ingredients for Cardamom Cornbread

* 1/2 stick unsalted butter, plus more for pan preparation
* 1/2 c. sugar
* 1 c. buttermilk
* 2 eggs
* 1 c. all-purpose flour
* 1 1/4 tsp. baking powder
* 1 c. cornmeal
* 1 tsp. crushed cardamom seeds
* 1 tsp. coarsely ground black pepper
* 1/2 tsp. salt

## Preparation

Preheat oven to 375 degrees. Grease a 9" square pan or large cast-iron skillet
with butter. Melt butter in the pan or skillet, and stir in sugar. Add the
buttermilk and eggs, and stir to combine. Stir in flour, baking powder,
cornmeal, cardamom seeds, pepper, and salt. Bake for 30 minutes.

To serve, place a square of cornbread on a plate, spoon pork over the cornbread,
and top with a large spoonful of stir-fried green beans.

_Garden & Gun, December/January 2012-3_
