---
title: "Collard greens"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - side dish
tags:
  - greens
---

## Ingredients

*   1/2 pound smoked meat (ham hocks, smoked turkey wings, or smoked neck bones)
*   2 tsp salt
*   1/2 tsp black pepper
*   1/2 tsp garlic powder
*   1 tablespoon seasoned salt
*   1 tablespoon hot red pepper sauce
*   1 large bunch collard greens
*   1 tablespoon butter

## Preparation

In a large pot, bring 3 quarts of water to a boil and add smoked meat, house seasoning, seasoned salt and hot sauce. Reduce heat to medium and cook for 1 hour.

Wash the collard greens thoroughly. Remove the stems that run down the center by holding the leaf in your left hand and stripping the leaf down with your right hand. The tender young leaves in the heart of the collards don't need to be stripped. Stack 6 to 8 leaves on top of one another, roll up, and slice into 1/2 to 1-ince thick slices. Place greens in pot with meat and add butter. Cook for 45 to 60 minutes, stirring occasionally. When done taste and adjust seasoning.

_Paula Deen_
