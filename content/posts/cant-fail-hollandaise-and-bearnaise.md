---
title: "Can't Fail Hollandaise and Bearnaise"
author: "pencechp"
date: "2010-02-27"
categories:
  - miscellaneous
tags:
  - french
  - sauce
---

## Ingredients

*   3 egg yolks
*   2 tbsp. lemon juice, barely warmed
*   8 tbsp. butter, melted
*   1/4 tsp. salt
*   dash pepper

## Preparation

For hollandaise: Heat the butter to bubbling but not brown and set aside.  Put yolks, lemon, salt, and pepper in the blender, and whirl at high speed for 30 seconds.  Continue blending as you remove the blender lid and begin to pour in the hot butter, at first very slowly by droplets, then in a steady thin stream.  Serve immediately.

For bearnaise: Boil together 1/4 c. dry white wine, 1/4 c. white wine vinegar, 1/2 tsp. dried tarragon, and 1 tbsp. minced onion or scallion until reduced to 2 tbsp.   Let cool.  Use this 2 tbsp. to replace the lemon juice, and increase the butter to 10-12 tbsp.
