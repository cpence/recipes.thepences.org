---
title: "Sesame-Crusted Tuna with Ginger Cream"
author: "pencechp"
date: "2013-12-04"
categories:
  - main course
tags:
  - fish
  - japanese
  - untested
---

## Ingredients

*   1/4 cup vegetable oil
*   1/2 cup thinly sliced peeled ginger
*   1/2 small onion, finely chopped
*   2 cloves garlic, thinly sliced
*   1/4 cup rice vinegar
*   1/4 cup fresh orange juice
*   2 tablespoons mirin
*   2 tablespoons dry white wine
*   1 tablespoon Sriracha chile sauce
*   1 1/2 cups heavy cream
*   Salt and freshly ground pepper
*   6 6-ounce, 1-inch-thick tuna steaks
*   1/2 cup sesame seeds

## Preparation

1\. In a saucepan, heat 2 tablespoons of the oil. Add the ginger, onion and garlic and cook over moderate heat, stirring, until softened, 5 minutes. Add the vinegar, orange juice, mirin, wine and Sriracha and simmer until the liquid is almost evaporated, 10 minutes. Add the cream and simmer until reduced by half, 15 minutes. Strain the sauce, season with salt and pepper; keep warm.

2\. Season the tuna with salt and pepper and coat both sides with the sesame seeds. In a nonstick skillet, heat the remaining 2 tablespoons of oil. Add the tuna; cook over moderately high heat, turning once, until the sesame seeds are browned and the tuna is medium-rare, 5 minutes. Slice the tuna 1/3 inch thick and serve with the ginger cream.

_Hosea Rosenberg, HuffPo_
