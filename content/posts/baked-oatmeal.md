---
title: "Baked Oatmeal"
author: "pencechp"
date: "2015-07-31"
categories:
  - breakfast
tags:
---

## Ingredients

*   8 tablespoons of butter
*   1 1/2 cups of flour
*   2 cups of oats \[CP: original recipe called for quick-cooking, have used regular\]
*   1 cup of sugar
*   2 teaspoons of baking powder
*   1/4 teaspoon of salt
*   2 1/2 teaspoons of cinnamon
*   2 teaspoons of vanilla
*   2 cups of buttermilk
*   4 cups of fruit (stone fruit, berries, etc.)

## Preparation

Preheat oven to 350 degrees and place the butter in the bottom of a 9x13 pan. Place the pan in the oven for about 5 minutes or until the butter is melted. While the butter is melting combine the flour, oats, sugar, baking powder, salt, and cinnamon in a bowl. Once the dry ingredients are combined add in the buttermilk and vanilla and stir until combined. Dollop the batter all over butter in the bottom of the pan, do not mix together. Then nestle your fruit into the oatmeal trying to be as even as possible. Its a lot of fruit so it will get a little crowded but just look for an empty spot. Bake for 45 - 50 minutes or until the center of the dish is firm.

_Salt and Chocolate_
