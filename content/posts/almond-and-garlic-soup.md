---
title: "Almond and Garlic Soup"
author: "pencechp"
date: "2015-05-06"
categories:
  - main course
tags:
  - soup
  - spanish
  - untested
---

## Ingredients

*   6 slices white bread, crusts removed
*   4 cups water
*   2 1/2 cups (8 3/4 ounces) plus 1/3 cup sliced blanched almonds
*   1 garlic clove, peeled
*   3 tablespoons sherry vinegar
*   Kosher salt and pepper
*   Pinch cayenne pepper
*   1/2 cup extra-virgin olive oil, plus extra for drizzling
*   1/8 teaspoon almond extract
*   2 teaspoons vegetable oil
*   6 ounces seedless green grapes, sliced thin (1 cup)

## Preparation

Combine bread and water in bowl and let soak for 5 minutes. Process 2 1/2 cups almonds in blender until finely ground, about 30 seconds, scraping down sides of blender jar as needed.

Using your hands, remove bread from water, squeeze it lightly, and transfer to blender with almonds. Measure 3 cups soaking water and set aside; transfer remaining soaking water to blender.

Add garlic, vinegar, 1 1/4 teaspoons salt, and cayenne to blender and process until mixture has consistency of cake batter, 30 to 45 seconds. With blender running, add olive oil in thin, steady stream, about 30 seconds. Add reserved soaking water and process for 1 minute. Season with salt and pepper to taste. Strain soup through fine-mesh strainer set in bowl, pressing on solids to extract liquid. Discard solids.

Measure 1 tablespoon of soup into second bowl and stir in almond extract. Return 1 teaspoon of extract mixture to soup; discard remainder. Chill for at least 3 hours or up to 24 hours.

Heat vegetable oil in 8-inch skillet over medium-high heat until oil begins to shimmer. Add remaining 1/3 cup almonds and cook, stirring constantly, until golden brown, 3 to 4 minutes. Immediately transfer to bowl and stir in 1/4 teaspoon salt.

Ladle soup into shallow bowls. Mound an equal amount of grapes in center of each bowl. Sprinkle cooled almonds over soup and drizzle with extra olive oil. Serve immediately.

_CI_
