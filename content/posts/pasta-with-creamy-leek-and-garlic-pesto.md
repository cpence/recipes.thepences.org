---
title: "Pasta with Creamy Leek and Garlic Pesto"
author: "pencechp"
date: "2013-06-13"
categories:
  - main course
tags:
  - bacon
  - italian
  - leek
  - pasta
---

## Ingredients

*   3 tablespoons extra virgin olive oil, or as needed
*   4 ounces bacon, chopped
*   4 or 5 garlic cloves, thinly sliced
*   About 1 1/2 pounds leeks (2 or 3 large), trimmed, well rinsed, and chopped
*   Salt
*   1 egg
*   1 cup chopped fresh parsley leaves, plus more for garnish
*   Black pepper
*   8 ounces any pasta

## Preparation

Heat 1 tablespoon of the oil in a medium skillet over medium-high heat; add the bacon and cook, stirring occasionally, until just beginning to crisp, 8 to 10 minutes. Remove the bacon from the pan with a slotted spoon and drain it on paper towels.

Turn the heat to medium. Add the garlic and leeks to the skillet, along with another tablespoon or so of oil if needed to keep things from sticking, and cook, stirring occasionally, until very soft, 20 to 30 minutes.

Meanwhile, bring a large pot of water to a boil and salt it. Transfer the garlic and leeks to a blender or food processor with the egg, parsley and a sprinkling of salt and pepper. Process, stopping to scrape down the sides of the container if necessary. Return the purée to the skillet, off heat.

Cook the pasta in the boiling water until it’s tender but not mushy, then drain, reserving some of the cooking liquid. Turn the heat under the leek mixture to medium, add about 1/4 cup of the reserved cooking liquid to thin the pesto, and toss in the pasta, adding more liquid as needed. Taste and adjust the seasoning, garnish with the bacon and more parsley, and serve.

_Serves 4_

_The New York Times_
