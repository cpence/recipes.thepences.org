---
title: "Birria De Res"
date: 2022-03-04T10:26:18+01:00
categories:
  - main course
tags:
  - mexican
  - beef
  - stew
  - chile
  - untested
---

## Ingredients

- 2 poblano chiles
- 5 guajillo chiles, seeded, stemmed and halved lengthwise
- 5 pounds bone-in beef shoulder, cut into large pieces, or goat or lamb stew cuts on the bone
- 1 tablespoon fine sea salt
- ¼ cup neutral oil, such as canola or grapeseed
- 1 medium white onion, finely chopped
- 1 (28-ounce) can crushed tomatoes
- ¼ cup plus 2 tablespoons distilled white vinegar
- 6 garlic cloves, peeled
- 2 tablespoons finely grated fresh ginger
- 2 teaspoons dried Mexican oregano
- 2 teaspoons toasted white sesame seeds
- ½ teaspoon ground cumin
- 4 cloves
- Fresh black pepper
- 1 cinnamon stick
- 2 fresh or dried bay leaves
- ½ cup chopped fresh cilantro
- 2 limes, quartered
- Corn tortillas, warmed

## Preparation

Heat the oven to 325 degrees.

Prepare the chiles: Use tongs to place the poblano chiles directly over the open flame of a gas burner set to high. Cook the poblanos until totally charred all over, turning as needed, about 2 minutes per side. Transfer to a small bowl and cover with plastic wrap so the poblanos can steam. After 10 minutes, use your fingers to pull the blackened skins away from the poblanos, then remove the stems and seeds. Roughly chop the poblanos and set aside.

While the poblano chiles steam, place a large skillet over medium heat. Working in batches to cook the guajillo chiles evenly in one layer, flatten the chile halves on the hot skillet and toast them for about 15 seconds, turning once. Put the chiles in a bowl and add 2 cups hot water to help soften them. Set aside.

Prepare the meat: Season the meat all over with the salt. Heat the oil in a large, oven-proof pot over medium-high. Working in batches, sear the meat on all sides until well browned, 2 to 3 minutes per side, transferring the browned meat to a large bowl as you work.

After you’ve seared all the meat, add the onion to the pot and cook, stirring occasionally, until golden, about 5 minutes. Return all the meat to the pot.

Meanwhile, add the tomatoes, vinegar, garlic, ginger, oregano, sesame seeds, cumin, cloves and a few grinds of black pepper to a blender, along with the chopped poblanos, toasted guajillos and the chile soaking liquid. Purée until smooth, scraping down the edges of the blender as needed.

Pour the blended mixture into the pot with the meat. Add the cinnamon stick and bay leaves, along with about 4 to 6 cups of water, enough to amply cover the meat.

Cover and cook in the oven until the meat is fork-tender, about 2 hours.

Divide among bowls and sprinkle with cilantro. Serve with lime wedges for squeezing on top, and a side of warm tortillas.

_NYT_
