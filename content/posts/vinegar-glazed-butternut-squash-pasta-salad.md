---
title: "Vinegar-Glazed Butternut Squash Pasta Salad"
author: "pencechp"
date: "2017-10-30"
categories:
  - side dish
tags:
  - pasta
  - salad
  - squash
---

## Ingredients

*   One 2-pound butternut squash—peeled, seeded and cut into 3/4-inch pieces (6 cups)
*   1/4 cup plus 2 tablespoons red wine vinegar
*   2 tablespoons extra-virgin olive oil
*   1 tablespoon honey
*   Kosher salt
*   Pepper
*   3/4 pound whole-wheat fusilli
*   1/4 cup plus 1 tablespoon unrefined hazelnut oil
*   1 head of Treviso or 1/2 a small head of radicchio, thinly sliced
*   1/2 cup packed parsley leaves
*   Shaved Pecorino Tuscano or Romano, for serving

## Preparation

Preheat the oven to 425°. On a rimmed baking sheet, toss the squash with 1/4 cup of the vinegar, the olive oil and honey and spread evenly on the baking sheet. Season with salt and pepper. Roast for about 35 minutes, tossing halfway through, until browned and glazed.

Meanwhile, in a large pot of salted boiling water, cook the pasta until al dente. Drain. Toss with 3 tablespoons of the hazelnut oil.

In a large bowl, whisk the remaining 2 tablespoons each of vinegar and hazelnut oil. Stir in the squash, Treviso and parsley, then fold in the pasta. Season with salt and pepper. Serve warm or at room temperature with shaved pecorino.

_Food & Wine_
