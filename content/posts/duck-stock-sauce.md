---
title: "Duck Stock Sauce"
author: "pencechp"
date: "2013-02-10"
categories:
  - miscellaneous
tags:
  - game
  - sauce
---

## Ingredients

*   2 c. duck stock or 3 c. of lighter stock, such as goose, pheasant, or grouse
*   salt
*   pepper
*   1/2 tsp. dried tarragon
*   zest of one tangerine
*   dash Worcestershire sauce
*   2 tbsp. bourbon (optional)
*   corn starch
*   1 tbsp. butter

## Preparation

Heat the stock with the next six ingredients (salt and pepper to taste, depending on stock).  Reduce volume to around 1 cup.  Thicken with corn starch, adding butter to finish.

_Dad_
