---
title: "Garden Rice Salad"
author: "pencechp"
date: "2015-06-13"
categories:
  - side dish
tags:
  - rice
  - salad
  - untested
---

## Ingredients

*   16 oz. long grain/wild rice mix
*   1/2 c. mayonnaise
*   1/4 c. yogurt
*   1 c. chopped celery (around 2 stalks)
*   1 c. chopped tomatoes (around 1 lg.)
*   1/2 c. diced cucumber
*   2 tbsp. fresh parsley, chopped
*   1/8-1/2 tsp. salt (or seasoned salt)
*   1/8-1/2 tsp. pepper
*   1/4 c. peanuts, chopped, optional

## Preparation

Cook the rice according to package directions, omitting any butter or oil. Let cool. Put in a bowl with all the remaining ingredients but the peanuts. Stir to combine. Can serve right away, or cover and serve cold. Garnish with the peanuts right before serving.

_Penzey's Spices_
