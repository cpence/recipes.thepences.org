---
title: "Pickled Radishes with Chiles and Ginger"
author: "pencechp"
date: "2012-07-21"
categories:
  - side dish
tags:
  - canning
  - pickle
  - radish
  - untested
---

## Ingredients

*   2 bunches of radishes
*   1 cup plain white vinegar
*   1/2 cup sugar
*   2 tbsp. kosher salt
*   3 cups water
*   2 fresh Thai chiles, split lengthwise
*   4 garlic cloves
*   1 small piece of ginger, peeled and cut into thin rounds

## Preparation

Wash and trim the radishes, leaving about half an inch of stem.  Cut the radishes in half lengthwise and put them in a 2-quart jar with a lid.  In a small nonreactive saucepan, combine the vinegar, sugar, and salt with water and bring to a boil.  Cook for a minute to dissolve the sugar and salt completely.  Remove from heat and let cool until warm.  Add the chiles, garlic, and ginger to the jar and pour enough of the warm liquid over the radishes to completely cover them.  Let cool, then cover and refrigerate for at least twenty-four hours and up to several weeks.

_Garden and Gun, August/September 2012_
