---
title: "Michael's Best Button Mushrooms"
author: "juliaphilip"
date: "2012-05-09"
categories:
  - side dish
tags:
  - mushroom
  - vegetarian
---

## Ingredients

*   6 tablespoons extra-virgin olive oil
*   1 1/2 pounds whole small button mushrooms, wiped clean
*   3 tablespoons butter
*   Gray sea salt
*   1 tablespoon minced garlic
*   1 1/2 teaspoons fresh thyme leaves, chopped
*   2 tablespoons lemon juice
*   1/2 cup white wine
*   1 tablespoon chopped parsley leaves

## Preparation

In a large skillet, heat the oil over high heat. Add the mushrooms. Do not move the mushrooms until they have caramelized on the bottom. If you toss them too soon, they will release their liquid and begin to steam. When the bottoms are caramelized, toss them and continue to cook for about 5 minutes.

Add the butter. Cook and toss for 5 minutes, until beautifully browned.

Season with salt and add the garlic. Saute another 2 minutes, and add the thyme, lemon juice, and white wine. Cook to evaporate the liquid.

Toss in the parsley and serve immediately.

_Michael Chiarello, Food Network_
