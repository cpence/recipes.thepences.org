---
title: "Kaiserschmarrn"
date: 2020-09-28T12:28:33+02:00
categories:
    - dessert
    - breakfast
tags:
    - pancake
    - german
    - austrian
    - untested
---

## Ingredients

*   3 eggs
*   100 g flour
*   1 teaspoon sugar
*   1/2 teaspoon vanilla extract
*   1/2 teaspoon salt
*   1/8 L milk
*   100 g butter
*   raisins

## Preparation

Mix yolk, flour, sugar, vanilla extract, salt and milk to a thin dough; whisk
the egg-white until it is stiff and mix it carefully with the dough. Melt the
butter in a pan and add the dough. Sprinkle some raisins over it and fry it on
low heat. Flip the omelette over, tear it in little pieces and fry through.
Serve with powdered sugar and apple sauce or "Zwetschkenröster" (plum sauce).

(Serves 1 as a main-course or 2 as a dessert.)

*Visit Salzburg*

