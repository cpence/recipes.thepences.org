---
title: "Pescado a la Veracruzana"
author: "pencechp"
date: "2010-04-02"
categories:
  - main course
tags:
  - chile
  - fish
  - mexican
  - scallops
---

## Ingredients

*   1 tbsp. olive oil
*   4 cloves garlic, chopped
*   ½ c. onion, finely chopped
*   2 lb. tomatoes, peeled and finely chopped
*   1 green pepper, julienned
*   1 poblano pepper, julienned or 3 jalapeño peppers, julienned
*   1 tsp. salt
*   ½ tsp. ground black pepper
*   2 bay leaves
*   1 tsp. dried oregano
*   ½ c. green olives, chopped \[optional\]
*   ¼ c. capers
*   6 fillets red snapper or other firm fish or 16 scallops
*   2 tbsp. butter

## Preparation

If using jalapeño peppers, heat some oil in a small skillet and sautee until the peppers just begin to shrivel.  Set aside.

Heat the oil in a skillet and add the garlic and onions; sautee for three minutes.  Add the tomatoes and bring to a boil.  Add the green pepper and poblano (or reserved sauteed jalapeños) and stir, cook for two minutes; add the salt, the pepper, the bay leaves, and the oregano.

When the mixture returns to a boil, cover and cook over a low fire for eight minutes.  Add the olives and capers and cook for five more minutes.  Check the seasoning and remove from the fire.

Twenty minutes before serving, preheat the oven to 375 F.  Wash the fillets (or scallops) and dry with a paper towel; then, season with salt and pepper.

Melt the butter in a skillet and brown the fillets on both sides.  Place them in a oven-proof dish and cover with the tomato sauce. Cover with aluminum foil and bake for 10 to 15 minutes.

_Translated from_ [_http://www.elgranchef.com/2007/10/20/pescado-a-la-veracruzana/_](http://www.elgranchef.com/2007/10/20/pescado-a-la-veracruzana/)
