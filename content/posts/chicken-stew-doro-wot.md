---
title: "Chicken Stew (Doro Wot)"
author: "pencechp"
date: "2012-12-02"
categories:
  - main course
tags:
  - chicken
  - ethiopian
---

## Ingredients

*   2 lbs. chicken (2 drumsticks, 2 thighs, 2 wings, bone-in recommended)
*   3 slices lemon or lime
*   1 tsp. salt (to taste)
*   4 lg. red onions, chopped
*   1/2 c. olive oil
*   1-2 tbsp. cayenne pepper \[CP: 2 tbsp. is around an 8 out of 10 spice-wise; next time will try 1 tbsp.\]
*   1/2-1 c. water
*   3 tbsp. [qibe]( {{< relref "qibe-spiced-butter" >}} ) or butter
*   1/4 c. minced fresh garlic
*   1 tbsp. whole black cardamom pods, or 1 tsp. ground cardamom
*   1/4 c. minced fresh ginger
*   3 tsp. ground nutmeg
*   3 tsp. ground allspice
*   3 tsp. black pepper
*   2 tsp. salt, to taste
*   4 hard-boiled eggs, halved \[CP: we skipped these\]

## Preparation

Wash the chicken thoroughly.  Put the chicken in a large bowl and add enough water to cover.  Add the lemon or lime slices and the salt and let marinate for at least 2 hours.

In a large pot, cook the onions over medium heat until browned (around 6-8 minutes). Add the olive oil and stir in the cayenne. Cook over medium heat for 30 minutes, adding 2 tbsp. of water or so gradually, only to keep it from burning. Drain your marinated chicken and add it to the pot. Cook for 20-30 minutes. Add the qibe/butter, garlic, cardamom, and ginger. Let it cook for about 20 minutes, gradually adding very little amounts of water (2 tbsp. or so at a time). Lower the heat and add the nutmeg, allspice, salt and pepper. Continue to cook for another 15 minutes.  Remove the cardamom pods if you used whole, and discard. Add the hard-boiled eggs and let it rest, covered, for about an hour. Serve warm, reheating gently if needed.

_Penzey's Spices, edited_
