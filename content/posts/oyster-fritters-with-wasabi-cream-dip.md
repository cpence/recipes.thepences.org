---
title: "Oyster Fritters with Wasabi Cream Dip"
author: "pencechp"
date: "2015-06-02"
categories:
  - main course
tags:
  - oyster
  - untested
---

## Ingredients

*   24 fresh oysters
*   1/2 c. corn flour
*   2/3 c. plain flour
*   Enough ice-cold sparkling water to make a sticky batter to coat the oysters

*For the wasabi cream dip:*

*   1/2 cup whipping cream
*   2 tsp wasabi paste

## Preparation

In a bowl, pour whipping cream, add the wasabi and whisk till stiff.

Open the oysters and drain the water.  With a sharp knife, cut of the oyster off. In a medium pan, heat 3" of oil until approx. 350F. Place each oyster in a teaspoon, dip in the batter until coated all over.  Fry in oil until golden.  The fritters will puff up instantly.  Fry oysters by batches (5 by batch).  Drain on kitchen towel before serving.

Drizzle oyster fritters with coarse salt and black pepper.  Serve immediately with wasabi cream dip.

_Manger_
