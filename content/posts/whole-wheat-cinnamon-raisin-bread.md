---
title: "Whole Wheat Cinnamon Raisin Bread"
author: "juliaphilip"
date: "2010-08-07"
categories:
  - bread
  - breakfast
tags:
---

## Ingredients

*   1 1/8 cup water
*   2 Tablespoons butter, cut into small pieces
*   2 Tablespoons Evaporated Cane Juice (you may substitute white sugar if you
    wish)
*   1 teaspoon salt
*   360 g whole wheat flour
*   1 Tablespoon cinnamon
*   2 1/2 teaspoons yeast
*   3/4 cup raisins

## Preparation

Place ingredients in the bread machine in the order listed, except for the
raisins. Run on the whole wheat bread cycle, adding the raisins as mix-ins.

## Variants

For dried fruit/nut bread, add 1/2 c. chopped dried fruit and 1/2 c. chopped
nuts as mix-ins, replacing the rasins.

For lemon poppyseed bread, add 3 tbsp. lemon rind, 1 tsp. lemon extract, 1
sm. container poppy seeds, omit raisins and cinnamon.

For blueberry-lemon bread, replace raisins with dried blueberries, replace
cinnamon with lemon rind, add 1 tsp. lemon extract.
