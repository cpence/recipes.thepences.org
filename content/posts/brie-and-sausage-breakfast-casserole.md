---
title: "Brie and Sausage Breakfast Casserole"
author: "pencechp"
date: "2010-05-09"
categories:
  - breakfast
tags:
  - cheese
  - sausage
---

## Ingredients

*   1 (8 oz.) round brie (2 cups grated swiss can be substituted)
*   1 lb. ground hot pork sausage
*   6 white sandwich bread slices
*   1 cup grated parmesan
*   7 large eggs, divided
*   3 cups whipping cream, divided
*   2 cups milk
*   1 TBSP fresh sage, or 1 tsp. dried sage
*   1 tsp. salt
*   1 tsp. dry mustard
*   Garnish: chopped green onions, shaved parmesan

## Preparation

Trim rind from brie and cut cheese into cubes.

Cook sausage until crumbled and done. Drain well.

Cut crusts from bread and place crusts evenly in the bottom of a greased 9x13 pan. Layer evenly with bread slices, sausage, brie and parmesan.

Whisk together 5 eggs, 2 cups whipping cream, and the next 4 ingredients; pour evenly over casserole. Cover and chill overnight.

Whisk together remaining 2 eggs and 1 cup whipping cream. Pour evenly over chilled casserole.

Bake at 350 for 1 hour or until set. Garnish, if desired.
