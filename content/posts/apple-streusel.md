---
title: "Apple Streusel"
author: "pencechp"
date: "2010-05-08"
categories:
  - dessert
tags:
  - apple
  - untested
---

## Ingredients

*   3 good-sized fresh fall apples—Cortland or McIntosh are both great
*   1/2 Cup sugar
*   1 tsp. cinnamon
*   1/4 tsp. vanilla extract

*Topping:*

*   1/2 stick cold butter (4 TB.)
*   1/2 Cup flour
*   1/3 Cup sugar
*   1/2 tsp. cinnamon

## Preparation

Preheat oven to 350°. Grease the bottom only of a small (4 cups or so) casserole dish. Peel and core the apples, slice and place in a bowl. Top with sugar, cinnamon and vanilla, toss to coat. Set aside while preparing topping. Make the streusel by cutting the cold butter into a bowl, adding flour, sugar and cinnamon, and rubbing it through your fingers until the streusel is a coarse, sandy/pebbly texture. Put the seasoned apples into the pan, top generously with the streusel, and bake at 350° about 30 minutes, until the streusel is golden brown and the apple is starting to bubble up through the topping.

_Penzey's Spices_
