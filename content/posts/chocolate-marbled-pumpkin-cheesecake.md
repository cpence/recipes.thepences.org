---
title: "Chocolate Marbled Pumpkin Cheesecake"
author: "juliaphilip"
date: "2010-11-18"
categories:
  - dessert
tags:
  - chocolate
  - pumpkin
  - untested
---

## Ingredients

*   1 box (9 ounces) thin chocolate wafers (such as Nabisco Famous chocolate wafers)
*   3/4 stick (6 tablespoons) unsalted butter, melted
*   6 ounces semisweet chocolate, coarsely chopped (do not use chocolate chips)
*   3 packages (8 ounces each) cream cheese, softened
*   1 cup granulated sugar
*   4 large eggs
*   1 can (15 ounces) solid-pack pumpkin
*   1 teaspoon ground cinnamon
*   1 teaspoon pure chocolate extract or vanilla
*   Cocoa powder
*   Confectioners' sugar

## Preparation

Heat oven to 350 degrees. Pulverize chocolate wafers in a food processor. Add melted butter; process to mix. (Alternatively, place wafers in zippered plastic bag; finely crush with a rolling pin. Transfer to a bowl; stir in melted butter.) Pat crumb mixture evenly over the bottom of a generously buttered 10-inch springform pan. Bake until set, 8 minutes. Cool on wire rack.

Reduce oven temperature to 300 degrees. For filling, melt chocolate in small bowl in microwave oven on medium (50 percent power), 50 to 60 seconds. Stir until smooth; cool slightly.

Beat cream cheese in large bowl of electric mixer (or food processor) until light. Beat in granulated sugar until smooth. Beat in eggs, one at a time, mixing well after each addition. Transfer 1 cup of this batter to the melted chocolate; mix until smooth. Beat pumpkin into remaining batter in mixer bowl (or food processor). Add cinnamon and chocolate extract; mix well.

Pour pumpkin batter over cooled crust in springform pan. Dollop chocolate batter over pumpkin batter. Swirl chocolate into pumpkin with a small knife making a marbled effect. Bake until set yet still wiggly in center, 50 minutes. Turn off oven, open door slightly and let cool, 1 hour. Cool completely on wire rack. Refrigerate covered up to 3 days. Serve dusted with cocoa powder and confectioners' sugar.
