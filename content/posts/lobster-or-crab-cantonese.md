---
title: "Lobster (or Crab) Cantonese"
author: "juliaphilip"
date: "2010-06-05"
categories:
  - main course
tags:
  - chinese
  - crab
  - lobster
  - untested
---

## Ingredients

*   3 tbl vegetable oil
*   2 cloves garlic, crushed
*   1/2 lb pork, finely minced
*   1 1/2 tbl light soy sauce
*   2 c chicken broth
*   1 tsp honey or granulated sugar
*   1/4 tsp white pepper (optional)
*   1/4 tsp powdered ginger (optional)
*   1 lb lobster meat--meat from 2 1-lb lobsters or 1 1/2 lbs lobster tail, cleaned, shelled, and cut into 2 inch pieces (may substitute equal parts crab)
*   2 tbl cornstarch
*   2 tbl cold water
*   2 green onions, washed and chopped (green part only)
*   1 egg, slightly beaten

## Preparation

Heat the oil in a large wok or skillet over high heat. Add the garlic and pork and stir-fry until the pork loses its pink color. Stir in the soy sauce, chicken broth, honey, pepper, and ginger; bring to a boil. Add the raw lobster pieces, cover, reduce heat and cook for 10 min. Mix the cornstarch and cold water into a smooth paste. Add to the lobster and stir until the gravy is thickened. Stir in the chopped green onions. Pour the beaten egg over the lobster and stir until the egg is just set. Do not over cook! Remove and arrange on a platter. Serve with rice and veggies.

_Penzey's, Summer 2010_
