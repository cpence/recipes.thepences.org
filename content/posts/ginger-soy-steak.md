---
title: "Ginger-Soy Steak"
date: 2021-12-23T01:39:26+01:00
categories:
  - main course
tags:
  - untested
  - steak
---

## Ingredients

- 1/2 c. soy sauce
- 2 tbsp. grated ginger
- 2 tbsp. sugar, divided
- 1 tbsp. toasted sesame oil
- 4 garlic cloves, pressed
- salt and pepper
- 1 1/2 lb. sirloin tips, cut into 2-inch pieces
- 6 tbsp. unseasoned rice vinegar
- 1 cucumber, quartered lengthwise, thinly sliced
- 1 firm pear, quartered, cored and thinly sliced crosswise
- 1 tbsp. toasted sesame seeds

## Preparation

In a medium bowl, stir together the soy sauce, ginger, 1 tablespoon of the
sugar, the sesame oil, garlic and 1 teaspoon pepper until the sugar dissolves.
Add the beef and stir to coat. Cover and let sit at room temperature for 15
minutes.

Meanwhile, in a separate medium bowl, combine the vinegar, the remaining 1
tablespoon of sugar and 1 teaspoon salt. Stir in the cucumber and pear. Cover
and refrigerate until ready to serve.

Heat the broiler with an oven rack about 4 inches from the heat. Remove the beef
from the marinade and pat dry with paper towels. Place the pieces on a wire rack
set in a rimmed baking sheet. Cook until 125° at the center for medium-rare, 7
to 10 minutes, flipping the pieces halfway through. Transfer to a plate and let
rest for 10 minutes.

Drain the cucumber mixture and transfer to a large platter, arranging it on one
side. Sprinkle with the sesame seeds. Arrange the steak opposite the salad.

_Oprah_
