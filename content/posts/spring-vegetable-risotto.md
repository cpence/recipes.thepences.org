---
title: "Spring Vegetable Risotto"
author: "pencechp"
date: "2016-04-28"
categories:
  - main course
tags:
  - asparagus
  - leek
  - peas
  - rice
---

## Ingredients

_For the gremolata:_

*   2 tbsp minced parsley, stems reserved
*   2 tbsp minced mint, stems reserved
*   ½ tsp. lemon zest

_For the risotto:_

*   1 lb. asparagus, reserving ends, cut into 1/2" pieces
*   2 leeks, white and light green parts sliced thin, green parts reserved
*   4 c. chicken broth
*   3 c. water
*   5 tbsp. butter
*   Salt and pepper
*   ½ c. peas
*   2 garlic cloves, pressed
*   1½ c. risotto rice
*   1 c. white wine
*   1½ oz. grated Parmesan
*   2 tsp. lemon juice

## Preparation

1\. For the Gremolata: Combine ingredients in small bowl and set aside.

2\. For the Risotto: Chop tough asparagus ends and leek greens into rough 1/2-inch pieces. Bring chopped vegetables, reserved parsley and mint stems, broth, and water to boil in large saucepan over high heat. Reduce heat to medium-low, partially cover, and simmer 20 minutes. Strain broth through fine-mesh strainer into medium bowl, pressing on solids to extract as much liquid as possible. Return strained broth to saucepan; cover and set over low heat to keep broth warm.

3\. . Heat 1 tablespoon butter in large Dutch oven over medium heat. When foaming subsides, add asparagus spears, pinch of salt, and pepper to taste. Cook, stirring occasionally, until asparagus is crisp-tender, 4 to 6 minutes. Add peas and continue to cook 1 minute. Transfer vegetables to plate and set aside.

4\. Melt 3 tablespoons butter in now-empty Dutch oven over medium heat. When foaming subsides, add leeks, garlic, 1/2 teaspoon salt, and 1/2 teaspoon pepper. Cook, stirring occasionally, until leeks are softened, 4 to 5 minutes. Add rice and cook, stirring frequently, until grains are translucent around edges, about 3 minutes. Add wine and cook, stirring frequently, until fully absorbed, 2 to 3 minutes.

5\. When wine is fully absorbed, add 3 cups hot broth to rice. Simmer, stirring every 3 to 4 minutes, until liquid is absorbed and bottom of pan is almost dry, about 12 minutes.

6\. Stir in about 1/2 cup hot broth and cook, stirring constantly, until absorbed, about 3 minutes; repeat with additional broth 3 or 4 times until rice is al dente. Off heat, stir in remaining tablespoon butter, Parmesan, and lemon juice; gently fold in asparagus and peas. If desired, add up to 1/4 cup additional hot broth to loosen texture of risotto. Serve immediately, sprinkling each serving with gremolata and passing Parmesan separately.

_CI_
