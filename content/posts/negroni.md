---
title: "Negroni"
author: "pencechp"
date: "2014-06-10"
categories:
  - cocktails
tags:
  - campari
  - gin
---

## Ingredients

*   1 ounce gin
*   1 ounce Campari (Gran Glassico Bitter can also be used)
*   1 ounce sweet vermouth, such as Carpano
*   Orange twist, for garnish

## Preparation

Place a cocktail or Old Fashioned glass in the freezer to chill.

Combine the gin, Campari, and sweet vermouth in a cocktail shaker and fill it halfway with ice. Stir until the outside of the shaker is frosted. If you’re using an Old Fashioned glass, add ice cubes to it. Strain the drink into the chilled cocktail or Old Fashioned glass. Garnish with the orange twist.

_Chow_
