---
title: "English Bloodhound"
author: "pencechp"
date: "2014-06-10"
categories:
  - cocktails
tags:
  - campari
  - gin
---

## Ingredients

*   1 ounce gin
*   1/2 ounce Campari
*   1 1/2 ounces pink grapefruit juice

## Preparation

In a shaker or large glass, combine ingredients and stir well to combine. Strain into a cocktail coupe or over ice in a cocktail glass. Garnish with a bit of peel or a cherry or nothing at all.

_Running with Tweezers_
