---
title: "Toronto"
author: "pencechp"
date: "2013-03-13"
categories:
  - cocktails
tags:
  - fernet
  - whiskey
---

## Ingredients

*   2 oz rye
*   1/2 oz. Fernet Branca
*   1/2 oz. simple syrup
*   1 dash Angostura bitters
*   1 twist lemon peel

## Preparation

Stir and strain down into a double-old fashioned glass. Express the lemon oil and wipe the peel along the glass rim before you drop it in.

_Kindred Cocktails_
