---
title: "Penne al Forno con Melanzane"
author: "juliaphilip"
date: "2009-12-06"
categories:
  - main course
tags:
  - eggplant
  - italian
  - pasta
  - untested
  - vegetarian
---

## Ingredients

*   2 pounds eggplant, sliced 1/2-inch thick
*   Sea salt
*   3 tablespoons extra-virgin olive oil, plus 1 tablespoon, plus 2 tablespoons
*   1 pound penne or other short pasta
*   2 cups basic tomato sauce (from Mario's cookbook)
*   1/2 cup fresh bread crumbs
*   Salt and pepper
*   1/3 cup grated caciocavallo or pecorino
*   4 sprigs fresh basil, roughly torn

## Preparation

Stack eggplant slices in a colander, each piece sprinkled with sea salt. Weigh the slices with a large, full can of tomatoes or other weight and set aside in the kitchen sink so that juices run free. After 1 hour, rinse and pat dry.

Bring 6 quarts of water to a boil and add 2 tablespoons salt.

In a 12 to 14-inch saute pan, heat 3 tablespoons olive oil over medium-high heat and saute eggplant slices until golden brown on both sides, working in batches if necessary and removing cooked eggplant to a plate lined with a paper towel.

Cook the penne in the boiling water until not quite done; about 5 minutes. Drain the pasta and toss with 3/4 cup of tomato sauce.

Preheat oven to 375 degrees F.

Grease a glass or ceramic oven dish with 1 tablespoon of olive oil. Cover the bottom of the dish with a few tablespoons of tomato sauce. Top with half the bread crumbs, and salt and pepper, to taste. Add half the cooked pasta, then half of the eggplant slices, arranged in a layer. Drizzle about 1/4 cup of the tomato sauce over, and top with half the grated cheese and torn basil leaves. Repeat the process with remaining tomato sauce, bread crumbs, pasta, eggplant, cheese, and basil.

Drizzle with remaining 2 tablespoons olive oil and bake in oven for 1 hour. Let rest 30 minutes before serving.

_Mario Batali_
