---
title: "Chargrilled Oysters"
author: "pencechp"
date: "2016-04-28"
categories:
  - main course
  - side dish
tags:
  - oyster
  - untested
---

## Ingredients

*   24 oysters
*   1 pound (4 sticks) unsalted butter
*   2 tablespoons minced garlic
*   1 teaspoon Worcestershire sauce
*   1 teaspoon fresh lemon juice
*   1 teaspoon finely chopped parsley
*   1/4 teaspoon Cajun seasoning
*   1 ½ cups grated Parmesan cheese

## Preparation

Melt butter in a medium saucepan over medium heat. Turn off heat and add remaining ingredients (except Parmesan). Stir to combine and set aside. Shuck oysters and place on a hot grill until the oysters start to ruffle around the edges. Pour 1 tablespoon of butter sauce onto each oyster and cook for 1 minute. Scoop 1 tablespoon of Parmesan on top of each oyster and cook until melted and bubbling.

_Casamento's, via Thrillist_
