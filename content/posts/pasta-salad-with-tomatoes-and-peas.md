---
title: "Pasta Salad with Tomatoes and Peas"
author: "pencechp"
date: "2013-07-14"
categories:
  - main course
  - side dish
tags:
  - pasta
  - peas
  - salad
  - tomato
  - vegan
  - vegetarian
---

## Ingredients

*   1/3 cup white-wine vinegar
*   2 tablespoons water
*   2 teaspoons salt
*   1/2 teaspoon sugar
*   2 teaspoons minced fresh tarragon leaves or 1/2 teaspoon dried, crumbled
*   1 large garlic clove, minced and mashed to a paste with 1/4 teaspoon salt
*   1/2 cup olive oil
*   1 pound medium pasta shells
*   1/2 pound shelled fresh or frozen peas (about 1 1/2 cups), boiled until tender and drained
*   2 pints red or yellow pear tomatoes or cherry tomatoes or a combination, halved
*   1/2 cup shredded fresh basil leaves

## Preparation

In a large bowl whisk together the vinegar, the water, the salt, the sugar, the tarragon, the garlic paste, and pepper to taste, add the oil in a stream, whisking, and whisk the dressing until it is emulsified. In a kettle of salted boiling water cook the pasta until it is tender, in a colander rinse it well, and drain it. In the bowl toss the pasta with the dressing, add the peas, the tomatoes, and the basil, and toss the salad well.

_Gourmet, June 1993_
