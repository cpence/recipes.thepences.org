---
title: "Fascinator"
author: "juliaphilip"
date: "2013-05-17"
categories:
  - cocktails
tags:
  - gin
---

## Ingredients

*   2 dashes absinthe
*   1 ounce dry vermouth
*   2 ounces gin
*   1 mint leaf

## Preparation

Shake liquid ingredients well with ice and strain into a chilled coupe. Garnish with mint leaf.

_Rosie Schapp, New York Times_
