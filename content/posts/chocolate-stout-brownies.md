---
title: "Chocolate-Stout Brownies"
author: "pencechp"
date: "2014-11-19"
categories:
  - dessert
tags:
  - beer
  - brownie
  - chocolate
---

## Ingredients

*   1 cup stout (such as Guinness)
*   16 ounces semisweet or bittersweet chocolate, chopped, divided
*   1 cup plus 2 tablespoons (2 1/4 sticks) unsalted butter
*   1 1/2 cups sugar
*   3 large eggs
*   1 teaspoon vanilla extract
*   3/4 cup all-purpose flour
*   1 1/2 teaspoons kosher salt, divided

## Preparation

Preheat oven to 350°. Line a 9x9x2" metal baking pan with foil, leaving a 2" overhang. Bring stout to a boil in a medium sauce- pan; cook until reduced to 1/2 cup, about 12 minutes. Let cool. Reserve 1/4 cup stout. Stir 12 oz. chocolate and 1 cup butter in a medium metal bowl set over a saucepan of simmering water until melted and smooth. Whisk sugar, eggs, and vanilla in a large bowl to blend. Gradually whisk in chocolate mixture, then 1/4 cup stout from pan. Fold in flour and 1 1/4 tsp. salt. Pour batter into prepared pan. Bake brownies until surface begins to crack and a tester inserted into center comes out with a few moist crumbs attached, 35–40 minutes. Transfer pan to a wire rack and let cool for at least 20 minutes. Stir remaining 4 oz. chocolate in a medium metal bowl set over a sauce-pan of simmering water until melted and smooth. Add reserved 1/4 cup reduced stout, remaining 2 Tbsp. butter, and 1/4 tsp. salt; whisk until well blended. Pour warm glaze over brownies. Let stand at room temperature until glaze is set, about 40 minutes. DO AHEAD: Can be made 8 hours ahead. Cover and let stand at room temperature. Using foil overhang, lift brownie from pan; cut into squares.

_Bon Appetit_
