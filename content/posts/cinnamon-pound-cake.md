---
title: "Cinnamon Pound Cake"
author: "pencechp"
date: "2010-05-08"
categories:
  - breakfast
  - dessert
tags:
  - cake
  - cinnamon
  - untested
---

## Ingredients

*   1 cup butter, softened
*   2 cups sugar
*   4 eggs
*   1/2 tsp vanilla extract
*   3 cups flour
*   1 tbsp baking powder
*   1 cup milk
*   5 tbsp cinnamon sugar (or 1 tbsp cinnamon mixed with 4 tbsp sugar)

## Preparation

Preheat oven to 300° F.  Grease a tube pan or loaf pan.  Cream the butter.  Add the eggs, sugar, and vanilla extract. Mix well.  In a separate bowl, combine the flour and baking powder.  Add half the flour to the butter mixture. Mix to combine.  Add the milk to the butter mixture. Mix to combine.  Add the remaining flour to the butter mixture. Mix to combine.  Sprinkle 1/3 of the cinnamon sugar on the bottom of the loaf pan.  Add half of the batter to the loaf pan.  Sprinkle 1/3 of the cinnamon sugar on top of the batter in the loaf pan.  Add the remaining batter to the loaf pan.  Sprinkle the remaining cinnamon sugar on the top of the batter.  Bake for 1 hour and 15 minutes.

_Penzey's Spices_
