---
title: "Split Pea Soup"
author: "pencechp"
date: "2016-04-28"
categories:
  - main course
tags:
  - peas
  - soup
---

## Ingredients

- 2 meaty ham hocks, or 1/2 lb meaty salt pork (rinsed)
- 16 cups water
- 2 lb onions, finely chopped (divided)
- 2 large carrots
- 1 medium leek, white and green parts only, chopped and rinsed
- 2 celery ribs, chopped
- 2 tbsp. butter
- 1 lb dried split peas, picked over and rinsed
- 1 teaspoon table salt
- 1/4 teaspoon black pepper
- 1 tsp. chopped fresh chives
- 1/2 tsp. dried savory, crumbled

## Preparation

1. Combine ham hocks, 16 cups water, and half of the onions in a large soup pot,
   uncovered, until meat is tender, 1 1/2 to 2 hours. Transfer ham hocks to a
   cutting board and measure broth: If it measures more than 12 cups, continue
   boiling until reduced; if less, add enough water to total 12 cups.
2. When hocks are cool enough to handle, discard skin and cut meat into 1/4-inch
   pieces (reserve bones).
3. Chop carrots, the remaining onions, leek, and celery in 2 tbsp. of butter in
   a heavy skillet over medium heat, stirring until softened, about 10 minutes.
4. Add the vegetables to the soup, along with salt, pepper, chives, and savory,
   and continue to simmer, partially covered and stirring occasionally, until
   peas are falling apart and soup is slightly thickened, about 1 to 1 1/2
   hours.
5. Add ham meat back to soup, heat through, and serve, adjusting seasoning if
   needed.

_Gourmet, combined from two recipes_
