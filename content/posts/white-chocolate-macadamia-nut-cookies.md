---
title: "White Chocolate Macadamia Nut Cookies"
author: "pencechp"
date: "2010-05-29"
categories:
  - dessert
tags:
  - cookie
  - macadamia
---

## Ingredients

*   2 cups all-purpose flour
*   ½ teaspoon baking soda
*   ½ teaspoon salt
*   ¾ cup butter, melted and cooled
*   1 cup packed light brown sugar
*   ½ cup white sugar
*   1 egg
*   1 egg yolk
*   1 tablespoon vanilla extract
*   1½ cups chopped macadamia nuts
*   1½ cups white chocolate chips
*   coarse sea salt (for topping)

## Preparation

1\. Preheat the oven to 325 degrees F. Grease baking sheets or line with parchment paper or a silicone baking mat.

2\. In a medium bowl, whisk together the flour, baking soda and salt and set aside.

3\. Beat together the melted butter, brown sugar, and white sugar. Beat in the egg, then beat in the egg yolk, then beat in the vanilla.

4\. Gradually add the flour mixture to the sugar mixture until just moistened. Stir in the macadamia nuts and white chocolate chips by hand with a rubber spatula.

5\. Roll into balls or drop by heaping tablespoons onto baking sheets, and press down on the cookies with your palm a bit to flatten (not too much!).  Sprinkle tops of cookies with sea salt.

6\. Bake for 12-15 minutes or until the edges look golden brown and the middles don’t look quite set. Allow to finish cooling on the baking sheet.

_Adapted from Brown Eyed Baker_
