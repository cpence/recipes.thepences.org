---
title: "Spicy Pecans"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - miscellaneous
tags:
  - pecan
  - snack
---

## Ingredients

*   1/3 cup sugar
*   ¼ cup butter or margarine
*   ¼ cup orange juice
*   1 ¼ teaspoons salt
*   1 ¼ tsp ground cinnamon
*   ¼ to ½ tsp ground red pepper
*   ¼ teaspoon mace
*   1 lb pecans

## Preparation

Stir together first 7 ingredients in a heavy skillet over medium heat, stirring until butter melts and sugar dissolves.  Remove skillet from heat; add nuts and toss to coat.

Place pecans mixture in a single layer in an aluminum foil-lined 15 x 10 –inch jellyroll pan.

Bake at 250 for 1 hour, stiring every 15 minutes.  Cool in pan on a wire rack, separating pecans with a fork.  Store in air tight container.
