---
title: "Garlic Shrimp"
author: "juliaphilip"
date: "2010-04-26"
categories:
  - main course
tags:
  - mexican
  - shrimp
---

## Ingredients

*   3/4 cup olive oil
*   1/2 cup coarsely chopped white onion
*   3 large garlic cloves, chopped
*   1 teaspoon fine sea salt
*   1/2 teaspoon ground black pepper
*   16 uncooked jumbo shrimp, shells intact, deveined (about 1 pound)

## Preparation

Puree 1/2 cup oil, onion, garlic, salt, and pepper in blender until almost smooth. Place shrimp in small bowl. Stir in oil mixture. Let shrimp marinate 1 hour.

Heat remaining 1/4 cup oil in heavy large skillet over high heat. Add shrimp with marinade and sauté just until shrimp are opaque in center, about 4 minutes. Divide shrimp and marinade from skillet among 4 plates and serve.

Serve with [Mexican White Rice]( {{< relref "mexican-white-rice" >}} ).

_Bon Appetit, May 2003_
