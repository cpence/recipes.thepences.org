---
title: "Instant Pot Lentil Risotto"
date: 2020-09-28T12:11:53+02:00
categories:
    - main course
    - side dish
tags:
    - instantpot
    - lentil
    - italian
---

## Ingredients

*   1 cup dry lentils, soaked overnight
*   1 tablespoon olive oil
*   1 medium onion, chopped
*   1 celery stalk, chopped
*   2 sprigs parsley, stems and leaves chopped (about 1 tablespoon)
*   1 cup Arborio rice
*   2 garlic cloves, lightly mashed
*   3¼ cups (750ml) vegetable stock
*   optional: 1 medium potato, chopped in 1cm cubes

## Preparation

In the pre-heated pressure cooker, add the olive oil and cook the onion until
just beginning to soften. Then add the celery, parsley and cook for another
minute. Add the rice and garlic cloves mix well and cook until it is all evenly
wet and pearly (about a minute). Add the stock, potato (if using), and strained
lentils, mix well. Close and lock the lid of the pressure cooker, and cook for 5
minutes at high pressure. When time is up, quick release pressure and open the
cooker. Mix well and serve immediately with extra virgin olive oil.

*Hip Pressure Cooking*

