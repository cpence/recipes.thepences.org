---
title: "Simple Almond Cake"
author: "juliaphilip"
date: "2010-12-29"
categories:
  - dessert
tags:
  - almond
  - cake
---

## Ingredients

*   1 1/2 sticks unsalted butter, softened
*   1 c superfine sugar
*   4 extra large eggs
*   1 2/3 c ground almonds
*   1/2 c all-purpose flour
*   1/4 tsp almond extract
*   powdered sugar for decoration
*   macerated raspberries for topping

## Preparation

Preheat the oven to 350 F.

Prepare a 9-inch cake pan: brush with melted butter then line the bottom with
parchment paper and brush paper with a little more melted butter. Lightly coat
with flour, rolling the pan around and tipping out the excess flour. Set aside.

In a large bowl, beat the butter until smooth, then gradually beat in the sugar
until light and fluffy. Beat in the eggs one at a time, then gradually beat in
almonds. When the mixture is well amalgamated, fold in the flour and stir in the
almond flavoring.

Transfer the mixture to the prepared pan and tap the pan on the work surface to
ensure even distribution. Bake for about 35 min or until the cake is firm to the
touch in the middle. Turn out onto a wire rack and leave to cool.

Dredge the top of the cake with sifted confectioner's sugar. Or, if you like,
you can sift the sugar over 3/4-inch wide strips of cardboard, arranged in
stripes of diamonds on top of the cake, to give an attractive appearance when
the strips are removed. Top with raspberries.

_Ten Late Breakfasts, Alexandra Carlier_
