---
title: "Mommawater's Crazy Dough"
date: 2021-05-09T11:23:40+02:00
categories:
  - bread
tags:
  - pizza
---

## Ingredients

- 1 kg. flour
- 500 ml milk
- 200 ml yogurt
- 1 tsp. salt
- 1 tsp. sugar
- 1 tsp. yeast
- 2 eggs
- 1/4 c. vegetable oil

## Preparation

Put the flour into a huge bowl and add the salt.

Set aside. Heat the milk until lukewarm. In a separate container, add the yeast
and sugar, and pour in half of the lukewarm milk. Let sit for five minutes for
the yeast to rise.

In a separate bowl, mix the yogurt, eggs, and vegetable oil.

Make a well in the middle of the flour, and add the yeast mix, followed by the
rest of the milk, followed by the yogurt mixture. Mix thoroughly (electric mixer
works).

Once everything is consistent, move it to a large bowl (or even a big clean
bag). Let sit for at least five hours, or (better) overnight in the fridge.

When you take it out, it's ready to work with. You'll have to punch it down, and
it will take dry flour to work with it because it's sticky. It works for pizza
dough, panzerotti, calzones, crazy bread, doughnuts, etc.

_xwater_
