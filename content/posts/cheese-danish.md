---
title: "Cheese Danish"
author: "juliaphilip"
date: "2010-12-29"
categories:
  - breakfast
tags:
  - danish
  - pastry
---

## Ingredients

*   1/2 recipe [Danish Pastry Dough]( {{< relref "danish-pastry-dough" >}} )
*   Cheese Filling (recipe follows)
*   Cherry preserves
*   1 egg, slightly beaten
*   1/2 c. light corn syrup

## Preparation

1\. Roll Danish pastry dough out on a lightly floured pastry board or cloth to a 20x15-inch rectangle; trim edges even; cut in twelve 5-inch squares with a sharp knife.

2\. Spoon cheese filling onto center of each square, dividing evenly; fold in all 4 corners to meet and overlap slightly in the center so filling is completely enclosed; press points down with fingertip.

3\. Place pastries, 2 inches apart, on greased cooky sheet.  Let rise in warm place, away from draft, till double in bulk, about 3o minutes.  Press points down again and fill center of each with a teaspoon of cherry preserves.  Brush pastries with egg.

4\. Place in hot oven (400 degrees); reduce the heat immediately to 350.  Bake 20 to 25 minutes, or until puffed and golden-brown.  Heat corn syrup just until warm in a small saucepan; brush over pastries.  Remove to wire rack; cool.  (Add more preserves after baking, ,if you wish, since pastries open as they bake.)  Makes 12 individual pastries.

**Cheese Filling**

Place 1 carton (8 ounces) of pot cheese \[CP: or perhaps well-drained ricotta?\], 1 egg yolk, 1/4 c. sugar and 1 tsp. of grated lemon rind in blender, whirl until smooth.  Or, press the pot cheese through a strainer into a small bowl and beat in egg yolk, sugar and lemon rind.  Makes 1 cup.

_Family Circle Cookbook_
