---
title: "Roasted Turkey with Juniper-Ginger Butter and Pan Gravy"
author: "pencechp"
date: "2010-01-08"
categories:
  - main course
tags:
  - turkey
---

## Ingredients

*For the juniper-ginger butter:*

*   7 oz. (14 Tbs.) unsalted butter, softened
*   1/4 cup minced fresh ginger
*   2 Tbs. chopped fresh flat-leaf parsley
*   2 Tbs. minced shallots
*   1 Tbs. ground juniper
*   1 Tbs. chopped fresh sage
*   1 Tbs. fresh thyme
*   2 tsp. minced garlic
*   2 tsp. chopped fresh rosemary

*For the brined turkey:*

*   2-1/2 lb. kosher salt
*   1-1/2 lb. (3 cups plus 3 Tbs.) granulated sugar
*   2/3 cup freshly ground black pepper
*   2-1/2 oz. fresh rosemary sprigs (about 2 large bunches), lightly crushed
*   2-1/2 oz. fresh thyme sprigs (about 2 large bunches), lightly crushed
*   14-lb. natural turkey (preferably fresh)

*For the gravy:*

*   1 cup lower-salt chicken broth
*   4 Tbs. unsalted butter
*   3 oz. (2/3 cup) all-purpose flour
*   Kosher salt and freshly ground black pepper

## Preparation

_At least one day ahead, make the butter_

Mix the butter ingredients in a bowl. Refrigerate 4 Tbs. of the butter for the gravy and set the rest aside at room temperature for the turkey.

_One day ahead, brine and prepare the turkey_

In a plastic container or stockpot large enough to hold the turkey, mix all the brine ingredients (except the turkey) in 3 gallons of cold water, stirring until the salt and sugar are mostly dissolved. Discard the neck and the giblets and trim any excess skin or fat. Trim the tail, if desired. Rinse the turkey and submerge it in the brine for at least 4 hours and no more than 6 hours. If the turkey floats, weight it down with a couple of dinner plates.

Remove the turkey from the brine and pat dry with paper towels. Starting at the top of the breast, run your fingers between the breast and the skin to separate them, being careful not to rip the skin. Once you're halfway down the breast, turn the turkey around and work from the bottom of the breast until you have loosened the skin from the breast, thighs, and as far down the legs as you can reach. Rub the juniper butter under the skin, covering the breast and as much of the legs as possible. Tuck the wings behind the breast and truss the turkey with twine, securing the legs to the body. Set the turkey on a rack in a large roasting pan and refrigerate, uncovered, for at least 6 and up to 24 hours.

_Roast the turkey_

Position a rack in the bottom of the oven and heat the oven to 350°F. If any brine has dripped from the turkey into the roasting pan, pour it out. Then pour 2 cups of warm water into the bottom of the pan and cover the entire roasting pan with foil. Roast undisturbed for 2 hours; remove the pan from the oven and remove the foil. Roast the uncovered turkey until an instant-read thermometer inserted in the thickest part of both thighs reads 165°F, 45 minutes to 1 hour longer.

Move the turkey to a cutting board, tent with foil to keep warm, and let rest for about 30 minutes.

_Make the gravy_

Strain the turkey drippings into a fat separator cup (or another clear, heatproof container). Let sit until the fat rises to the top and then separate exactly 2 cups of the turkey juice from the fat—don't use more than that or the gravy will be too salty. Combine the 2 cups juice with the chicken broth and enough water to make 4-1/2 cups liquid.

In a medium saucepan, melt the reserved juniper-ginger butter and the unsalted butter over medium-high heat until foaming. Whisk in the flour and cook, whisking constantly, until the mixture is golden brown, 2 to 3 minutes. Gradually whisk in the liquid, bring just to a boil, and reduce to a simmer. Whisking frequently, continue to cook about 5 minutes longer to meld the flavors. Season to taste with salt and pepper.

_Alfred Portale, Fine Cooking Magazine_
