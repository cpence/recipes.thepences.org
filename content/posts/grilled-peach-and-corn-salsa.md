---
title: "Grilled Peach and Corn Salsa"
author: "pencechp"
date: "2011-09-03"
categories:
  - side dish
tags:
  - mexican
  - peach
---

## Ingredients

*   2 large tomatoes
*   1/2 large red onion
*   1 ear of corn
*   3 firm peaches
*   1 minced jalapeño
*   1/2 cup chopped cilantro
*   1 lime, zest and juice
*   1 tsp. olive oil
*   Salt and pepper, to taste

## Preparation

Lightly oil, salt and pepper corn and peaches.  Grill on all sides.  Cut corn kernels off of the corn cob.  De-seed and dice tomatoes.  Dice peaches and red onion.  Mix all ingredients together with lime juice, cilantro, and jalapeño.  Let sit at least 15 minutes to marry flavors.

_Rachel Mabb_
