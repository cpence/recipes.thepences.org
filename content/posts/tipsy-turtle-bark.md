---
title: "Tipsy Turtle Bark"
author: "pencechp"
date: "2015-06-02"
categories:
  - dessert
tags:
  - chocolate
  - pecan
---

## Ingredients

*   2 cups pecan halves
*   1 cup (about 24) caramel candies, such as Kraft, unwrapped
*   1 tablespoon rum, bourbon, or whisky
*   1 1/2 teaspoons heavy cream
*   1/4 teaspoon salt
*   1 pound high-quality semisweet chocolate, finely chopped

## Preparation

Preheat oven to 350°F. Spread pecans on large shallow baking sheet and toast until golden brown, about 10 minutes. Transfer to plate and let cool. Line baking sheet with parchment or wax paper.

In small bowl, combine caramels, liquor, cream, and salt. Microwave uncovered at medium power for 2 minutes. Stir with fork. Microwave at medium power for 1 additional minute. Stir until smooth and set aside.

In medium bowl set over saucepan of simmering water, melt half chocolate, stirring occasionally. Remove from heat, add remaining chocolate, and stir until smooth. Pour half melted chocolate into small bowl and reserve.

Stir 1 cup nuts into remaining chocolate. Transfer mixture to baking sheet, spreading to 1/4- to 1/2-inch thickness. Spoon caramel over and pat on remaining pecans. Drizzle with reserved chocolate. Let cool at room temperature until set, about 2 hours. Do not chill.

_Epicurious_
