---
title: "Vinagreta de Ajo Tostado"
author: "pencechp"
date: "2010-02-27"
categories:
  - miscellaneous
tags:
  - dressing
  - spanish
---

## Ingredients

*   1/4 cup garlic cloves
*   1/4 cup extra-virgin olive oil, plus 1/2 cup
*   2 tablespoons sherry vinegar
*   1 tablespoon chopped shallots
*   Salt and pepper

## Preparation

Preheat the oven to 350 degrees F.

Roast garlic in 1/4 cup olive oil in a small baking dish until garlic is very tender, about 20 minutes. Allow to cool slightly. Transfer to blender. Add sherry vinegar, shallots, salt and freshly ground pepper. Blend. Gradually add remaining 1/2 cup olive oil.

_Jaleo, Washington D.C._
