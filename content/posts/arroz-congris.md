---
title: "Arroz Congris"
author: "pencechp"
date: "2014-11-11"
categories:
  - main course
tags:
  - bean
  - cuban
  - rice
  - untested
  - vegan
  - vegetarian
---

## Ingredients

*   1 c. dry black beans
*   2 c. black bean stock
*   3 strips bacon (or, for vegetarian, 1/4 c. vegetable oil)
*   1 lg. onion, chopped
*   4 cloves garlic, finely chopped
*   2 sweet red or green bell peppers, cut into thin strips
*   2 c. long-grain white rice
*   1/2-1 tsp. salt
*   1 tsp. ground cumin
*   1/4 tsp. oregano
*   1 lg. bay leaf

## Preparation

Soak the beans overnight in a pot of water and drain well (or fast-soak). Place on the stove with plenty of fresh water. Simmer for about 1 1/2 hours, until nice and tender but not mushy. Reserve 2 c. of the stock and then drain the beans. Sautee the bacon (or heat the oil) in a pot with the onion, garlic, and peppers until the onion is golden. Add rice, salt, cumin, oregano, and bay leaf, then bean stock and beans. Cover and place on medium heat until it comes to a boil. Stir to prevent from sticking to the bottom of the pot, reduce heat to low and cook until the rice looks completely dry. Should take 10-15min. Stir and serve.

_Penzey's Spices_
