---
title: "Lemony Smoked Salmon and Spinach Pasta"
date: 2022-09-03T11:35:21+02:00
categories:
  - main course
tags:
  - pasta
  - salmon
  - spinach
---

## Ingredients

- 100 g pasta (like tagliatelle), fresh or dried
- 1 tbsp olive oil
- 1 clove garlic
- 50 g spinach
- 75 g cream cheese
- zest and juice of 1/2 lemon
- 75 g smoked salmon, sliced
- small handful fresh basil, finely chopped

## Preparation

Cook the pasta as per directions. Meanwhile, heat the oil in a saucepan and cook the garlic until softened. Add the spinach and continue to cook until just wilted. Add the cream cheese, lemon zest and juice, and black pepper.

Drain the pasta, reserving a few tablespoons of the cooking water. Add the salmon, basil, pasta, and reserved pasta water to the sauce. Toss and season.

_BBC Good Food_
