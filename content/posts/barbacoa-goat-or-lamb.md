---
title: "Barbacoa Goat or Lamb"
author: "pencechp"
date: "2016-11-25"
categories:
  - main course
tags:
  - goat
  - lamb
  - mexican
---

## Ingredients

*   4 guajillo chiles
*   2 teaspoons cumin seeds
*   1 teaspoon ground cloves
*   10 allspice berries
*   1/3 cup Mexican oregano
*   12 sprigs fresh thyme
*   6 garlic cloves
*   1 onion, roughly chopped
*   1/3 cup cider vinegar
*   1 (12-pound) goat, quartered or 1 (6 to 8-pound) lamb shoulder
*   Salt and pepper
*   1 (2-pound) package dried avocado leaves (we substituted banana leaves)

## Preparation

Toast chiles, cover with boiling water in a deep bowl, and set aside for 20 minutes. Grind cumin, cloves, allspice, and oregano in coffee grinder. Drain soaked chiles, puree in blender the chiles, ground spices, thyme, garlic, onion, vinegar, and 1/2 cup of water. Process until smooth Push mixture through a sieve, season goat with salt and pepper. Rub paste all over the meat. Arrange in a bowl and allow to marinate for 4 hours, refrigerated. Preheat the oven to 325 degrees F. In a deep roaster pan scatter half of the avocado leaves on the bottom, place meat on top of the avocado leaves and scatter the remaining leaves over the meat. Cover the pan tightly with aluminum foil. Cook the goat 6 to 7 hours until meat is falling off the bone. If using lamb shoulder cook for 4 hours.

_Food Network_
