---
title: "Tenderloins with Peanuts"
author: "pencechp"
date: "2012-05-04"
categories:
  - main course
tags:
  - bellpepper
  - curry
  - pork
  - untested
---

## Ingredients

*   3 green bell peppers
*   3 yellow bell peppers
*   1 bunch fresh cilantro
*   2 tbsp curry paste (pref. Indian)
*   2 garlic cloves, crushed
*   scant 1 c. peanuts, chopped
*   scant 1 c. coconut milk
*   4 tbsp. peanut oil
*   3 small pork tenderloins, about 2 1/2 lb.

## Preparation

Preheat the oven to 350F. Place the whole bell peppers on a cookie sheet and roast, turning occasionally, for 20 minutes, until the skins are charred. Remove from the oven and let cool. Peel off the skins and set the whole bell peppers aside.

Chop the stems and leaves of the cilantro, then mix with the curry paste, garlic, and peanuts in a bowl. Alternatively, place the bunch of cilantro, the curry paste, and garlic in a food processor and process until finely chopped and thoroughly combined. Dry-fry this paste in a pan, stirring constantly, until it gives off its aroma. Stir in the coconut milk and cook until reduced.

Heat the oil in a skillet. Add the tenderloins and cook over high heat, turning occasionally, for 10 minutes, until evenly browned and cooked through. Meanwhile, reheat the bell peppers in the oven at 350F.

Arrange the tenderloins and the whole peppers on a board and serve the sauce separately.

_Stéphane Reynaud, Pork & Sons_
