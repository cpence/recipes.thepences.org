---
title: "Southern Comfort Champagne Cocktail"
author: "pencechp"
date: "2014-01-19"
categories:
  - cocktails
tags:
  - champagne
  - untested
---

## Ingredients

*   1 oz. Southern Comfort
*   dash angostura bitters
*   4 oz. champagne, chilled
*   twist lemon peel

## Preparation

Combine. Variations: Brandy instead of Southern Comfort, no bitters, orange peel; Cognac, extra dash or two of bitters, no citrus.

_Epicurious, February 2003_
