---
title: "Field Pea Panzanella Salad"
author: "pencechp"
date: "2017-03-30"
categories:
  - side dish
tags:
  - peas
  - salad
  - untested
---

## Ingredients

*   2 cups day-old bread, cut into 1-inch dice
*   1 tablespoon olive oil
*   2 pounds cooked field peas (crowder, black-eye, lady-peas)
*   1 cup thinly sliced green onions
*   1/2 cup thinly sliced red onion
*   1 cup diced tomato
*   1 cup peeled, diced cucumber
*   1/2 cup chopped banana peppers
*   1/4 cup each chopped rosemary and parsley
*   2/3 cup olive oil
*   1/2 cup sherry or balsamic vinegar
*   1 tablespoon local honey
*   1 teaspoon kosher salt

## Preparation

Preheat oven to 275 degrees. Spread the diced bread on a sheet pan and bake for 20 to 25 minutes, until very lightly toasted. Set aside to cool. Place the field peas, green and red onions, tomatoes, cucumber and chili peppers in a large mixing bowl. Add the herbs, olive oil, vinegar, honey and salt, toss well. Add the bread and toss everything together. Serve with grilled fish, chicken, pork or as a vegetarian main course.

_Edible Nashville_
