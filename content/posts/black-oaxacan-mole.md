---
title: "Black Oaxacan Mole"
author: "pencechp"
date: "2012-11-28"
categories:
  - main course
tags:
  - mexican
  - sauce
---

## Ingredients

*   9 oz. (250 g) chilhuacles negros (about 40) \[CP: can replace w/ guajillo\]
*   9 oz. (250 g) mulato chiles (about 7) \[CP: can replace w/ ancho\]
*   9 oz. (250 g) (Mexican not Oaxacan) pasilla chiles
*   15 chipotle mora chiles
*   Approximately 9 oz. melted pork lard
*   9 oz. (250 g) sesame seeds, about 1 3/4 cups
*   9 oz. (250 g) shelled peanuts, about 1 3/4 cups
*   9 oz. (250 g) almonds, about 1 1/2 cups
*   4 1/2 oz. (125 g) walnuts
*   4 1/2 oz. (125 g) pecans
*   9 oz. (250 g) raisins, about 2 cups
*   9 oz. (250 g) plantain, peeled
*   1 1/2 small semisweet rolls (pan de yema), sliced and dried
*   9 oz. (250 g) white onion, cut into wedges and toasted
*   1 1/2 heads garlic, toasted, heads separated and peeled
*   3 inches cinnamon stick
*   1/2 tbsp. mild black peppercorns
*   1/4 tsp. cloves, about 10
*   1/2 tbsp. cumin seeds
*   3/4 tbsp. Mexican oregano
*   1/2 tbsp. dried thyme
*   1/2 tbsp. dried marjoram
*   2 bay leaves
*   9 oz. (250 g) Oaxacan drinking chocolate
*   9 oz. (250 g) sugar
*   salt to taste

## Preparation

Begin preparing the chiles the day before.  Remove seeds and veins from the chiles, reserving the seeds, except leave the chipotles whole.

Toast the chiles carefully (very briefly on a hot fire, _not_ allowing them to char).  Cover with warm water and leave to soak about 1 hour, no longer.  Strain.

Toast the chile seeds in an ungreased pan until very dark brown but not charred.  Rinse in two changes of water and strain.

Heat a small quantity of the lard in a skillet and fry the following ingredients separately, adding more lard as necessary and straining to remove excess oil: sesame seeds, peanuts, almonds, walnuts, pecans, raisins, plantain, bread.  Mix these ingredients with the chiles, onion, garlic, spices, and herbs and grind almost dry, to a paste.  Very little water should be used.

Heat the remaining lard in a heavy casserole, add the paste, and fry, adding a little boiling water from time to time to prevent sticking.  Stir continuously (get helpers!) over medium heat for about 30 minutes.

Add the chocolate, sugar, and salt and continue cooking for about 1 hour more.

The consistency should be that of a thick paste, and you should be able to see the bottom of the pan as you stir.

Make the whole batch and freeze it; when you need it, dilute and cook with tomatoes and chicken or turkey stock. Our standard ratio has been something like a 15-oz. can of diced tomatoes, around 1 or 2 cups chicken broth (depending on how much liquid your meat gives off), and a few pounds of meat (say, a turkey breast or 4-5 chicken breasts).

_Señora Luz Allec de Calderon, Cookbook Unknown, via Adam Savage_
