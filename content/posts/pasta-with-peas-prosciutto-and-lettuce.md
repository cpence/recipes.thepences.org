---
title: "Pasta with Peas, Prosciutto, and Lettuce"
author: "pencechp"
date: "2013-06-13"
categories:
  - main course
tags:
  - pasta
  - peas
  - untested
---

## Ingredients

*   Salt
*   3 tablespoons olive oil
*   2 to 3 ounces thinly sliced prosciutto, cut crosswise into 1/2-inch-wide strips
*   1 pound pasta
*   2 tablespoons butter
*   1 shallot, minced
*   Black pepper to taste
*   2 cups peas, fresh or frozen
*   1 head Bibb or Boston lettuce (about 6 ounces), cored, leaves cut into 3/4-inch slices
*   1/2 cup chicken or vegetable stock or dry white wine, more as needed
*   1 cup finely grated Parmesan cheese

## Preparation

Bring a large pot of water to boil and salt it. Meanwhile, put one tablespoon oil in a small skillet over medium-high heat. When hot, add prosciutto and cook, turning occasionally, until crisp, about 4 to 5 minutes; set aside.

When water boils, add pasta and cook until just tender; drain pasta, reserving some cooking liquid. Meanwhile, melt butter with remaining 2 tablespoons oil in a large skillet over medium heat. Add shallot and sprinkle with salt and pepper; cook until shallot begins to soften, about 5 minutes.

Add peas, lettuce and stock or wine to skillet and cook until peas turn bright green and lettuce is wilted, about 5 minutes. Add pasta to pan and continue cooking and stirring until everything is just heated through, adding extra stock or some reserved cooking liquid if needed to moisten. Toss with Parmesan cheese, garnish with prosciutto, adjust seasoning to taste and serve.

_Serves 4_

_The New York Times_
