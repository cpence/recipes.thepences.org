---
title: "Chard and Onion Omelet (Trouchia)"
author: "pencechp"
date: "2011-09-03"
categories:
  - breakfast
  - main course
tags:
  - french
  - greens
  - omelet
---

## Ingredients

*   3 tablespoons olive oil
*   1 large red or white onion, quartered and thinly sliced crosswise
*   1 bunch chard, leaves only, chopped
*   Salt and freshly milled pepper
*   1 garlic clove
*   6 to 8 eggs, lightly beaten
*   2 tablespoons chopped parsley
*   2 tablespoons chopped basil
*   2 teaspoons chopped thyme
*   1 cup grated Gruyère
*   2 tablespoons freshly grated Parmesan

## Preparation

Heat 2 tablespoons of the oil in a 10-inch skillet, add the onion, and cook over low heat, stirring occasionally, until completely soft but not colored, about 15 minutes. Add the chard and continue cooking, stirring occasionally, until all the moisture has cooked off and the chard is tender, about 15 minutes. Season well with salt and pepper.

Meanwhile, mash the garlic in a mortar with a few pinches of salt (or chop them finely together), then stir it into the eggs along with the herbs. Combine the chard mixture with the eggs and stir in the Gruyère and half the Parmesan.

Preheat the broiler. Heat the remaining oil in the skillet and, when it's hot, add the eggs. Give a stir and keep the heat at medium-high for about a minute, then turn it to low. Cook until the eggs are set but still a little moist on top, 10 to 15 minutes. Add the remaining Parmesan and broil 4 to 6 inches from the heat, until browned.

Serve trouchia in the pan or slide it onto a serving dish and cut it into wedges. The gratinéed top and the golden bottom are equally presentable.

_Epicurious: Vegetarian Cooking for Everyone_
