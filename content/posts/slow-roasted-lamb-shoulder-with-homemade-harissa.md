---
title: "Slow Roasted Lamb Shoulder with Homemade Harissa"
author: "pencechp"
date: "2017-10-30"
categories:
  - main course
tags:
  - chile
  - french
  - lamb
  - mediterranean
---

## Ingredients

*   1/4 teaspoon caraway seeds
*   1/4 teaspoon coriander seeds
*   1/4 teaspoon cumin seeds 
*  2 ounces ancho chiles (about 4) stemmed and seeded
*  1 tablespoon smoked sweet paprika
*  1 tablespoon lemon juice
*  3 large garlic cloves, 1 clove mashed to a paste
*  1/4 cup extra-virgin olive oil
*  Kosher salt
*  One 3-pound lamb shoulder roast on the bone
*  1 cup plain Greek yogurt
*  2 tablespoons chopped cilantro
*  Freshly ground pepper
*  Lettuce leaves and warm naan, for serving

## Preparation

In a spice grinder, finely grind the caraway, coriander and cumin seeds. In a microwave-safe bowl, cover the ancho chiles with water and microwave at high power for 2 minutes. Let cool slightly, then transfer the softened chiles and 2 tablespoons of the soaking liquid to a blender. Add the ground spices, paprika, lemon juice, the 2 whole garlic cloves, 2 tablespoons of the olive oil and 1 tablespoon of salt. Puree the harissa until smooth.

Set the lamb in a medium roasting pan and rub 1/2 cup of the harissa all over the meat; let stand at room temperature for 2 hours or refrigerate overnight.

Preheat the oven to 325°. Add 1/2 cup of water to the roasting pan and cover the pan loosely with foil. Roast the lamb for 2 1/2 hours, adding water to the pan a few times to prevent scorching. Remove the foil and roast for about 2 1/2 hours longer, until the lamb is very brown and tender; occasionally spoon the pan juices on top. Let stand for 20 minutes.

Meanwhile, in a small bowl, combine the yogurt with the cilantro, mashed garlic clove and the remaining 2 tablespoons of olive oil. Season with salt and pepper.

Using forks or tongs, pull the lamb off the bone in large chunks. Using your fingers, pull the meat into smaller shreds and serve with the yogurt sauce, lettuce leaves, naan and the remaining harissa.

_Food & Wine_
