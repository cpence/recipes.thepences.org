---
title: "Chipotle Dry Rub"
author: "juliaphilip"
date: "2010-04-03"
categories:
  - miscellaneous
tags:
  - chile
---

## Ingredients

*   2-3 dried chipotle peppers
*   3 tbl black pepper
*   2 tbl dried oregano
*   1 tbl dried cilantro
*   1 bay leaf
*   1 tsp cumin
*   1 tsp onion
*   1 tsp dry orange peel

## Preparation

Combine all ingredients in a spice mill or blender and grind until even and finely ground. Store in an airtight jar. Will store for about 6 months in the freezer.
