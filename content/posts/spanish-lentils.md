---
title: "Spanish Lentils"
author: "pencechp"
date: "2011-12-30"
categories:
  - side dish
tags:
  - lentil
  - spanish
---

## Ingredients

*   250g dried Puy lentils
*   1 410g (15 0z, small) tin chickpeas
*   150ml red wine
*   100g chorizo (optional)
*   6 shallots
*   2 garlic cloves
*   1 red pepper
*   Handful chopped fresh coriander
*   Handful chopped fresh parsley
*   2 tbsp. extra virgin olive oil
*   1 litre vegetable stock
*   1 tsp. smoked paprika
*   Salt and pepper

## Preparation

1\. Finely chop the shallots, chop and deseed the pepper and mince the garlic cloves. Slice the chorizo.

2\. Bring the stock to a simmer and then add the lentils and simmer for twenty minutes.

3\. Meanwhile, heat half of the oil to a frying pan and fry the chorizo for five minutes. Once it has released some of its oil, add the shallots, garlic and pepper and fry for another 6-8 minutes until the pepper and shallots begin to soften.

4\. Add the paprika and fry for one minute, then add the cooked drained lentils. Drain the chickpeas and add these, and then stir in the rest of the olive oil.

5\. Add the wine and simmer until it has almost all evaporated. Once it is at this stage, season the dish well with salt and pepper and then stir in the freshly chopped herbs.

_[Food Recipes Online](http://foodrecipes.wordpress.com/2011/09/16/spanish-lentils/)_
