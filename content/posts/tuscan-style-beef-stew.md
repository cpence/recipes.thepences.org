---
title: "Tuscan-Style Beef Stew"
author: "pencechp"
date: "2016-04-28"
categories:
  - main course
tags:
  - beef
  - italian
---

## Ingredients

*   4 lb. boneless beef short ribs, trimmed and cut into 2-inch pieces (or a 5 lb. chuck roast)
*   Salt
*   1 tbsp. vegetable oil
*   1 bottle Chianti
*   1 c. water
*   4 shallots, peeled and halved lengthwise
*   2 carrots, peeled and halved lengthwise
*   1 garlic head, cloves separated, unpeeled, and crushed
*   4 sprigs fresh rosemary
*   2 bay leaves
*   1 tbsp. cracked black peppercorns, plus extra for serving
*   1 tbsp. gelatin
*   1 tbsp. tomato paste
*   1 tsp. anchovy paste
*   2 tsp. ground black pepper
*   2 tsp. cornstarch

## Preparation

1\. Toss beef and 1 1/2 teaspoons salt together in bowl and let stand at room temperature for 30 minutes. Adjust oven rack to lower-middle position and heat oven to 300 degrees.

2\. Heat oil in large Dutch oven over medium-high heat until just smoking. Add half of beef in single layer and cook until well browned on all sides, about 8 minutes total, reducing heat if fond begins to burn. Stir in 2 cups wine, water, shallots, carrots, garlic, rosemary, bay leaves, cracked peppercorns, gelatin, tomato paste, anchovy paste, and remaining beef. Bring to simmer and cover tightly with sheet of heavy-duty aluminum foil, then lid. Transfer to oven and cook until beef is tender, 2 to 2 1/4 hours, stirring halfway through cooking time.

3\. Using slotted spoon, transfer beef to bowl; cover tightly with foil and set aside. Strain sauce through fine-mesh strainer into fat separator. Wipe out pot with paper towels. Let liquid settle for 5 minutes, then return defatted liquid to pot.

4\. Add 1 cup wine and ground black pepper and bring mixture to boil over medium-high heat. Simmer briskly, stirring occasionally, until sauce is thickened to consistency of heavy cream, 12 to 15 minutes.

5\. Combine remaining wine and cornstarch in small bowl. Reduce heat to medium-low, return beef to pot, and stir in cornstarch-wine mixture. Cover and simmer until just heated through, 5 to 8 minutes. Season with salt to taste. Serve over polenta, passing extra cracked peppercorns separately. (Stew can be made up to 3 days in advance.)

_CI_
