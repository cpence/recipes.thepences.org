---
title: "Macaroni and Cheese Casserole"
date: 2022-09-04T19:17:27+02:00
categories:
  - main course
tags:
  - pence
  - pasta
  - beef
---

## Ingredients

- two boxes (or equivalent) of macaroni and cheese mix
- pint box of mushrooms, sliced
- ground beef
- onions, chopped
- grated cheddar cheese
- 1/4 c. sour cream

## Preparation

Preheat oven to 350F.

Make the macaroni and cheese according to the package directions.

Sautée the sliced mushrooms, onion, and hamburger together until fully cooked. Stir into the macaroni and cheese, along with some cheddar cheese and the sour cream.

Put into a 13"x9" baking dish, and bake for around 30 minutes or so, until warm through. Add extra grated cheddar on top, and bake until melted.
