---
title: "Pork Tenderloin Churrasco"
author: "pencechp"
date: "2011-12-30"
categories:
  - main course
tags:
  - pork
  - spanish
---

## Ingredients

*   1 cup vegetable oil
*   2 tablespoons hot smoked Spanish paprika (Pimentón de la Vera)
*   4 garlic cloves, minced
*   2 tablespoons fresh thyme leaves
*   1 teaspoon (packed) minced fresh rosemary
*   1 teaspoon salt
*   1/4 teaspoon ground black pepper
*   2 1-pound pork tenderloins, trimmed of fat and silver skin membrane

## Preparation

Combine oil and paprika in heavy small saucepan. Cook over medium heat 5 minutes to infuse oil, whisking occasionally. Cool to room temperature.

Pour oil mixture into blender. Add garlic, thyme leaves, rosemary, salt, and pepper; blend marinade until herbs are finely chopped.

Cut each pork tenderloin lengthwise into 4 strips. Place each strip between sheets of waxed paper and pound to 1/3-inch thickness. Arrange pork strips in 13x9x2-inch glass baking dish. Pour marinade over and turn to coat pork evenly. Cover and chill at least 2 hours and up to 6 hours.

Prepare barbecue (medium-high heat). Scrape off most of marinade from pork strips. Grill pork until just cooked through, about 2 minutes per side.

Arrange 2 pork strips on each of 4 plates. Spoon pineapple salsa atop pork.

_Bon Appetit, September 2003_
