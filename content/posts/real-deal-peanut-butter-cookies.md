---
title: "Real Deal Peanut Butter Cookies"
author: "pencechp"
date: "2010-05-09"
categories:
  - dessert
tags:
  - cookie
  - peanutbutter
  - vegan
  - vegetarian
---

## Ingredients

*   1/4 c. maple syrup
*   1/4 c. brown sugar
*   1/4 c. canola oil
*   1/2 c. peanut butter
*   1 tsp. vanilla extract
*   a pinch salt
*   1 c. all-purpose flour

## Preparation

Mix together maple syrup, brown sugar, canola oil, peanut butter and vanilla extract.  Then add the cup of flour and the salt and mix to combine.

Form teaspoon-sized balls with your hands and press down with a fork.  Bake for 15-20 minutes at 350.

To make these chocolate peanut butter cookies, change the flour for 2/3 c. flour and 1/3 c. cocoa powder.

Makes 2-3 dozen.

_VegWeb.com_
