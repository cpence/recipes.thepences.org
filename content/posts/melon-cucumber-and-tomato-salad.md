---
title: "Melon, Cucumber, and Tomato Salad"
author: "pencechp"
date: "2012-07-21"
categories:
  - side dish
tags:
  - cucumber
  - melon
  - salad
  - vegan
  - vegetarian
---

## Ingredients

*   1 European cucumber, peeled if desired and cut in medium dice
*   Salt to taste
*   1 pound ripe tomatoes, peeled and seeded if desired, cut in thin wedges or diced
*   1 small ripe honeydew melon or cantaloupe, peeled and cut into 1-inch dice or shaped into melon balls
*   2 to 3 tablespoons Champagne vinegar or sherry vinegar
*   1 tablespoon fresh lime juice
*   1 teaspoon mild honey, like clover, or agave nectar
*   4 tablespoons grapeseed oil, rice bran oil or canola oil
*   1 tablespoon chopped flat-leaf parsley
*   1 tablespoon chopped fresh mint
*   1 tablespoon chopped chives
*   Fresh watercress for garnish (optional)

## Preparation

1\. Put the cucumbers in a colander set in the sink or a bowl. Sprinkle with salt and let drain for 30 minutes. Meanwhile, prepare the other ingredients.

2\. Toss the cucumbers, tomatoes and melon together in a bowl. Whisk together the vinegar, salt to taste, lime juice, honey and oil and toss with the fruit and vegetable mixture. Cover the bowl and refrigerate for 1 to 2 hours.

3\. Just before serving, toss with the herbs. Line plates with watercress if desired. Taste the salad, adjust the seasonings and serve over the watercress.

_Martha Rose Shulman, New York Times_
