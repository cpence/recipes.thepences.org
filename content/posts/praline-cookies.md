---
title: "Praline cookies"
author: "juliaphilip"
date: "2010-04-03"
categories:
  - dessert
tags:
  - cookie
  - pecan
---

## Ingredients

*   12 whole graham crackers
*   1 c butter
*   1 c brown sugar
*   1 c nuts

## Preparation

Arrange the graham crackers on an ungreased cookie sheet. Combine the butter and sugar in a sauce pan; heat to boiling point, stirring constantly. Boil 3 min. Stir in nuts and pour over crackers. Bake at 350 for 10 min. While they are warm, cut the crackers in half.
