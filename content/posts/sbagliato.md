---
title: "Sbagliato"
date: 2020-06-06T13:53:15+02:00
categories:
    - cocktails
tags:
    - champagne
    - campari
---

## Ingredients

*   4 ounces dry Prosecco or other sparkling white wine
*   1 ounce sweet vermouth
*   ½ ounce Campari
*   Club soda
*   Lime wheel (for serving)

## Preparation

Pour Prosecco into an ice-filled large wine or rocks glass. Add vermouth and Campari and top off with club soda. Gently stir together; garnish with lime wheel.

*BA*

