---
title: "Garden Bean Salad"
author: "pencechp"
date: "2017-10-29"
categories:
  - side dish
tags:
  - bean
  - greenbean
  - salad
---

## Ingredients

*   1 small red onion, diced
*   1/4 c. apple cider vinegar
*   2 tsp. sugar
*   salt
*   1/4 c. olive oil
*   1 15-oz. can white beans, drained and rinsed
*   3/4 lb. green beans, trimmed and halved
*   2 tbsp. chopped parsley
*   2 tbsp. chopped chives
*   pepper

## Preparation

Soak the onion in water for 10 minutes. Drain and pat dry. Whisk the vinegar, sugar, and 1/2 tsp. salt in a bowl, then whisk in olive oil. Add the onion and white beans. Bring a saucepan of salted water to a boil. Add the green beans and cook until just tender, 4-5 minutes. Drain beans, and drop into an ice bath. Drain again, dry, and add to the salad. Marinate at room temperature for about 1 hour. Stir in chives and parsley just before serving, check seasoning with salt and pepper.
