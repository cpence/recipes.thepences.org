---
title: "Japanese Soufflé Pancakes"
date: 2020-01-27T18:41:55+01:00
categories:
    - breakfast
tags:
    - pancake
---

## Ingredients

*   4 egg whites and 2 egg yolks from 4 large eggs, separated and chilled
*   6 tablespoons granulated sugar
*   2 teaspoons vanilla extract
*   1 teaspoon baking powder
*   6 tablespoons cake flour
*   ¼ cup milk, chilled
*   ½ teaspoon fresh lemon juice
*   ½ teaspoon kosher salt
*   Unsalted butter, for greasing and serving
*   Maple syrup, for serving
*   Confectioners’ sugar, whipped cream and fresh berries, for serving (optional)

## Preparation

Place egg whites in the bowl of a stand mixer or a medium bowl; set aside. Place egg yolks in a large bowl. Add 1 tablespoon granulated sugar, the vanilla and baking powder to egg yolks and whisk until blended. Add flour and milk; whisk until fully combined.

Add lemon juice and salt to egg whites. Using a stand mixer fitted with the whisk attachment or a hand mixer, whip mixture on medium speed until foamy, about 1 minute. Continue to whip over medium while gradually sprinkling with remaining 5 tablespoons granulated sugar. Turn speed to high and whip until stiff, glossy peaks form and mixture doubles in size, about 1 minute. Take care not to overbeat meringue.

Heat a lidded nonstick skillet over the lowest heat setting and set the lid aside.

Using a rubber spatula, scoop about 1/3 of meringue into egg yolk mixture and gently fold almost combined. Repeat with half the remaining meringue until almost combined, then fold in the remaining meringue just until no streaks remain.

Carefully grease the warm skillet and the inside of four 3-inch-wide pastry rings (they should be at least 1 1/2 inches tall) using the butter. Check the heat of the pan by sprinkling a bit of water in it: Droplets should steam off the surface, but not dance or sputter. Place the greased pastry rings in the warm pan and ladle a scant 1/2 cup batter into each ring. Place lid on top of skillet and cook pancakes on very low heat until they start to rise and a few small bubbles start to form on top, 3 to 4 minutes.

Remove lid, carefully slide a flat spatula underneath each pancake and position another spatula on top, then gently flip pancakes in their rings. Immediately replace lid and cook until pancakes are cooked through and spring back to the touch, 2 to 3 minutes. Transfer cooked pancakes to a platter, grease the skillet and pastry rings and repeat to make 4 additional pancakes.

Top pancakes with a pat of butter and drizzle with maple syrup; serve immediately. Serve with any combination of confectioners’ sugar, whipped cream and berries, if desired.

*NYT*

