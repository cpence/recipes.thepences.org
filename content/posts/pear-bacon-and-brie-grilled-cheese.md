---
title: "Pear, Bacon, and Brie Grilled Cheese"
author: "pencechp"
date: "2014-06-10"
categories:
  - main course
tags:
  - bacon
  - cheese
  - pear
  - sandwich
  - untested
---

## Ingredients

*   2 slices sourdough bread
*   1 tablespoon butter, divided
*   ½ large yellow onion, thinly sliced
*   ½ tablespoon chopped rosemary 
*    a ripe pear, thinly sliced
*    4 large slices of brie
*    2 slices bacon, cooked until crispy

## Preparation

Heat a large skillet over medium-low heat. Melt butter and add onions and rosemary. Cook, stirring only a few times until caramelized, about 20 minutes. Butter one side of each piece of bread. Lay half the cheese on the non-buttered side of one slide of bread. Add half the caramelized onions on top of the cheese. Add the pear slices on top of the onions. Add the remaining cheese slices and then the bacon and remaining onions. Top with remaining slice of bread leaving the buttered side up. Place the sandwich in a large skillet over medium-high heat, press down firmly with a spatula or heavy object to help brown the bread and melt the cheese. Cook on one side for about 3 minutes, flip and do the same on the other side. Cut in half and serve warm.

_Running to the Kitchen_
