---
title: "Instant Pot Barbecue Beef"
date: 2022-09-03T11:29:13+02:00
categories:
  - main course
tags:
  - beef
  - instantpot
---

## Ingredients

- 3-4 lb. beef roast, cut into large pieces
- 2 c. (or more) barbecue sauce
- 1 large onion, sliced
- 1 1/2 c. beef broth

## Preparation

Season the roast pieces with salt and pepper. Put into the instant pot with the onions. Add the broth and 1/2 c. of barbecue sauce.

Seal Instant Pot and cook for 60 minutes on high pressure. Let pressure release naturally for 10 minutes, then release manually.

Remove meat to a cutting board and shred. Strain the liquid, preserving the onions, and return them to the pot with the beef and the remaining barbecue sauce.

_The Typical Mom_
