---
title: "Charbonneau Way"
author: "pencechp"
date: "2014-01-15"
categories:
  - cocktails
tags:
  - whiskey
---

## Ingredients

*   2 oz. rye
*   1/2 oz. maple syrup
*   1/2 oz. lemon juice
*   1/4 oz. Suze (or punt e mes, campari, Fernet, etc.)
*   absinthe

## Preparation

Combine first four ingredients in an ice-filled shaker. Hard-shake, then strain into a cocktail glass that has been rinsed with absinthe.  Garnish with a thyme sprig.

_Sobou, via Spirit Magazine_
