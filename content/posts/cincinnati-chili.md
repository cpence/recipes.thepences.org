---
title: "Cincinnati Chili"
author: "pencechp"
date: "2012-11-05"
categories:
  - main course
tags:
  - chile
  - soup
  - stew
---

## Ingredients

*   2 lbs ground chuck
*   2 medium onions, finely chopped
*   1 quart water
*   2 (8 ounce) cans tomato sauce
*   1/2 teaspoon allspice
*   1 teaspoon garlic powder
*   4 tablespoons chili powder
*   2 teaspoons ground cumin
*   1/2 teaspoon cayenne pepper
*   1/2 teaspoon ground cloves
*   1/2 ounce unsweetened chocolate
*   2 tablespoons cider vinegar
*   1 bay leaf, crumbled
*   2 teaspoons Worcestershire sauce 
*  2 teaspoons ground cinnamon
*  1 teaspoon salt
*  4 drops Tabasco sauce
*  2 teaspoons paprika
*  2 beef bouillon cubes

## Preparation

Bring the water to the boil and add the ground beef. Stir until the beef is separated and add the rest of the ingredients.

Reduce heat and simmer, uncovered, for 2 to 3 hours, or until thickened. Cool, then refrigerate overnight. Skim off any accumulated fat and reheat the chili.

Serves with any or all of the following accompaniments:

- 1 way: cooked spaghetti.
- 2 way: finely grated cheddar cheese
- 3 way: chopped raw onion
- 4 way: cooked kidney beans
- 5 way: oyster crackers

_KelBel, Food.com_
