---
title: "Vietnamese-Style Pork Chops with Fresh Herb Salad"
author: "pencechp"
date: "2016-09-04"
categories:
  - main course
tags:
  - pork
  - vietnamese
---

## Ingredients

*   1 large shallot, chopped
*   3 cloves garlic, chopped
*   1/3 c. light brown sugar
*   1/4 c. fish sauce
*   2 tbsp. soy sauce
*   2 tbsp. vegetable oil
*   2 tsp. pepper
*   4 1/4"-1/2" thick bone-in pork chops
*   3 firm red plums, cut into 1/2" wedges
*   2 green onions, thinly sliced
*   1 small chile (pref. red), thinly sliced
*   2 c. mixed herbs (basil, cilantro, mint, etc.)
*   1/2 c. bean sprouts
*   2 tbsp. rice wine vinegar
*   lime wedges

## Preparation

Blend shallots, garlic, brown sugar, fish sauce, soy sauce, oil, and pepper in a blender. Pour into a ziploc bag with the pork. Toss and refrigerate for at least one hour and up to 12 hours. Prepare a grill for medium-high heat. Remove pork chops, season both sides with salt, and grill until lightly charred (about 2 min. per side). Meanwhile, toss plums, scallions, chile, herbs, bean sprouts, and vinegar in a bowl. Season with salt. Serve pork with salad and lime wedges.

_Bon Appetit, June 2016_
