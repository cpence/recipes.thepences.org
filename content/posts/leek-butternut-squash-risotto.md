---
title: "Leek and Butternut Squash Risotto"
date: 2020-01-27T18:20:43+01:00
categories:
    - main course
    - side dish
tags:
    - squash
    - vegetarian
    - rice
---

## Ingredients

*   1 small butternut squash, halved
*   3 small leeks, cut into rings
*   2 garlic cloves, minced
*   4 slices bacon, diced (optional)
*   1 1/4 c. risotto rice
*   1 c. heavy cream, plus extra if needed
*   1/2 c. Parmesan, grated, plus extra for serving
*   4 c. vegetable stock, warm
*   1/4 c. white wine
*   3 springs thyme
*   6-10 sage leaves
*   olive oil (can be flavored, e.g. thyme)

## Preparation

Add squash halves to a dish, drizzle with olive oil, and season with salt and pepper. Bake for 45 minutes at 375F, or until the squash is soft.

Meanwhile, add the bacon to a pot and render on medium-low heat until crispy. Reserve the bacon bits. Alternatively, replace with oil to make the recipe vegetarian. Fry the sage leaves in oil until crispy, about 1 minute. Set aside.

Add the leeks and cook for 5 minutes, then garlic and cook for 1 minute more. Add rice and thyme, and cook for 2–3 minutes. Deglaze with the wine. Add the vegetable stock, one ladle at a time, making sure that it is mostly absorbed before adding more. Cook until no stock remains, stirring often.

Scoop out the flesh of the squash and add to a food processor with the cream. Blend until smooth and season with more salt and pepper.

When the risotto is almost ready, add the squash and the parmesan. Season again, and add more cream if needed to thin the risotto. Serve topped with sage, bacon, more parmesan, and olive oil.

*Nudo*

