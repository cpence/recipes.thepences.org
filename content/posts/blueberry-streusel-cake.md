---
title: "Blueberry Streusel Cake"
author: "pencechp"
date: "2010-05-08"
categories:
  - dessert
tags:
  - blueberry
  - cake
  - untested
---

## Ingredients

*Cake Batter:*

*   2 3/4 Cups flour
*   1 1/2 tsp. baking powder
*   1 1/2 tsp. baking soda
*   1 tsp. salt
*   3/4 Cup butter, softened
*   1 Cup sugar
*   3 Eggs
*   16 oz. sour cream
*   2 tsp. pure vanilla extract

*Streusel:*

*   3/4 Cup brown sugar
*   3/4 Cup chopped walnuts
*   1 tsp. cinnamon
*   2 Cups blueberries, rinsed and drained

## Preparation

Preheat oven to 375°. Grease and flour a 10 inch tube pan or bundt pan. Combine flour, baking powder, baking soda, and salt in a bowl. Set aside. In a separate bowl with an electric mixer cream the butter and sugar until fluffy. Add eggs one at a time beating well after each addition. Add the flour mixture alternately with the sour cream and vanilla. Mix until blended well. Combine the streusel ingredients except for the blueberries and reserve 1/2 cup. Toss the rest of the streusel with the blueberries. Spread 1/3 of the batter in the pan, top with 1/2 of the berry mixture, another third of the batter, the rest of the berries, and finally the remaining batter. Sprinkle on the reserved 1/2 cup streusel topping. Bake 60-65 minutes or until a toothpick comes out clean. Cool 10 minutes on a wire rack. Carefully turn the pan over onto a serving plate or cake stand and remove from pan. When completely cooled (2 hours or so) sprinkle with powdered sugar.

_Penzey's Spices_
