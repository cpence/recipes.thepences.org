---
title: "Char Siu (Chinese Barbecued Pork)"
author: "pencechp"
date: "2014-11-19"
categories:
  - main course
tags:
  - chinese
  - pork
  - untested
---

## Ingredients

*   1 lb pork (I use boneless chops)
*   3 garlic cloves
*   1 teaspoon salt
*   1/2 teaspoon fresh ginger, minced
*   1 tablespoon soy sauce
*   1 tablespoon honey
*   1 tablespoon wine
*   1/2 teaspoon Chinese five spice powder
*   red food coloring

## Preparation

Trim the fat from the pork and cut into chunks; each chunk should be between 2-4 bites. Blend all remaining ingredients. The pork and marinade should be fairly bright red- add enough food coloring to give it a nice color. Marinate for at least 3 and preferable 24 hours. Broil or grill for 3-4 minutes on a side or until the pork is done through.

_ChrisMc, Food.com_
