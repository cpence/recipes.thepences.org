---
title: "Toffee Almond Bars"
date: 2021-12-21T01:03:30+01:00
categories:
  - dessert
tags:
  - almond
---

## Ingredients

- 1 c. (2 sticks) butter, softened
- 1/2 c. sugar
- 2 c. all-purpose flour
- 1 3/4 c. (8-10 oz.) toffee bits (Skor, Heath, etc.)
- 3/4 c. light corn syrup
- 1 c. sliced almonds
- 3/4 c. coconut flakes

## Preparation

Heat oven to 350F. Grease a 13x9" baking pan.

Beat butter and suar until fluffy. Gradually add flour, beating until well
blended. Press evenly into pan.

Bake 15-20 minutes, or until edges lightly browned. Meanwhile, combine toffee
bits and corn syrup in medium saucepan. Cook over medium heat, stirring
constantly, until toffee is melted (10-12 minutes). Stir in 1/2 c. almonds and
1/2 c. coconut. Spread mixture to within 1/4" of edges of crust. Sprinkle
remaining almonds and coconut over top.

Bake an additional 15 minutes, or until bubbly. Cool completely, and cut into
bars.
