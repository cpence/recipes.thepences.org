---
title: "Sidecar"
author: "pencechp"
date: "2011-04-28"
categories:
  - cocktails
tags:
  - brandy
---

## Ingredients

*   8 parts brandy/Cognac
*   2 parts Cointreau
*   1 part lemon juice

## Preparation

Shake with ice, strain, serve in martini glass.
