---
title: "Steam Buns"
author: "pencechp"
date: "2017-02-21"
categories:
  - main course
tags:
  - chinese
  - pork
  - sandwich
  - untested
---

## Ingredients

*   [pork belly]( {{< relref "pressure-cooker-pork-belly" >}} )
*   1 cup warm water (105-115°F), divided
*   1/2 teaspoon active dry yeast
*   3 tablespoons sugar plus a pinch
*   2 tablespoons nonfat dried milk
*   3 1/2 cups cake flour (not self-rising)
*   1 1/2 teaspoons baking powder
*   Canola oil for greasing and brushing
*   hoisin sauce; thinly sliced cucumber; chopped scallions cucumber scallions (for serving)

## Preparation

Stir together 1/4 cup warm water with yeast and pinch of sugar. Let stand until foamy, 5 to 10 minutes. (If mixture doesn't foam, start over with new yeast.) Whisk in dried milk and remaining 3/4 cup warm water.

Stir together flour and remaining 3 tablespoons sugar in a bowl, then stir in yeast mixture (do not add baking powder yet) with a fork until a dough forms. Knead dough with your hands in bowl until all of flour is incorporated. Turn out dough onto a floured surface and knead, dusting surface and hands with just enough flour to keep dough from sticking, until dough is elastic and smooth but still soft, about 5 minutes. Form dough into a ball.

Put dough in an oiled large bowl and turn to coat. Cover with plastic wrap and let dough rise in a draft-free place at warm room temperature until doubled, about 2 hours.

Punch down dough, then transfer to a lightly floured surface and flatten slightly into a disk. Sprinkle baking powder over center of dough, then gather edges of dough and pinch to seal in baking powder. Knead dough with just enough flour to keep dough from sticking until baking powder is incorporated, about 5 minutes. Return dough to bowl and cover with plastic wrap, then let dough stand 30 minutes.

Cut 16 (3- by 2-inch) pieces of wax paper.

Form dough into a 16-inch-long log. Cut into 16 equal pieces, then lightly dust with flour and loosely cover with plastic wrap. Roll out 1 piece of dough into a 6- by 3-inch oval, lightly dusting surface, your hands, and rolling pin. Pat oval between your palms to remove excess flour, then brush half of oval lightly with oil and fold in half crosswise (do not pinch). Place bun on a piece of wax paper on a large baking sheet and cover loosely with plastic wrap. Make more buns with remaining dough, then let stand, loosely covered, until slightly risen, about 30 minutes.

Set a large steamer rack inside skillet (or wok) and add enough water to reach within 1/2 inch of bottom of rack, then bring to a boil. Carefully place 5 to 7 buns (still on wax paper) in steamer rack (do not let buns touch). Cover tightly and steam over high heat until buns are puffed and cooked through, about 3 minutes. Transfer buns to a plate with tongs, then discard wax paper and wrap buns in kitchen towels (not terry cloth) to keep warm. Steam remaining buns in 2 batches, adding boiling-hot water to skillet as needed.

Return buns (still wrapped in towels) to steamer rack in skillet and keep warm (off heat), covered.

Brush bottom half of each bun with hoisin sauce, then sandwich with 2 or 3 pork slices and some cucumber and scallions.

_Gourmet, October 2007_
