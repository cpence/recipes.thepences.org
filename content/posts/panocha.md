---
title: "Panocha"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - dessert
tags:
  - pence
---

## Ingredients

*   1/2 cup butter
*   1 cup (7 oz.) brown sugar
*   1/4 cup milk
*   2 cups powdered sugar
*   1 cup walnuts or pecans

## Preparation

Melt butter, add brown sugar.  Cook over low heat for two minutes, stirring constantly.  Add milk and continue cooking and stirring until mixture boils.  Remove from heat, cool.  Gradually add powdered sugar until the mixture is of fudge consistency. Stir in nuts and spread in a buttered 8x8x2" pan.  Cool.
