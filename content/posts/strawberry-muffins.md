---
title: "Strawberry Muffins"
date: 2020-06-06T13:46:18+02:00
categories:
    - breakfast
    - bread
tags:
    - muffin
    - strawberry
---

## Ingredients

*   2 c. (240 g) all purpose flour, plus extra
*   2 tsp. baking powder
*   3/4 tsp. salt
*   1/2 c. (4 oz) butter, softened
*   1 c. (200 g) granulated sugar
*   2 eggs
*   1 1/2 tsp. vanilla
*   1/4 tsp. almond extract
*   1/2 c. milk
*   2 1/4 c. diced strawberries (divided)
*   2 tbsp. turbinado sugar

## Preparation

Preheat the oven to 375 F/190 C, and prepare your muffin tin.

Mix together the flour, baking powder, and salt. In another bowl, beat the
butter and sugar for about 2 minutes. Add eggs one at a time, beating well after
each. Add the vanilla and almond extracts. (The batter may be a bit grainy.) Add
the dry ingredients in three additions, alternating with the milk.

Toss the strawberries with 2 tsp. of flour. Set 1/2 c. of them aside, and fold
the rest into the batter. Do not overmix.

Scoop batter into muffin tins. Scatter the reserved berries on top, then
sprinkle with sugar. (Don't let the sugar touch the edges or they will stick.)

Bake for 30 minutes (for normal-size muffin tins, longer for jumbo), until a
toothpick comes out clean. Let cool in the pan before transferring to a rack.


Scoop the batter into the prepared muffin tin (an ice-cream scoop with a wire scraper works well here); they will be very full. Scatter the reserved berries evenly over the muffins, then sprinkle the turbinado sugar over the center of each muffin (keeping the sugar away from the edges will prevent sticking).
Bake for about 30 minutes, until lightly golden and a cake tester comes out clean. Run a sharp knife around the top edge of each muffin to free it from the pan if necessary (the edges can stick), then let the muffins cool in the pan for about 25 minutes. Transfer the muffins to a rack to cool completely.

*Once Upon a Chef*

