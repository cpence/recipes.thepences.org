---
title: "Scallop and Onion Tartes Fines"
author: "pencechp"
date: "2011-02-07"
categories:
  - main course
tags:
  - scallops
  - untested
---

## Ingredients

*   1 sheet frozen puff pastry (about 8 1/2 ounces), thawed
*   4 strips bacon or 1/4 pound pancetta, cut in thin strips
*   1 TBSP butter
*   3/4 lb onions (about 2 medium), thinly sliced
*   S & P
*   1 lb sea scallops
*   olive oil

## Preparation

Preheat oven to 400.

Roll pastry out to a 13" square. \[Recipe says to cut into 4 rounds, using a 6" saucer as a guide - I didn't.\] Line baking sheet with parchment paper, put pastry on it, and prick all over with fork. Top with parchment and a second baking sheet to weigh down dough. Bake for 15 minutes. Carefully remove top baking sheet and parchment. If pastry is not well browned and crisp, return it to the oven, uncovered, until done. (Can be made up to 8 hours in advance and kept uncovered at room temperature.)

Cook bacon or pancetta over medium-low heat until crisp. Drain on paper towel.

Pour out all but 1 TBSP of fat, add butter, onions and season with S & P. Cook until caramelized, about 20 minutes.

If you turned off oven, reheat it to 400.

Thinly slice scallops horizontally into rounds - you'll probably get 3 slices per scallop. Spread onions on pastry all the way to the edges. Top with scallops, slightly overlapping. (If you've made rounds, put scallops on in a circular pattern, so they look like a flower.) Season with S & P and drizzle with a little olive oil.

Bake 3 or 4 minutes (mine took a little longer), just until warm or "a smidgen past raw." The texture is almost custard-like and the flavor wonderful!
