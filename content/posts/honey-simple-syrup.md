---
title: "Honey Simple Syrup"
author: "pencechp"
date: "2015-10-13"
categories:
  - cocktails
tags:
  - honey
---

## Ingredients

*   4 parts honey
*   1 part hot water

## Preparation

Heat honey and water gently in a saucepan over medium-low heat until the mixture reaches a simmer. Remove from heat and cool.
