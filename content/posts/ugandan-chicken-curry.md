---
title: "Ugandan Chicken Curry"
author: "pencechp"
date: "2015-05-14"
categories:
  - main course
tags:
  - african
  - chicken
  - curry
  - untested
---

## Ingredients

*   2 lb. boneless, skinless chicken breasts
*   1 lemon, cut in pieces
*   2 tsp. salt
*   2 tbsp. fresh ginger, minced
*   2 tbsp. olive oil
*   3 medium yellow onions, finely chopped
*   1-2 garlic cloves, pressed
*   2 tbsp. cilantro, chopped
*   1 tsp. corn starch
*   1 c. water
*   1 8 oz. can tomato sauce
*   1 blade fresh lemon grass, gently crushed
*   3 tbsp. coconut butter or 1 tbsp. coconut oil
*   1 large carrot, diced
*   1/2 c. green peas (fresh or frozen)
*   1/2 tsp. thyme
*   1 tbsp. sweet curry power
*   1 tsp. turmeric
*   1/2 tsp. garam masala
*   1/2 tsp. ground cumin
*   1/2 tsp. ground black pepper
*   1/2 tsp. ground white pepper
*   1/2 tsp. salt
*   1/2 tsp. cayenne pepper

## Preparation

Rub the chicken with the lemon pieces. Place the chicken in a large bowl and sprinkle with salt and ginger. Cover and place in the refrigerator for 15-20 minutes. In a small bowl, combine the last 9 ingredients and set aside.

Preheat the oven for 300 degrees. Heat the oil in a skillet over medium-high heat. Add the onions and cook until slightly brown and transparent. Reduce the heat to low and add the garlic and cilantro. Mix the cornstarch with the water and tomato sauce. Add to the skillet along with the spice mix. Let simmer for 20 minutes over medium-low heat. Place the chicken in a casserole dish. Add the coconut butter/oil, lemon grass, carrots, and peas. Pour the curry sauce over the chicken. Bake at 300 for 30 minutes. Reduce the heat to 250 for another 45 minutes or until the chicken is cooked through and very tender. Serve with boiled potatoes or basmati rice.

_Penzey's Spices_
