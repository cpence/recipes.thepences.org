---
title: "Hazelnut-Crusted Chicken with Raspberry Sauce"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - main course
tags:
  - chicken
  - dressing
  - hazelnut
  - raspberry
---

## Ingredients

*Raspberry sauce:*

*  3/4 cup lightly packed fresh raspberries (about 3 1/2 ounces)
*  3 tablespoons white wine vinegar
*  1 tablespoon sugar
*  1/2 cup safflower oil
*  3 to 6 teaspoons water (optional)

*Chicken:*

*  1 cup chopped hazelnuts (about 4 1/2 ounces)
*  3/4 cup panko (Japanese breadcrumbs) or plain dried breadcrumbs
*  1 tablespoon plus 1 teaspoon coarse kosher salt
*  3 teaspoons coarsely ground black pepper, divided
*  1/3 cup honey mustard
*  1/3 cup finely chopped fresh mint leaves
*  1/4 cup mayonnaise
*  1 tablespoon Dijon mustard
*  4 large skinless boneless chicken breast halves, butterflied
*  4 tablespoons (1/2 stick) unsalted butter
*  3 tablespoons peanut oil
*  4 cups baby salad greens
*  1/2 cup fresh raspberries

## Preparation

*For raspberry sauce:* Puree raspberries, white wine vinegar, and sugar in
blender until smooth. With blender running, gradually add safflower oil. Add
water by teaspoonfuls as needed to thin to desired consistency. Season raspberry
sauce to taste with salt and pepper. [JP: This sauce would also work well as a
raspberry vinaigrette.]

*For chicken:* Preheat oven to 375°F. Mix hazelnuts, panko, 1 tablespoon coarse
salt, and 2 teaspoons pepper in shallow bowl.

Mix honey mustard, mint leaves, mayonnaise, Dijon mustard, remaining 1 teaspoon
coarse salt, and remaining 1 teaspoon pepper in bowl. Add chicken; coat, leaving
to marinate for a bit.

Dip chicken pieces, 1 at a time, into crumb-nut mixture, coating both sides and
pressing to adhere. Transfer coated chicken pieces to baking sheet.

Divide equal amounts of butter and peanut oil between 2 large nonstick skillets
(try to use a minimal amount of oil for your skillets); heat over medium-high
heat. Add 2 chicken pieces to each skillet; reduce heat to medium and cook until
chicken is light brown, about 4 minutes per side. Place chicken on rimmed baking
sheet; transfer to oven. Roast chicken until cooked through, about 15 minutes.

Divide salad greens among 4 plates; top with chicken. Garnish with fresh
raspberries and serve raspberry sauce alongside.

_Bon Appetit, July 2008_
