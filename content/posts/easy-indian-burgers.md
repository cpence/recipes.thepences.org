---
title: "Easy Indian Burgers"
author: "juliaphilip"
date: "2010-06-05"
categories:
  - main course
tags:
  - hamburger
  - indian
---

## Ingredients

*   1 1/2 lb ground turkey or lean ground beef
*   2-4 Tbl dried minced white onion
*   1-3 Tbl dried minced garlic
*   1/4 c water
*   1-2 Tbl sweet curry poweder
*   1 tsp dried cilantro
*   1/4 c crunchy peanut butter
*   2 tsp chopped crystalized ginger (optional)

## Preparation

Cover the minced white onion and garlic with the water for 5 minutes. Drain off any excess liquid and combine the onions and garlic with the ground meat, curry powder, and cilantro. Mix well and form 4 patties. Grill or broil just short of your preferred degree of doneness, turning once. Put 1 tbl of crunchy peanut butter atop each patty, cover the grill, and finish grilling or broiling to your desired doneness--at least another minute to soften and warm the peanut butter. Chop the crystalized ginger into small bits and sprinkle on the peanut butter topping for the final minute of cooking if desired.

_Penzey's, Summer 2010_
