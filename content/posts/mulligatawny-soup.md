---
title: "Mulligatawny Soup"
author: "pencechp"
date: "2010-03-28"
categories:
  - main course
tags:
  - chicken
  - indian
  - soup
---

## Ingredients

*   2 pounds chicken thighs
*   3 tbsp. vegetable oil
*   1 med. onion, thinly sliced
*   2 cloves garlic, finely minced
*   1 one-inch piece ginger, peeled and minced
*   1 tbsp. curry powder
*   2 tbsp. water
*   4 c. chicken stock
*   1/2 tsp. salt
*   1 c. unsweetened coconut milk (optional)
*   1/2 c. cooked rice
*   fresh cilantro

## Preparation

Skin, bone, and cut the chicken thighs into bite-sized pieces.  Heat the vegetable oil over medium-high heat in a soup pot.  Add the onion and cook until golden brown,  7-8 minutes.  Add the garlic, ginger, and curry powder, and cook for 30 seconds.  Add the chicken and water and cook, stirring, until the chicken loses its raw color and the oil sizzles and pools around the meat, around 3 to 4 minutes.  Stir in the chicken broth and salt.  Bring to a boil, reduce the heat to medium, and simmer until the chicken is cooked through, 20-30 minutes.  If using, stir in the coconut milk and simmer for 5 minutes more.  Divide the rice among four bowls, ladle the soup on top and garnish with cilantro.

_Joy of Cooking Calendar_
