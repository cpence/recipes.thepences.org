---
title: "Mint Chocolate Truffle Torte"
author: "pencechp"
date: "2011-07-21"
categories:
  - dessert
tags:
  - cake
  - chocolate
---

## Ingredients

*   About 1 tbsp. unsalted butter, softened, for the pan
*   1 tbsp. all-purpose flour, for the pan
*   6 ounces (1-1/2 stick) unsalted butter
*   1/2 cup (gently packed) fresh chocolate mint or peppermint leaves \[CP: we used more, more like 1 cup\]
*   12 ounces premium bittersweet chocolate, chopped
*   6 large eggs, at room temperature
*   6 tbsp. granulated sugar
*   Garnish with powdered sugar

## Preparation

Preparing the pan: Generously butter a 9 inch springform pan and lightly dust the interior with the flour. Turn the pan upside down and bang out the excess flour. Wrap a large square of heavy-duty aluminum foil around the bottom of the pan and partially up the sides. Turn the pan right side up and set it in a shallow baking pan or on a half-sheet pan.

Infusing the butter: Melt the butter in a small saucepan. Stir in the mint leaves and let the butter sit in a warm place for about 30 minutes to absorb the flavor of the leaves.

Butter and chocolate mixture: Preheat the oven to 350 F. Create a double boiler by selecting a medium (10-12") stainless-steel mixing bowl that will rest on top of large (6-quart) pot. The top of the bowl can extend beyond the rim of the pot, but the bottom of the bowl must not touch the water. Put about 2" water in the pot and bring it to a simmer. If the butter has cooled, heat it again to thin it. Pour the butter through a fine sieve into the mixing bowl and press the leaves with the back of a spoon to extract all the butter. Add the chocolate to the bowl and place it over the simmering water. Stir until the chocolate is completely melted, then remove the bowl from the water.

Eggs: Beat the eggs and granulated sugar with an electric mixer on high speed for a full 10 minutes. They should quadruple in volume and become light colored, very thick and fluffy. Fold 1/4 of the egg mixture into the chocolate mixture, then very gently fold in the remaining egg mixture until completely incorporated. Pour the batter into the prepared pan.

Baking: Put the baking pan with the cake on the center oven rack and pour in enough water to come about 1/2" up the sides of the cake pan. Bake until an instant-thermometer inserted in the center of the cake register 155 to 160 F, 25 to 30 minutes. The top of the cake will lose its glossiness and be slightly mounded, but it should not bake so long that it rises and cracks. If you insert a skewer into the center, it should come out gooey. Let the cake cool completely in its pan on a wire rack. Run a thin knife around the edge of the cake and remove the outer ring. The cake will keep tightly covered in the refrigerator for up to 3 days. Bring it to room temperature before serving. Dust the top of the cake with powdered sugar and serve with whipped cream, ice cream or custard sauce.

_Recipe from "The Herbfarm Cookbook," via the GardenWeb Herbs Forum_
