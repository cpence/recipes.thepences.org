---
title: "Southwest Pasta Salad"
author: "pencechp"
date: "2017-10-29"
categories:
  - side dish
tags:
  - pasta
  - salad
---

## Ingredients

*   2/3 c. mayonnaise
*   1/3 c. sour cream
*   1/4 c. cilantro, chopped
*   2 tbsp. milk
*   2 tbsp. lime juice
*   1 jalapeño pepper, seeded and minced
*   1 tsp. salt
*   7 oz bowtie pasta, cooked, rinsed, and drained
*   2 lg. tomatoes, seeded and chopped
*   1 yellow bell pepper, cut into sticks
*   1 zucchini, quartered length-wise and sliced thin
*   3 green onions, thinly sliced
*   pine nuts to taste

## Preparation

In a large bowl, combine the first seven ingredients and mix. Add the rest of the ingredients, toss, and cover. Chill.
