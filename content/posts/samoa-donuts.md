---
title: "Samoa Donuts"
author: "pencechp"
date: "2015-03-13"
categories:
  - breakfast
tags:
  - caramel
  - chocolate
  - coconut
  - donut
  - untested
---

## Ingredients

_For the dough:_

*   1 ¼ cups milk, heated to 110°F
*   2 ¼ teaspoons (one package) active dry yeast
*   2 eggs
*   8 tablespoons (1 stick) butter, melted and cooled
*   1/4 cup granulated sugar
*   1 teaspoon salt
*   4 ¼ cups all-purpose flour, plus more for rolling out the dough
*   2 quarts canola oil

_For the topping:_

*   4 ½ cups shredded sweetened coconut
*   22 ounces soft caramels
*   3 tablespoons milk
*   1/2 teaspoon salt

_For the chocolate glaze:_

*   8 ounces dark chocolate chips
*   1 cup confectioners' sugar
*   1 teaspoon vanilla extract
*   1/8 teaspoon salt
*   1 cup heavy cream
*   1 tablespoon unsalted butter

## Preparation

In the bowl of a stand mixer, combine warm milk with yeast. Stir lightly and let sit until the mixture is foamy, about 5 minutes.

Attach mixer with a dough hook and beat eggs, butter, sugar, and salt into the yeast mixture. Add half of the flour and mix until combined. Gradually add the rest of the flour until the dough pulls away from the sides of the bowl.

Grease a large bowl with a little oil. Transfer the dough to the bowl and cover with plastic wrap. Let rise at room temperature until it doubles in size, about 1 hour.

Turn the dough out onto a floured surface, and roll it out until 1/2-inch thick. Using floured 3½" and 1½" cookie cutters, cut out 6 donuts and holes.

Put the donuts on a parchment-lined baking sheet with plenty of room between each one. Cover with a kitchen towel and let rise in a warm place until puffy, about 45 minutes.

Pour oil in a heavy-bottomed pot or Dutch oven over medium-high heat until it reaches 350°F on an oil thermometer.

Carefully add the donuts to the oil, a few at a time. Fry, flipping once until puffed and golden, about 4 minutes.

Transfer donuts to a wire rack to drain.

Make the topping while donuts cool. Preheat oven to 350ºF. Spread coconut on a rimmed baking sheet and bake for 10-15 minutes, stirring frequently. Remove from oven and set aside to cool.

Melt caramels, milk, and salt in a double-boiler by placing a medium bowl over a smaller pot of simmering water.

Cook, stirring constantly, until the caramels are melted. Remove from heat.

Carefully spread 1 tablespoon of caramel on the top of each donut. Combine the remaining caramel with toasted coconut in a large bowl. Mix well. Spoon 1/4 cup of the coconut mixture on the top of each donut, covering the caramel layer.

Make the chocolate glaze by mixing chocolate, sugar, vanilla, and salt in a bowl. Bring cream and butter to a boil in a small saucepan. Pour cream over chocolate; let sit for 2 minutes without stirring. Whisk until smooth.

Dip the bottom half of each donut in the chocolate glaze and return to wire rack to let excess drip off. Use a fork to drizzle the tops with chocolate. Let donuts sit until the chocolate hardens, about 20 minutes.

_Thrillist_
