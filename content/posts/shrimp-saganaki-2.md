---
title: "Shrimp Saganaki"
author: "pencechp"
date: "2015-11-01"
categories:
  - main course
tags:
  - greek
  - shrimp
  - untested
---

## Ingredients

*   1 ½ lb. shrimp, peeled and deveined, tails left on
*   4 tbsp. extra-virgin olive oil
*   3 tbsp. ouzo (or Pernod)
*   5 cloves garlic, pressed
*   zest of one lemon
*   1 sm. onion, diced medium (about ¾ cup)
*   ½ medium red bell pepper, stemmed, seeded, and diced medium (about ½ cup)
*   ½ medium green bell pepper, stemmed, seeded, and diced medium (about ½ cup)
*   ½ tsp. red pepper flakes
*   28 oz. can diced tomato, drained, ⅓ cup juices reserved
*   ¼ c. dry white wine
*   2 tbsp. coarsely chopped parsley
*   6 oz. feta cheese, crumbled (about 1½ cups)
*   2 tbsp. chopped fresh dill

## Preparation

1\. Toss shrimp, 1 tablespoon oil, 1 tablespoon ouzo, 1 teaspoon garlic, lemon zest, 1/4 teaspoon salt, and 1/8 teaspoon black pepper in small bowl until well combined. Set aside while preparing sauce.

2\. Heat 2 tablespoons oil in 12-inch skillet over medium heat until shimmering. Add onion, red and green bell pepper, and 1/4 teaspoon salt and stir to combine. Cover skillet and cook, stirring occasionally, until vegetables release their moisture, 3 to 5 minutes. Uncover and continue to cook, stirring occasionally, until moisture cooks off and vegetables have softened, about 5 minutes longer. Add remaining 4 teaspoons garlic and red pepper flakes and cook until fragrant, about 1 minute. Add tomatoes and reserved juice, wine, and remaining 2 tablespoons ouzo; increase heat to medium-high and bring to simmer. Reduce heat to medium and simmer, stirring occasionally, until flavors have melded and sauce is slightly thickened (sauce should not be completely dry), 5 to 8 minutes. Stir in parsley and season to taste with salt and pepper.

3\. Reduce heat to medium-low and add shrimp along with any accumulated liquid to pan; stir to coat and distribute evenly. Cover and cook, stirring occasionally, until shrimp are opaque throughout, 6 to 9 minutes for extra-large or 7 to 11 minutes for jumbo, adjusting heat as needed to maintain bare simmer. Remove pan from heat and sprinkle evenly with feta. Drizzle remaining tablespoon oil evenly over top and sprinkle with dill. Serve immediately.
