---
title: "Quinotto"
author: "juliaphilip"
date: "2010-04-26"
categories:
  - main course
  - side dish
tags:
  - quinoa
  - shrimp
---

## Ingredients

*   1 1/8 lb (500 g) quinoa
*   8 1/2 c boiling water
*   4 tbsp butter
*   1 onion, finely chopped
*   5 1/2 oz bacon, diced
*   5 cloves of garlic, finely chopped
*   9 oz mushrooms, sliced into strips
*   1 tsp saffron
*   1 c dry white wine
*   1 c cream
*   7 oz grated parmesan cheese
*   salt
*   pepper
*   nutmeg
*   4 tbsp parsley, chopped

*Optional Ingredients for shrimp quinotto:*

*   1 3/4 c cream
*   3 dozen whole shrimp
*   4 tbsp butter
*   1 c water
*   oil
*   parsley, chopped
*   1 red pepper, chopped

## Preparation

Wash the quinoa and drain well, until the water remains clear and free of dirt. Heat the water or broth (if making shrimp quinotto, cook the quinoa in seafood broth) and cook the quinoa until it is done. Drain and cool.

Put the butter in a large skillet and add the onion, garlic, and bacon. Let this cook for a few minutes over low heat. Add the saffron, the wine, the mushrooms, ground nutmeg, salt, pepper, and cream. Bring to a boil and incorporate the quinoa. Cook until it obtains a sticky texture. Add the parmesan cheese and the chopped parsley. Serve garnished with grated parmesan.

**Optional Shrimp quinoa**

If the quinotto will be prepared with shrimp, clean the shrimp, remove the shells and reserve them. Remove the bodies of the shrimp. Place the tails of the shrimp in a separate bowl. Saute the shells in butter approximately 5 min. Add the tail and water then bring to a boil. Remove from the heat, blend and strain. Return to the heat and add the cream. Reduce to medium heat and cook until it gets the texture of a sauce.

Just before you make the quinotto, heat 10 tbsp of oil in each of two frying pans and sautee the shrimp over high heat. Garnish with parsley and minced pepper.
