---
title: "Vieux Mot"
author: "juliaphilip"
date: "2011-11-20"
categories:
  - cocktails
tags:
  - gin
  - stgermain
---

## Ingredients

*   1 1/2 parts gin
*   3/4 part St. Germain
*   1/4 part simple syrup
*   3/4 part fresh lemon juice

## Preparation

Shake all ingredients with ice, serve.

_St. Germain_
