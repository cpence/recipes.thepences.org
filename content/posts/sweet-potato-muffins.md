---
title: "Sweet Potato Muffins"
author: "pencechp"
date: "2010-12-17"
categories:
  - breakfast
tags:
  - muffin
  - sweetpotato
  - untested
---

## Ingredients

*   1/2 c. butter, softened
*   1 1/4 c. sugar
*   2 eggs
*   1 1/4 c. cooked, mashed sweet potatoes (1 lg. sweet potato, about 1 lb.)
*   1/4-1/3 c. milk
*   1 1/2 c. flour
*   2 tsp. baking powder
*   1/4 tsp. salt
*   1 tsp. cinnamon
*   1/4 tsp. ground nutmeg
*   1/2 c. raisins
*   1/4 c. chopped pecans

_Topping:_

*   2 tbsp. sugar
*   1/4 tsp cinnamon

## Preparation

Preheat oven to 375.  Cream the butter until light and fluffy.  Gradually add the sugar, beating on medium speed.  Add the eggs, one at a time, beating well after each addition.  Stir in the sweet potatoes and milk.  In a separate bowl, combine the flour, baking powder, salt, cinnamon, and nutmeg.  Add to the creamed mixture, stirring just until moistened.  Fold in the raisins and pecans.  Spoon the batter into greased muffin pans, filling 2/3 full.  Mix together the sugar and cinnamon and sprinkle liberally over each muffin.  Bake at 375 for 25-27 minutes.

_Penzey's Muffins_
