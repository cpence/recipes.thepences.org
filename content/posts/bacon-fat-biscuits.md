---
title: "Bacon Fat Biscuits"
author: "juliaphilip"
date: "2017-08-15"
categories:
  - bread
  - breakfast
tags:
---

## Ingredients

*   2 Cups of All Purpose Flour
*   8 Tablespoons (1/2 c) of Bacon Fat (or Butter) (Melted)
*   1 Cup 2% Milk or buttermilk
*   2 Tsp. Baking Powder
*   1 Tsp. Salt
*   1/2 Tsp. Baking Soda

## Preparation

Pre-heat oven to 425F

Mix your dry ingredients together

Mix the melted butter and milk together until clumps of fat form in milk (put in fridge if necessary)

Combine ingredients together in large mixing bowl

Use a greased quarter cup measuring cup to place biscuit batter on baking sheet lined with parchment paper.

Bake in oven until tops are golden brown, about 15 minutes.  Brush tops with melted butter as they cool.

_The Unmanly Chef_
