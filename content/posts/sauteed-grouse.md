---
title: "Sauteed Grouse"
author: "pencechp"
date: "2013-02-10"
categories:
  - main course
tags:
  - game
---

## Ingredients

*   grouse
*   1 tbsp. butter
*   2 tbsp. olive oil
*   salt
*   pepper

_For the caper sauce:_

*   1/4 c. vermouth or dry white wine
*   2 tbsp. capers, roughly chopped
*   1 tbsp. butter

_For the brandy sauce:_

*   1/4 c. brandy or sherry
*   1 shallot, minced
*   2 oz. mushrooms, minced
*   1 tbsp. butter

## Preparation

Fillet off the breasts and legs of the grouse, making sure to separate the breast tender and remove its tendon.  Sautee in the butter and olive oil, being careful not to overcook.  For either sauce, add the requisite ingredients and cook to reduce the alcohol and blend flavors.  Return grouse to sauce and serve.

_Dad_
