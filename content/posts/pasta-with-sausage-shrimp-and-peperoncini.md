---
title: "Pasta With Sausage, Shrimp, And Peperoncini"
author: "pencechp"
date: "2011-03-07"
categories:
  - main course
tags:
  - italian
  - pasta
  - sausage
  - shrimp
---

## Ingredients

*   1 tablespoon olive oil
*   1 pound spicy Italian sausages, casings removed
*   1/2 cup sliced pepperoncini plus 1 cup liquid from jar
*   1/2 cup chopped shallots
*   2 garlic cloves, chopped
*   1 teaspoon smoked paprika
*   1 teaspoon dried thyme
*   1 teaspoon dried oregano
*   1 teaspoon dried basil
*   1/4 teaspoon cayenne pepper
*   1 24-ounce jar marinara sauce
*   3/4 cup heavy whipping cream
*   1 pound uncooked large shrimp, peeled, deveined
*   8 ounces penne pasta
*   3 tablespoons grated Asiago cheese
*   Sliced fresh basil (for garnish)
*   Additional grated Asiago cheese

## Preparation

Heat oil in heavy large pot over high heat. Add sausage; cook until browned, breaking into pieces, about 4 minutes. Add peperoncini, shallots, and next 6 ingredients; cook until shallots are tender, stirring often, about 5 minutes. Add pepperoncini liquid; stir until most of liquid is absorbed, about 1 minute. Add Marinara, cream, and shrimp. Cook until shrimp are cooked through, stirring often, about 5 minutes.

Meanwhile, cook pasta in another large pot of boiling salted water until just tender but still firm to bite, stirring often. Drain, reserving 1/2 cup pasta cooking liquid.

Transfer pasta and 3 tablespoons cheese to pot with sauce; stir to incorporate. Transfer to bowl. Sprinkle with basil and additional cheese.

_Storie Street Grille, Blowing Rock, NC_
