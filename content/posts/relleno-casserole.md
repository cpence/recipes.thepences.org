---
title: "Relleno Casserole"
author: "pencechp"
date: "2010-04-03"
categories:
  - breakfast
  - side dish
tags:
  - chile
  - greenchile
  - pence
---

## Ingredients

*   1 doz. (approx.) green chiles
*   8 oz. grated cheese
*   1 egg
*   1 c. milk
*   1/4 c. flour
*   1/2 tsp. salt

## Preparation

Line the bottom of a 13x9 greased pan with green chiles.  Top with cheese.  Combine egg, milk, flour, and salt.  Pour over chiles.  Bake at 350 for 45 minutes.
