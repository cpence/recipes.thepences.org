---
title: "Boudin-Stuffed Quail"
author: "pencechp"
date: "2015-08-01"
categories:
  - main course
tags:
  - boudin
  - cranberry
  - quail
  - untested
---

## Ingredients

*   Canola oil
*   1 pound ground pork
*   2 ounces chicken liver, chopped
*   1 onion, diced
*   1/2 bell pepper, diced
*   2 stalks celery, diced
*   4 cloves garlic, minced
*   1 cup rice
*   1 pint cranberries
*   1 cup bourbon whiskey
*   3 tablespoons sugar
*   8 whole quail
*   Kosher salt and black pepper
*   Cayenne
*   Chili powder
*   Smoked paprika

## Preparation

Heat 2 tablespoons canola oil in a Dutch oven over medium-high heat. Add the ground pork and chicken liver and cook, stirring occasionally, until browned, about 10 minutes. Add the onion, bell pepper, celery and garlic, and continue to saute for 5 minutes.

Bring 2 cups water and a pinch of salt to a boil in a small saucepan. Add rice, cover, and bring back to a boil. Reduce to a simmer and cook for 20 minutes.

In a small saucepan, combine the cranberries, bourbon and sugar in pan. Bring to a boil and cook, uncovered, over medium heat until the berries are soft and the sugar is dissolved, about 20 minutes. Remove from the heat and let cool bit, then pour into a blender and puree until smooth.

Heat a cast-iron skillet over medium-high heat. Sprinkle the quail with salt, pepper, cayenne, chili powder and smoked paprika. Swirl some canola oil in the skillet and then add the quail. Cook on one side for about 5 minutes; flip and cook for another 5 minutes-the quail won't be fully cooked yet, but that's okay. Set aside.

Mix the cooked rice into the ground pork mixture and season with salt, pepper and cayenne-now you have boudin.

Preheat the oven to 350 degrees F.

Stuff each quail with boudin and place back in the cast-iron skillet. Roast until cooked through, 10 to 15 minutes.

_Jay Ducote, Next Food Network Star_
