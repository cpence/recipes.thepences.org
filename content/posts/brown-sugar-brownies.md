---
title: "Brown Sugar Brownies"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - dessert
tags:
  - cookie
---

## Ingredients

*   1/2 cup butter, room temperature
*   2 cups dark brown sugar
*   2 eggs, well beaten
*   1 cup flour
*   1 tsp baking powder
*   1 tsp vanilla
*   1 cup pecans

## Preparation

Heat oven to 325. Cream softened butter, add sugar and eggs and mix throughly. Add flour and baking powder to bowl and mix well. Add vanilla and pecans. Bake in well greased 8 x 8 pan for 40 minutes. Cool in pan. Cut into small squares.
