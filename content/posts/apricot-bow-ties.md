---
title: "Apricot Bow Ties"
author: "juliaphilip"
date: "2010-12-29"
categories:
  - breakfast
tags:
  - danish
  - pastry
---

## Ingredients

*   1/2 recipe [Danish Pastry Dough]( {{< relref "danish-pastry-dough" >}} )
*   Apricot preserves
*   1 egg, slightly beaten
*   2 tbsp. chopped walnuts mixed with 2 tbsp. sugar

## Preparation

1\. Roll Danish Pastry Dough out on a lightly floured pastry board or cloth to a 20x15-inch rectangle; trim edges even; cut in twelve 5-inch squares with a sharp knife.

2\. Place 1 teaspoon apricot preserves along one side of each square, 1/2 inch in from edge.  Fold over opposite side; press edges together to seal.  With sharp knife, make a lengthwise slit in folded pastry to within 1 inch of each end.  Slip one end under and pull it through the slit.

3\. Place pastries, 2 inches apart, on greased cooky sheet.  Let rise in a warm place, away from draft, until double in bulk, 30 to 45 minutes.  Brush with egg; sprinkle with walnut-sugar mixture.

4\. Place in hot oven (400 degrees); reduce the heat immediately to 350.  Bake 20 minutes, or until puffed and golden-brown.  Remove to wire rack, cool.  Makes 12 individual pastries.

_Family Circle Cookbook_
