---
title: "Korvapuusti (Finnish Cinnamon Rolls)"
author: "pencechp"
date: "2016-08-18"
categories:
  - breakfast
tags:
  - cinnamon
  - finnish
---

## Ingredients

*   500 mL lukewarm milk
*   16 g yeast
*   180 g granulated sugar
*   1 ½ tsp fine sea salt
*   1 tbsp + 1 tsp ground cardamom (preferably freshly ground)
*   1 egg
*   about 1 kg bread flour
*   170 g unsalted butter, at room temperature
*   150 g soft butter
*   6 tbsp granulated sugar
*   1 tbsp + 2 tsp cinnamon
*   1 egg
*   pearl sugar, to sprinkle

## Preparation

Combine the yeast with some flour and add to the warm milk mixture. Stir with a spoon until the yeast is completely dissolved. Add sugar, salt, cardamom, and egg and mix until combined. Gradually add about two thirds of the flour and knead. Add butter and knead until well combined. Continue to knead the dough, and gradually add just enough flour so the dough comes clean off the sides of the bowl and doesn't stick to your hand.

Don't overwork the dough or you'll end up with hard rolls. Shape into a ball and cover with a clean kitchen towel. Let the dough rise in a warm, draft-free place for about 1 hour, or until double in size.

Meanwhile, mix together the butter, sugar, and cinnamon for the filling. Set aside.

Line four baking sheets with parchment paper.

Punch down the dough and divide into two equally sized portions. Lightly dust a clean work surface with flour. Roll out the first portion of dough into a large, about 60-by-40-centimeter / 23-by-16-inch rectangle. Spread half of the filling evenly on top. Beginning with the long side, roll the dough into a tight tube shape, seam side down. Cut into 15 cylinders and press each point tightly into the center with your index finger.

Place the shaped cinnamon rolls on the baking sheets, spacing them about 5 cm / 2" apart. Cover with a clean kitchen towel and let rise for further 30 minutes, or until they're double in size. Repeat with the second batch.

Meanwhile, preheat the oven to 225°C (435°F).

For the egg wash, lightly whisk the egg. Before baking, brush each roll with the egg wash and sprinkle generously with pearl sugar. Bake the rolls on the middle rack for 10 – 15 minutes, or until golden to dark brown in color. When the rolls are done, the bottoms will most probably be dark brown in color; this is totally normal and typical for Nordic cardamom-spiced sweetbread. Repeat with the other sheets of rolls.

The cinnamon rolls are best eaten while still slightly warm or on the same day. However, you can freeze them once baked and warm them up when ready to serve.

_My Blue & White Kitchen_
