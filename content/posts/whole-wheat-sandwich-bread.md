---
title: "Whole Wheat Sandwich Bread"
author: "juliaphilip"
date: "2010-07-10"
categories:
  - bread
tags:
---

## Ingredients

*   1 cup milk
*   1 egg
*   2 Tbsp oil
*   2 Tbsp honey
*   2 Tbsp ground flax seed
*   2 tsp salt
*   360 g whole wheat flour
*   1 Tbsp yeast
*   1 Tbsp gluten

## Preparation

Place all ingredients in the bread machine in the order given. Use whole wheat cycle and 1.5 lb loaf size.

_My Byrd House_
