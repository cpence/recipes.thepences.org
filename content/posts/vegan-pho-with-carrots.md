---
title: "Vegan Pho with Carrots"
author: "pencechp"
date: "2013-03-13"
categories:
  - main course
tags:
  - soup
  - untested
  - vegan
  - vegetarian
  - vietnamese
---

## Ingredients

*   1 recipe [pho broth]( {{< relref "simple-vegetarian-pho-broth" >}} ) (about 2 1/2 quarts)
*   3/4 pound rice noodles, preferably wide ones
*   2 medium or 1 large carrot (about 5 ounces) peeled and cut in 1 1/2-inch julienne
*   2 cups edamame (can use frozen, thawed)
*   6 ounces tofu, cut in matchsticks
*   1/2 cup Asian or purple basil leaves, or Italian basil, slivered
*   4 scallions, chopped, or 1/4 cup chopped chives
*   1 cup chopped cilantro (optional)
*   Several sprigs fresh mint
*   2 cups bean sprouts (mung are traditional) or slivered romaine leaves (chiffonade)
*   2 to 4 bird or serrano chiles, finely chopped (to taste) or 1/4 teaspoon cayenne (more or less to taste)
*   2 to 3 limes, cut in wedges

## Preparation

1\. Have the broth at a simmer in a soup pot. Season to taste with cayenne if you are not using fresh chiles.

2\. Bring a large pot of water to a boil and add the noodles. Cook until just al dente, firm to the bite, following the timing instructions on the package (my wide noodles take about 5 minutes). Drain and divide among 6 large soup bowls.

3\. Add the carrots and edamame to the simmering broth and simmer until just tender, about 3 to 4 minutes. Ladle a generous amount of hot broth with the carrots and edamame into the bowls. Divide the tofu among the bowls. Sprinkle on half the cilantro, half the basil leaves and the scallions or chives. Pass the bean sprouts or lettuce, chopped chiles if using, the remaining basil and cilantro (if using), mint sprigs, and the lime wedges. Serve with chopsticks for the noodles and soupspoons for the broth.

_Serves 6_

_Martha Rose Shulman, New York Times_
