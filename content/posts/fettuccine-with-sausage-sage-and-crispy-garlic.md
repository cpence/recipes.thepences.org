---
title: "Fettuccine with Sausage, Sage, and Crispy Garlic"
author: "pencechp"
date: "2011-04-23"
categories:
  - main course
tags:
  - italian
  - pasta
  - sausage
---

## Ingredients

*   3/4 pound fettuccine
*   2 tablespoons butter, divided
*   1/4 cup olive oil
*   8 garlic cloves, peeled, thinly sliced
*   2 tablespoons finely chopped fresh sage
*   1 pound sweet Italian sausages, casings removed
*   1/4 teaspoon dried crushed red pepper (optional)
*   1 cup grated Asiago cheese (about 3 ounces)

## Preparation

Meanwhile, melt 1 tablespoon butter with oil in heavy large skillet over medium heat. Add garlic slices and sauté until light golden, about 45 seconds. Using slotted spoon, transfer garlic to bowl.

Increase heat to medium-high; add sage to same skillet and stir until beginning to crisp, about 10 seconds. Add sausage and sauté until browned and crisp in spots, breaking up with fork, about 8 minutes.

Drain pasta; add pasta and remaining 1 tablespoon butter to skillet. Using tongs, toss pasta with sausage mixture. Add crushed red pepper, if desired; season to taste with salt and pepper. Transfer pasta to large bowl. Top with crispy garlic and grated Asiago cheese.

_Bon Appetit, March 2008_
