---
title: "Whole Wheat Hamburger/Hot Dog Buns"
author: "juliaphilip"
date: "2010-08-10"
categories:
  - bread
tags:
  - hamburger
  - hot dog
---

## Ingredients

*   1 cup water
*   2 tbsp. canola oil
*   1 egg
*   1/4 cup honey
*   3 1/4 cups (390 g) whole wheat flour
*   1/4 cup wheat gluten
*   1 teaspoon salt
*   1 tbsp. instant yeast or active dry yeast

## Preparation

Place all ingredients in your bread machine. Select ‘dough’ cycle. Allow cycle to run.

Dump out onto lightly floured surface.

Divide into 8 pieces.

Shape each piece into a hot dog or hamburger bun.

Place on greased cookie sheets (or bun pans), cover; let rise 30 to 40 minutes.

Bake in preheated 375 degree oven for 12 to 15 minutes until golden.

Cool on wire racks.
