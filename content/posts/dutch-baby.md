---
title: "Dutch Baby"
author: "pencechp"
date: "2016-11-25"
categories:
  - breakfast
tags:
  - pancake
---

## Ingredients

*   3 eggs
*   ½ cup flour
*   ½ cup milk
*   1 tablespoon sugar
*   Pinch of nutmeg
*   4 tablespoons butter
*   Syrup, preserves, confectioners' sugar or cinnamon sugar

## Preparation

Preheat oven to 425 degrees. Combine eggs, flour, milk, sugar and nutmeg in a blender jar and blend until smooth. Batter may also be mixed by hand. Place butter in a heavy 10-inch skillet or baking dish and place in the oven. As soon as the butter has melted (watch it so it does not burn) add the batter to the pan, return pan to the oven and bake for 20 minutes, until the pancake is puffed and golden. Lower oven temperature to 300 degrees and bake five minutes longer. Remove pancake from oven, cut into wedges and serve at once topped with syrup, preserves, confectioners' sugar or cinnamon sugar.

_New York Times_
