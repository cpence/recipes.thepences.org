---
title: "Momofuku's Crack Pie"
author: "pencechp"
date: "2010-02-27"
categories:
  - dessert
tags:
  - pie
  - untested
---

Total time: 1 1/2 hours, plus cooling and chilling times Servings: Makes 2 pies (6 to 8 servings each)

Note: Adapted from Momofuku. This pie calls for 2 (10-inch) pie tins. You can substitute 9-inch pie tins, but note that the pies will require additional baking time, about 5 minutes.

## Cookie for crust

*   2/3 cup plus 1 tablespoon (3 ounces) flour
*   Scant 1/8 teaspoon baking powder
*   Scant 1/8 teaspoon baking soda
*   1/4 teaspoon salt
*   1/2 cup (1 stick) softened butter
*   1/3 cup (2 1/2 ounces) light brown sugar
*   3 tablespoons (1 1/4 ounces) sugar
*   1 egg
*   Scant 1 cup (3 1/2 ounces) rolled oats

Heat the oven to 375 degrees.

In a medium bowl, sift together the flour, baking powder, baking soda and salt.

In the bowl of a stand mixer using the paddle attachment, or in a large bowl using an electric mixer, beat the butter, brown sugar and sugar until light and fluffy. Whisk the egg into the butter mixture until fully incorporated.

With the mixer running, beat in the flour mixture, a little at a time, until fully combined. Stir in the oats until incorporated.

Spread the mixture onto a 9-inch-by-13-inch baking sheet and bake until golden brown and set, about 20 minutes. Remove from heat and cool to the touch on a rack. Crumble the cooled cookie to use in the crust.

## Crust

*   Crumbled cookie for crust
*   1/4 cup (1/2 stick) butter
*   1 1/2 tablespoons (3/4 ounce) brown sugar
*   1/8 teaspoon salt

Combine the crumbled cookie, butter, brown sugar and salt in a food processor and pulse until evenly combined and blended (a little of the mixture clumped between your fingers should hold together).

Divide the crust between 2 (10-inch) pie tins. Press the crust into each shell to form a thin, even layer along the bottom and sides of the tins. Set the prepared crusts aside while you prepare the filling.

## Filling

*   1 1/2 cups (10 1/2 ounces) sugar
*   3/4 cup plus a scant 3 tablespoons (7 ounces) light brown sugar
*   1/4 teaspoon salt
*   1/3 cup plus 1 teaspoon milk powder
*   1 cup (2 sticks) butter, melted
*   3/4 cup plus a scant 2 tablespoons heavy cream
*   1 teaspoon vanilla extract
*   8 egg yolks
*   2 prepared crusts
*   Powdered sugar, garnish

Heat the oven to 350 degrees.

In a large bowl, whisk together the sugar, brown sugar, salt and milk powder. Whisk in the melted butter, then whisk in the heavy cream and vanilla. Gently whisk in the egg yolks, being careful not to add too much air.

Divide the filling between the shells.

Bake the pies, one at a time, for 15 minutes, then reduce the heat to 325 degrees and bake until the filling is slightly jiggly and golden brown (similar to a pecan pie), about 10 minutes. Remove the pies and cool on a rack.

Refrigerate the cooled pies until well chilled. The pies are meant to be served cold, and the filling will be gooey. Dust with powdered sugar before serving.

_Momofuku_
