---
title: "Seared Cod with Spicy Mussel Aioli"
date: 2020-10-18T16:19:01+02:00
categories:
    - main course
tags:
    - fish
    - mussels
    - untested
---

## Ingredients

* 1/3 cup dry white wine
* 2 dozen mussels
* 1/2 cup mayonnaise
* 2 garlic cloves, minced
* 2 tablespoons chopped parsley
* 2 teaspoons fresh lemon juice
* Cayenne pepper
* Salt and freshly ground black pepper
* 1 tablespoon vegetable oil
* Four 6-ounce skinless cod fillets
* 1 roasted red bell pepper, cut into thin strips

## Preparation

1. Bring the wine to a boil in a medium saucepan. Add the mussels, cover and
   cook over high heat, shaking the pan a few times, until they open, 3 minutes;
   transfer to a large bowl. Shell the mussels and reserve. Rinse out the pan.
   Pour the mussel liquid into the pan, stopping before you reach the grit at
   the bottom.
2. In a small bowl, mix the mayonnaise with the garlic, parsley and lemon juice.
   Season with cayenne, salt and pepper.
3. Heat the oil in a large skillet. Season the cod with salt and black pepper
   and cook over moderate heat until lightly browned, about 4 minutes per side.
4. Bring the mussel liquid to a boil and remove from the heat. Whisk in the
   mayonnaise, then stir in the mussels. Transfer the cod to shallow bowls and
   spoon the mussels and sauce around it. Garnish with the red pepper strips and
   serve.

*Food and Wine*

