---
title: "Strawberry Pie"
author: "pencechp"
date: "2017-09-17"
categories:
  - dessert
tags:
  - pie
  - strawberry
  - untested
---

## Ingredients

*   4 pints (~3lb) fresh strawberries
*   3/4 c.sugar
*   2 tbsp. cornstarch
*   1 1/2 teaspoons Sure-Jell low-sugar pectin (pink box)
*   pinch salt
*   1 tbsp. lemon juice
*   1 pie shell, baked

## Preparation

Select 6 oz. of less pretty berries. Blend to smooth puree. Whisk sugar, cornstarch, Sure-Jell, and salt in medium saucepan. Stir in berry puree. Cook over medium-high heat, stirring constantly. Boil, scraping bottom and sides of pan to prevent scorching, for 2 minutes to ensure that cornstarch is fully cooked. Transfer to large bowl and stir in lemon juice. Let cool to room temperature.

Measure 2 lb. of pretty beries; halve only extra-large berries. Add berries to bowl with glaze and fold gently until berries are evenly coated. Scoop berries into pie shell, piling into mound. Refrigerate pie until chilled, about 2 hours. Serve within 5 hours of chilling.

_CI_
