---
title: "Venison Marinade"
author: "pencechp"
date: "2011-09-20"
categories:
  - miscellaneous
tags:
  - marinade
  - venison
---

## Ingredients

*   1/3 cup red wine
*   ¼ cup olive oil
*   1 tsp sugar
*   pinch of allspice
*   2 cloves garlic, minced
*   ½ small onion, chopped
*   1 tbsp Worcestershire sauce
*   1 tbsp Soy Sauce
*   1 tsp dried thyme
*   1 tsp salt
*   1 tsp ground black pepper
*   1 tsp Dijon mustard
*   4 venison steaks

## Preparation

Marinate for at least 6 hours. Place on very hot grill, cook each side 2-3 minutes, placing careful attention not to overcook.

_Arkansas Outdoors Online_
