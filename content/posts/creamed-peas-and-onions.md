---
title: "Creamed Peas and Onions"
author: "juliaphilip"
date: "2014-11-23"
categories:
  - side dish
tags:
  - onion
  - peas
  - vegetarian
---

## Ingredients

*   2 cups whole milk
*   3 whole cloves
*   1 bay leaf
*   1 pound pearl onions (about 2 cups), peeled, trimmed, or 1 pound frozen pearl onions, thawed
*   3 tablespoons unsalted butter, divided
*   1½ teaspoon kosher salt, divided
*   1 teaspoon sugar
*   2 tablespoons all-purpose flour
*   ½ teaspoon freshly ground black pepper
*   ¼ teaspoon freshly grated nutmeg
*   2 pounds bags frozen peas, thawed
*   ¼ cup crème fraîche, whole-milk Greek yogurt, or sour cream

## Preparation

Bring milk, cloves, and bay leaf to a boil in a small saucepan; reduce heat to low and simmer for 15 minutes to let flavors meld. Strain into a medium bowl; discard solids. Cover milk and keep warm. Meanwhile, place onions in a large skillet (at least 12”). Add water to half-cover the onions (about 1 cup). Add 1 Tbsp. butter, 1 tsp. salt, and sugar. Cook over medium-high heat, stirring occasionally, until liquid has evaporated and onions are golden brown, about 12 minutes. Gently transfer onions to a large plate. Melt remaining 2 Tbsp. butter in same skillet over medium heat; add flour and whisk to combine. Cook, stirring occasionally and scraping up browned bits from bottom of pan, for 1 minute. Whisk in reserved milk, adding ¼-cupful at a time. Cook, stirring constantly, until thickened and bubbly. Stir in remaining ½ tsp. salt, pepper, and nutmeg. Add peas and onions; stir until heated through. Gently fold in crème fraîche.

_Bon Appetit, November 2012_
