---
title: "Caramel Apple Cinnamon Roll Pie"
author: "pencechp"
date: "2015-05-06"
categories:
  - breakfast
tags:
  - cinnamon
  - pie
---

## Ingredients

*   2 tubes canned cinnamon rolls with icing (one regular and one caramel)
*   2 small apples, gala or honey crisp
*   1/4 cup caramel sauce, optional

## Preparation

Preheat oven to 400 degrees F.

Remove cinnamon rolls from the packaging and lightly flour your work surface. Using a rolling pin, roll out each cinnamon roll until flat enough to roll up, about double the original size. Place each cinnamon roll, seam side down, into a pie dish and form a circle. Add two additional circles (depending on your pie pan size), ending with one single cinnamon roll in the middle.

Peel and slice 2 small apples and push slices in between the gaps of each cinnamon roll circle. Cover with tin foil and bake until the cinnamon rolls are nearly cooked through, possibly as long as 30 minutes. Uncover and brown. \[CP: This is a tweak to the recipe, which didn't begin to cook all the way through the first time we made it; we haven't tried it this way yet. Edit me to add times when we cook it again.\]

Meanwhile, spoon regular icing into a microwave-safe bowl and heat in the microwave for 10 seconds or until warm and slightly runny. Drizzle over the warm pie. Repeat with the caramel icing. If you can't find cinnamon rolls that come with caramel icing, simply combine the regular icing with 1/4 cup caramel sauce and heat in a microwave-safe bowl for 10 seconds as well. Stir to combine and drizzle over the top.

_Deliciously Yum_
