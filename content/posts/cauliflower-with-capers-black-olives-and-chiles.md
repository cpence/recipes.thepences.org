---
title: "Cauliflower with Capers, Black Olives, and Chiles"
author: "pencechp"
date: "2016-02-24"
categories:
  - side dish
tags:
  - cauliflower
---

## Ingredients

*   About 1 pound of cauliflower
*   ½ cup pitted black olives, coarsely chopped
*   1 heaped tablespoon salt-packed capers, rinsed and drained
*   2 tablespoons finely chopped flat-leaf parsley
*   Grated zest and juice of a lemon
*   Sea salt
*   Pinch of crushed red-chile pepper
*   2 garlic cloves, crushed and chopped
*   ⅓ cup olive oil

## Preparation

Trim the cauliflower, and break the head apart into florets.

On a chopping board, combine the olives, capers, parsley and lemon zest, and chop together to mix well.

Bring a pot of water large enough to hold the cauliflower to a rolling boil. Add a big pinch of salt, and when it returns to a boil, add the florets. Cook until the florets are just barely tender, about 6 minutes.

Meanwhile, in a skillet large enough to hold all the ingredients, warm the chile flakes and the garlic in the oil over medium-low heat until hot, about 3 or 4 minutes. The chile and garlic should be starting to melt in the oil, rather than sizzling and browning. Stir in the lemon juice, and cook for another 2 minutes, then add the olive-caper mix, give it a stir, take it off the heat and set aside.

Drain the cauliflower well, shaking the colander. Combine the cauliflower with the olive-caper dressing in the skillet, and set the skillet back on medium heat. Warm it up to serving temperature, taste to make sure the seasoning is right and serve.

_The New York Times_
