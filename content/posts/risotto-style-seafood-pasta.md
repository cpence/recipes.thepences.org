---
title: "Risotto-Style Seafood Pasta"
author: "pencechp"
date: "2010-12-07"
categories:
  - main course
tags:
  - crab
  - pasta
  - shrimp
  - untested
---

## Ingredients

*   3 tablespoons olive oil
*   1 ½ cups dried gemelli pasta (or fusilli, orrechiette, really any pasta that is non-noodle-like. Different pastas may require different amounts of liquid/cooking time.)
*   1 small yellow onion, finely minced
*   2 leeks, white and light green parts only, thinly sliced
*   2 garlic cloves, finely minced
*   ½ cup white wine
*   1 quart fish stock (you can use water if necessary)
*   ½ pound lump crab meat
*   12 shrimp, cleaned and deveined
*   juice of half a lemon
*   2 tablespoons lemon zest
*   1 tablespoon red chili flakes
*   2 handfuls of arugula

## Preparation

Heat olive oil over medium heat until shimmering.

Add pasta and toast, stirring frequently, until it just begins to turn light brown.

Add onion, leeks, and garlic cloves (along with a pinch of salt and a few grinds of pepper), cooking until translucent, but with no color, about three minutes.

Add white wine and cook until almost entirely absorbed, about two minutes.

Add stock, a ladleful at a time, stirring until absorbed. Continue until you have about one more ladle to add and the pasta still has a little more bite than you want. (Don't worry.)

Add the crabmeat, shrimp, and final ladleful of stock, along with the lemon juice, lemon zest, and chili flakes. Cook until shrimp is pink and pasta has absorbed the stock and has the right bite, approximately three minutes. Check for seasoning.

Add arugula right at the end. Toss until it begins to wilt slightly, and serve immediately.

_Alain Ducasse, The Atlantic_
