---
title: "Avgolemono Soup"
author: "pencechp"
date: "2011-05-18"
categories:
  - side dish
tags:
  - greek
  - lemon
  - soup
---

## Ingredients

*   12 cups chicken broth
*   5 TBSP lemon juice
*   zest of 1 lemon
*   12 egg yolks
*   salt and white pepper to taste
*   thinly sliced lemon

## Preparation

Heat the chicken broth until it is scalding hot. Add the lemon juice and zest. Whisk egg yolks, then gradually whisk in some of the hot broth. Slowly return to broth, lower heat, and stir constantly while warming and thickening. Serve immediately, garnished with the lemon slices.
