---
title: "Sausage Balls"
author: "pencechp"
date: "2010-04-02"
categories:
  - appetizer
tags:
  - pence
  - sausage
---

## Ingredients

*   1 lb. hot breakfast sausage
*   10 oz. sharp grated cheese
*   2 1/2 to 3 c. Bisquick

## Preparation

Let sausage and cheese stand at room temperature until soft.  Mix thoroughly.  Add bisquick and knead.  Scoop w/ teaspoon, and roll into balls.  Place on cookie sheet.  Cook at 350 degrees for 15 to 20 minutes, or until brown.

May be frozen on a cookie sheet and kept in plastic bag until ready for use.
