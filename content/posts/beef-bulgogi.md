---
title: "Beef Bulgogi"
author: "pencechp"
date: "2013-11-26"
categories:
  - main course
tags:
  - beef
  - korean
---

## Ingredients

*   1/4 cup low-sodium soy sauce
*   2 tablespoons sugar
*   1 tablespoon Asian (toasted) sesame oil
*   1 tablespoon mirin (Japanese sweet rice wine)
*   1/4 Asian pear, coarsely grated (about 1/4 cup)
*   1/2 medium onion, coarsely grated (about 1/2 cup)
*   2 cloves garlic, minced (about 1 teaspoon)
*   1/2 teaspoon fresh ginger, peeled and finely grated
*   1 teaspoon sesame seeds, toasted, plus additional for garnish
*   1/4 teaspoon freshly ground black pepper
*   1 pound beef sirloin, trimmed of excess fat and thinly sliced
*   2 tablespoons vegetable oil
*   8 red leaf or bibb lettuce leaves, for serving
*   1 cup cooked white rice, for serving
*   1/2 cup kimchi, for serving
*   1 cup fresh enoki mushrooms, trimmed
*   4 teaspoons hot bean paste

## Preparation

In large bowl, whisk together soy sauce, sugar, sesame oil, mirin, pear, onion, garlic, ginger, sesame seeds, and pepper. Let marinade stand 30 minutes at room temperature, then add beef and toss to coat. Refrigerate, covered, 1 hour.

In large skillet over moderately high heat, heat oil. Remove beef from marinade, draining it very briefly over bowl to remove excess liquid, and then cook until browned and done medium-well, 6 to 7 minutes.

Remove beef from heat and serve by filling each lettuce leaf with about 2 tablespoons Korean sticky rice, small handful of beef, 1 tablespoon kimchi, about 8 enoki mushrooms, and about 1/2 teaspoon hot bean paste. Serve immediately.

_Epicurious, January 2009_
