---
title: "Campari and IPA Spritzer"
author: "pencechp"
date: "2014-06-10"
categories:
  - cocktails
tags:
  - beer
  - campari
  - untested
---

## Ingredients

*   1 1/2 ounces Campari
*   1/2 bottle (6 ounces) IPA beer
*   Lemon twist, to garnish (optional)

## Preparation

Fill a glass with ice. Add Campari. Slowly pour in the beer. If garnishing with the lemon, squeeze the lemon peel over the drink to release the oils, and rub around the rim of the glass. Drop garnish into the glass and serve.

_The Kitchn_
