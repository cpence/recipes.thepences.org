---
title: "Bon Bon Chicken"
author: "pencechp"
date: "2013-03-21"
categories:
  - main course
tags:
  - chicken
  - chinese
  - untested
---

## Ingredients

*   1 ½ pounds boneless, skinless chicken breasts
*   ¼ cup Shaoxing rice wine (or sherry)
*   2 green onions, chopped
*   1 inch fresh ginger, chopped
*   2 1/2 teaspoons whole black, white, or Sichuan peppercorns
*   1 pound cucumbers, peeled
*   3 tablespoons soy sauce
*   1 tablespoon Chinkiang black vinegar
*   1 tablespoon sesame oil
*   2 teaspoons chili oil
*   1 garlic clove, minced
*   2 teaspoons sugar
*   2 tablespoons finely chopped fresh cilantro

## Preparation

Place the chicken in a large pot. Add the wine green onions, 3/4 of the ginger, 1 teaspoon of the Sichuan peppercorns, and enough water to cover by 2 inches. Turn heat to high and bring to a boil. Immediately reduce heat to a simmer, cook for 3 1/2 minutes. Cover the pot, turn off the heat, and let sit for 30 minutes. Remove the chicken from the pot and let cool for a few minutes. Then shred the chicken with your fingers.

Quarter the cucumbers and scoop out the seeds. Then chop into 2 inch lengths. Cut these into 1/4-inch thick sticks.

Combine the soy sauce, vinegar, sesame oil, chili oil, the rest of the Sichuan peppercorn, rest of the ginger, garlic, sugar, and cilantro in a blender. Process until smooth.

Scatter the cucumber pieces on a plate. Top with the shredded chicken, and pour on the sauce. Garnish with more cilantro.

_Nick Kindlesperger, Serious Eats_
