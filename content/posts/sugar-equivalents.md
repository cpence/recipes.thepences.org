---
title: "Sugar Equivalents"
date: 2020-07-16T19:19:40+02:00
categories:
    - cocktails
---

Equivalent replacement table for bar sugars:

*   simple syrup ↔ table sugar
    *   1/4 to 1/2 fl. oz. simple syrup = 1 tsp. table sugar
    *   2 to 4 tsp. table sugar = 1 fl. oz. simple syrup
*   table sugar ↔ light candi syrup
    *   1 g table sugar = 1.64 g candi syrup
    *   1 g candi syrup = 0.61 g table sugar
    *   1 tsp. table sugar = 4.68 mL / 0.16 oz candi syrup
*   simple syrup ↔ light candi syrup
    *   1 fl. oz. simple syrup = 1/3 to 2/3 fl. oz. candi syrup
    *   1 oz candi syrup = 2 to 3 fl. oz. simple syrup

Weight/volume conversions:

*   table sugar
    *   1 tsp. table sugar = 4 g table sugar
    *   1 g table sugar = 1/4 tsp. table sugar
*   candi syrup
    *   1 mL candi syrup = 1.4 g candi syrup
    *   1 g candi syrup = 0.71 mL candi syrup


