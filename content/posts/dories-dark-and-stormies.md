---
title: "Dorie's Dark and Stormies"
author: "pencechp"
date: "2010-05-09"
categories:
  - dessert
tags:
  - chocolate
  - cookie
  - untested
---

## Ingredients

*   1 1/4 c. flour
*   1/3 c. cocoa powder
*   1/2 tsp. baking soda
*   1 stick plus 3 tbsp. butter, at room temperature
*   2/3 c. packed brown sugar
*   1/4 c. sugar
*   1 tsp. vanilla
*   1/2 tsp. sea salt
*   5 oz. bittersweet chocolate, chopped into small bits

## Preparation

1\. Sift the flour, cocoa and baking soda together in small bowl; set aside.  Beat the butter until smooth in bowl of an electric mixer at medium speed.  Add the sugars, vanilla and salt; beat 2 minutes.  Reduce speed to low; add the flour mixture, mixing until incorporated but still crumbly, and being careful not to overwork the dough.  Stir in the chocolate pieces.

2\. Turn the dough out onto a smooth work surface; squeeze it so that it sticks together in large clumps. (If you need to, it's okay to lightly flour the work surface.)  Gather the dough into a ball; divide in half.  Shape each ball into a log 1 1/2 inches in diameter.  Wrap logs in plastic wrap; chill at least 1 hour.

3\. Heat oven to 325 degrees.  Line two baking sheets with parchment.  Gently slice logs into 1/2-inch rounds using a serrated knife (some will crumble; simply press broken bits back onto cookie).  Place 1 inch apart on the baking sheets.

4\. Bake, one sheet at a time, 14 minutes; cookies will not look done or be firm.  Cool on pan 5 minutes; transfer to cooling rack.  Cool to room temperature.
