---
title: "Sweet Potato, Kale, and Lentil Soup"
author: "pencechp"
date: "2017-03-30"
categories:
  - main course
tags:
  - greens
  - lentil
  - soup
  - sweetpotato
  - untested
---

## Ingredients

*   1 tablespoon olive oil
*   1 onion, chopped
*   3 garlic cloves, minced
*   2 sweet potatoes, cubed
*   1 teaspoon each cinnamon, nutmeg, cayenne pepper
*   1 tablespoon curry powder
*   1/2 tablespoon each black pepper and Himalayan pink sea salt
*   32 ounces vegetable broth
*   1 cup red lentils
*   4 kale or turnip green leaves, ribs removed and chopped

## Preparation

In a dutch oven or soup pot, heat olive oil on medium/ high. Add onions and cook for about 3-5 minutes or until transparent. Add garlic and cubed sweet potatoes. Stir until ingredients are incorporated and cook for 5 minutes. Add seasonings and stir well. Pour in vegetable broth and bring to a boil. Once boiling, stir and reduce to simmer. Add lentils. Cover and cook for about 20 minutes, stirring occasionally. Remove lid and add in kale. Cook for an additional 5 minutes until kale has wilted a bit but not lost its vibrant color. Remove from heat and serve warm.

_Edible Nashville_
