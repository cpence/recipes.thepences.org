---
title: "Pinto Bean Mole Chili"
author: "juliaphilip"
date: "2010-04-03"
categories:
  - main course
tags:
  - chile
  - stew
  - vegan
  - vegetarian
---

## Ingredients

*   2 medium dried ancho chiles, wiped clean
*   1 dried chipotle chile, wiped clean
*   1 teaspoon cumin seeds, toasted and cooled
*   1 teaspoon dried oregano, crumbled
*   Rounded 1/8 teaspoon cinnamon
*   2 medium onions, chopped
*   2 tablespoons olive oil
*   4 garlic cloves, finely chopped
*   3 medium zucchini and/or yellow squash, quartered lengthwise and cut into 1/2-inch pieces
*   3/4 pound kale, stems and center ribs discarded and leaves coarsely chopped
*   1 teaspoon grated orange zest
*   1/8 teaspoon sugar
*   1 ounce unsweetened chocolate, finely chopped (3 tablespoons)
*   1 (14 1/2-ounce) can whole tomatoes in juice, drained, reserving juice, and chopped
*   1 1/4 cups water
*   3 (15-ounce) cans pinto beans, drained and rinsed
*   Accompaniments: rice; chopped cilantro; chopped scallions; sour cream

## Preparation

Slit chiles lengthwise, then stem and seed. Heat a dry heavy medium skillet over medium heat until hot, then toast chiles, opened flat, turning and pressing with tongs, until pliable and slightly changed in color, about 30 seconds. Tear into small pieces.

Pulse cumin seeds and chiles in grinder until finely ground. Transfer to a small bowl and stir in oregano, cinnamon, and 1 1/2 teaspoons salt.

Cook onions in oil in a large heavy pot over medium-high heat, stirring occasionally, until softened. Add garlic and cook, stirring, 1 minute, then add chile mixture and cook, stirring, 30 seconds. Stir in zucchini and kale and cook, covered, 5 minutes. Add zest, sugar, chocolate, tomatoes with their juice, and water and simmer, covered, stirring occasionally, until vegetables are tender, about 15 minutes.

Stir in beans and simmer 5 minutes. Season with salt.

_Gourmet, November 2007_
