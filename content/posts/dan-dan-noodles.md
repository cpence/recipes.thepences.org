---
title: "Dan Dan Noodles"
author: "pencechp"
date: "2011-10-03"
categories:
  - main course
tags:
  - chinese
  - pasta
  - pork
  - untested
---

## Ingredients

*   8 oz. Shanghai-style noodles (cu mian) or udon
*   2 Tbsp. vegetable oil
*   12 oz. ground pork
*   salt and pepper
*   2 Tbsp. chopped peeled ginger
*   3/4 c. chicken stock
*   2 Tbsp. (or less) chili oil \[this is where to control the heat\]
*   2 Tbsp. red wine vinegar
*   2 Tbsp. soy sauce
*   2 tsp. tahini
*   1 tsp. Szechuan peppercorns
*   Pinch sugar
*   2 Tbsp. chopped roasted peanuts
*   2 Tbsp. thinly sliced scallions

## Preparation

Cook noodles in a large pot of boiling water until just tender but still firm to the bite.  Drain; transfer to a large bowl of ice water and let stand until cold.  Drain well and divide between 2 bowls.

Heat vegetable oil in a medium skillet over medium heat.  Add pork, season with salt and pepper, and stir, breaking up pork with a spoon, until halfway cooked, about 2 minutes.  Add ginger; cook until pork is cooked through and lightly browned, about 2 minutes.  Stir in chicken stock and next 6 ingredients; simmer until sauce thickens, about 7 minutes.  Pour pork mixture over noodles; garnish with peanuts and scallions.

Serves 2.

_Peter Chang's Tasty 2, via Bon Appetit_
