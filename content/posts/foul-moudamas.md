---
title: "Foul Moudamas"
author: "pencechp"
date: "2017-07-20"
categories:
  - main course
tags:
  - bean
  - mediterranean
  - vegan
  - vegetarian
---

## Ingredients

*   2 15 ounce cans cooked small fava beans
*   1 tablespoon ground cumin
*   4 cloves garlic, mashed
*   ½ cup lemon juice
*   ¼ cup olive oil
*   1 medium red onion, finely chopped
*   2 ripe tomatoes, diced
*   1 bunch parsley, finely chopped
*   salt and pepper to taste

## Preparation

Pour the cooked fava beans with the liquid into heavy saucepan. Add the mashed garlic, the cumin, the salt and the pepper. Bring to a boil. Using a potato masher, mash the fava beans partially and cook over medium heat for 10 minutes. Add the lemon juice, the olive oil and half of the chopped vegetables. Stir, adjust the seasoning and remove from the heat. Spoon the foul moudamas into shallow serving dish and top with the rest of the chopped vegetables.

_Sanaa Cooks_
