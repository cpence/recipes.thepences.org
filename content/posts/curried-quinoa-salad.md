---
title: "Curried Quinoa Salad"
author: "pencechp"
date: "2010-05-08"
categories:
  - main course
  - side dish
tags:
  - quinoa
  - salad
  - vegetarian
---

## Ingredients

*   2 c. water
*   1 c. quinoa
*   2 tsp. curry powder or garam masala
*   8 green onions, chopped
*   2 15 oz. cans chickpeas, rinsed and drained
*   2 c. shredded carrots
*   1/2 c. dried cranberries
*   Yogurt-cumin dressing, recipe below

## Preparation

Heat the water to boil in a large saucepan.  Add the quinoa and curry powder; lower heat to a simmer.  Cover pan; cook 20 minutes.  Meanwhile, combine the green onions, chickpeas, carrots and cranberries in a large bowl; arrange on top of quinoa, or stir all ingredients together.  Serve drizzled with dressing.

**Yogurt-cumin dressing:** Combine 1 c. plain non-fat yogurt, 1/2 c. chopped fresh cilantro, 3 tsp. lemon juice, 1 tsp. each grated lemon zest and hot chili sauce, and 1/4 tsp. ground cumin in a small bowl.  Refrigerate before serving.

You can substitute a 4-oz. piece of cooked salmon, tuna, or other fish for the chickpeas.  Also, the dressing is optional.

_Chicago Tribune_
