---
title: "Negroni and the Goat"
date: 2020-11-10T14:45:21+01:00
categories:
    - cocktails
tags:
    - gin
---

## Ingredients

*   1.5 oz gin
*   0.75 oz Aperol
*   0.75 oz sweet vermouth
*   0.5 oz Averna

## Preparation

Stir with ice, strain into an ice-filled glass. Garnish with a twist of orange
zest.

*Cocktail Collective*

