---
title: "The Smoking Gun"
author: "pencechp"
date: "2013-01-04"
categories:
  - cocktails
tags:
  - fernet
  - whiskey
---

## Ingredients

*   2 oz. Islay scotch
*   3/4 tsp. Fernet Branca
*   1/2 tsp. brown sugar simple syrup
*   2 dashes bitters

## Preparation

Shake over ice, strain into small cocktail or scotch glass.

_Mark Allen, Red Feather Lounge, Boise, ID, via Imbibe Magazine_
