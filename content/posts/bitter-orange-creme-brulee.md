---
title: "Bitter Orange Crème Brûlée"
author: "pencechp"
date: "2014-02-24"
categories:
  - dessert
tags:
  - custard
  - orange
---

## Ingredients

*   2 cups/480 ml heavy (whipping) / double cream
*   1 cup/240 ml whole milk
*   Grated zest from two oranges
*   12 large egg yolks
*   1/2 cup/100 g sugar, plus 6 tbsp/75 g
*   Sprigs of fresh mint or chocolate mint, for garnish

## Preparation

Preheat the oven to 300°F/150°C/gas 2.

In a medium saucepan, combine the cream, milk, and orange zest and heat until steam begins to rise. Do not let boil. Remove from the heat and nestle the pot in an ice bath. Let stand, stirring occasionally, until the cream mixture cools to room temperature, 5 to 10 minutes.

While the cream mixture is cooling, in a large bowl, combine the egg yolks and the 1/2 cup/100 g sugar. Whisk until the sugar is dissolved and thoroughly blended with the yolks. Gently whisk in the cream mixture.

Pour the custard through a fine-mesh sieve set over a large glass measuring pitcher or bowl with a pouring lip to strain out any solids. Divide the custard evenly among six 4-oz/120-ml ramekins. Place in a roasting pan/tray and add water to come 1 in/2.5 cm up the sides of the ramekins. Bake until the custards are firm, 35 to 40 minutes. Remove from the oven and let cool in the water bath to room temperature. Cover with plastic wrap and refrigerate until well chilled, at least 2 hours and up to 2 days.

To serve, remove the plastic wrap/cling film and gently lay a paper towel/absorbent paper on top of each custard. Gently press down on the towel to remove any moisture buildup, being careful not to dent the custard. Sprinkle 1 tbsp sugar evenly over each custard. Using a blowtorch, pass the flame above the sugar until it melts and turns golden brown. (Alternatively, preheat the broiler/grill and slip the custards under the broiler 4 to 6 in/10 to 15 cm from the heat source to melt the sugar; leave the oven door open slightly and watch closely, as the sugar can scorch suddenly.) Let the crème brûlée stand at room temperature until the sugar hardens, 1 to 2 minutes.

_Epicurious, October 2010_
