---
title: "Roasted Eggplant, Tomato, Garlic, and White Bean Spread"
author: "pencechp"
date: "2011-05-06"
categories:
  - appetizer
tags:
  - bean
  - dip
  - eggplant
  - mediterranean
  - untested
---

## Ingredients

*   1 medium eggplant, peeled, cut into thick chunks
*   1 head fresh garlic, peeled
*   5-6 tomatoes (smaller, vine-ripe), quartered and seeds removed
*   1/4 c. olive oil
*   3 tbsp. balsamic vinegar
*   1/2-1 tsp. salt (optional)
*   1/4 tsp. pepper
*   2 tbsp. Penzey's Tuscan Sunset or other spice blend
*   1 16-oz. can white beans
*   1 tsp. chili powder
*   1/2 tsp. oregano (pref. Mexican)
*   2 tbsp. lemon juice

## Preparation

Preheat oven to 375. Place the vegetables in a shallow baking dish. Add the oil, vinegar, salt, pepper, and spice blend. Toss well to coat. Carefully spread into a single layer and place the pan in the oven. Roast for 45 minutes. Note: Vegetables can also be roasted on a pan in a gas grill. Cool vegetable mixture, or let rest overnight in the fridge.

Rinse and drain the white beans and place in a blender with 2 c. of the vegetable mixture. The vegetables should have exuded quite a bit of liquid so make sure to include some of this in the 2 cups. Blend the beans and vegetables. Add the chili powder and oregano and the lemon juice. Blend well and pour into a bowl. Serve with any firm cracker.

_Penzey's Spices_
