---
title: "Pickled Black-Eyed Peas"
author: "pencechp"
date: "2010-08-10"
categories:
  - side dish
tags:
  - bean
---

## Ingredients

*   One recipe [black-eyed peas]( {{< relref "black-eyed-peas" >}} ), or 3 16-ounce cans, rinsed and drained
*   1/2 small green bell pepper, minced (about 1/2 cup)
*   1/2 small red bell pepper, minced (about 1/2 cup)
*   4 scallions including green parts, sliced thin
*   1/2 cup extra-virgin olive oil
*   1/4 cup red-wine vinegar
*   1 garlic clove, minced
*   1 teaspoon minced seeded habanero or other fresh hot chili, or to taste (wear rubber gloves)

## Preparation

In a bowl, combine all ingredients with salt and pepper to taste and toss well. Chill mixture, covered, for at least 5 hours and up to 2 days.

Serve black-eyed peas chilled or at room temperature.

_Gourmet, February 1995_
