---
title: "Farro Salad with Green Apple, Toasted Spices & Pine Nuts"
author: "juliaphilip"
date: "2010-04-30"
categories:
  - side dish
tags:
  - apple
  - farro
  - vegan
  - vegetarian
---

## Ingredients

*   6 cups water
*   1 1/3 cups farro
*   ¾ teaspoon kosher salt, divided
*   ½ cup pine nuts
*   1 ½ teaspoons coriander seeds
*   ¼ teaspoon fennel seeds
*   3 tablespoons extra-virgin olive oil, divided
*   1 medium onion, coarsely chopped
*   3 tablespoons vinegar, such as aged sherry or white wine
*   ½ cup chopped Italian parsley
*   1 medium Granny Smith apple, cored and diced
*   3 tablespoons lemon juice
*   ½ teaspoon freshly ground black pepper

## Preparation

Bring water to a boil in a 2-quart saucepan. Add farro, cover and reduce heat to a simmer. Cook 20 minutes, add ½ teaspoon salt and cook an additional 15 to 20 minutes. Farro should be tender. Drain well through a colander, then spread onto a large plate lined with several layers of paper towels. Set aside about 10 minutes.

Spread pine nuts in a large skillet and place over medium heat. Set timer for 5 minutes and toast, shaking pan occasionally, until nuts are lightly browned. Transfer to a cutting surface and chop coarsely. Place pan back on heat. Add coriander and fennel seeds and toast until grant. Transfer to a mortar and crush lightly with a pestle. Or spread on a cutting board and crush with the side of a chef's knife.

Pour 1 tablespoon olive oil into the skillet and heat over medium heat. Add onions and cook about 5 minutes or until translucent. Add ¼ teaspoon salt, increase heat slightly and cook uncovered until golden brown, about 5 minutes longer. Stir in vinegar and cook until reduced by half. Add remaining 2 tablespoons olive oil and the reserved spices. Remove from heat.

Place farro in a bowl. Add nuts, onion mixture, parsley, apple and 2 tablespoons lemon juice. Toss to coat, seasoning with a little more lemon juice and fresh ground black pepper. Serve warm or at room temperature.

_Seattle Times_
