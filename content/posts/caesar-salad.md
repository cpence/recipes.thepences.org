---
title: "Caesar Salad"
author: "pencechp"
date: "2011-10-10"
categories:
  - side dish
tags:
  - dressing
  - salad
---

## Ingredients for Breadcrumbs

*   4 cups cubed, day old/stale Italian or French bread
*   1/4 to 1/3 cup extra virgin olive oil
*   4 cloves garlic - minced, or 1 tablespoon garlic paste
*   1 tablespoon fresh, or 1 teaspoon dried oregano
*   1/2 teaspoon salt

## Preparation for Breadcrumbs

Preheat the oven to 350 degrees Fahrenheit.  Cut the bread into 1 to 2 inch cubes.  Place the cubed bread into a large mixing bowl.  In a small bowl, whisk together the oil, garlic, oregano and salt.  Toss the bread cubes with the olive oil mixture, until all the bread is well coated.  Place the seasoned bread cubes onto a baking sheet.  Bake in the oven for 10 to 15 minutes, until golden brown, and crisp.

## Ingredients

*   3-4 teaspoons minced garlic or 1 tablespoon garlic paste
*   4 anchovy fillets, finely chopped
*   1 teaspoon capers, drained
*   1 teaspoon grainy Dijon mustard
*   2 tablespoons lemon juice
*   1 teaspoon Worcestershire sauce
*   1 egg, room temperature
*   1/3 cup extra virgin olive oil
*   Freshly cracked black pepper
*   Grated Parmesan cheese
*   2-3 heads Romaine lettuce, chopped

## Preparation

Place the garlic and anchovies into a mortar, and grind into a fine paste with a pestle. Add the capers, and mustard and continue grinding until a smooth paste is formed. Use a spatula to scrape the paste out of the mortar, and place it in a mixing bowl. Whisk in the lemon juice and Worcestershire sauce.

Place the egg in a large coffee mug, or other heat proof container. Pour boiling water over the egg, and let sit for 3 minutes. Drain the hot water off the egg, and cover with cold water. Whisk the coddled egg yolk into the mixing bowl with the other Caesar dressing ingredients (discard the egg white).

Slowly whisk in the olive oil, until well blended. The dressing should have a consistency which is just a bit thicker than heavy cream.

Add the Romaine lettuce to a large salad bowl. Toss with a bit of the Caesar salad dressing, then garnish with Parmesan cheese, freshly cracked black pepper and a few of the homemade breadcrumbs. The salad should be well coated, but not drowning, with dressing.

_Cuisine Diva_
