---
title: "Garlic Soup"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - side dish
tags:
  - philip
  - soup
---

## Ingredients

*   2 tbs olive oil (approximately)
*   4 large cloves peeled garlic, whole
*   ½ loaf French bread, thinly sliced
*   1 tbs paprika
*   1 liter chicken, beef, or vegetable broth
*   2 eggs

## Preparation

Brown the garlic whole in the olive oil in a large pot, then remove the garlic and chop it finely.  Brown the bread in the same oil that the garlic was cooked in.  Chop the bread into small (crouton-sized) pieces and add the garlic and bread back into the pot and add the broth, then bring to a simmering boil.  Beat the eggs and drizzle in.

Serves 4
