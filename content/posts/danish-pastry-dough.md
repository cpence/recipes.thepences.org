---
title: "Danish Pastry Dough"
author: "juliaphilip"
date: "2010-12-29"
categories:
  - breakfast
  - dessert
tags:
  - danish
  - pastry
  - pence
---

## Ingredients

*   2 envelopes (4 1/2 tsp.) active dry yeast
*   1/2 c. very warm water
*   1/3 c. sugar
*   3/4 c. cold milk
*   2 eggs
*   4 1/4 c. sifted all-purpose flour
*   1 tsp. salt
*   1 lb. (4 sticks) butter
*   flour

## Preparation

1\. Sprinkle yeast into very warm water in a 1-cup measure.  (Very warm water should feel comfortably warm when dropped on wrist.)  Stir in 1/2 teaspoon of the sugar.  Stir until yeast dissolves.  Let stand until bubbly and double in volume, about 10 minutes.

2\. Combine remaining sugar, milk, eggs, 3 cups of the flour, salt and yeast mixture in large bowl.  Beat, with electric mixer at medium speed, for 3 minutes (or beat 300 vigorous strokes by hand).  Beat in remaining flour with a wooden spoon until dough is shiny and elastic.  Dough will be soft.  Scrape down sides of bowl.  Cover with plastic wrap.  Refrigerate dough 3o minutes.

3\.  Place the sticks of butter 1 inch apart, between 2 sheets of waxed paper; roll out to a 12-inch square.  Chill on a cooky sheet until ready to use.

4\. Sprinkle working surface heavily with flour, about 1/3 cup; turn dough out onto flour; sprinkle flour on top of dough.  Roll out to an 18x12-inch rectangle.  Brush off the excess flour with a soft pastry brush.

5\. Peel off top sheet of waxed paper from butter; place butter paper-side up, on one end of dough to cover two-thirds of dough; peel off the remaining sheet of waxed paper.  For easy folding, carefully score butter lengthwise down center. without cutting into dough.  Fold uncovered third of dough over middle third of dough and butter; brush off excess flour; then fold remaining third of dough and butter over middle third to enclose butter completely.  Turn the dough clockwise so open side is away from you.

6\. Roll out to 24x12-inch rectangle using enough flour to keep the dough from sticking.  Fold ends in to meet in center; then fold in half to make 4 layers.  Turn again so open side is away from you.  Repeat rolling and folding 2 more times.  Keep the dough to a perfect rectangle by rolling straight up and down and from side to side.  (When it is necessary, chill the dough between rollings.  Clean off the working surface each time and dust lightly with flour.)

7\. Refrigerate dough 1 hour or longer (or even overnight, if you wish, to relax dough and firm up the butter layers).  When you are ready to fill and bake the pastry, cut dough in half and work with only half the recipe at a time.  Keep the other half refrigerated until ready to use.  Makes 2 large pastries, or about 24 individual pastries, or 1 large pastry and 12 individual pastries.

## Tips

1\. It is important to keep butter enclosed in dough.  If it oozes out, immediately sprinkle with flour.  If dough becomes too sticky to handle, butter has probably softened.  Just chill 30 minutes before continuing rolling and folding.

2\. Use more flour than you normally would to roll out pastries; brush off excess with soft brush before folding or filling; this way flour will not build up in pastry.

3\. Since dough is very rich, it is best to let pastries rise at room temperature.  Do not try to hasten the rising by using heat; doing so would melt the butter and spoil the texture of the pastry.

4\. Have ready a rolling pin, a soft pastry brush, ruler and a working surface large enough to roll dough to 30 inches.

For freezing: Place shaped Danish on cooky sheets.  Don't glaze or decorate until ready to bake.  Cover with foil or plastic wrap, freeze.  Use within 4 weeks.

To bake: Remove the Danish from the freezer the night before and place in refrigerator.  Next morning, let rise at room temperature, away from draft, until double in volume, about 1 1/2 hours.  Brush with egg; sprinkle with topping; bake following individual recipes.

For refrigerating: Place the shaped Danish on cooky sheets; cover; refrigerate.  Bake within 48 hours.  To bake: Remove Danish from refrigerator; let rise; bake as above.

_Family Circle Cookbook_
