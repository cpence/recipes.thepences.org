---
title: "Honey Chili Beer Chicken"
author: "pencechp"
date: "2014-01-05"
categories:
  - main course
tags:
  - chicken
  - untested
---

## Ingredients

*   3 tbs olive oil
*   ½ cup sliced sweet white onions
*   2 cloves garlic, minced
*   1 cup brown ale, divided in half
*   1 tbs balsamic vinegar
*   2 tbs honey
*   ½ tsp red chili sauce (such as Sriracha) plus additional if desired
*   6 boneless skinless chicken thigh fillets
*   1/2 tsp salt
*   1 tsp pepper
*   1-2 tbs flour

## Preparation

In a cast iron skillet over medium heat add the olive oil. Add the onions and caramelize over medium heat until golden brown, about 10 to 15 minutes.

Stir in the garlic then add ½ cup brown ale, balsamic vinegar, honey and chili sauce. Simmer until reduced and thickened, remove sauce from pan and set aside.

Sprinkle the chicken thighs on all sides with salt, pepper and flour.

Increase heat to medium-high, cook the chicken thighs until browned on all sides, about 3 minutes per side.

Pour the sauce back into the pan along with the remaining ½ cup brown ale.

Cover loosely with a lid, lower heat to maintain a simmer and allow to cook until chicken is cooked through, about an additional 10 minutes. Turning once during cooking.

_The Beeroness_
