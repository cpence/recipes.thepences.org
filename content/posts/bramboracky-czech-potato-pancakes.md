---
title: "Bramboracky (Czech Potato Pancakes)"
author: "pencechp"
date: "2018-12-26"
categories:
  - side dish
tags:
  - potato
---

## Ingredients

*   4 large potatoes
*   3 cloves garlic, crushed
*   salt and pepper
*   1 pinch (or more) dried marjoram (optional)
*   2 tsp. caraway seeds
*   2 eggs
*   1 tbsp. milk
*   3 tbsp. flour
*   oil for frying

## Preparation

Peel and coarsely grate the potatoes, squeezing out as much liquid as you can. Transfer the shredded potatoes to a mixing bowl. Stir in the crushed garlic, salt, pepper, marjoram, and caraway seeds. Beat the eggs with the milk. Add the egg mixture to the potatoes and stir well to combine. Gradually mix in the flour.

Heat the oil in a skillet over medium-high heat; the oil should be about 1/4-inch deep. Spoon about 1/4 cup of batter into the hot oil, flattening it slightly. Fry the pancake until golden brown, about 3 minutes on each side. Drain on paper towels. Taste the first pancake and adjust the seasoning if necessary; repeat with remaining batter.

_AllRecipes, edited_
