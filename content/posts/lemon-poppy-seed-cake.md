---
title: "Lemon Poppy Seed Cake"
date: 2020-09-28T12:07:34+02:00
categories:
    - dessert
    - breakfast
    - bread
tags:
    - lemon
    - untested
---

## Ingredients

*   1 c. whole wheat pastry flour (or all-purpose)
*   1 package (2 1/4 tsp., 1/4 oz., 7g) yeast
*   3/4 c. sugar (divided)
*   1/2 tsp. salt
*   1/2 c. warm milk
*   6 tbsp. butter
*   4 eggs, at room temperature
*   1 1/4 c. all-purpose flour
*   1/4 c. Greek yogurt or sour cream
*   2 tsp. vanilla extract
*   1/2 tsp. almond extract
*   zest of 2 lemons
*   2 tbsp. poppy seeds
*   juice of 2 lemons + 1 tbsp.
*   1 c. powdered sugar
*   1 tbsp. milk

## Preparation

Grease and flour a 10-cup bundt or tube pan. Set aside.

In the bowl of a standing mixer fitted with the paddle attachment or a large
mixing bowl, combine the whole wheat pastry flour, yeast, 1/4 c. of the sugar,
and salt.

In a microwave-safe bowl or saucepan, heat the milk and butter to 120 to 130°F.
Stir the liquid together well before taking the temperature, and for best
results, use a digital read thermometer. The liquid should feel hotter than
lukewarm, but not so hot that it is uncomfortable or burns.

Add the milk mixture to the flour and beat on medium speed for 2 minutes. Add
the eggs, all purpose flour, Greek yogurt, vanilla extract, almond extract,
lemon zest, and poppy seeds. Beat on high for 2 minutes.

Spread the dough/batter evenly into the prepared pan. Cover and let rise in a
warm, draft-free place for 1 hour. Towards the end of the cake's rising time,
place a rack in the center of the oven and preheat the oven to 375°F.

Bake the cake for 20 to 25 minutes, until toothpick inserted into the center
comes out clean. Remove from the oven and let cool in the pan for 5 minutes,
then turn out onto a wire rack.

While the cake is baking, make the lemon soak: Place the juice of your two
lemons and the other 1/2 c. sugar in a small saucepan. Heat over medium until
the sugar dissolves, stirring occasionally. Let stand at room temperature while
the cake bakes. Once the cake is turned out of the pan, pour half of the soak
all over the outside of the warm cake, let absorb for 30 seconds, then pour the
remaining half over.

To prepare glaze: whisk together the powdered sugar, lemon juice and milk in a
small bowl, until smooth. Drizzle glaze over cooled cake and serve.

*Red Star Yeast*

