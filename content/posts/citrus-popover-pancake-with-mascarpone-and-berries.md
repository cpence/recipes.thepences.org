---
title: "Citrus Popover Pancake with Mascarpone and Berries"
author: "juliaphilip"
date: "2010-07-15"
categories:
  - breakfast
tags:
  - pancake
---

## Ingredients

*   3/4 cup milk
*   1/2 cup flour
*   2 large eggs
*   2 tablespoons sugar
*   1 teaspoon vanilla extract
*   1 teaspoon finely chopped lemon zest
*   1 teaspoon finely chopped orange zest
*   2 tablespoons unsalted butter
*   2 tablespoons softened mascarpone, to garnish
*   Powdered sugar, to garnish
*   2 cups fresh sliced strawberries or 1 bag (12 ounces) frozen mixed berries, defrosted, to garnish

## Preparation

Heat the oven to 450 degrees. Combine the milk, flour, eggs, sugar, vanilla and citrus zests in a food processor fitted with the metal blade, a blender, or in a bowl, and whisk or process until smooth.

Place the butter in a 10-inch pie plate or ovenproof skillet and put it in the oven to melt, for about 3 minutes. Swirl the inside of the pan to coat it evenly with butter.

Pour the batter into the pan and bake for 15 minutes. Reduce the heat to 350 degrees and bake for about 15 minutes longer or until the pancake is nicely browned, cooked in the center and well puffed. Remove from the oven and slide onto a round platter, using a spatula. Spread the pancake with the mascarpone and sprinkle powdered sugar generously on top. Accompany with the berries and serve immediately.

Makes 2 to 4 servings.
