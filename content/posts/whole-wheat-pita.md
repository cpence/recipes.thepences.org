---
title: "Whole-Wheat Pita"
author: "pencechp"
date: "2010-05-08"
categories:
  - bread
tags:
  - mediterranean
---

## Ingredients

*   A bit more than 1-1/8 c. warm water (but exactly 1-1/8 c. if you use all-purpose flour)
*   3 c. whole wheat pastry flour (or all-purpose flour)
*   1 t. salt
*   1 T. oil
*   1-1/2 t. white sugar
*   1-1/2 t. active dry yeast

## Preparation

Mix all ingredients together in a large bowl.

Knead by hand for about 8 minutes (or let the KitchenAid do it for you).

Collect dough into a large lump in the bowl. Cover the bowl with a kitchen towel. Place covered bowl in a warm-ish area and leave until the dough has risen to about twice its original size.

When sufficiently puffy, turn the dough out onto a lightly floured surface. Gently roll and stretch dough into a 12 inch rope. Using a sharp knife, cut the dough rope into 8 pieces. Roll each piece into a smooth ball, then use a rolling pin to flatten each ball into a 6 or 7 inch diameter circle.

Set dough circles onto a lightly floured surface. Cover with a kitchen towel and let rise for about 30 minutes. After the 30 minutes, they should be just slightly puffier than they were at first.

At some point during the time when the dough circles are rising, preheat the oven to 500 degrees F. When dough circles are ready, place 2 or 3 circles on a wire cake rack, and place rack directly on an oven rack. Bake for 4 to 5 minutes. Pitas are done when they are puffed and their tops are slightly brown.

Remove pitas from oven and place them in a sealed brown paper bag (or just cover them with a damp kitchen towel) until they are soft. Once softened, cut pitas as desired.

Pitas can be stored in some kind of sealed container for several days. You can also freeze them for 1-2 months.

_Erica Freeman_
