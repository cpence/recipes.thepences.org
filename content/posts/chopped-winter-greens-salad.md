---
title: "Chopped Winter Greens Salad"
date: 2022-11-28T13:12:06+01:00
categories:
  - side dish
tags:
  - salad
  - vegetarian
  - pear
---

## Ingredients

- 1/3 cup extra-virgin olive oil
- 3 tablespoons apple cider vinegar
- 3 tablespoons fresh orange or tangerine juice
- 2 tablespoons date syrup (silan), maple syrup or honey
- 1 tablespoon finely chopped shallot
- 1/2 teaspoon kosher salt
- 1/4 teaspoon freshly ground black pepper, plus more as needed
- 2 Bosc pears, halved, cored and thinly sliced
- 1/3 cup (1 ounce) dried, pitted dates, thinly sliced
- 7 cups (5 ounces) sturdy greens, such as Belgian endive, frisée, radicchio or arugula
- 1/2 cup (1 ounce) loosely packed fresh flat-leaf parsley leaves
- 1/2 cup (2 ounces) shelled, raw pistachios, toasted
- 4 ounces ricotta salata, shaved with a Y-shaped vegetable peeler (about 2 cups)

## Preparation

1. In a medium bowl, whisk together the olive oil, vinegar, juice, silan (or maple syrup or honey) and shallot until blended. Season with 1/2 teaspoon salt and 1/4 teaspoon pepper.

2. Place the pears and dates in a large bowl; make sure the date slices aren’t sticking together. Drizzle the vinaigrette over the fruit mixture and let stand for 10 minutes.

3. Add the greens and parsley, and gently toss to coat. Season with the remaining 1/4 teaspoon salt and with a bit more pepper, if desired.

4. Divide the salad evenly among 6 plates. Garnish with pistachios and shaved ricotta salata and serve.

_WaPo_
