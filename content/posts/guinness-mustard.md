---
title: "Guinness Mustard"
author: "juliaphilip"
date: "2010-03-31"
categories:
  - miscellaneous
tags:
  - irish
---

## Ingredients

*   1/2 c coarse-grained Dijon mustard
*   2 tbl regular Dijon mustard
*   2 tbl Guinness stout or other stout or porter
*   1 tbl minced shallot
*   1 tsp golden brown sugar

## Preparation

Whisk all ingredients together in small bowl to blend. Cover and refrigerate at least 2 hours. Can be made 2 days ahead, keep refrigerated.
