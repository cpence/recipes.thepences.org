---
title: "Fried Eggplants With Honey"
date: 2021-07-10T11:41:49+02:00
categories:
  - side dish
tags:
  - spanish
  - eggplant
  - untested
---

## Ingredients

- 4 Japanese eggplants (1 1/2 lb. total), trimmed and cut crosswise, on a bias,
  1/4" thick
- 2 tbsp. kosher salt
- 2 1/2 c. whole milk
- canola oil, for frying
- 1 c. all-purpose flour
- sea salt
- 3 tbsp. honey
- finely grated lemon zest, for garnish

## Preparation

Place the eggplant in a single layer on a rack set over a large rimmed baking
sheet and season with the kosher salt. Top with another baking sheet and press
firmly. Let stand for 20 minutes. Press again to help release the juices from
the eggplant. Transfer the slices to a large bowl and cover with the milk. Cover
and refrigerate for at least 2 hours or overnight.

In a large enameled cast-iron casserole, heat 1 inch of oil to 350°. In a
shallow bowl, spread the flour. Remove the eggplant from the milk, shaking off
any excess liquid. Dredge in the flour and shake off the excess. Working in
batches, fry the eggplant, turning a few times, until golden brown, about 3
minutes. Transfer the eggplant to a paper towel–lined baking sheet and
immediately season with sea salt.

Transfer the eggplant to a serving platter, drizzle with the honey and garnish
generously with lemon zest. Sprinkle with more sea salt and serve immediately.

_José Andrés, Food & Wine_
