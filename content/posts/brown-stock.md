---
title: "Brown Stock"
date: 2021-08-09T18:15:03+02:00
categories:
  - miscellaneous
tags:
  - sauce
  - french
  - untested
---

## Ingredients

- 5 lb. beef bones, veal bones, or feet, cut into 3-4" pieces
- 1/2 lb. onions, roughly chopped
- 1/4 lb. carrots, roughly chopped
- 1/4 lb. celery, roughly chopped
- 6 oz. tomato paste
- 5 qt. cold water
- 1 tbsp. dried thyme
- a few fresh parsley stems
- a bay leaf
- several whole peppercorns
- several whole cloves

## Preparation

Arrange the bones in a heavy roasting pan, and drizzle with a bit of vegetable
oil. Roast them in a hot oven (400 F) for around an hour. They should be
moderately browned. Add the onions, carrots, and celery, and continue roasting
for around 20 minutes. Add the tomato paste, stir, and roast for another 10
minutes.

Remove the bones to a stockpot. Deglaze the roasting pan with a bit of water.
Add the cold (important!) water to the stockpot, along with the mirepoix and the
deglazing liquid from the roasting pan. Add the spices, tied together in a
bundle of cheesecloth.

Bring the pot to a boil, then lower it to a simmer. Simmer for 4 to 6 hours,
skimming fat and scum from the surface every so often. The goal is to end with 4
qt. of stock for every 5 qt. of water that you started with, so partially cover
the pot and/or add more water to maintain that level.

Strain through cheesecloth and chill.

_Spruce Eats_
