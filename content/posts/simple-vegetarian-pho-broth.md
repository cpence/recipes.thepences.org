---
title: "Simple Vegetarian Pho Broth"
author: "pencechp"
date: "2013-03-13"
categories:
  - main course
tags:
  - soup
  - untested
  - vegan
  - vegetarian
  - vietnamese
---

## Ingredients

*   1 large onion (about 1/2 pound), peeled and quartered
*   1 3-inch piece of fresh ginger
*   3 quarts water
*   1 pound leeks (1 1/2 large), tough ends cut away, halved lengthwise, cleaned and cut in thick slices (including the dark green parts, just discarding the very ends)
*   2 medium turnips (about 10 ounces), peeled and cut in wedges
*   1 pound carrots (3 large), peeled and sliced thick
*   2 ounces mushroom stems (from about 8 ounces mushrooms), or 4 dried shiitakes
*   1 head of garlic, cut in half
*   2 stalks lemon grass, trimmed, smashed with the side of a knife, and sliced
*   Salt to taste
*   1 to 1 1/2 tablespoons sugar (to taste), preferably raw brown sugar
*   6 star anise pods
*   5 whole cloves
*   1 tablespoon black peppercorns
*   A 2- to 3-inch cinnamon stick
*   1 to 2 tablespoons fish sauce (nuoc mam), to taste (optional)

## Preparation

1\. Scorch the onion and ginger by holding the pieces above a flame with tongs, or in a dry frying pan if using an electric stove. Turn the pieces until they are scorched black in places on all sides. Slice the ginger lengthwise.

2\. Combine the scorched onion and ginger with the water, leeks, turnips, carrots, mushroom stems or dried shiitakes, garlic, lemon grass, salt to taste and 1 tablespoon sugar in a large soup pot and bring to a boil. Tie the spices in a cheesecloth bag and add to the soup. Reduce the heat, cover and simmer for 1 hour. Add the fish sauce if using, and simmer for another hour (2 hours total, with or without the fish sauce). Strain through a cheesecloth-lined strainer. Taste and adjust salt and sugar.

_Serves 6_

_Martha Rose Shulman, New York Times_
