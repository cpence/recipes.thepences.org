---
title: "Cake Au Butternut et Noisettes"
date: 2020-02-23T13:30:49+01:00
categories:
    - dessert
tags:
    - cake
    - squash
---

## Ingredients

*   500g butternut squash flesh
*   110g sugar
*   75g butter, softened
*   4 eggs
*   150g flour (self-rising, or ordinary + 1 tsp. baking powder)
*   80g ground hazelnut or almond powder

## Preparation

Steam the butternut squash flesh, if uncooked, and turn it into a smooth purée. Let cool. Preheat the oven to 170C. Separate the whites of the eggs, and beat to stiff peaks. Cream the softened butter and the sugar. Add the egg yolks, flour, baking powder (if using), and butternut purée. Finish by folding in the beaten egg whites. Turn out into a rectangular cake pan, buttered and flowered, around 5–6cm high. Bake 50 minutes: the cake is cooked when the tip of a knife comes out clean. Let cool a bit, then cut into squares or turn out.

*Partenamut*

