---
title: "Fondue Moitié-Moitié"
date: 2021-07-10T11:52:08+02:00
categories:
  - main course
tags:
  - swiss
  - fondue
  - cheese
---

## Ingredients

- 400g Gruyère AOP, râpé [grated]
- 1 + 1 càc fécule de maïs [1 + 1 tsp. corn starch]
- 400g Vacherin Fribourgeois AOP, râpé [grated]
- 1 gousse d'ail coupée en 2 [1 clove garlic, cut in half]
- 1.5 à 2.5 dl de vin blanc sec [150-250 ml dry white wine]
- 1 pointe de noix de muscade [dash ground nutmeg]
- 2 càs de kirsch [2 tbsp. kirsch]
- 1 trait de jus de citron [1 squeeze fresh lemon juice]
- sel, poivre
- 600g de pain rassis, en dès [600g stale bread, in cubes]
- oignons, cornichons

## Preparation

Mélangez dans un récipient Le Gruyère AOP avec 1 càc de fécule de maïs. Mélangez
dans un autre récipient le Vacherin Fribourgeois AOP avec la fécule restante.
Mélangez ensuite les deux fromages. Réservez. [Toss the two cheeses with the
corn starch in a bowl, reserve.]

Frottez le caquelon avec la gousse d’ail. Faites chauffer le vin blanc. Réduisez
la chaleur avant que le point d’ébullition ne soit atteint. Ajoutez le fromage
par petites poignées à feu moyen en remuant sans arrêt à la cuillère en bois
jusqu’à ce que le fromage soit fondu et que la préparation soit homogène. Salez,
poivrez et ajoutez la noix de muscade. Ajoutez ensuite le kirsch (et/ou le jus
de citron). [Rub the pot with the garlic clove. Warm the white wine, reducing
the heat before it boils. Add the cheese in small handfuls over medium heat,
mixing constantly with a wooden spoon until the cheese is melted and the fondue
is smooth. Salt, pepper, and add the nutmeg. Add the kirsch and the lemon
juice.]

Placez ensuite le caquelon sur le réchaud que vous aurez allumé. La flamme du
réchaud doit être faible, car la fondue ne peut atteindre le point d’ébullition.
Présentez des corbeilles de pains variés. Accompagnez d’oignons et de
cornichons. Trempez des dés de pain piqués au bout d’une fourchette à fondue.
[Place the pot on the lit heater. Keep the flame low, because the fondue must
not boil. Serve with a basket of assorted bread, onions, and cornichons. Dunk
the bread, stabbed on the end of a fondue fork.]

_Les Fromages de Suisse AOP_
