---
title: "Harvest Wild Rice"
author: "juliaphilip"
date: "2010-04-30"
categories:
  - side dish
tags:
  - bean
  - rice
  - vegetarian
---

## Ingredients

*   3 cups chicken broth
*   3 cups water
*   1/2 pound dried flageolets or Great Northern beans, picked over
*   3/4 cup wild rice (about 4 ounces)
*   2 large leeks, white and pale green parts only
*   2 tablespoons unsalted butter
*   1/4 pound fresh shiitake mushrooms, sliced thin
*   1/4 cup hazelnuts, toasted and skinned and chopped coarse
*   1/4 cup dried cranberries

## Preparation

In a large saucepan simmer broth, water, and beans, covered, 45 minutes. Stir in wild rice and simmer, covered, 45 minutes, or until beans and rice are tender. Drain rice mixture and return to pan.

Cut leeks crosswise into 1/2-inch slices and in a bowl soak in water, agitating occasionally to dislodge any sand, 5 minutes. Lift leeks out of water and drain in a colander. In a non-stick skillet sauté leeks in butter over moderately high heat, stirring occasionally, until almost tender, about 5 minutes. Add mushrooms with salt to taste and cook, stirring occasionally, 2 minutes, or until vegetables are tender. Stir leek mixture into rice mixture. Rice mixture may be made up to this point 1 day ahead and chilled, covered. Reheat mixture, adding water to prevent it from sticking to skillet, before proceeding.

Stir hazelnuts and cranberries into rice mixture and serve warm.

_Gourmet, November 1994_
