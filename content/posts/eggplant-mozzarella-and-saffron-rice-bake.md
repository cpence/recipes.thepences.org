---
title: "Eggplant, Mozzarella, and Saffron Rice Bake"
author: "pencechp"
date: "2013-03-13"
categories:
  - main course
  - side dish
tags:
  - eggplant
  - rice
  - untested
  - vegetarian
---

## Ingredients

*   2 tablespoons plus 1 cup olive oil
*   1 medium onion, minced
*   1 cup arborio rice
*   Pinch of saffron
*   1/4 cup dry white wine
*   1 cup vegetable broth or water
*   Kosher salt
*   3 large eggplants (about 3 pounds), cut crosswise into 1/4-inch rounds
*   Freshly ground black pepper
*   4 cups store-bought tomato sauce (such as marinara), divided
*   1 pound fresh mozzarella, cut into 3/4-inch cubes (about 2 cups), divided
*   1 cup coarsely grated Parmesan (4 ounces), divided

## Preparation

Arrange racks in top and bottom thirds of oven; preheat to 425°. Heat 2 Tbsp. oil in a large saucepan over medium heat. Add onion; cook, stirring often, until softened, about 8 minutes. Add rice; cook, stirring constantly, for 3 minutes. Stir in saffron, then wine. Cook until wine reduces slightly, about 1 minute. Add broth; season with salt. Cover and cook over medium heat until rice is very al dente and still crunchy, about 6 minutes; remove pan from heat.

Meanwhile, divide eggplant between two rimmed baking sheets, overlapping slightly to fit. Drizzle 1 cup oil over; season with salt and pepper. Bake, turning eggplant and rotating sheets halfway through, until tender and golden brown, 20–25 minutes.

Cover the bottom of a 13x9x2-inch baking dish with 1 cup tomato sauce. Sprinkle 1/3 of mozzarella over, then 1/3 of Parmesan.

Cover with a layer of eggplant rounds, overlapping so no sauce is visible beneath them. Add saffron rice, spreading out in an even layer. Sprinkle over another 1/3 each of mozzarella and Parmesan, then 1/2 of the remaining tomato sauce and another layer of eggplant. Top with remaining tomato sauce and remaining mozzarella and Parmesan. Cover dish with foil; transfer to a foil-lined baking sheet.

Bake until sauce is bubbling and cheese is melted, about 15 minutes. Uncover dish and bake until golden on top, about 20 minutes longer. Let stand for at least 10 minutes to set before serving.

_Serves 8._

_Bon Appetit, September 2012_
