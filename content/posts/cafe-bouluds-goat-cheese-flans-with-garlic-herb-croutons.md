---
title: "Cafe Boulud's Goat Cheese Flans with Garlic-Herb Croutons"
author: "pencechp"
date: "2012-09-19"
categories:
  - main course
tags:
  - cheese
  - chevre
  - untested
---

## Ingredients

_For the flans:_

*   6 oz soft fresh goat cheese, at room temperature
*   6 large eggs, at room temperature
*   1 c. heavy cream or half and half, at room temperature
*   large pinch freshly grated nutmeg
*   salt and white pepper

_For the croutons:_

*   olive oil
*   sourdough baguette or bread, thinly sliced
*   split garlic cloves
*   finely chopped parsley, chives, rosemary, and thyme
*   salt and white pepper

_For the salad:_

*   mixed salad greens
*   olive oil
*   sherry vinegar
*   grated aged goat cheese
*   salt and white pepper

## Preparation

_For the flans:_

1\. Center rack in oven and preheat to 300.  Grease or spray six four-ounce (or vice-versa) custard cups, and place them in a small baking pan.

2\. Whisk (or hand-mixer mix) together the goat cheese and eggs until smooth.  Whisk in (just to combine) cream, nutmeg, salt and pepper.

3\. Pour into cups.  Add hot water to halfway up the cups.  Cover tightly with foil.  Poke two holes in the opposite corners of the pan.  Bake for 50 minutes -- if you tap the cups gently, the flan shouldn't jiggle.

4\. Remove from the oven and let stand 10 minutes, covered.  (If not serving immediately, refrigerate and reheat in a water bath on top of the stove, or a microwave on low.)

_For the croutons:_

Toast the bread in the olive oil in a skillet.  Rub with garlic, sprinkle with herbs, salt and pepper.

_To finish:_

Make salad.  Run a knife around the edge of the flans, and invert on a serving plate.  Add a mound of salad, two croutons, and grate more cheese over all.

Serve as-is, or in the middle of a bowl of soup (like barley-mushroom, or tomato).

_Daniel Boulud_
