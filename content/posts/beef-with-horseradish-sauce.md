---
title: "Beef with Horseradish Sauce"
author: "juliaphilip"
date: "2010-02-06"
categories:
  - main course
tags:
  - beef
  - pence
---

## Ingredients

*   5 lbs. chuck or pot roast, cut in 1 1/2 in cubes
*   6 tbs. butter
*   2 large onions
*   2 tsp curry
*   1 tsp ginger or mace
*   2 tbs. Worcestershire sauce
*   1 tsp salt
*   1/2 tsp pepper
*   1/2 to 1 c white wine
*   1 c sour cream
*   2 tbs horseradish
*   2 tbs chopped parsley

## Preparation

Brown meat in butter, place in casserole. Brown onions in the same pan; add to casserole with all spices and wine. Bake at 300 degrees uncovered for 3 h or until meat is tender. Just before serving, combine sour cream and horseradish and stir into meat with parsley. Serve over egg noodles.
