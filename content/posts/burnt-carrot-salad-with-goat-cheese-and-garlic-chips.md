---
title: "Burnt Carrot Salad with Goat Cheese and Garlic Chips"
author: "juliaphilip"
date: "2010-04-28"
categories:
  - side dish
tags:
  - carrot
  - chevre
  - salad
  - untested
---

## Ingredients

*   1 cup extra-virgin olive oil
*   4 large cloves garlic, peeled, very thinly sliced
*   2 tablespoons red wine vinegar
*   1/2 teaspoon coarse salt
*   Freshly ground pepper
*   8 medium carrots (about 1 1/4 pounds), peeled
*   1 tablespoon chopped fresh thyme or 1/2 teaspoon dried
*   1 small bunch flat-leaf parsley, leaves only
*   2 bunches arugula, trimmed, rinsed, dried
*   6 ounces Bucheron or similar goat cheese, sliced 1/2-inch thick

## Preparation

Heat oil in a large cast-iron skillet over medium-high heat until hot. Add garlic; cook, stirring, until just crisp and barely golden, about 20 seconds. Immediately remove with a slotted spoon to a paper towel-lined plate. Let the oil cool; pour into a bowl. Wipe out skillet.

Put vinegar in small bowl; whisk in 5 tablespoons of the cooled garlic oil. Season with 1/4 teaspoon of the salt and pepper to taste. (JP: Save remaining garlic oil to use at a later time).

Cut carrots crosswise in half; cut the halves into thick, rough sticks. Toss in a bowl with 3 tablespoons of the garlic oil, thyme, remaining 1/4 teaspoon of the salt and pepper to taste.

Heat the same skillet over high heat. Add carrots in a single layer, working in batches if necessary. Cook until lightly charred (almost burnt) on one side, 3-5 minutes. Turn; cook other side, adjusting the heat as necessary, until lightly charred and tender within, 2-3 minutes. Transfer to a tray. Wipe out the skillet; set aside.

Toss parsley, arugula and half of the vinaigrette on large serving platter. Arrange carrots on top.

Reheat skillet to very high heat; coat with 1-2 tablespoons of the remaining oil. Immediately add goat cheese slices (be careful — oil may spatter). As soon as you see the cheese blacken on the bottom, remove the slices with a thin spatula; invert onto the carrots. Sprinkle the garlic chips over the salad; drizzle with vinaigrette to taste.

_Jean Marie Brownson, Chicago Tribune_
