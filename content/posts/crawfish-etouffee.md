---
title: "Crawfish Étouffée"
date: 2022-09-03T11:39:30+02:00
categories:
  - main course
tags:
  - cajun
  - crawfish
---

## Ingredients

- 1/4 cup unsalted butter (½ stick)
- 1/4 cup all-purpose flour
- 1 cup of chopped onion
- 1/2 cup of chopped green bell pepper
- 1/4 cup of chopped celery
- 2 teaspoons minced garlic
- 2 cups seafood or chicken stock/broth
- 1 teaspoon kosher salt
- Freshly cracked black pepper, to taste
- 1/4 to 1/2 teaspoon Cajun seasoning
- 1 pound of crawfish tails
- 1 tablespoon chopped fresh parsley, plus extra for garnish
- 1/4 cup sliced green onion, plus extra for garnish
- rice for serving

## Preparation

Chop the trinity (onion, green bell pepper and celery); parsley and green onions; then mince garlic and set aside.

Make a roux by melting butter in a large skillet over medium heat and stir in the flour; cook and stir constantly (this is important otherwise your flour might burn) for about 4 minutes or until caramel colored.

Add the onion, bell pepper and celery; cook another 3-4 minutes or until tender, add the garlic and cook another minute.

Slowly stir in the stock or broth until fully incorporated. Add salt, pepper and Cajun seasoning.

Bring mixture to a boil; reduce heat to a medium low simmer, cover and simmer for 15 minutes, stirring occasionally.

Add the crawfish tails, cook and stir until crawfish is heated through; stir in the parsley and green onion, reserving a bit for garnish.

_Louisiana Travel_
