---
title: "Catalan-Style Chicken Stew"
author: "pencechp"
date: "2014-01-15"
categories:
  - main course
tags:
  - chicken
  - spanish
---

## Ingredients

*   4 chicken thighs
*   salt and pepper
*   5 tbsp. olive oil
*   1/2 c. tomate frito (or a sofrito or other rich tomato sauce, pref. homemade)
*   1 c. [samfaina]( {{< relref "samfaina" >}} ) (pisto)
*   1/3 c. roasted red peppers
*   1/3 c. roasted onions
*   1/3 c. roasted eggplant
*   2 tbsp. parsley, chopped

## Preparation

Cut the eggplant, peppers, and onions into 1/4" pieces, place them in 3 tbsp. of the olive oil (or, if your peppers or eggplant were cured in olive oil, the olive oil from the vegetables), and set aside.

Cut the chicken thighs into quarters and season them with salt and black pepper.  In a large saute pan, heat the other 2 tbsp. of olive oil over medium-high heat.  When hot, add the chicken, skin side down, and brown on all sides, 5-6 minutes.  Set aside.

In the same pan, add the tomate frito and simmer for 4 minutes over medium-low heat.  Return the chicken to the pain and continue to simmer for 6-7 minutes, until just about cooked through.

Stir in the samfaina and the vegetables in olive oil. Stirring frequently, cook over medium-high heat for 2 minutes, until heated through.  Sprinkle with parsley and serve.

_Jose Andres Foods_
