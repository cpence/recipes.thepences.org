---
title: "German Apple Pancakes"
author: "pencechp"
date: "2014-10-03"
categories:
  - breakfast
tags:
  - apple
  - pancake
  - untested
---

## Ingredients

*   4 eggs
*   3/4 c. flour
*   1/2 tsp. salt
*   3/4 c. milk
*   1/4 c. sugar
*   1/2 tsp. cinnamon
*   4 tbsp. butter
*   2 medium apples, peeled if desired, thinly sliced

## Preparation

Preheat oven to 400. Place two pie pans in the oven to heat. Beat the eggs, flour, milk, and salt on medium speed for 1 minute. Combine sugar and cinnamon and set aside. Take the pans out of the oven and put 2 tbsp. butter in each one, swirling to coat bottom and sides. Spread the apple slices in the bottom of the pans. Pour half the batter in each pan. Sprinkle with cinnamon sugar. Bake for 20-25 minutes until golden brown and puffy.

_Penzey's Spices_
