---
title: "Red Pozole"
date: 2022-03-04T13:25:38+01:00
categories:
  - main course
tags:
  - mexican
  - stew
  - pork
  - chile
---

## Ingredients

_For the pozole:_

- 4 qt. water
- 2 lb. cubed pork
- 1 lb. pork spare ribs or baby back ribs
- 1 white onion, quartered
- 8 cloves garlic
- salt
- 3 15-oz. cans hominy (or, see below for dry)

_For the chile sauce:_

- 5 dried guajillo peppers
- 5 dried ancho peppers
- 6 cloves garlic
- 1 medium white onion, coarsely chopped
- 1/2 tsp. Mexican oregano
- 2 tbsp. vegetable oil
- salt

_Garnishes:_

- 1 head lettuce or cabbage, shredded finely
- 1 1/2 c. onion, finely chopped
- ground chile piquin
- 1 1/2 c. radishes, sliced
- Mexican oregano
- Tostadas or tortilla chips
- Lime wedges
- Avocado, diced

## Preparation

If you want to use dried hominy (1 to 1 1/2 lb.), rinse it and pick it over, place it in an Instant Pot, cover with water a few inches above the kernels. Add a quarter of an onion, two cloves of garlic, and a few bay leaves. Run the pot on the "beans" setting one or two times, until the hominy is fully tender and starts to pop. Drain it and use it as canned.

Heat water in a large stock pot. Add pork, ribs, onion, and garlic. Bring to a boil, lower heat, and simmer, partially covered, for 2 1/2 hours, or until meat is tender and falling off the bone. Season with salt when the meat is almost done. While cooking, skim the foam and fat occasionally, adding water if needed.

Remove pork from broth. Trim excess fat, discard bones, onion, and garlic. Shred the meat.

Meanwhile (or in advance, if you want), soak the anchos and guajillos in just enough water to cover, for 25-30 minutes, until soft. Blend the peppers, garlic cloves, onion, and oregano, adding some of the soaking water if needed, until smooth.

Heat oil in a large skillet over medium-high heat. Add the pepper sauce and salt to taste, stirring constantly as it cooks. Reduce heat to medium and simmer for about 25 minutes.

Add the sauce to the broth, straining it if you want. Bring to a boil and add the meat. Simmer gently for 10 minutes. Add the hominy, and adjust seasoning. Simmer until heated through.

Serve with any garnishes desired.

_Mexico Kitchen_
