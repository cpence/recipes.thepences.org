---
title: "Chilled Ginger Carrot Chile Soup"
author: "pencechp"
date: "2013-03-21"
categories:
  - side dish
tags:
  - carrot
  - greenchile
  - soup
  - untested
---

## Ingredients

*   2 lbs baby carrots, packaged and peeled
*   32 oz low sodium chicken or vegetable broth
*   6 roasted mild Hatch chiles, seeded and chopped
*   1 roasted hot Hatch chile, seeded and chopped
*   1 sweet onion, diced
*   2 tbsp olive oil
*   1 tbsp fresh grated ginger root
*   2 1/2 cups whole milk Greek yogurt
*   1/4 cup half and half
*   Basil leaves for garnish

## Preparation

Place carrots in microwave-safe bowl and steam for 10 minutes, or until fully cooked. Cool in refrigerator. This may be done a few days in advance.

Sauté onions in olive oil until translucent. Puree onions and carrots in several batches in a blender with cream, yogurt, and broth until the mixture is velvety. Chiles may be puréed with carrots or chopped and added as garnish or a combination of both. Add fresh ginger and stir. Cool soup in refrigerator until ready to serve. Garnish with a dollop of Greek yogurt, basil leaf, and chopped Hatch chiles.

_Central Market_
