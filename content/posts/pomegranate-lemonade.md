---
title: "Pomegranate Lemonade"
author: "pencechp"
date: "2009-12-05"
categories:
  - cocktails
tags:
  - vodka
---

## Dad's Recipe

*   3 meas. lemonade
*   1 meas. pomegranate juice
*   1 meas. vodka
