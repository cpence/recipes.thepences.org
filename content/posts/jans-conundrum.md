---
title: "Jan's Conundrum"
author: "pencechp"
date: "2012-12-25"
categories:
  - cocktails
tags:
  - rum
  - shrub
---

## Ingredients

*   2 1/4 oz. dark rum
*   1/2 oz dry sherry
*   1/2 oz. berry shrub
*   3 dashes bitters
*   lemon twist

## Preparation

Stir ingredients with ice, garnish with twist.

_Jamie Boudreau, [spiritsandcocktails.com](http://spiritsandcocktails.wordpress.com/2007/11/08/berry-shrub/)_
