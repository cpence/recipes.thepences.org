---
title: "(Mostly) Vegan Nutella Cookies"
author: "pencechp"
date: "2013-02-18"
categories:
  - dessert
tags:
  - chocolate
  - cookie
  - vegan
---

## Ingredients

*   1 c. Nutella (which does contain milk)
*   1/2 tbsp. baking soda
*   1 c. flour
*   1/2 c. sugar
*   1/4 c. brown sugar
*   1/4 c. applesauce
*   1 tsp. vanilla

## Preparation

Combine the sugars, applesauce, vanilla and Nutella in a bowl.  Mix until smooth.  Add the flour and baking soda and mix until combined.  The dough should be sticky and easy to remove from the bowl.  Wrap in plastic wrap and refrigerate for an hour and a half.

Preheat the oven to 350.

Roll into tbsp-sized balls, and place on a baking sheet.  Sprinkle (or roll) each cookie in a small bit of sea salt, kosher salt, or fleur de sel.  Bake for about 10 minutes until the cookies are puffed and crinkled (they will look under-done, this is normal).  Remove from oven and cool for about 5 minutes on a baking sheet and about 5 minutes on a rack.

_Emma, Mama Ozzy's Table_
