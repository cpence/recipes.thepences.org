---
title: "Quinoa with Spiced Lentil Dal"
author: "pencechp"
date: "2013-03-13"
categories:
  - main course
tags:
  - indian
  - lentil
  - quinoa
  - untested
  - vegan
  - vegetarian
---

## Ingredients

_For the quinoa:_

*   3/4 cup regular blond quinoa
*   1/4 cup red quinoa
*   1 1/4 cups water
*   1/2 to 3/4 teaspoon salt, to taste

_For the dal:_

*   1 cup brown or split yellow lentils (toor dal), rinsed
*   1 teaspoon minced fresh ginger
*   1 1/2 teaspoons turmeric
*   1/2 medium onion (intact), peeled
*   Salt to taste
*   1 1/2 tablespoons fresh lime juice or 1/4 cup tamarind water (made from soaking 1 tablespoon tamarind paste in warm water for 10 minutes; optional)
*   2 tablespoons grape seed oil, canola oil, safflower oil or sunflower oil
*   1/2 cup finely minced red bell pepper
*   1 plump garlic clove, minced
*   1 teaspoon cumin seeds
*   Cayenne pepper to taste
*   3 tablespoons chopped cilantro

## Preparation

1\. Rinse the quinoa thoroughly and combine with the water and salt in a large saucepan. Bring to a boil, cover and reduce the heat to low. Simmer 15 to 20 minutes, until the white quinoa displays a little white spiral. Turn off the heat, remove the lid and place a dish towel over the top of the pot. Return the lid and let sit for 15 minutes. The quinoa will now be fluffy. Keep warm.

2\. Meanwhile, combine the lentils, ginger, turmeric and onion half (don’t chop it) with 1 quart water and salt to taste (about 1 to 1 1/2 teaspoons) and bring to a gentle boil. Stir only once to make sure there are no lentils sticking to the bottom of the pot. Reduce the heat to medium – the lentils should simmer briskly – and cook uncovered until the lentils are tender, 30 to 40 minutes. Stir in the lime juice or tamarind concentrate and add another 1/2 cup water. Stir together and simmer for another minute. Turn off the heat and using an immersion blender, an Indian mathani (a wooden tool used for mashing dal) or a wooden Mexican hot chocolate mixer, partially pureé the dal. It should be thick but not like a pureed soup.

3\. Heat the oil over medium-high heat in a small saucepan or small frying pan (such as an 8-inch omelet pan). Add the cumin seeds and allow to sizzle, stirring, for 10 seconds. Add the garlic and cook until lightly colored, about 15 seconds. Add the red pepper and cook until slightly softened, about 1 minute. Remove from the heat and pour over the lentils. Add the cayenne pepper and 1 tablespoon of the cilantro and stir gently. Taste and adjust seasoning.

4\. Serve the quinoa with the dal spooned on top. Garnish with chopped fresh cilantro.

_Serves 4 to 6_

_Martha Rose Shulman, New York Times_
