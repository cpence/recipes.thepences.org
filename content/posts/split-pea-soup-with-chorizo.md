---
title: "Split-Pea Soup with Chorizo"
author: "pencechp"
date: "2014-11-18"
categories:
  - main course
tags:
  - chorizo
  - peas
  - soup
---

## Ingredients

*   1 1/2 pounds Spanish chorizo, sliced thin \[CP: We did this with 6 oz. of super nice (Olli) chorizo, and that was enough.\]
*   1 onion, chopped
*   1 rib of celery, chopped fine
*   2 garlic cloves, minced
*   1 pound split peas, picked over
*   4 cups chicken broth
*   4 cups water
*   1/2 teaspoon dried thyme
*   1 bay leaf
*   3 carrots, halved lengthwise and sliced thin crosswise
*   croutons as an accompaniment

## Preparation

In a heavy kettle brown the chorizo over moderate heat, stirring, transfer it with slotted spoon to paper towels to drain, and pour off all but 1 tablespoon of the fat. In the fat remaining in the kettle cook the onion, the celery, and the garlic over moderately low heat, stirring, until the celery is softened, add the split peas, the broth, the water, the thyme, and the bay leaf, and simmer the mixture, uncovered, stirring occasionally, for 1 1/4 hours, Stir in the carrots \[CP: and the chorizo\] and simmer the soup, uncovered for 30 to 35 minutes, or until the carrots are tender. \[CP: Both of these were covered, and if you did that, you'd have **way** too much liquid.\] Discard the bay leaf, season the soup with salt and pepper, and serve it with the croutons.

_Gourmet, March 1992_
