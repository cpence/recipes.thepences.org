---
title: "Pastitsio"
author: "pencechp"
date: "2017-07-20"
categories:
  - main course
tags:
  - beef
  - casserole
  - greek
  - lamb
  - pasta
  - pork
---

## Ingredients

*   1 lb. thick macaroni
*   7 tbsp olive oil
*   2 vegetable stock cubes (optional)
*   1 lg. onion, finely chopped
*   1 lg. clove garlic
*   2 lb. minced meat (half pork and half beef, or lamb, etc.)
*   2 tsps dried mint, rubbed
*   1 tsp oregano
*   Freshly ground pepper
*   1 cup pureed tomatoes
*   1 tsp tomato paste
*   1 cup hot water
*   1 tsp sugar
*   ½ tsp cinnamon
*   1 cup fresh flat leaf parsley, chopped
*   1 heaped tbsp butter
*   1 egg
*   1 cup grated Halloumi (or other dry, hard) cheese

_For the white sauce:_

*   8 cups milk scant
*   ½ c. corn flour
*   1 tsp salt
*   1 egg, lightly beaten
*   1 tbsp finely chopped parsley
*   1 tbsp butter
*   3 tbsp breadcrumbs
*   3 tbsp grated cheese
*   zest of 1 lemon

## Preparation

_Macaroni:_

Grease a shallow baking dish (15x13"). Break the macaroni in half (this will enable you to arrange the cooked pasta better in the baking dish later). Add macaroni to a large pan of boiling water, together with 1 tbsp olive oil, 1 tsp salt and 1 vegetable stock cube (optional). Boil, uncovered, until al dente. Drain and rinse under cold water. Drain well and put aside in a bowl. Add 1 tbsp butter to the pasta, 3/4 cup of your grated cheese, the beaten egg and 1tsp dry mint. Mix well and set aside.

_Meat Sauce:_

Heat the rest of your oil in a large frying pan; add your chopped onion and garlic and cook until soft. Add the mince and cook until well browned. Add the pasata and the tomato paste, the sugar, oregano, dried mint, cinnamon, the stock cube dissolved in 1 cup of hot water, the salt and the freshly ground black pepper. Towards the end, add the parsley and remove from the heat.

_White sauce:_

Dissolve your corn flour in 1 cup of milk, then and1 tsp salt. Add the egg (lightly beaten), and the finely chopped parsley and lemon zest. In a big non-stick pan add the remaining 7 cups of milk and bring almost to the boil. Gradually whisk the dissolved corn flour and egg mixture into the hot milk, stir well over the heat until the mixture boils and thickens; mix until smooth and remove from heat.

_Next steps:_

Add half of the macaroni into your greased dish. Top the macaroni with the meat sauce and spread evenly. Add rest of the pasta over meat sauce. Pour over ¾ of the white sauce and use a fork to make sure that some of the white sauce goes through the pasta to the base of the dish. Now add the rest of the white sauce and smooth the surface, then sprinkle the breadcrumbs and cheese on top. Spread several nobs of butter equally along the surface of the dish.

Bake uncovered at 350 degrees for 1 hour, or until golden brown. Leave to stand for 15 minutes before serving.
