---
title: "Dolmades with Avgolemono"
author: "pencechp"
date: "2011-05-18"
categories:
  - main course
  - side dish
tags:
  - greek
  - lamb
  - lemon
---

## Ingredients for Dolmades

*   2 lbs. ground lamb
*   2 large onions, grated
*   1/2 cup minced parsley
*   1/2 cup long grain rice
*   2 egg whites (save yolks for sauce)
*   2 TBSP minced fresh mint
*   s & p
*   1 jar grape leaves
*   chicken broth

## Preparation for Dolmades

Combine all filling ingredients and mix well. Rinse, drain, and separate grape leaves. Cover the bottom of a 4-quart saucepan with 4-5 leaves. Roll 1 TBSP filling in each grape leaf. Place close together in even layers in saucepan. Add enough chicken broth to cover rolls. Place a small plate directly on top of rolls to weigh down. Cover and simmer for 1 hour.

## Ingredients for Sauce

*   2 to 3 cups broth from pot
*   2 eggs
*   2 egg yolks
*   1/2 cup lemon juice
*   1 TBSP cornstarch

## Preparation for Sauce

In small saucepan, boil broth. Combine eggs, yolks, juice, and cornstarch in blender. With blender running, slowly add hot broth. Return to saucepan and stir over low heat until thickened. Pour sauce over rolls. Eat.

_Grandmommy_
