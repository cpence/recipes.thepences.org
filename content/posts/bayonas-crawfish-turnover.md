---
title: "Bayona's Crawfish Turnover"
author: "pencechp"
date: "2018-06-06"
categories:
  - appetizer
  - side dish
tags:
  - cajun
  - crawfish
  - pie
  - untested
---

## Ingredients

* 1 lb. crawfish tails
* 1 lb. cream cheese, softened
* 4 scallions
* 1 tbsp. chopped cilantro
* 1 tbsp. chopped tarragon
* 1/2 tsp. chopped garlic
* 1/2 tsp. curry powder
* 1/2 tsp. red pepper flakes
* salt
* lime juice
* 1/2 lb. phyllo sheets
* 1/2 c. butter, melted
* 1/2 c. breadcrumbs

## Preparation

Preheat oven to 400 degrees. Roughly chop crawfish meat and combine with cream cheese. Chop green and white of scallions and add to cream cheese along with cilantro, tarragon, garlic, and curry powder. Add pepper flakes, salt, and lime juice to taste.

Lay out one sheet of dough. Brush lightly with butter and sprinkle with breadcrumbs. Repeat with two more sheets, stacking one on top of the other. Cut widthwise into three pieces. Place a large spoonful of filling in the corner of each piece and fold flag-style into triangle. Brush with butter and place on a tray sprinkled with breadcrumbs. Repeat with remaining phyllo sheets. Bake until brown and crispy, about 10 minutes. Serves 10-12.

_The hallway by the bathroom, Bayona_
