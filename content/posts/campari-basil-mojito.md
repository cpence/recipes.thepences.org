---
title: "Campari-Basil Mojito"
author: "pencechp"
date: "2014-06-10"
categories:
  - cocktails
tags:
  - campari
  - untested
---

## Ingredients

*   2 oz Campari
*   1 tablespoon brown sugar
*   2 oz lime juice
*   1/4 cup basil leaves, loosely packed
*   Basil leaves for garnish

## Preparation

In the bottom of a cocktail shaker or glass, add the brown sugar, lime juice and 1/4 cup basil leaves. Muddle together with a muddler or a spoon until the sugar has mostly dissolved into the lime juice and there is a nice basil scent released from the leaves. Add the Campari and stir well to combine. Fill a Collins glass or other glass with crushed ice. Pour the Campari-basil mixture over the ice and top with a splash of sparkling water. Serve with basil leaves for garnish.

_Set the Table_
