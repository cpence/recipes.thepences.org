---
title: "Coconut Butter Thins"
author: "pencechp"
date: "2010-05-29"
categories:
  - dessert
tags:
  - coconut
  - cookie
  - lime
  - macadamia
---

## Ingredients

*   1 1/2 cups all-purpose flour
*   1/4 cup cornstarch
*   1/4 teaspoon salt
*   Pinch of ground coriander
*   2/3 cup sugar
*   Grated zest of 1 lime
*   2 sticks (8 ounces) unsalted butter, at room temperature
*   1 1/2 teaspoons pure vanilla extract
*   2/3 cup sweetened shredded coconut
*   1/2 cup finely chopped macademia nuts (don't be afraid to use salted nuts)

## Preparation

Whisk together the flour, cornstarch, salt and coriander.

If you want to get a little more flavor out of the lime zest, put the sugar and zest in the mixer bowl and, using your fingertips, work the zest into the sugar until the sugar is moist and the mixture fragrant.

Working with a stand mixer, preferably fitted with a paddle attachment, or with a hand mixer in a large bowl, beat the butter, sugar and zest on medium speed for about 3 minutes, or until smooth. Beat in the vanilla extract. Reduce the mixer speed to low and add the flour mixture, mixing only until the dry ingredients disappear. Add the coconut and nuts and pulse to incorporate them. There will probably be some dry ingredients in the bottom of the bowl--don't work them in with the mixer, just reach into the bowl and knead them in.

Transfer the soft, sticky dough to a gallon-size zipper-lock bag. Put the bag on a flat surface, leaving the top open, and roll the dough into a 9-x-10 1/2-inch rectangle that's 1/4 inch thick. As you roll, turn the bag occasionally and lift the plastic from the dough so it doesn't cause creases. When you get the right size and thickness, seal the bag, pressing out as much air as possible, and refrigerate the dough for at least 2 hours or for up to 2 days.

GETTING READY TO BAKE: Position the racks to divide the oven into thirds and preheat the oven to 325 degrees F. Line two baking sheets with parchment or silicone mats.

Put the plastic bag on a cutting board and slit it open. Turn the dough out onto the board (discard the bag), and, using a ruler as a guide and a sharp knife, cut it into 32 squares, each roughly 1 1/2 inches on a side. Transfer the squares to the baking sheets, leaving about 2 inches between them, and carefully prick each one twice with a fork, gently pushing the tines through the cookies to the sheet.

Bake for 18 to 20 minutes, rotating the sheets from top to bottom and fromt to back at the midway point. The shortbreads will be very pale--they shouldn't take on much color. Transfer the cookies to a rack and cool to room temperature.

Makes 32 cookies.

_Dorie Greenspan, Baking: From My Home to Yours, pg 145, from Barefoot Kitchen Witch_
