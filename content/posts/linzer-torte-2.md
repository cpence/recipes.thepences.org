---
title: "Linzer Torte"
author: "pencechp"
date: "2010-12-07"
categories:
  - dessert
tags:
  - pie
---

## Ingredients

**Raspberry Preserves:**

*   2 cups (225 grams) (8 ounces) frozen raspberries, unsweetened
*   1/4 cup (50 grams) granulated white sugar, or to taste
*   lemon juice

**Linzer Torte:**

*   1 cup (150 grams) whole almonds (can use blanched almonds)
*   1/2 cup (60 grams) whole hazelnuts
*   1 1/2 cups (195 grams) all purpose flour
*   2/3 cup (135 grams) granulated white sugar
*   Zest of one lemon
*   1 teaspoon ground cinnamon
*   1/8 teaspoon ground cloves
*   1/4 teaspoon salt
*   1/2 teaspoon baking powder
*   14 tablespoons (195 grams) cold unsalted butter, cut into pieces
*   2 large (40 grams) egg yolks
*   1 teaspoon pure vanilla extract
*   Confectioners' (Icing or Powdered) Sugar for dusting

## Preparation

**Raspberry Preserves:** Place the frozen unsweetened raspberries and the sugar in a small saucepan and bring to a boil over medium heat. Reduce the heat and simmer, stirring occasionally, for about 15 to 20 minutes or until most of the liquid has evaporated. Do not let it burn. Remove from heat and pour into a heatproof measuring cup. Add a drop or two of lemon juice. Cover and place in the refrigerator while you make the crust. (The raspberry preserves can be made several days in advance. Cover and store in the refrigerator.)

**Linzer Torte:** Preheat the oven to 350 degrees F (177 degrees C) and position rack in the center of the oven. Place the almonds on a baking sheet and bake for about 8-10 minutes or until lightly browned and fragrant. Then place the hazelnuts on a baking sheet and bake for 15 minutes or until fragrant and the outer skins begin to flake and crack. Remove from oven and place on wire rack to cool. Once the almonds and hazelnuts have cooled, place in a food processor and process, along with 1/2 cup (65 grams) of flour, until finely ground. Add the remaining flour, sugar, lemon zest, ground cinnamon, ground cloves, salt, and baking powder and process until evenly combined. Add the butter and pulse until the mixture looks like fine crumbs. Add the 2 egg yolks and vanilla extract and pulse until the dough just begins to come together.

Gather the dough into a ball and then divide it into two pieces, one slightly larger than the other. Wrap the smaller ball of dough in plastic wrap and refrigerate for about an hour, or until firm enough to roll. Take the larger ball of dough and press it onto the bottom and up the sides of a buttered 9-10 inch (23-25 cm) tart pan or springform pan. If using a springform pan press the dough about 1 inch (2.5 cm) up the sides of the pan.

Take the cooled raspberry preserves and spread them over the bottom of the crust. Cover with plastic wrap and place in the refrigerator. Once the smaller ball of dough is firm, remove from the fridge and roll it between two sheets of wax paper into a circle that is about 12 inches (30 cm) in diameter. Using a pastry wheel or pizza cutter, cut the pastry into 1 inch (2.5 cm) strips. Place the strips of pastry on a parchment paper-lined baking sheet, cover with plastic wrap, and place in the refrigerator for about 10 minutes. When strips are firm, using an offset spatula, gently transfer the strips to the tart pan. Lay half the strips, evenly spaced, across the torte and then turn the pan a quarter turn and lay the remaining strips across the first strips. If desired, weave the top strips over and under the bottom strips. (Do not worry if the pastry tears, just press it back together as best as you can.) Trim the edges of the strips to fit the tart pan.

If you have any leftover scraps of dough, roll them into a long rope. Don't worry if the rope breaks. Just take the pieces of rope and place them around the outer edge of the tart where the ends of the lattice strips meets the bottom crust. Using a fork or your fingers, press the rope into the edges of the bottom crust to seal the edges.

Bake the tart in a preheated 350 degree F (177 degree C) oven for about 30 - 35 minutes or until the pastry is golden brown and set. Let the torte cool on a wire rack before unmolding. Although you can serve this torte the same day as it is baked I like to cover and store it overnight before serving. This torte is lovely served warm with a dollop of whipped cream. Dust the top of the torte with confectioners' sugar.

This torte will keep for a few days at room temperature or in the refrigerator for about a week. It can also be frozen.

Serves 8.

_Joy of Baking.com_
