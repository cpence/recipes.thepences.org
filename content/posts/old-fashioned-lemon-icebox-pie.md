---
title: "Old Fashioned Lemon Icebox Pie"
author: "pencechp"
date: "2016-08-18"
categories:
  - dessert
tags:
  - lemon
  - pie
  - untested
---

## Ingredients

*   3 cups vanilla cookie crumbs
*   ¾ cup melted butter
*   1 cup whipping cream
*   2 rounded tsp powdered sugar
*   1 tsp vanilla extract
*   9 extra large egg yolks
*   ⅓ cup sugar
*   Two 10 ounce cans of sweetened condensed milk
*   1 cup lemon juice
*   zest of 2 lemons finely minced

## Preparation

Grease a 10 inch deep dish pie plate or line the bottom of a 10 inch spring-form pan and lightly grease the sides. Mix together the cookie crumbs and butter. Press into the bottom and sides of the pie plate or to about ¾ of the height of a spring form pan if you are using one. Bake for 10 minutes at 325 degrees F. Set aside to cool. Whip the cream, icing sugar and vanilla to soft peaks and set aside. In the bowl of a stand mixer with the whisk attachment in place, add the egg yolks and ⅓ cup sugar. Whisk at high speed until light, foamy and pale yellow in color, about 5 minutes. Slowly whisk in the sweetened condensed milk lemon zest and lemon juice. Gently fold in the previously whipped cream. Pour into the prepared pie crust. Freeze overnight. Garnish with whipped cream, additional Nilla cookies and lemon zest before serving.

_Barry Parsons_
