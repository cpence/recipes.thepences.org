---
title: "Microwave Steamer"
author: "pencechp"
date: "2010-05-25"
categories:
  - miscellaneous
tags:
---

**Standard Vegetable Steaming:** 1/4 c. water, 3 min.  Use both the inner (clear plastic) and outer lids, and don't forget the steaming insert.

**Rice Cooking:** You can steam anywhere from 3/4 c. rice to 6 c. rice.  Rinse the rice and add to the bottom of the steamer.  Add the following amount of water:

(1.15 - 0.0375 \* _N_) \* _N_,

where _N_ is cups of rice.  Use both the inner and outer lids, and remove the steaming insert.  Start by cooking for 9 + 2_N_ minutes, and cook for up to another two or three minutes until the rice is done.
