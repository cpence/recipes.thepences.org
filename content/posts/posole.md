---
title: "Posole"
author: "pencechp"
date: "2010-04-03"
categories:
  - main course
tags:
  - greenchile
  - mexican
  - pence
  - pork
  - stew
---

## Ingredients

*   cubed pork (country spare ribs)
*   2 qts. chicken broth
*   2 lg. onion, sliced
*   canned hominy, drained and rinsed, or dried hominy
*   red pepper flakes
*   green chiles (1/2 to 1 doz., depending on heat)
*   3 cloves garlic

## Preparation

Boil the pork in the chicken broth with one of the onions, salt, and pepper until almost tender.  Add hominy, a couple dashes of red pepper flakes, green chiles, garlic, and the other onion.  Simmer slowly until thickened.

If using dried hominy: rinse and soak overnight.  Cook meat as above, then remove it from the broth.  Add the hominy, and cook until tender.  Then return the meat to the pot and proceed as above.
