---
title: "Green Chile Mole Lettuce Wraps"
author: "pencechp"
date: "2013-03-21"
categories:
  - appetizer
  - main course
tags:
  - chicken
  - greenchile
  - mexican
  - untested
---

## Ingredients

*   2 green chiles, grilled, peeled, and seeded
*   1 lb peanuts
*   1/2 cup honey
*   1/2 cup mustard
*   1/2 cup hot water
*   1 lb boneless, skinless chicken
*   10 romaine lettuce leaves
*   1 tbsp salt, divided
*   1 tbsp pepper, divided
*   1 tbsp garlic powder, divided
*   1 tomato, chopped
*   1 cup green onion
*   1 tbsp olive oil

## Preparation

Chop raw chicken into small, ½ inch squares. Season chicken with ½ tbsp salt, ½ tbsp pepper, and ½ tbsp garlic powder. Pan fry in olive oil until fully cooked. In a blender, combine the Hatch chiles, peanuts, honey, mustard, remaining salt, pepper, and garlic powder. Add the hot water slowly while blending. Once the blended mole mix and chicken are done, toss chicken in the mole mix. Add chopped tomato and green onion to mixture. Chop romaine lettuce into approximately 5 inch long sections. Wrap about 2 tbsp of mix in lettuce and secure with a toothpick. Serve for crispy wrapping. For soft wrapping, bake for 10 minutes.

_Central Market_
