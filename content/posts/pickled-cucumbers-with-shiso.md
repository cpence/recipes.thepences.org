---
title: "Pickled Cucumbers with Shiso"
author: "pencechp"
date: "2012-07-21"
categories:
  - miscellaneous
tags:
  - canning
  - pickle
  - untested
---

## Ingredients

*   2 cups plus 2 quarts cold water
*   1/4 c. plus 2 tbsp. kosher salt
*   10 black peppercorns
*   1 head of garlic, unpeeled and cut in half crosswise
*   3 quarts (about 2 lb.) small pickling cucumbers, any stems trimmed to 1/2"
*   5 purple shiso leaves

## Preparation

In a small saucepan over high heat, bring 2 cups water to a boil.  Remove from heat and add the salt, peppercorns, and garlic.  Let cool, and then add the mixture to 2 quarts cold water.  Place the cucumbers and shiso leaves in a large crock or food-safe plastic container.  Pour the brine mixture over the cucumbers, covering them completely.  Place one or more small plates on top of the cucumbers to keep them completely submerged.  Store in a cool, dark room for three days to a week, checking every day or so and removing any mold or foam that rises to the top.  The pickles are done when they are pleasantly sour and tangy but still firm.  Store refrigerated for several weeks.

_Garden and Gun, August/September 2012_
