---
title: "Spiced Garbanzo Beans"
author: "pencechp"
date: "2014-10-03"
categories:
  - side dish
tags:
  - mediterranean
  - snack
  - untested
---

## Ingredients

*   1 15 oz. can garbanzo beans, drained and rinsed
*   2 tbsp. vegetable oil
*   1 tsp. ground coriander
*   1/4 tsp. cayenne pepper
*   1/4 tsp. salt
*   1 tbsp. flour

## Preparation

Preheat oven to 425. Combine all ingredients. Spread on a baking sheet and roast for 30 minutes or until crisp (when they start to pop).

_Penzey's Spices_
