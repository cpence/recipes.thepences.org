---
title: "Thai Peanut Dipping Sauce"
author: "juliaphilip"
date: "2009-12-08"
categories:
  - miscellaneous
tags:
  - sauce
  - thai
---

## Ingredients

*   3 garlic cloves, minced
*   1/4 tsp red pepper flakes
*   1 tbsp vegetable oil
*   1 tbsp chili garlic sauce
*   3 tbsp creamy peanut butter
*   3 tbsp hoisin sauce
*   1/2 tsp sugar
*   3/4 cup water or chicken broth
*   squeeze lime juice

## Preparation

In a small saucepan, cook the garlic and red pepper flakes in the oil, over medium heat, until garlic is golden. Whisk in remaining ingredients and bring to a boil, whisking. Simmer sauce, whisking, until thickened, around 1 min. If too thick, thin with more water or chicken broth.
