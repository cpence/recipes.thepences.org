---
title: "Gypsy"
author: "pencechp"
date: "2011-11-12"
categories:
  - cocktails
tags:
  - gin
  - stgermain
---

## Ingredients

*   1 1/2 parts gin
*   3/4 part St. Germain
*   splash Pernod / Absinthe
*   1/2 part lime juice

## Preparation

Shake over ice, strain, serve in cocktail glass.

_St. Germain, adapted via The Mark_
