---
title: "Pasta with Shrimp Ragu"
author: "juliaphilip"
date: "2010-04-26"
categories:
  - main course
tags:
  - pasta
  - shrimp
---

## Ingredients

*   1 1/2 pounds medium-to-large shrimp, in their shells
*   Salt and ground black pepper
*   Pinch cayenne
*   3 tablespoons extra virgin olive oil
*   2 medium or 1 large chopped onion
*   1 medium carrot, peeled and finely chopped
*   1 large or 3 plum tomatoes, chopped, with juice
*   1 teaspoon chopped fresh marjoram or oregano, plus a few leaves for garnish
*   1 pound pasta, preferably fresh

## Preparation

Shell shrimp; boil shells with just enough water to cover, a large pinch of salt, a grinding of pepper and a pinch of cayenne. Simmer 10 minutes, then drain, reserving liquid (discard shells). Bring a pot of water to boil for pasta and salt it.

Meanwhile, finely chop about a third of the shrimp. Put olive oil in a large skillet over medium-high heat; a minute later add onion and carrot, and cook, stirring occasionally, until onions are quite soft, about 10 minutes. Add tomatoes, herb and chopped shrimp, and cook, still over medium-high heat, stirring occasionally, until tomatoes begin to break down. Add stock from shrimp shells and cook, stirring occasionally, until mixture is no longer soupy but still moist.

When sauce is almost done, cook pasta. When pasta has about 5 minutes to go, stir whole shrimp into sauce. Serve pasta with sauce and shrimp, garnished with a few leaves of marjoram or oregano.

_New York Times, August 2007_
