---
title: "Skillet Soda Bread"
author: "pencechp"
date: "2015-05-06"
categories:
  - bread
tags:
  - irish
  - untested
---

## Ingredients

*   3 cups all-purpose flour
*   1 cup cake flour
*   1 1/2 teaspoons baking soda
*   1 1/2 teaspoons cream of tartar
*   1 1/2 teaspoons salt
*   2 tablespoons sugar
*   2 tablespoons unsalted butter, softened
*   1 1/2 cups low-fat buttermilk
*   1 tablespoon melted butter, optional

## Preparation

Heat the oven to 400 degrees and adjust a rack to the center position. Place the flours, soda, cream of tartar, salt and sugar in a large mixing bowl. Add the butter and rub it into the flour using your fingers until it is completely incorporated and the mixture resembles coarse crumbs. Make a well in the center and add the buttermilk. Work the liquid into the flour mixture using a fork until the dough comes together in large clumps. Turn the dough onto a work surface and knead briefly until the loose flour is just moistened. The dough will still be scrappy and uneven.

Form the dough into a round about 6 to 7 inches in diameter and place in a cast iron skillet. Score a deep cross on top of the loaf and place in the heated oven. Bake until nicely browned and a tested comes out clean when inserted into the center of the loaf, about 40 to 45 minutes. Remove from oven and brush with a tablespoon of melted butter if desired. Cool for at least 30 minutes before slicing. Serve slightly warm or at room temperature.

_CI_
