---
title: "Fresh Whisky Sours"
date: 2021-07-10T12:01:09+02:00
categories:
  - cocktails
tags:
  - whiskey
  - untested
---

## Ingredients

- 180ml whiskey (such as Jack Daniel's)
- 120ml fresh lemon juice (~4 lemons)
- 120ml fresh lime juice (~4 limes)
- 160ml simple syrup
- ice cubes
- maraschino cherries, for garnish

## Preparation

Combine the whisky, lemon, lime, and syrup. Shake over ice, add a cherry, and
serve ice cold in a cocktail glass.

_Food Network_
