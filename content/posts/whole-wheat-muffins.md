---
title: "Whole Wheat Muffins"
author: "pencechp"
date: "2015-06-02"
categories:
  - breakfast
tags:
  - muffin
  - untested
---

## Ingredients

*   ½ cup melted unsalted butter, more for greasing tins
*   2 ½ cups whole wheat flour, preferably pastry flour
*   ¾ to 1 cup sugar, depending on sweetness of fruit
*   2 teaspoons baking powder
*   ¼ teaspoon baking soda
*   ¼ teaspoon salt
*   1 cup mashed or puréed banana, sweet potato, apple, zucchini, cooked or canned pumpkin, or other fruits or vegetables
*   1 egg, beaten
*   ½ cup buttermilk

## Preparation

Preheat oven to 375 degrees and grease two 6-cup muffin tins or fill with liners. In a large bowl, mix together the flour, sugar, baking powder, baking soda and salt. In another bowl, whisk together the melted butter, banana, egg and buttermilk. Fold wet mixture into dry mixture and stir until just combined.

Fill muffin tins or liners; bake for about 25 to 30 minutes, or until muffins are puffed and turning golden brown on top. Serve warm if possible.

_Mark Bittman, NYT_
