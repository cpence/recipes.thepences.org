---
title: "Aviation"
author: "juliaphilip"
date: "2016-09-06"
categories:
  - cocktails
tags:
  - gin
  - violette
---

## Ingredients

*   2 ounces gin
*   1/4 ounce maraschino liqueur (preferably Luxardo)
*   1/2 ounce lemon juice
*   1/4 ounce crème de violette (or 1/4 ounce simple syrup)
*   Garnish: brandied cherry (preferably Luxardo)

## Preparation

Combine all ingredients in cocktail shaker, shake with ice. Strain into cocktail or coupe glass, garnish with cherry.
