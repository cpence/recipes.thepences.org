---
title: "Slow-Cooked Carnitas"
author: "pencechp"
date: "2013-09-29"
categories:
  - main course
tags:
  - mexican
  - pork
---

## Ingredients

*   4 pounds bone-in pork shoulder roast, cut into 1 1/2- to 2-inch slabs
*   Salt

## Preparation

1\. Moist cooking. Heat the oven to 375 degrees. Cut each slab of pork in half and lay the pieces in a baking dish (they should fit into a 13 x 9-inch baking dish without being crowded). Liberally sprinkle with salt (about 1 teaspoon) on all sides. Pour 1/3 cup water around the meat, cover tightly with foil, and bake for 1 hour.

2\. Dry cooking. Raise the oven temperature to 450 degrees. Uncover the meat and cook until the liquid has completely reduced and only the rendered fat remains, about 30 minutes. Now, roast, carefully turning the meat every 7 or 8 minutes, until lightly browned, about 20 minutes longer. Break the meat into large pieces and serve on a warm platter, sprinkled with salt.

_Rick Bayless_
