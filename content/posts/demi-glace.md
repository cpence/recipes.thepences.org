---
title: "Demi-Glace"
date: 2021-08-09T18:25:04+02:00
categories:
  - miscellaneous
tags:
  - french
  - sauce
---

## Ingredients

- 1/2 teaspoon dried thyme
- 3 to 4 fresh parsley stems
- 7 to 8 whole black peppercorns
- 1 bay leaf
- 2 cups [sauce espagnole]({{<relref sauce-espagnole.md>}})
- 2 cups beef stock

## Preparation

Wrap the thyme, parsley stems, peppercorns, and bay leaf in a piece of
cheesecloth.

Combine the brown sauce and the beef stock in a heavy-bottomed saucepan. Bring
to a boil over medium-high heat, then lower heat to a simmer. Add the
cheesecloth bundle, and reduce the liquid for about 45 minutes, or until the
total volume has reduced by half. Remove pan from heat and retrieve the sachet.

Carefully pour the demi-glace through a wire mesh strainer lined with a piece of
cheesecloth.

_Spruce Eats_
