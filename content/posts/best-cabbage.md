---
title: "Best Cabbage"
author: "pencechp"
date: "2017-03-26"
categories:
  - side dish
tags:
  - cabbage
---

## Ingredients

*   3 slices bacon
*   4 tbsp. butter
*   1 cup chopped onion
*   1 head of cabbage, chopped
*   seasoning to taste

## Preparation

Fry bacon. Remove bacon and add butter. Saute onion. Add a splash of water to deglaze the pan. Add cabbage, season, stir, and simmer for about 30 minutes. (Optional: return bacon to pan, add hamburger meat or sausage.)

_Luckett Farms_
