---
title: "Bœuf Bourgignon"
date: 2020-09-28T12:21:41+02:00
categories:
    - main course
tags:
    - beef
    - french
    - stew
---

## Ingredients

*   150 gr lardons fumés [150 g. bacon, chopped]
*   800 gr carbonnades de boeuf [800 g. beef stew meat]
*   3 gousses d'ail [3 cloves garlic]
*   1 oignon ciselé [1 onion, chopped]
*   2 échalotes ciselées [2 shallots, chopped]
*   2 carottes en rondelles [2 carrots, sliced in rounds]
*   500 ml vin rouge [500 mL red wine]
*   Un bouquet de thym [a bouquet of thyme]
*   250 gr champignons de Paris [250 g. white mushrooms]
*   2 cuil. à soupe persil haché [2 tbsp. chopped parsley]
*   2 cuil. à soupe concentré de tomates [2 tbsp. tomato paste]
*   1 cuil. à soupe farine [1 tbsp. flour]
*   Pastis
*   Beurre
*   Sel
*   Poivre

## Preparation

Faites dorer les lardons dans le beurre, puis retirez-les de la poêle. Faites
dorer les carbonnades de tous côtés dans la graisse des lardons. Saupoudrez de
farine et remuez. Ajoutez l’ail pressé, l'oignon, les échalotes, les carottes et
le concentré de tomates. Faites cuire 2 minutes en remuant.

[Brown the bacon in some butter, then take it out of the pan. Brown the stew
meat on all sides in the bacon fat. Sprinkle with flour, and mix. Add the garlic
(pressed), the onion, the shallots, the carrots, and the tomato paste. Cook for
two minutes, stirring.]

Déglacez la poêle avec le vin. Ajoutez les lardons, le bouquet de thym et une
goutte de Pastis, puis remuez. Laissez mijoter à couvert (CP: partiellement vers
la fin) et à feu doux pendant environ 3 heures. Versez éventuellement un peu de
vin s’il n’y a pas assez de jus. Ajoutez les champignons pendant les 10
dernières minutes de cuisson. Salez et poivrez. Garnissez le bœuf bourguignon de
persil avant de servir. Servez avec une purée de pommes de terre crémeuse et une
salade verte.

[Deglaze the pan with the wine. Add the bacon, the thyme, and a splash of
pastis, then mix. Let simmer, covered (CP: partially covered near the end to let
it thicken), over low heat for around 3 hours. Add a bit more wine if there
isn't enough juice. Add the mushrooms during the last 10 minutes of cooking.
Season with salt and pepper. Garnish with parsley, and serve with mashed
potatoes and a green salad.]

*Source*

