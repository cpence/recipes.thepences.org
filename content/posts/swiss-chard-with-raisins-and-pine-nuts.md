---
title: "Swiss Chard with Raisins and Pine Nuts"
author: "pencechp"
date: "2011-10-21"
categories:
  - side dish
tags:
  - greens
---

## Ingredients

*   5-6 tbsp. golden or dark raisins
*   2 bunches red or rainbow Swiss chard (or 2 bags of spinach)
*   5 tbsp. pine nuts
*   3-4 tbsp. olive oil
*   6-8 garlic cloves, coarsely chopped
*   1-2 tbsp. water, if needed
*   1/2 - 1 tsp. salt
*   1/4 - 1/2 tsp. pepper

## Preparation

Unbundle the chard, rinse, drain and dry well. Place the raisins in a small bowl; add very hot water to cover, and soak until plump, 10-15 minutes. Drain well and pat dry with paper towels, then set aside. While the raisins are plumping, tear the chard leaves from the stems, discard stems. Chop the leaves coarsely. Heat the olive oil in a pan over medium-low heat. Add the garlic, pine nuts, and soaked raisins, and cook until the nuts and the garlic are lightly golden, 3-6 minutes. Increase the heat to medium, add the chard, toss to coat with the oil mixture and cook, stirring, until it wilts and gets tender and begins to release a bit of liquid, approximately 2-3 minutes more. Season with salt and pepper to taste.

_Penzey's Spices_
