---
title: "Tartiflette"
date: 2021-01-09T21:47:36+01:00
categories:
  - main course
tags:
  - french
  - potato
---

## Ingredients

- 1 Reblochon [1 wheel Reblochon de Savoie]
- 1 kg de pommes de terre à chair ferme [2 lb firm-flesh potatoes]
- 200 g de lardons fumés [200g bacon, chopped]
- 2 gros oignons (200 g environ) [2 large onions, around 200g]
- 10 cl de vin blanc [1/2 c white wine]
- Poivre du moulin [fresh-ground pepper]
- En option : noix de muscade râpée [ground nutmeg (optional)]

## Preparation

Préchauffez votre four à 200°C. [Preheat your oven to 400F.]

Épluchez et découpez les pommes de terre en morceaux (environ 5mm). Découpez les oignons en rondelles. [Peel and chop the potatoes into around 1/4" pieces. Slice the onions into rounds.]

Dans une poêle, faites revenir les lardons avec les oignons afin qu'ils commencent à dorer (5-10 min). Ajoutez les pommes de terre et laisser cuire à feu moyen durant 30-45 minutes. Déglacez au vin blanc et laisser cuire 5 minutes. Poivrez (pas besoin de saler avec les lardons), et ajoutez la noix de muscade selon vos goûts. [In a large skillet, cook the bacon and onions until they start to brown (5-10 min). Add the potatoes and let them cook over medium heat for 30-45 minutes. Deglaze the pan with the wine and cook for five minutes. Season with pepper (no need for salt with the bacon), and add nutmeg to taste.]

Découpez votre Reblochon en deux dans le sens de l'épaisseur en conservant la croûte. Conservez une face pour mettre sur le dessus du plat afin de faire gratiner (que vous découperez en 2 ou 4 selon la forme de votre plat), et découpez l'autre face en petits morceaux à intégrer dans la préparation pommes de terre-lardons-oignons. [Cut your Reblochon in half lengthwise, keeping the rind. Keep one of the halves to melt on top of the dish (which you can cut into 2 or 4 depending on the shape of your dish), and cut the other half into small pieces to mix into the potato-onion-bacon mixture.]

Dans un plat allant au four, disposez le mélange pommes de terre-lardons-oignons et les morceaux de reblochon puis déposez sur le dessus le ½ reblochon pour gratiner, croûte vers le haut. [In an oven-safe baking dish, add the potato-bacon-onion mixture and the pieces of Reblochon, then set the half-reblochon on top to melt, rind toward the top.]

Placez au four durant 15 à 20 minutes afin que le Reblochon dore. Servez chaud avec une salade verte. [Put in the oven for 15-20 minutes so that the Reblochon melts. Serve hot with a green salad.]

_Reblochon AOP_
