---
title: "Poached Salmon with Herb and Caper Vinaigrette"
author: "pencechp"
date: "2010-04-01"
categories:
  - main course
tags:
  - fish
---

## Ingredients

*   2 lemons
*   2 tablespoons chopped fresh parsley leaves, stems reserved
*   2 tablespoons chopped fresh tarragon leaves, stems reserved
*   2 small shallots, minced (about 4 tablespoons)
*   1/2 cup dry white wine
*   1/2 cup water
*   1 skinless salmon fillet (1 3/4 to 2 pounds), about 1 1/2 inches at thickest part, white membrane removed, fillet cut crosswise into 4 equal pieces
*   2 tablespoons capers, rinsed and roughly chopped
*   1 tablespoon honey
*   2 tablespoons extra-virgin olive oil

## Preparation

1\. Cut top and bottom off 1 lemon; cut into 8 to ten 1/4-inch-thick slices. Cut remaining lemon into 8 wedges and set aside. Arrange lemon slices in single layer across bottom of 12-inch skillet. Scatter herb stems and 2 tablespoons minced shallots evenly over lemon slices. Add wine and water.

2\. Place salmon fillets in skillet, skinned-side down, on top of lemon slices. Set pan over high heat and bring liquid to simmer. Reduce heat to low, cover, and cook until sides are opaque but center of thickest part is still translucent (or until instant-read thermometer inserted in thickest part registers 125 degrees), 11 to 16 minutes. Remove pan from heat and, using spatula, carefully transfer salmon and lemon slices to paper towel-lined plate and tent loosely with foil.

3\. Return pan to high heat and simmer cooking liquid until slightly thickened and reduced to 2 tablespoons, 4 to 5 minutes. Meanwhile, combine remaining 2 tablespoons shallots, chopped herbs, capers, honey, and olive oil in medium bowl. Strain reduced cooking liquid through fine-mesh strainer into bowl with herb-caper mixture, pressing on solids to extract as much liquid as possible. Whisk to combine; season with salt and pepper to taste.

4\. Season salmon lightly with salt and pepper. Using spatula, carefully lift and tilt salmon fillets to remove lemon slices. Place salmon on serving platter or individual plates and spoon vinaigrette over top. Serve, passing reserved lemon wedges separately.

_Cook's Illustrated, May 2008_
