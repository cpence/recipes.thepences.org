---
title: "Sugar Snap Salad"
author: "pencechp"
date: "2011-10-05"
categories:
  - side dish
tags:
  - peas
---

## Ingredients

*   1 1/4 lb. sugar snap peas, trimmed, stringed, cut in half on diagonal
*   Kosher salt
*   3 Tbsp. extra-virgin olive oil
*   1 Tbsp. (or more) fresh lemon juice
*   1 tsp. white wine vinegar
*   1/2 tsp. sumac plus more for garnish
*   1 bunch radishes (about 6 oz.), trimmed, thinly sliced
*   4 oz. ricotta salata or feta, crumbled
*   black pepper
*   2 Tbsp. coarsely chopped fresh mint

## Preparation

Fill a large bowl with ice water; set aside. Cook peas in a large pot of boiling salted water until crisp-tender, about 2 minutes. Drain; transfer to bowl with ice water to cool. Drain peas; transfer to a kitchen-towel-lined baking sheet to dry.

Whisk oil, 1 Tbsp. lemon juice, vinegar, and 1/2 tsp. sumac in a small bowl.  Toss peas, radishes, and cheese in a large bowl. (Can make ahead, store dressing and salad separately.)

Add dressing to salad and toss to coat.  Season salad with salt, pepper, and more lemon juice, if desired.  Garnish with mint and sprinkle with sumac.

_Bon Appetit, 7/2011_
