---
title: "Swiss Chard with Olives and Lemon"
author: "pencechp"
date: "2010-10-01"
categories:
  - side dish
tags:
  - greens
---

## Ingredients

*   3 large bunches Swiss Chard (about 2 1/4 pounds total)
*   5 tablespoons extra-virgin olive oil, divided
*   1/3 cup quartered pitted oil-cured black olives (2 to 3 ounces)
*   2 garlic cloves, crushed
*   1 tablespoon fresh lemon juice

## Preparation

Cut stem from center of each chard leaf. Slice stems crosswise into 1/4-inch pieces; place in medium bowl. Cut leaves crosswise into 1 1/2-inch-wide strips.

Bring large pot of water to boil; salt generously. Add chard stems; cook until just tender, 3 to 4 minutes. Add chard leaves. Cook until just tender, stirring occasionally, about 2 minutes. Drain in large colander, pressing out any water. DO AHEAD: Can be made 2 hours ahead. Let stand in colander at room temperature.

Heat 4 tablespoons oil in heavy large skillet over medium heat. Add olives and garlic. Sauté until fragrant, about 2 minutes. Add chard and chard stems. Toss until heated through and any remaining water evaporates, about 4 minutes. Mix in lemon juice and remaining 1 tablespoon oil; season to taste with salt and pepper. Transfer to bowl and serve.

_Bon Appetit, March 2010_
