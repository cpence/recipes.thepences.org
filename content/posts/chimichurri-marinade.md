---
title: "Chimichurri Marinade"
author: "pencechp"
date: "2013-12-04"
categories:
  - miscellaneous
tags:
  - marinade
  - sauce
---

\[**CP:** Originally meant to go on steak, this goes on everything.  Mom did quail in it and it was really nice.\]

## Ingredients

*   1/2 cup red wine vinegar
*   1 teaspoon kosher salt plus more
*   3–4 garlic cloves, thinly sliced or minced
*   1 shallot, finely chopped
*   1 Fresno chile or red jalapeño, finely chopped
*   2 cups minced fresh cilantro
*   1 cup minced fresh flat-leaf parsley
*   1/3 cup finely chopped fresh oregano
*   3/4 cup extra-virgin olive oil

## Preparation

Combine vinegar, 1 teaspoon salt, garlic, shallot, and chile in a medium bowl and let stand for 10 minutes. Stir in cilantro, parsley, and oregano. Using a fork, whisk in oil. Remove 1/2 cup chimichurri to a small bowl, season with salt to taste, and reserve as sauce. Put meat in a glass, stainless-steel, or ceramic dish. Toss with remaining marinade. Cover and chill for at least 3 hours or overnight. Cook meat, serve with sauce.

_Matt Lee and Ted Lee, Epicurious_
