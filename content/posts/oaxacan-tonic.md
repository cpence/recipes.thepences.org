---
title: "Oaxacan Tonic"
author: "pencechp"
date: "2015-01-01"
categories:
  - cocktails
tags:
  - untested
  - vodka
---

## Ingredients

*   2 oz Cathead pecan vodka
*   1/2 oz. cold strong coffee
*   9 drops Bittermens Xocolatl mole bitters
*   3 oz. tonic water

## Preparation

Combine all ingredients in a Collins glass. Stir, add ice.

_Garden & Gun, June/July 2014_
