---
title: "Lemon Pudding Cakes"
author: "pencechp"
date: "2014-11-18"
categories:
  - dessert
tags:
  - cake
  - lemon
  - untested
---

## Ingredients

*   1 c. granulated sugar, divided
*   1/3 c. all-purpose flour
*   1/8 tsp. salt
*   1 c. low-fat milk
*   2 tsp. freshly grated lemon zest 
*   1/2 c. lemon juice
*   2 tbsp. butter, melted
*   2 large egg yolks
*   3 large egg whites, at room temperature
*   Confectioners' sugar for dusting

## Preparation

Preheat oven to 350 degrees F. Coat eight 4- to 6-ounce (1/2- to 3/4-cup) ovenproof ramekins or custard cups with cooking spray; place in a large deep roasting pan or baking dish. Put a kettle of water on to boil for the water bath.

Whisk 3/4 cup granulated sugar, flour, and salt in a medium bowl. Make a well in the dry ingredients. Add milk, lemon zest, lemon juice, butter, and egg yolks. Whisk until smooth.

Beat egg whites in a mixing bowl with an electric mixer on medium speed until soft peaks form (see Tip). Gradually add the remaining 1/4 cup sugar and continue beating until stiff and glossy. Fold the egg whites into the batter (it will be thinner than other cake batter and it's okay if it's a little lumpy).

Evenly divide among the prepared ramekins placed in the roasting pan.

Place the roasting pan in the oven and carefully pour in enough boiling water to come almost halfway up the sides of the ramekins.

Bake the pudding cakes until golden brown and the cakes have pulled away slightly from the sides of the ramekins, 25 to 30 minutes. Transfer the ramekins to a wire rack to cool for 15 minutes. Dust with confectioners' sugar and serve warm or at room temperature.

_EatingWell.com, via Delish_
