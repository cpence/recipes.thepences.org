---
title: "Mean Green Pig (Tomatillo Chili)"
author: "pencechp"
date: "2012-02-03"
categories:
  - main course
tags:
  - chile
  - greenchile
  - mexican
  - newmexican
  - pork
  - stew
  - tomatillo
---

## Ingredients

*   3 1/2 lb. tomatillos, husked, rinsed, and halved 
*  1/4 c. olive oil
*  1/2 tsp. salt
*  2 1/2 lb. boneless pork shoulder, cut into 1" pieces
*  fresh black pepper
*  2 medium yellow onions, chopped
*  3 cloves garlic, minced
*  2 jalapeños, diced
*  2 poblanos, roasted and diced small \[CP: sub. green chiles for these two chiles\]
*  2 c. chicken stock
*  fresh cilantro
*  queso fresco

## Preparation

Heat oven to 400.  Toss half the tomatillos with 2 tbsp. olive oil and 1/4 tsp. salt, roast for 45 minutes.  Meanwhile, puree the remaining tomatillos in a food processor.  Reserve 1 1/2 c. in a bowl.  Add the roasted tomatillos to the processor, pulse until slightly chunky.  Season the pork with salt and pepper to taste.  Heat the remaining 2 tbsp. olive oil in a large skillet.  Brown the pork in batches, transfer to slow cooker.  Add onions and jalapeños to skillet, cook until softened.  Add garlic and cook 1 minute.  Put everything except the reserved tomatillo into the slow cooker.  Cook on low until the pork is falling apart tender, about 6 hours.  Add reserved tomatillo puree to the pot, stir to incorporate.  (Alternately, cook in a dutch oven on low heat until the meat is tender, about 2 hours.)

Garnish with cilantro and queso fresco.

_Adapted from Everyday Food Magazine, via Austin-American Statesman_
