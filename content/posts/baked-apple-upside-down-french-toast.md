---
title: "Baked Apple Upside-Down French Toast"
author: "pencechp"
date: "2010-05-09"
categories:
  - breakfast
tags:
  - apple
  - untested
---

## Ingredients

*   3 lbs. Granny Smith apples, peeled, quartered and thinly sliced
*   6 Tablespoons butter
*   ¾ cup granulated sugar
*   1 teaspoon cinnamon
*   8 slices French bread, sliced ¾” thick
*   6 to 7 eggs
*   ½ teaspoon vanilla
*   ¼ teaspoon freshly grated nutmeg
*   1 cup milk
*   ½ cup heavy cream

## Preparation

Melt butter in large sauté pan and add sliced apples. Top apples with sugar and cinnamon and stir to incorporate. Saute over medium heat until apples are limp and translucent, about 10 minutes, stirring so they don’t scorch.

Pour apples into a 9” x 13” glass ovenproof baking dish. Top with sliced French bread. Fill any spaces with small pieces of sliced bread so that entire surface is covered. Whisk eggs with vanilla, nutmeg, milk and cream. Pour this mixture over the Bread evenly so that all pieces absorb the liquid. Cover with plastic wrap and refrigerate until morning.

The next morning, pre-heat the oven to 400º. Bake French toast for 25 minutes or until bread is golden and custard is set. Invert carefully onto a sheet pan and cut into squares to serve, apple-side-up. You may add the syrup of your choice and a dollop of crème fraiche. Serves 6 to 8.

_Shelburne Inn_
