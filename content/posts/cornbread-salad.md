---
title: "Cornbread Salad"
author: "pencechp"
date: "2010-05-08"
categories:
  - main course
  - side dish
tags:
  - pence
  - salad
---

## Ingredients

*   2 packages jalapeno cornbread mix - cooked and crumbled
*   1 can drained niblet corn
*   1 cup chopped green onion
*   1 cup chopped tomatoes
*   1 cup chopped bell pepper
*   8 slices chopped crisp bacon
*   1 cup cheddar cheese - chopped in small pieces or coarsely grated
*   1 1/2 cups mayonnaise
*   1 1/2 cups sour cream
*   1 tablespoon chili powder

Optional: sliced avocado to garnish. Can be served with salsa.

## Preparation

Combine salad ingredients in large bowl. Combine dressing ingredients and stir in. Refrigerate - it is better the second day.
