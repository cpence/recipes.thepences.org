---
title: "Black Bean Quinoa Salad"
author: "pencechp"
date: "2011-05-06"
categories:
  - main course
  - side dish
tags:
  - mango
  - quinoa
  - untested
  - vegan
  - vegetarian
---

## Ingredients

*   1 mango, peeled and diced small
*   1 red bell pepper, seeded and diced small
*   1 c. thinly sliced scallions
*   1 c. chopped cilantro
*   1 tbsp. vegetable oil
*   2 tbsp. wine or balsamic vinegar
*   1/4 tsp. salt
*   1/2 tsp. black pepper
*   2 c. cooked quinoa, cooled (~3/4 c. raw)
*   1 15-oz. can black beans, rinsed and drained
*   1-2 finely diced jalapeño peppers (optional)

## Preparation

Mix everything together and serve over greens.

_Penzey's Spices_
