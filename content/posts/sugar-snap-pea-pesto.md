---
title: "Sugar Snap Pea Pesto"
author: "pencechp"
date: "2016-12-29"
categories:
  - main course
tags:
  - pasta
  - peas
---

## Ingredients

*   4 cups sugar snap peas
*   1 large onion, sliced
*   1 tablespoon butter
*   1 cup parmeggiano reggiano (parmesan) cheese
*   1 clove garlic
*   1 pound penne rigate
*   1 cup olive oil
*   1/2 cup pignoli nuts
*   1 pinch crushed red pepper flakes
*   1 tablespoon fresh spearmint
*   Salt and pepper, to taste

## Preparation

Prep the sugar snap peas, pulling the string off the side. Then blanch them in salted boiling water for a minute (basically, you get the water boiling, you add the peas to the water, once it starts boiling again, you can fish them out). Refresh them in a bowl of salted ice water. Cut them up in halves or thirds, depending on their size (1/2" size is good). Reserve 1.5 cups of the prepped peas. The rest will go in the pesto.

Put a large pot of salted water on high heat on the stove for the pasta.

Heat the butter and 1 tbsp of the olive oil together in a heavy bottomed pan. Add the garlic clove to the heated butter and oil until it gets a nice lightly toasted color. You can pull it out now and reserve it. Add the sliced onions and the crushed red pepper and cook on medium heat until they are caramelized. This takes a little while (as much as 20 min) so be patient, young grasshopper.

While you are waiting on the onions to caramelize, you can start work on your pesto. If you have a hunk of parmesan, cut it into smaller pieces and blitz it in the food processor. Make sure you have a cup. Next you can blitz the pine nuts.

Once the onions are finished caramelizing, you can pull them off the heat so they cool slightly. Then you can add the onions, the cooked garlic clove, and the non-reserved peas (i.e., the majority of them) to the food processor with the pine nuts. Go ahead and add in stages if the food processor is getting too full. Pulse for the first few times to get it started and then hold down the button. You can add the olive oil through the hole in the top to keep the mixture wet and moving. When all the ingredients are processed, it should be grainy and wet, but will never be totally smooth. That's ok.

Once the water is boiling, drop the pasta and cook according to package directions.

Do a chiffonade of the mint -- this means you stack the mint leaves together and roll them up like a little cigar. Then you cut them up cross-wise as thinly as possible.

When the pasta is ready, reserve a ladleful of pasta water. Drain the pasta and put it back in the pot you cooked it in. Add the pesto, a few tablespoons at a time, tossing to combine. If it is too dry, add the reserved pasta water, a little at a time. Once you have the right pesto-pasta ratio (this is up to you), taste to see if it needs more salt and pepper and add as necessary. Toss in the reserved peas and the mint. This is good at room temperature too, so you can make it ahead of time.

_JORJ, Food52_
