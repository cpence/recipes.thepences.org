---
title: "Pumpkin Soup (Sopa de calabaza)"
author: "pencechp"
date: "2010-11-04"
categories:
  - main course
  - side dish
tags:
  - mexican
  - pumpkin
  - soup
  - vegan
  - vegetarian
---

## Ingredients

Makes: 6 servings

*   9 cups water
*   4 cloves garlic
*   1 teaspoon salt
*   1/2 white onion
*   2 1/4 pounds pumpkin or winter squash, peeled, seeded, cut in 1/2-inch cubes (about 4 cups)
*   1 tablespoon vegetable oil
*   1/2 pound tomatoes, finely chopped
*   2 sprigs epazote

Toppings: Crumbled queso fresco, chopped onion, chopped green chilies, lime quarters

## Preparation

1\. Combine water, 2 of the garlic cloves and salt in a stockpot. Cut one slice off the onion half; add to pot. Heat to a boil; cook 5 minutes. Meanwhile, chop remaining onion; reserve. Add pumpkin to the pot; lower heat to medium. Cook until tender, about 20 minutes. Discard onion slice and garlic.

2\. Finely chop remaining 2 garlic cloves. Heat oil in a skillet over medium heat. Add chopped garlic and chopped onion; fry until translucent, 1 minute. Add tomatoes; fry until reduced, about 5 minutes.

3\. Place about 1 cup cooked pumpkin in a blender with some broth; puree. Return to pot. Add tomato mixture to soup. Season with salt if necessary. Add epazote; simmer over low heat, about 10 minutes. Set aside to season, about 30 minutes. Heat through; serve with toppings as desired.

_Diana Kennedy, Chicago Tribune_
