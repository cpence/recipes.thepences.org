---
title: "Trout Schnitzel with Lemon-Chile Butter"
author: "pencechp"
date: "2012-07-21"
categories:
  - main course
tags:
  - chile
  - fish
---

## Ingredients

*   6 tablespoons unsalted butter, softened
*   6 anchovy fillets, minced
*   1 fresh red chile, minced
*   1 medium shallot, minced
*   1 1/2 teaspoons finely grated lemon zest
*   Salt and freshly ground pepper
*   2 large eggs
*   3 cups soft, fresh, coarsely ground brioche bread crumbs
*   4 large skinless trout fillets (8 ounces each) \[CP: any thin/medium white fish fillet\]
*   Vegetable oil, for frying
*   Lemon wedges, for serving

## Preparation

Preheat the oven to 325°. In a bowl, blend the butter, anchovies, chile, shallot and lemon zest and season with salt and pepper.

In a large, shallow bowl, beat the eggs. Put the bread crumbs in another large shallow bowl. Season the trout with salt and pepper and dip each fillet in the egg, letting the excess drip off. Dredge the trout in the bread crumbs, pressing to help them adhere.

Set a plate lined with paper towels and a baking sheet with a rack near the stove. In a large nonstick skillet, heat 1/4 inch of oil until shimmering. Add 2 of the trout fillets and cook over moderately high heat, turning once, until browned and crisp, 1 1/2 to 2 minutes per side; reduce the heat to moderate if the fillets brown too quickly. Drain the trout on the paper towels, transfer the fillets to the rack and keep warm in the oven. Using a slotted spoon, discard any dark crumbs in the skillet and add more oil if needed to fry the remaining 2 trout fillets. Serve the trout with the lemon-chile butter and lemon wedges.

_Food and Wine, January 2012_
