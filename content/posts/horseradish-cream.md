---
title: "Horseradish Cream"
author: "juliaphilip"
date: "2010-03-31"
categories:
  - miscellaneous
tags:
  - irish
---

## Ingredients

*   1 c sour cream
*   6 tbl prepared white horseradish (4 oz)
*   1 tbl finely chopped dill pickle
*   1 tbl chopped fresh chives or green onion tops

## Preparation

Whisk all ingredients in small bowl to blend. Cover and refrigerate at least 2 hours. Can be made at least 2 days ahead, keep refrigerated.
