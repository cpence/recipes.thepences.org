---
title: "Top of the World Lamb Tagine"
author: "pencechp"
date: "2011-01-23"
categories:
  - main course
tags:
  - lamb
  - moroccan
---

## Ingredients

_For the tagine:_

*   6 pounds lamb shanks
*   Salt and pepper
*   2 tablespoons butter
*   2 large onions, thickly sliced
*   Pinch of saffron threads
*   6 garlic cloves, chopped
*   One 2-inch chunk fresh ginger, peeled and slivered
*   1 small cinnamon stick
*   1 rounded teaspoon coriander seeds
*   1 rounded teaspoon cumin seeds
*   1 tablespoon ground ginger
*   2 scant teaspoons cayenne
*   1 cup golden raisins
*   2 cups pitted prunes
*   4 cups chicken broth or water
*   1 cup tomato puree

_For the garnish:_

*   1 tablespoon butter
*   1 cup whole blanched almonds
*   Large pinch of salt
*   Small pinch of sugar

## Preparation

Serves 6; Total Time: 4½ Hours

1\. Season lamb generously with salt and pepper and set aside for 2 hours (or refrigerate over-night). Preheat oven to 325 degrees.

2\. Melt butter in a large skillet. Add onions, season with salt and crumble saffron over them. Saute over medium heat until softened and slightly browned. Add garlic, fresh ginger, cinnamon stick, coriander and cumin seeds, powdered ginger and cayenne. Stir together, remove from heat and correct the seasoning. Add raisins and half the prunes.

3\. Put the lamb in an enamelware dutch oven or deep sided baking dish and spread the onion mixture over the meat. Stir together broth or water and tomato puree and pour over. Cover the pan with foil and a tight-fitting lid.

4\. Bake for about 2 hours, or until meat is meltingly tender.

5\. Remove the foil and lid, add the second cup of prunes and submerge them. Raise the oven temperature to 400 degrees. Return lamb to the oven, uncovered, for about 15 minutes, to let it brown a bit.

6\. Carefully lift meat from sauce and put in a low wide bowl or storage container. Skim any fat from the surface of the sauce. If sauce seems thin, pour it into a low saucepan and reduce over high heat. Check the seasoning and adjust. Pour sauce over meat and let it cool to room temperature. Refrigerate.

7.The next day, remove and discard any congealed fat. Gently reheat the stew, covered, in a low oven.

8\. Just before serving, fry the almonds for the garnish. Heat the butter in a small skillet over medium heat and fry the almonds gently, stirring occasionally. When the almonds are golden, blot on paper towels and sprinkle with the salt and sugar.

9\. Transfer tagine to a warmed platter and scatter almonds over lamb.

_David Tanis, Wall Street Journal_
