---
title: "Almond Crescents"
author: "juliaphilip"
date: "2010-12-29"
categories:
  - breakfast
tags:
  - danish
  - pastry
---

## Ingredients

*   1/2 recipe [Danish pastry dough]( {{< relref "danish-pastry-dough" >}} )
*   Almond filling (recipe follows)
*   1 egg, slightly beaten
*   sugar
*   sliced almonds

## Preparation

1\. Roll Danish pastry dough out on a lightly floured pastry cloth or board to a 20x15-inch rectangle; trim edges even; cut in twelve 5-inch squares with a sharp knife.

2\. Spoon almond filling onto one corner of each square, dividing evenly.  Roll each square around filling to opposite corner.

3\. Place pastries, points down, 2 inches apart, on greased cooky sheet.  Curve into crescent shapes.  Let rise in a warm place, away from draft, until double in bulk, about 30 minutes.  Brush with egg; sprinkle with sugar and almonds.

4\. Place in hot oven (400 degrees); reduce heat immediately to 350.  Bake 20 to 25 minutes, or until puffed and golden.  Remove to wire rack.  Makes 12 individual pastries.

**Almond Filling**

Beat 1 can almond paste (or tube, or marzipan, 8 ounces) with 1/4 cup (1/2 stick) softened butter and 1/4 cup sugar in a small bowl until well blended.  Makes 1 cup.

_Family Circle Cookbook_
