---
title: "Beef Noodle Soup"
author: "pencechp"
date: "2015-03-13"
categories:
  - main course
tags:
  - beef
  - chinese
  - pasta
  - soup
---

## Ingredients

*   1.1 lb. (500g) beef chuck
*   2 whole star anise
*   2 bay leaves
*   1 black cardamom pod
*   1 stick cinnamon
*   1 tbsp. Szechuan pepper
*   2-3 pieces Mandarin orange peel
*   1 tbsp. chili sauce
*   1 tbsp. dark soy sauce
*   2 tbsp. light soy sauce
*   2 tbsp. Shaoxing wine or rice wine
*   1 tbsp. sugar
*   4 tbsp. garlic
*   4 slices fresh ginger
*   3 spring onions
*   ramen noodles, or similar

## Preparation

Cut the beef into 2" pieces, then put everything in a soup pan with around 3 1/2 c. water. Heat until boiling, then simmer for 2 hours. Cook noodles and place them in a bowl, add meat and soup. Garnish with half a boiled egg and some greens. Add some home made chili oil if you dare!

_Yi Cooks_
