---
title: "Easy Artisan Rolls"
date: 2020-11-10T14:56:17+01:00
categories:
    - bread
---

## Ingredients

*   4 c. bread flour, plus extra for shaping
*   2 tsp. kosher salt
*   1 tsp. active dry yeast
*   2 c. room temperature tap water

## Preparation

In a medium-large bowl, whisk together the bread flour, salt, and yeast. Make a
well in the center and add the water. Mix with a sturdy rubber spatula until all
flour is incorporated. Don’t worry, the dough will be wet and sticky, that’s how
it should be. Cover bowl with a plastic wrap and leave to rise at room
temperature overnight or for up to 12 hours.

The following morning (or after 8-12 hours), the dough will have risen, but it
may still look shaggy and it's surface will be covered with bubbles.

Line a sheet pan with parchment paper. Preheat the oven to 425ºF.

Spread a generous ¼ cup of flour on a work surface. Dump the dough out onto the
floured surface and turn it several times to coat with flour. I like to use a
bench scraper for this.

Divide the dough into 12-16 equal portions, turning each piece in the flour to
coat. (The bench scraper is also great for cutting the dough). Shape each piece
into a ball, pulling edges under and pinching together to make a smooth top.
Invert balls and place on prepared pan, pinched side up. This will give you
craggy, rustic textured rolls. If the dough is sticky as you’re shaping, just
roll the piece in more of the flour. Let shaped rolls rise for 20 minutes.

Transfer pan to the oven. Bake 15 minutes. Rotate pan. Bake 5 minutes more or
until nicely golden. Transfer rolls to cooling rack to cool completely.

If making in advance, remove from oven when pale golden brown (about 3-4 minutes
less). Cool completely, then freeze on a baking sheet. Once frozen, transfer
rolls to a large zip lock bag and store in the freezer. To serve, allow rolls to
thaw, then heat for 8-10 minutes at 350˚F.

*Café Sucre Farine*

