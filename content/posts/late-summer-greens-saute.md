---
title: "Late-Summer-Greens Sauté"
author: "pencechp"
date: "2011-10-05"
categories:
  - side dish
tags:
  - greens
---

## Ingredients

*   1/4 c. extra-virgin olive oil
*   1 c. thinly sliced shallots
*   2 garlic cloves, crushed
*   1 tsp. crushed red pepper flakes
*   1 3/4 lb. assorted greens (kale, Swiss chard), stems removed and thinly sliced, leaves chopped
*   1/4 c. apple cider vinegar
*   3 Tbsp. unsalted butter
*   Salt and pepper

## Preparation

Heat oil in a large heavy pot over medium heat.  Add shallots and garlic.  Cook, stirring often, until soft, about 5 minutes.  Add red pepper flakes, stir 1 minute.  Add greens stems, sautee for 4 minutes.  Add leaves and cook, tossing often, until crisp-tender, about 5 minutes.  Stir in vinegar.  Add butter; toss until melted.  Season with salt and pepper.

Serves 8.

_Bon Appetit, 9/2011_
