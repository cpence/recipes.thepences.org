---
title: "Green Chile Sweet Potato Enchiladas"
author: "pencechp"
date: "2013-03-21"
categories:
  - main course
tags:
  - greenchile
  - newmexican
  - untested
  - vegetarian
---

## Ingredients

_Tomatillo Puree and Enchilada Sauce_

*   10 medium tomatillos
*   1/2 jalapeno, smoked and seeded
*   1/2 Serrano, smoked and seeded
*   8 green chiles, roasted, peeled, seeded and diced
*   1 1/2 tbsp cornstarch
*   1 1/2 tbsp olive oil
*   2 garlic cloves, peeled
*   1 1/2 cups chicken broth
*   1 tbsp lime juice
*   1/2 green bell pepper, diced
*   1 sweet onion, diced
*   1/4 tsp cayenne pepper
*   1 1/2 tsp cumin powder
*   1/2 red bell pepper, diced
*   1/2 tsp thyme

_Sweet Potato Filling and Enchiladas_

*   5 medium sweet potatoes
*   1/2 tsp cumin powder
*   1 tsp Chili powder
*   8 oz cream cheese
*   1 tsp curry powder
*   Salt and pepper
*   4 green onions
*   1/2 tsp oregano
*   16 oz Monterey Jack cheese
*   12 corn tortillas

## Preparation

For tomatillo puree: Preheat oven to 375. Place tomatillos on parchment paper or foil-lined cookie sheet. Cook 30 minutes or until soft in the middle. The last 10 minutes, place jalapeno, Serrano, and Hatch pepper on a cookie sheet and put in the oven, so the skins can blacken.

Once skin is blackened, place the peppers in a paper bag to cool. Slice tomatillos open at bottom and squeeze juice into food processor. When peppers are cooled lightly, rub paper bag until skin is peeled. Then slice Serrano and jalapeno in half. Place only half of each pepper in processor. Peel, seed, and chop Hatch pepper and set aside for enchilada sauce. Add a little water. Puree until mixed thoroughly and add to the sauce.

For enchilada sauce: In a skillet, sauté the cornstarch and olive oil over medium heat for 1 minute. Add herbs and spices and cook 11 additional minutes. Add garlic and vegetables and sauté another minute. Add broth, tomatillo puree, and lime juice. Raise heat and bring to a boil for 5 minutes. Stir constantly until thickened. Turn heat down to low and simmer for 15 minutes. Cool. Add back to the food processor and blend until smooth.

For sweet potato filling: Preheat oven to 400 degrees. Wrap sweet potatoes in foil and bake the sweet potatoes until tender, about 1 hour. Cool and peel the sweet potatoes. Place the sweet potatoes in a bowl and mix in the cream cheese, chili powder, curry, cumin, oregano, salt, and pepper until well blended. Stir in green onions.

Putting it all together: Preheat oven to 350 and grease a 13x9 baking dish. Heat vegetable oil in a skillet over medium heat and fry the tortillas, one at a time, for about 10-20 seconds per side. Remove the tortillas with tongs and drain on paper towels. Place a regular spoonful of sweet potato filling down the center of each tortilla, add 1 tablespoon cheese, roll it up, and place filled tortillas seam side down in dish. Pour enchilada sauce over the tortillas and sprinkle with Monterey Jack cheese. Bake for 30 minutes, or until the enchilada sauce is bubbling and the cheese is beginning to brown. Cool for 5 minutes and serve.

_Central Market_
