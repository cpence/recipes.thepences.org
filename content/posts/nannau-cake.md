---
title: "Nannau (Nanaw) Cake"
author: "pencechp"
date: "2010-05-09"
categories:
  - dessert
tags:
  - cake
  - pence
---

## Ingredients

*   2 sticks butter
*   2 cups sugar
*   6 eggs
*   1 12 oz. package vanilla wafers (bashed into fine crumbs)
*   1 c. nuts (pecans)
*   1 tsp. vanilla
*   7 oz. flaked coconut

## Preparation

Cream butter and sugar, add eggs one at a time.  Add crumbs, vanilla, coconut, and nuts.  Bake in tube pan that has been greased and lightly dusted with flour at 325 for 1 1/2 hours.
