---
title: "Fresh Strawberry Bars"
author: "juliaphilip"
date: "2017-05-03"
categories:
  - dessert
tags:
  - strawberry
---

## Ingredients

*   125 g / 1 stick unsalted butter, melted
*   1½ cups/ 225 g plain flour (all purpose flour)
*   1½ cups / 135 g rolled oats (traditional oats, not quick, instant or steel cut)
*   ½ cup / 110 g brown sugar
*   ½ tsp baking powder
*   Pinch of salt
*   1 egg
*   2/3 cup strawberry jam
*   2 cups chopped strawberries (about 375g)
*   2 tbsp white sugar
*   2 tsp cornflour / cornstarch

## Preparation

Preheat oven to 180C/350 (fan forced).

Spray a 20cm/9" square tin with oil and line with baking/parchment paper with overhang (so it can be lifted out once cooked).

Mix together flour, oats, sugar, baking powder and salt. Add butter and egg then mix. Measure out 1 cup of the mixture (for topping) and set aside. Place remaining mixture in the tin and press into base.

Mix together Strawberries, sugar and cornflour in a bowl (don't do this in advance because sugar will make strawberries sweat = runny filling).

Spread base with jam, then scatter over strawberries. Use your hands to crumble the remaining mixture over the top.

Bake for 35 - 40 minutes or until the top is deep golden. Remove and allow to cool for 10 minutes in the tin before lifting out of the tin using the baking paper. It will sag - don't worry, it will set.

Cut into squares. Serve at room temperature. Store in airtight container (in fridge if it's very hot where you are).

_Recipe tin eats_
