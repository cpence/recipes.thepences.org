---
title: "Chocolate Mousse"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - dessert
tags:
  - chocolate
---

## Ingredients

*   200 g dark chocolate (7 oz)
*   6 eggs
*   pinch salt

## Preparation

Melt chocolate and let cool. Separate the eggs. Add salt to the whites and beat them into firm peaks. Slowly pour the chocolate over the yolks and beat energetically.

_Translated from French by Jenny_
