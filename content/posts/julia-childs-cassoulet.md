---
title: Julia Child's Cassoulet
author: pencechp
date: 2020-01-24
categories:
    -   main course
tags:
    -   french
    -   stew
    -   untested
    -   pork
    -   lamb
---

## Ingredients

*   2 1/2 lb. bone-in pork loin, excess fat removed (preferably marinated overnight)
*   2 lb. or 5 c. dry white beans (in the US, Great Northern beans)
*   1/2 lb. fresh pork rind or salt pork rind
*   6-8 sprigs parsley
*   4 unpeeled cloves garlic
*   2 cloves
*   1/2 tsp. thyme
*   2 bay leaves
*   1 lb. fresh, unsalted, unsmoked lean bacon (or very good, lean salt pork simmered for 10 minutes in 2 qt. water, drained)
*   1 c. sliced onions
*   2–2 1/2 lb. boned shoulder or breast of mutton
*   4–6 tbsp. fresh pork fat, pork-roast drippings, goose fat, or oil
*   1 lb. cracked mutton or lamb or pork bones
*   2 c. minced onions
*   4 cloves mashed garlic
*   6 tbsp. fresh tomato purée, or tomato paste, or 4 large tomatoes, peeled, seeded, and juiced
*   1/2 tsp. thyme
*   2 bay leaves
*   3 cups dry white wine or 2 cups dry white vermouth
*   1 qt. brown stock or 3 cups canned beef stock with 1 cup water
*   salt and pepper
*   1 1/2 lb. saucisse de Toulouse or other garlic-forward sausag
*   2 c. dry white bread crumbs mixed with 1/2 c. chopped parsley
*   3–4 tbsp. pork roasting fat or goose fat

## Preparation

Bake the pork loin to an internal temperature of 175–180 degrees, and set it aside to cool. Reserve its cooking juices.

Place the beans into an 8 quart pot containing 5 quarts of water at a rolling boil. Boil for two minutes. Remove from heat and let the beans soak for 1 hour. The rest of the cooking should proceed as soon as possible after the soaking is completed.

While the beans are soaking, place the pork rind in a saucepan and cover with 1 qt. cold water. Bring to a boil and boil for one minute. Drain, rinse, and repeat the process. Then, with kitchen shears, cut the rind into 1/4" strips, and cut the strips into small triangles. Cover the rind again with 1 qt. water, bring to a simmer, and simmer slowly for 30 minutes. Set the saucepan aside.

Tie the parsley, garlic, cloves, thyme, and bay leaves in cheesecloth. Place the unsmoked bacon (or blanched salt pork), onions, the pork rind and its cooking liquid, and the herb bouquet into the kettle with the soaked beans. If you did not use salt pork so far, add 1 tbsp. salt. Bring to a simmer. Skim off any fat which might rise. Simmer, uncovered, for about 1 1/2 hours or until the beans are just tender. Add more boiling water during cooking, if necessary, to keep the beans covered. Season to taste near the end of the cooking process. Leave the beans in their cooking liquid until you are ready to use them, then drain, reserving the cooking liquid. Remove the bacon or salt pork and set aside. Discard the herbs.

Cut the lamb or mutton into chunks roughly 2" square. Dry each piece in paper towel. Pour a thin layer of fat into a heavy, 8-quart, fireproof casserole and heat until almost smoking. Brown the meat, a few pieces at a time, on all sides. Set the meat aside. Brown the bones and add them to the meat. If fat has burned, discard it and add 3 tbsp. more. Lower the heat and brown the onions lightly, about 5 minutes.

Return the bones and the lamb to the casserole and stir in the garlic, tomato, thyme, bay leaves, wine/vermouth, and stock. Bring to a simmer and season to taste with salt. Cover and simmer slowly, or in a 325-degree oven, for 1 1/2 hours. Then remove the meat to a dish; discard the bones and bay leaves. Remove all but 2 tbsp. fat and carefully correct the seasoning of the cooking liquid.

Pour the cooked and drained beans into the lamb cooking liquid. Stir in any juices you may have from roasting the pork. Add bean cooking liquid, if needed, until the beans are covered. Bring to a simmer and simmer 5 minutes, then let the beans stand in the liquid for 10 minutes. Drain the beans when you are ready for final assembly, again reserving the liquid.

Brown the saucisse de Toulouse, cut into lengths between 1/2" and 3" long (depending on size/preference) and drain on paper towels.

Cut the roast pork into 1 1/2–2" serving chunks. Slice the bacon or salt pork into serving pieces 1/4" thick. Arrange a layer of beans in the bottom of your cassoulet dish, then continue with layers of lamb, roast pork, bacon slices, sausage, and beans, ending with a layer of beans and sausage. Pour on the meat cooking juices, and enough bean cooking juice so that liquid comes just to the top layer of beans. Spread on the bread crumbs and dribble the fat over the top. You can set this aside until you're ready for final cooking (about 1h).

Preheat your oven to 375 degrees. Bring the casserole to a simmer on top of the stove. Set it in the upper level of the oven. When the top has crusted lightly, after about 20 minutes, turn the oven down to 350 degrees. Break the crust into the beans with the back of a spoon, and baste with the liquid in the casserole. Repeat this process several times, as the crust re-forms, but leave a final crust for serving. If the liquid in the cassoulet becomes too thick, add a spoonful or two of bean cooking liquid. It should bake for about one hour.

## Variations

You could use goose confit instead of the roast pork. Scrape the fat off, and cut it into serving portions, then brown it lightly in some of the fat from the package. Then arrange it directly in the final step.

If you have fresh goose, duck, turkey, or partridge, you can roast or braise it, carve it into serving pieces, and use it with or instead of the roast pork.

Ham hock or veal shank can be added to the simmer with the beans, then cut into serving pieces and added at final assembly.

*Julia Child*
