---
title: "Lamb Ribs"
date: 2021-12-23T01:30:07+01:00
categories:
  - main course
tags:
  - lamb
---

## Ingredients

- 1/2 c. fresh lemon juice
- 2 lg. cloves garlic, pressed
- 1/4 tsp. red pepper flakes
- 1/2 tsp. salt
- 1/3 c. olive oil
- 3 lb. lamb ribs (2 racks)
- 2 tsp. salt
- 1 tsp. black pepper

## Preparation

Combine the first five ingredients in a blender, and pulse on high for 1 minute
until the sauce is smooth and pale yellow.

Preheat the oven to 300 F. Pour half of the sauce over the ribs and marinate at
room temperature for 30 minutes (or refrigerate overnight).

Season the racks with salt and pepper and wrap individually with foil. Place on
a baking sheet lined with foil and bake for three hours. Remove the foil and
flip the ribs. Add extra sauce and increase the temperature to 400 F. Roast for
30 minutes, rest for at least 15 minutes, and serve with extra sauce.

_Frayed Apron_
