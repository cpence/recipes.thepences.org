---
title: "Lamb Chops Old-Dutch Style"
author: "pencechp"
date: "2010-07-16"
categories:
  - main course
tags:
  - dutch
  - lamb
---

## Ingredients

*   4 double lamb chops
*   salt and black pepper
*   125 grams butter
*   2 shallots
*   4 cloves garlic
*   2 tsp fresh rosemary
*   2 sprigs fresh thyme
*   1 small bay leaf
*   1 deciliter / ½ cup red wine
*   4 deciliter / 2 cups meat ‘fond’ (glazed crystallized drippings) or stock
*   3 tsp corn starch
*   1 tbsp parsley

## Preparation

Rub salt and freshly ground black pepper on the chops and fry them in 75 grams of butter in a skillet for three minutes each side.

Put them in a lightly buttered oven dish and bake them for 12 to 15 minutes in a warm oven at 120ºC/235ºF.

Take the dish from the oven and cover with foil or with a fitting lid.

Sautee the finely sliced shallots and pressed garlic in the grease/butter left in the skillet. Mix in the coarsely chopped rosemary and thyme, add the crumbled bay leaf and the wine. Bring to a boil and add the fond or bouillon. Slowly bring to a boil again and let it simmer for 5 minutes.

Mix in the cornstarch and pour the stock through a sieve into a small saucepan. Bring to a boil again and whisk in the rest of the butter.

Put the chops on individual plates and pour the glazed sauce over them. Garnish with parsley. Serve with pan-fried potato slices, and green beans or spinach.

_Adapted from goDutch.com_
