---
title: "Pisco Sour"
author: "pencechp"
date: "2010-12-17"
categories:
  - cocktails
tags:
  - peruvian
  - pisco
---

## Ingredients

*   1 part fresh lime juice
*   3 parts pisco
*   ice
*   sugar to taste (less than one part)
*   1 tsp. egg white
*   Angostura bitters

## Preparation

Combine lime and pisco in a blender.  Add sugar to taste.  Add plenty of ice and blend (or, alternately, shake and strain, with the egg white).  Spoon in egg white and blend again.  Serve immediately in highball or rocks glasses.  Add a drop of bitters to the top for decoration.

_Lonely Planet Peru_
