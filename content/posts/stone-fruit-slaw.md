---
title: "Stone Fruit Slaw"
author: "pencechp"
date: "2011-09-03"
categories:
  - side dish
tags:
  - peach
  - salad
---

## Ingredients

*   1 Tbsp. grated peeled fresh ginger
*   1 Tbsp. unseasoned rice vinegar
*   1 Tbsp. vinegar oil
*   2 tsp. (packed) light brown sugar
*   1/4 tsp. curry powder
*   1/8 tsp. crushed red pepper flakes
*   1 1/2 lb. assorted firm stone fruid (about 5; such as plums, nectarines, peaches, or apricots), julienned
*   2 scallions, thinly sliced diagonally
*   Kosher salt and freshly ground black pepper

## Preparation

Whisk first 6 ingredients in a medium bowl.  Add fruit and scallions; toss gently to coat.  Season to taste with salt and pepper.

_Bon Appetit, July 2011_
