---
title: "Cod with White Beans and Tarragon Mayonase"
author: "juliaphilip"
date: "2010-03-31"
categories:
  - main course
tags:
  - fish
---

## Ingredients

*   8 1/2 c water
*   1 cup dried small white beans
*   4 shallots, chopped
*   1 tsp salt
*   3/4 c mayonnaise
*   6 tbl olive oil
*   2 tsp chopped fresh tarragon
*   2 tsp tarragon vinegar
*   1 tsp dijon mustard
*   1/4 tsp Tabasco
*   1 c dry white wine
*   4 6 oz cod fillets
*   2 plum tomatoes, seeded and chopped
*   1 tbl drained capers cayenne pepper

## Preparation

Soak beans overnight or quick-soak and drain. Bring 5 1/2 c water, beans, and half of shallots to boil in a heavy medium saucepan. Reduce heat to medium-low and simmer 45 minutes. Add 1 tsp salt and simmer until beans are tender, about 30 min. Drain.

Mix mayonase, 4 tbl olive oil, tarragon, 1 tsp vinegar, mustard, and Tabasco in a bowl.

Bring remaining 3 cups of water and wine to boil in a large skillet. Add fish, bring just to a boil. Cover and remove from heat. Let stand until fish is opaque, about 10 minutes.

Meanwhile, heat remaining 2 tbl of olive oil in medium skillet over medium heat. Add remaining shallots, saute 2 minutes, then add beans, tomatoes, and 1 tsp vinegar. Saute until just heated through, about 3 minutes. Keep warm over lowest heat.

Mix 1 tbl poaching liquid into mayonnaise to thin. Divide bean mixture among plates. Using a slotted spatula, place fish atop beans. Top fish with sauce. Sprinkle with capers and caynne.
