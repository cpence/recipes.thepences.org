---
title: "Baked Oatmeal"
author: "pencechp"
date: "2013-02-03"
categories:
  - breakfast
tags:
---

## Ingredients

*   2 1/2 c. rolled oats
*   1/2 c. brown sugar
*   1/2 tbsp. cinnamon
*   1 tsp. baking powder
*   pinch salt
*   1/3 c. dried fruit \[CP: add 1/4 c. nuts to this?\]
*   1 c. milk
*   1/2 c. greek yogurt
*   1 egg
*   1 tsp. vanilla
*   1 tbsp. butter (plus extra for buttering)

## Preparation

Preheat oven to 350.  Mix first 6 (dry) ingredients in bowl.  Mix next 4 (wet) ingredients in another bowl.  Mix together wet and dry.  Butter an 8x8" glass dish, and pour into dish.  Dot with the 1 tbsp. of butter.  Bake for 30 minutes.

_Liana Krissoff, via Hannah, via Facebook_
