---
title: "Italian Sausage and Mustard Greens"
author: "pencechp"
date: "2016-12-04"
categories:
  - main course
tags:
  - greens
  - italian
  - sausage
---

## Ingredients

*   2 bunches mustard greens
*   6 italian sausages
*   2 cloves garlic, pressed
*   grated parmesan
*   lemon

## Preparation

Rinse and chop the mustard greens. Brown the sausages, very thoroughly, with a tiny bit of olive oil over medium-high heat. They do not, however, need to be fully cooked. Add the garlic and sautee for around 30 seconds. Add the greens in batches, cooking them down. Add around 1/2 cup of water (if the greens haven't produced enough of their own liquid) and reduce the heat. Cover and simmer for around ten minutes, until the greens are fully cooked. Serve with parmesan and a squeeze of lemon juice.

_Epicurious, modified_
