---
title: "Mulled Wine"
author: "pencechp"
date: "2019-01-31"
categories:
  - cocktails
tags:
  - wine
---

## Ingredients

*   1.5L red wine (two bottles; not too full-bodied)
*   1/2 orange, or some zests
*   150g sugar
*   2 cinnamon sticks
*   2 stars anise
*   2 whole cloves

## Preparation

Bring very slowly to a boil. Simmer very gently for five minutes. Filter and serve very hot.

_La Libre, translated_
