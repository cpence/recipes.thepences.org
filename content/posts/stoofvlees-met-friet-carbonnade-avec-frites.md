---
title: "Stoofvlees met friet (Carbonnade avec frites)"
author: "pencechp"
date: "2018-12-26"
categories:
  - main course
tags:
  - beef
  - belgian
  - potato
  - stew
---

## Ingredients

*   2 large onions, chopped
*   butter, cubed
*   1 kg round steak, in equal 1–1.5" pieces
*   250g of bacon (thick slices, not lardons; optional)
*   fresh-ground black pepper
*   salt
*   2–3 bottles of Sint Bernardus Abt 12
*   2 bay leaves
*   2 sprigs thyme
*   1 whole clove
*   1 tbsp. Liège syrup [CP: this was 2 tbsp., 1 tbsp. needs testing] OR 1 tbsp. brown sugar
*   enough slices of brown sandwich bread or [pain d'épices]( {{< relref "pain-depices" >}} ) to cover the surface of your pot
*   2 tbsp. (sharp) mustard
*   dash white vinegar
*   1 kg potatoes
*   [mayonnaise]( {{< relref "mayonnaise" >}} )

## Preparation

Heat a large stew-pot over medium heat and melt a knob of butter. Cook the onions for around 10 minutes, covered, and make sure not to let them brown. If using the bacon, add it now, raise the heat a bit, and stir regularly, still keeping it covered as much as possible. When the bacon is cooked (or if you're not using it), take everything out of the pot (leaving the juices) and reserve.

Add another knob of butter. Sear the meat cubes on all sides until golden brown, seasoning with a bit of salt and pepper. Reserve the meat. If you are using cassonade instead of Liège syrup, add it to the pot now, mix with all of the meat and onion juices, and let it reduce by half to make a "syrup."

Return the onions (and possibly bacon) and the meat to the pan. Deglaze with the beer, adding enough just to cover the meat all the way, and bring to a boil. Tie the thyme and bay leaves together and add the herbs, clove, and Liège syrup (if using) to the pot. Smear the bread with the mustard and add it to the pot, smeared side down.

Let the stew simmer for three and a half hours, uncovered. The cooking time will depend on the quality of the meat. Stir occasionally to check doneness. Cover only when the sauce has sufficiently thickened.

Finish the stew with a dash of white vinegar and salt and pepper to taste.

*For the frites:*

Peel the potatoes and cut them into strips (ideally, 1.3cm). Do not wash them, as this rinses off the starch. Heat the fryer to 140C. Fry the fries for the first time, but do not let them brown. Cool them in a baking dish with some paper towels. Heat the fryer to 180C, and fry the fries until golden brown and crispy. Drain the fries over some paper towel and sprinkle with salt.

Serve a portion of stew with fries and a spoonful of fresh mayonnaise.

_Dagelijkse kost + Marmiton, translated and adapted and etc._
