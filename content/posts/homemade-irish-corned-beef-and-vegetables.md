---
title: "Homemade Irish Corned Beef and Vegetables"
author: "juliaphilip"
date: "2010-03-31"
categories:
  - main course
tags:
  - beef
  - irish
  - untested
---

## Ingredients

*   6 c water
*   2 c lager beer
*   1 1/2 c kosher salt
*   1 c golden brown sugar
*   1 1/2 tbl Insta Cure No. 1 (optional)
*   1/4 c pickling spices
*   6-8 lb flat-cut beef brisket, trimmed, with some fat remaining
*   12 oz bottle Guinnesss stout or other stout or porter
*   4 bay leaves
*   1 tbl coriander seeds
*   2 whole allspice
*   1 dired chile de arbol, broken in half
*   12 baby turnips, trimmed, or 3 medium turnips or rutabagas, peeled, quartered
*   8 unpeeled medium white-skinned or red-skinned potatoes (about 3 lb)
*   6 medium carrots, peeled
*   4 medium onions, peeled, halved through root ends
*   2 medium parsnips, peeled, cut into 2-in lengths
*   2 lb head of cabbage, quartered
*   [Horseradish cream]( {{< relref "horseradish-cream" >}} )
*   [Guinness mustard]( {{< relref "guinness-mustard" >}} )

## Preparation

Pour 6 cups of water and 2 cups beer into large deep roasting pan. Add coarse salt; stir until dissolved. Add sugar, stir until dissolved. If desired, stir in Insta Cure No. 1. Mix in pickling spices. Pierce brisket all over with the tip of a small sharp knife. Submerge brisket in liquid, then top with heavy platter to weigh down. Cover and refrigerate 4 days.

Remove brisket from brine. Stir liquid to blend. Return brisket to brine; top with heavy platter. Cover; refrigerate 4 days. Remove brisket from brine. Rinse in cold running water. Can be made 2 days ahead, wrap corned beef in plastic, cover with foil, and refrigerate.

Place corned beef in a very large wide pot. Add stout and water to cover by 1 in. Wrap cheesecloth around bay leaves, coriander seeds, allspice, and chile, enclosing completely, and tie with kitchen string to secure. Add spice bag to pot with beef; bring to a boil. Reduce heat to medium-low, cover, and simmer until beef is tender, about 2 1/4 hours. Transfer beef to a large baking sheet.

Add all vegetables to liquid in pot, bring to a boil. Reduce heat to medium and boil gently until all vegetables are tender, about 25 min. Using a slotted spoon, transfer vegetables to baking sheet with beef. Return beef to pot and rewarm 5 min. Discard spice bag

Cut beef against the grain into 1/4 in thick slices. Arrange beef and vegetables on platter. Serve with horseradish cream and Guinness mustard.
