---
title: "Rigatoni alla Genovese"
author: "pencechp"
date: "2016-04-28"
categories:
  - main course
tags:
  - italian
  - onion
  - pasta
---

## Ingredients

*   2.2 lb (1 kg) yellow onions, finely chopped
*   3/4 lb. ground beef or pork
*   2 tbsp. garlic
*   1/2 lb. rigatoni
*   3/4 c. shaved parmesan cheese, plus more for serving
*   salt and pepper
*   1/4 c. cream
*   1/4-1/2 c. dry white wine
*   olive oil

## Preparation

Liberally coat the bottom of a heavy bottom sauce pot with olive oil. Add all the onions, coat with more olive oil and a liberal amount of salt. Stir to combine. Cover and heat on medium for 40 minutes, stirring every 10-20 minutes. If the onions are completely translucent and a good amount of liquid has appeared, stir in the meat and garlic. Stir hard and long to completely break apart the meat and combine it with the onions. Cover and simmer on very low for 2-3 hours, stirring every 30 mins or so.

Bring a pot of salted water to boil, cook the rigatoni until al dente.

While the pasta cooks, deglaze the pot with the wine, stir to combine and turn the heat to medium-low to cook off the wine. Reduce heat back to very low, stir the cream into the sauce, and adjust seasoning with salt and pepper. Drain the pasta and add it to the sauce, stirring to combine. Spoon the pasta onto plates and cover each with 1/4 cup shaved parmesan.

_Raymond's Food, edited_
