---
title: "Cauliflower Gratin with Manchego and Almond Sauce"
author: "pencechp"
date: "2017-10-30"
categories:
  - side dish
tags:
  - cauliflower
  - spanish
  - vegetarian
---

## Ingredients

*   3/4 cup half-and-half
*   4 tablespoons unsalted butter
*   2 tablespoons all-purpose flour
*   1 cup whole milk
*   Pinch of freshly grated nutmeg
*   Salt and freshly ground pepper
*   One 2-pound head of cauliflower, cut into 1 1/2-inch florets
*   1/2 cup whole roasted almonds with skin, plus 2 tablespoons coarsely chopped almonds (3 ounces)
*   3/4 cup plus 2 tablespoons finely shredded Manchego or other mildly nutty semi-aged sheep's- or cow's-milk cheese (3 1/2 ounces), such as Gouda
*   1 medium onion, finely chopped
*   1/4 teaspoon Pimentón de la Vera (smoked Spanish paprika)

## Preparation

Preheat the oven to 400°. In a small saucepan, heat the half-and-half until steaming, then transfer it to a food processor or blender. Add the 1/2 cup of whole almonds and process until finely ground. Let stand for 10 minutes. Strain the half-and-half through a fine sieve set over a bowl, pressing on the almonds to extract as much liquid as possible. Discard the ground almonds.

In a medium saucepan, melt 2 tablespoons of the butter. Add the flour and whisk over moderately high heat for 1 minute. Add the milk and the half-and-half and cook, whisking constantly, until thickened, 5 minutes. Remove from the heat. Add 3/4 cup of the Manchego and whisk until melted. Whisk in the nutmeg; season with salt and pepper. Keep warm.

In a large skillet, bring 1/2 inch of salted water to a boil. Add the cauliflower, cover and cook over high heat until crisp-tender, about 4 minutes. Drain the cauliflower in a colander. Wipe out the skillet.

Melt the remaining 2 tablespoons of butter in the skillet. Add the onion and cook over moderately high heat, stirring until lightly browned, about 5 minutes. Add the cauliflower and cook, stirring until lightly golden, about 2 minutes. Season lightly with salt and pepper. Transfer the cauliflower to a 7-by-10-inch glass or ceramic baking dish and spread the Manchego sauce on top.

Sprinkle the gratin with the remaining cheese, the 2 tablespoons of chopped almonds and the paprika and bake in the center of the oven for 20 minutes, or until bubbling and browned on top. Let stand for 10 minutes before serving.

_Food & Wine_
