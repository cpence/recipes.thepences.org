---
title: "Indian Tomato Mint Salad"
author: "pencechp"
date: "2017-10-30"
categories:
  - side dish
tags:
  - indian
  - mint
  - salad
  - tomato
---

## Ingredients

*   6 large ripe tomatoes, diced about 1/2"
*   4 spring onions (or yellow onions, shallots), finely chopped
*   1 large handful fresh mint leaves, finely minced
*   juice of 1 - 2 lemons
*   salt and pepper
*   cayenne pepper

## Preparation

Place tomatoes and onions into a bowl. Juice the lemons. Grind salt and pepper to taste into the lemon juice. Add any other spices, such as cayenne. Add the minced mint to the lemon dressing, mix well, then pour over the tomatoes.

_Delishably_
