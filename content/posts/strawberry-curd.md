---
title: "Strawberry Curd"
author: "juliaphilip"
date: "2017-05-03"
categories:
  - dessert
tags:
  - strawberry
---

## Ingredients

*   4 cups sliced fresh strawberries
*   1/2 cup sugar
*   2 tablespoons cornstarch
*   1/4 cup fresh lime juice
*   3 large eggs
*   2 large egg yolks
*   3 tablespoons butter

## Preparation

Process strawberries in a blender or food processor until smooth, stopping to scrape down sides as needed. Press strawberries through a large wire-mesh strainer into a medium bowl, using back of a spoon to squeeze out juice; discard pulp and seeds. Combine sugar and cornstarch in a 3-qt. saucepan; gradually whisk in strawberry puree and fresh lime juice. Whisk in 3 large eggs and 2 egg yolks. Bring mixture to a boil over medium heat, whisking constantly, and cook, whisking constantly, 1 minute. Remove from heat, and whisk in butter. Place plastic wrap directly on warm curd (to prevent a film from forming); chill 8 hours. Serve with hot biscuits, Strawberry-Lemonade Muffins, or use as a filling for tart shells.

_My Recipes_
