---
title: "Chinese Meat Pie (Xian Bing)"
author: "pencechp"
date: "2015-07-31"
categories:
  - main course
tags:
  - chinese
  - untested
---

## Ingredients

_For the filling:_

*   1/2 cup minced beef or pork
*   1 small section of scallion, finely chopped
*   1 teaspoon Chinese five spice powder
*   1/2 teaspoon salt
*   1 teaspoon grated ginger
*   2 tablespoons light soy sauce
*   Pinch of fresh ground pepper
*   2 teaspoons sesame oil
*   1 tablespoon water

_For the dough:_

*   2 cups all-purpose flour
*   1/4 cup hot water (just off the boil)
*   1/4 cup room temperature water
*   Vegetable oil for brushing
*   Pinch of salt

## Preparation

Mix around 2 cups of all-purpose flour with pinch of salt in a large bowl. Swirl the hot water in firstly and then the cold water. Mix with chop stickers during the process until you get floccule texture. Wait for cool.

Grasp all the floccules with hands and then roll into a smoothie dough. Cover with plastic wrap and set aside for 15 minutes.

In a large bowl, add water by three batches. After adding the water each time, stir the beef quickly in one direction until all the water is absorbed by the meat. Then mix with salt, soy sauce, sesame oil and herbs firstly and then followed by scallion and grated ginger. Set aside.

Divide the dough into 8 small ones.Brush some oil on surface and roll each one into a round wrapper; place around 1 tablespoon of filling in the center of the wrapper. Press the filling a little bit and seal the wrapper completely.

Press the sealed bun prepared in previous step; turn over. Brush some oil on both sides and saute with slow fire until golden-brown.

_China Sichuan Food_
