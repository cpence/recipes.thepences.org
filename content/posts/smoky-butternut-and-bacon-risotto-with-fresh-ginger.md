---
title: "Smoky Butternut and Bacon Risotto with Fresh Ginger"
author: "juliaphilip"
date: "2010-02-06"
categories:
  - side dish
tags:
  - squash
  - untested
---

## Ingredients

*   3 thick slices applewood smoked bacon, cut crosswise into matchstick-size strips
*   1 large sweet onion, diced
*   1/2 small butternut squash, peeled, halved, seeded, cut into 1/2-inch cubes
*   4 cloves garlic, finely chopped or crushed
*   1 teaspoon minced ginger root
*   5 cups chicken broth or stock
*   1/2 cup dry white wine or broth
*   1 teaspoon salt
*   1/2 teaspoon freshly ground pepper
*   2 cups arborio rice
*   2 cups shredded roasted duck, chicken or turkey, optional
*   1/2 cup finely grated aged cheese, such as manchego, smoked aged gouda, Parmesan
*   2 tablespoons chopped fresh chives or parsley or a combination

## Preparation

1\. Place bacon in a Dutch oven; cook until the fat is rendered and the strips are crisp, 7 minutes. Remove the bacon with a slotted spoon to drain on paper towels. Add the onion and squash to the bacon fat; cook, stirring often, until tender, about 8 minutes. Stir in the garlic and ginger; cook 1 minute.

2\. Meanwhile, heat the broth, wine, salt and pepper in a saucepan to a simmer. Stir rice into squash mixture; cook, stirring, over medium heat to coat the grains of rice with the fat, 1 minute. Stir in 2 cups of the broth mixture; cook over medium-high heat until the broth comes to a simmer. Reduce heat to keep the broth at a gentle simmer. Cook, stirring often, until most of the liquid has been absorbed, about 8 minutes. Add another 1/2 cup of the broth. Simmer gently, stirring until it is absorbed, 5 minutes. Continue simmering and adding broth, 1/2 cup at a time, until the rice is just tender but not hard in the interior, 12-15 minutes.

3\. Stir in the shredded duck or chicken, cheese and chives. Serve in wide bowls; sprinkle each serving with the bacon and more cheese, if desired.

_Source: Chicago Tribune_
