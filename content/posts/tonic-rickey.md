---
title: "Tonic Rickey"
date: 2022-11-28T10:15:32+01:00
categories:
  - cocktails
tags:
  - mocktail
  - untested
---

## Ingredients

- ½ oz freshly-squeezed lime juice
- ½ oz simple syrup\*
- 1 ½ oz tonic water
- 1 ½ oz club soda
- 3 dashes Angostura aromatic bitters

## Preparation

1. Fill a Collins or highball glass all the way up with ice.
2. Add the freshly-squeezed lime juice and simple syrup. Stir with a bar spoon.
3. Pour in the tonic water and club soda, stirring gently to combine.
4. Add the bitters to the top of the glass. You can choose to stir them in or
   let them settle on their own in the drink.
5. Garnish with a lime wheel or fresh herb.

_Girl and Tonic_
