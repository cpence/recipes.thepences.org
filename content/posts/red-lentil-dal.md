---
title: "Red Lentil Dal"
author: "juliaphilip"
date: "2009-12-08"
categories:
  - main course
  - side dish
tags:
  - indian
  - lentil
  - vegan
  - vegetarian
---

## Ingredients

*   1 tablespoon vegetable oil
*   2 cups chopped onions
*   3 garlic cloves, minced
*   3 cups water
*   1 cup dried red lentils
*   3/4 teaspoon turmeric
*   3/4 teaspoon ground cumin
*   1/2 teaspoon ground ginger \[OR: substitute these spices for 1-2 tbsp. Indian curry, such as Rogan Josh\]
*   1 cup basmati rice, cooked according to package directions
*   2 plum tomatoes, seeded, chopped
*   1/4 cup chopped fresh cilantro
*   1 jalapeño chili, seeded, chopped (optional)

## Preparation

Heat oil in medium skillet over medium heat. Add 1 cup onion and 1 minced garlic clove and sauté until tender and golden brown, about 10 minutes. Set aside. Combine 3 cups water, lentils, remaining 1 cup onion, 2 minced garlic cloves, turmeric, cumin and ginger in heavy medium saucepan. Bring to boil. Reduce heat, cover and simmer until lentils are tender, about 15 minutes. Mix in sautéed onion mixture. Simmer 5 minutes to blend flavors. Season to taste with salt and pepper.

Spoon rice into bowls. Spoon dal over. Top with tomatoes, cilantro and chili.

_Bon Appetit, March 1999_
