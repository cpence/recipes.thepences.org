---
title: "French Apple Cake"
date: 2020-04-11T14:15:08+02:00
categories:
    - dessert
tags:
    - cake
    - apple
---

## Ingredients

*   1 cup (about 4 1/4 oz.) all-purpose flour
*   1 teaspoon baking powder
*   1/4 teaspoon baking soda
*   1/4 teaspoon kosher salt
*   1/2 cup (4 oz.) unsalted butter, softened
*   1/2 cup granulated sugar
*   2 large eggs
*   3 tablespoons (1 1/2 oz.) dark rum (such as Myers’s)
*   1 teaspoon vanilla extract
*   1 large Granny Smith or Fuji apple, peeled, cored, and cut into 1/2-in. cubes (about 1 1/2 cups)
*   Powdered sugar

## Preparation

Preheat oven to 350°F. Grease and flour an 8-inch round cake pan; line with parchment paper.

Whisk together flour, baking powder, baking soda, and salt in a large bowl; set aside. Beat butter and granulated sugar with a stand mixer fitted with a paddle attachment on medium speed until lightened and creamy, about 2 minutes. Add eggs 1 at a time, beating until just incorporated. Beat in rum and vanilla until just incorporated. Add flour mixture; beat until just combined (do not overmix). Fold in apples.

Spoon batter into prepared pan, spreading to edges. Bake in preheated oven until top is golden brown, 25 to 30 minutes. Let cool completely in pan on a wire rack, about 30 minutes. Run a knife around cake edges to loosen from pan; invert onto a cake plate. Dust with powdered sugar, and serve.

*MyRecipes*
