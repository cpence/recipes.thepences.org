---
title: "Reverse Sear"
author: "pencechp"
date: "2016-03-25"
categories:
  - main course
tags:
  - lamb
  - steak
---

This isn't a recipe, but a catalog of times and internal temperatures for the
reverse sear method. Preheat the oven to 250F, and take the piece of meat to the
internal temperature specified. Then sear it off in as hot a skillet as you can
get with a bit of vegetable oil.

*   **Ribeye, Cowboy Ribeye, Côte à l'os:** 115F for medium-rare, 110F for rare,
    105F for blue-rare, 1 minute per side sear
*   **Lamb:** 125F, 2 minutes on the fat cap

