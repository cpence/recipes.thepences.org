---
title: "Oyster and Artichoke Soup"
author: "pencechp"
date: "2015-06-02"
categories:
  - main course
tags:
  - artichoke
  - oyster
  - soup
  - untested
---

## Ingredients

*   1 pint oysters (in oyster liquor)
*   4 cups chicken stock or broth
*   1 stick unsalted butter
*   1/4 cup all purpose flour
*   1/2 cup chopped onions
*   1/2 cup chopped green onions
*   2 garlic cloves, minced
*   1 14-ounce can artichoke hearts, drained, rinsed and chopped
*   1/2 teaspoon Worcestershire sauce
*   1/4 teaspoon dried thyme leaves
*   1/8 teaspoon cayenne pepper
*   1/2 cup heavy cream
*   Salt and black pepper to taste

**Prepration**

Strain oysters over a bowl to remove grit and separate them from the oyster liquor; reserve liquor. Chop oysters, cover and refrigerate until needed. Measure oyster liquor and add enough chicken stock to make 4 cups; set aside. In a large saucepan, melt the butter over medium heat. Gradually whisk in flour. Cook, whisking constantly, until a blond roux is achieved. Add onions and green onions; cook, whisking constantly, until tender, about 3 minutes. Add garlic and artichoke hearts; cook an 2 additional minutes. Gradually whisk in reserved oyster liquor and chicken stock. Add Lea & Perrins, thyme and cayenne pepper; bring to a boil. Reduce heat, cover and simmer for 30 minutes. Add chopped oysters and simmer until oysters are cooked through and edges curl, about 5 minutes. Stir in heavy cream and season to taste with salt and black pepper. Serve immediately. Makes 6 servings.

_Raised on a Roux_
