---
title: "Tarte tatin de chicons"
date: 2020-10-18T15:48:18+02:00
categories:
    - side dish
tags:
    - belgian
    - endive
---

## Ingredients

*1 pâte brisée lègere [1 light tarte crust]*

- 150g de farine [flour]
- 100g de fromage blanc [white cheese; substitute very thick greek yogurt or
  cream cheese]
- 1 œuf
- 1 pincée de sel

*la tarte*

- 8 petit chicons [8 small Belgian endives]
- 100g de fromage de chèvre [100g goat cheese, preferably the kind that comes in
  rounds with a brie-esque skin]
- 4 cuillères à soupe de miel [4 tbsp. honey]
- 20g beurre [20g butter]
- 30g de sucre semoule [30g white sugar]
- huile d'olive [olive oil]

## Preparation

1. Préchauffez le four à 180C. [Preheat your oven to 180C/350F.]
2. Coupez les pieds des chicons et enlevez les premières feuilles qui sont
   abîmées. Lavez les chicons puis coupez-les en 2 dans le sens de la longueur.
   Retirez l'intérieur du chicon qui est la partie la plus amère. [Cut off the
   bottoms of the endives and take off any damaged outside leaves. Wash them,
   cut them in half length-wise, and take out the central part, which is the
   bitterest.]
3. Sur feu moyen, faites chauffer 2 cuillères à soupe d'huile avec le miel.
   Faites-y revenir les chicons pendant 10 minutes en les retournant
   régulièrement. [Over medium heat, heat 2 tbsp of olive oil and the honey.
   Cook the endives for 10 minutes, turning often.]
4. Réalizes la pâte brisée : dans un saladier, mélangez la farine avec le
   fromage préalablement mélangé avec l'œuf et le sel. Réservez au frais une
   fois la pâte hoogène. [Make the crust: in a bowl, combine the cheese with the
   egg and salt, then add the flour. Keep cold once it comes together.]
5. Beurrez le fond d'un plat à tarte et parsemez d'un peu de sucre. Déposez les
   chicons sur le fond puis ajoutez les rondelles de chèvre. Étalez la pâte
   brisée puis déposez-la sur les chicons et le chèvre. [Butter the bottom of a
   tarte plate and sprinkle with a bit of sugar. Add the endives to the bottom,
   then the rounds of goat cheese. Roll out the crust then place it on the
   endives and cheese.]
6. Faites cuire pendant 30 minutes jusqu'à ce que les dessus de la pâte soit
   bien doré. À la sortie du four, retournez la tarte dans un plat et servez.
   [Bake for 30 minutes, until the top of the crust is well browned. When it
   comes out of the oven, invert it on a plate and serve.]

*Partenamut Magazine*

