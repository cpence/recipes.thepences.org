---
title: "Enfrijoladas"
author: "pencechp"
date: "2013-03-13"
categories:
  - main course
tags:
  - bean
  - mexican
  - untested
  - vegan
  - vegetarian
---

## Ingredients

*   1/2 pound (1 1/8 cups) black beans, washed, picked over and soaked for 4 to 6 hours or overnight in 1 quart water
*   1 onion, cut in half
*   2 plump garlic cloves, minced
*   1 to 2 sprigs epazote or 2 tablespoons chopped cilantro, plus additional for garnish (optional)
*   1 to 2 teaspoons ground cumin, to taste
*   1/2 to 1 teaspoon ground mild chili powder (more to taste)
*   Salt to taste
*   12 corn tortillas
*   1/4 cup chopped walnuts (optional)

## Preparation

1\. In a large soup pot or Dutch oven combine the black beans with their soaking water (they should be submerged by at least 1 1/2 inches of water; add if necessary), one half of the onion, and half the garlic and bring to a boil. Reduce the heat, cover and simmer gently for 1 hour. Add the remaining garlic, epazote or cilantro if using, cumin, chili powder, and salt to taste and simmer for another hour, until the beans are very soft and the broth thick, soupy and aromatic. Remove from the heat. Remove and discard the onion.

2\. Using an immersion blender or a food processor fitted with the steel blade coarsely puree the beans. The mixture should retain some texture and the consistency should be thick and creamy. Heat through, stirring the bottom of the pot so the beans don’t stick. Taste and adjust salt. Keep warm.

3\. Slice the remaining onion half crosswise into thin half-moons and cover with cold water while you assemble the enfrijoladas. Heat the corn tortillas: either wrap them in a damp dish towel and heat them, 4 at a time, in the microwave for about 30 seconds at 100 percent power, or wrap in a dish towel and steam for 1 minute, then let rest for 5 minutes.

4\. Assemble the enfrijoladas just before serving them. Spoon about 1/2 cup of the hot, thick beans over the bottom of a large lightly oiled baking dish or serving platter. Using tongs, dip a softened tortilla into the beans and flip over to coat both sides with black beans. Remove from the beans and place on the baking dish or platter (this is messy; have the serving dish right next to the pot.) Fold into quarters. Use the tongs to do this, and if you find that the tortilla tears too much, then just coat one side with the black beans, transfer to the baking dish and spoon some of the black beans over the other side, then fold into quarters. Continue with the remaining tortillas, arranging the quartered bean-coated tortillas in overlapping rows. When all of the tortillas are in the dish, spoon the remaining black bean sauce over the top. Drain and rinse the onions, dry briefly on paper towels and sprinkle over the bean sauce. Garnish with cilantro and chopped walnuts if desired and serve at once.

_Serves 4_

_Martha Rose Shulman, New York Times_
