---
title: "Duo de Saumon et Cabillaud au Four"
date: 2021-02-19T09:12:50+01:00
categories:
  - main course
tags:
  - fish
  - salmon
---

## Ingredients

- 400g de filet de saumon, en dès
- 400g de filet de cabillaud, en dès
- 10 crevettes tigrées décortiquées
- 400g pommes de terre épluchées
- 300g épinards frais
- 50ml lait
- 50g fromage râpé
- 1 boîte bisque de homard concentrée (env. 500ml)
- beurre

## Preparation

Préchauffez le four à 190C (chaleur tournante 180C). Coupez le saumon et le
cabillaud en dès.

Faites cuire les pommes de terre durant 20 minutes dans assez d'eau. Réchauffez
entre-temps 1 cuillère à soupe de beurre, faites y ensuite réduire les épinards.

Mélangez le contenu de soupe en boîte avec 100ml d'eau (pas le 500ml normal!)
dans un bol. Faites une purée avec les pommes de terre et 50ml de lait.

Graissez le plat allant au four et couvrez le fond avec la purée. Déposez
ensuite les épinards et répartissez le poisson (le poisson ne doit pas être
précuit) et les crevettes au dessus des épinards. Ajoutez la soupe et répandez
ensuite le fromage râpé.

Laissez cuire au milieu du four durant 25 min.

_Knorr_
