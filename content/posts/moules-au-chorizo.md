---
title: "Moules au chorizo et aux poivrons"
date: 2021-08-09T10:26:03+02:00
categories:
  - main course
tags:
  - mussels
  - untested
---

## Ingredients

- 4 kg de moules de Zélande
- 200 g de chorizo extra (doux ou piquant)
- 2 poivrons verts
- 2 oignons
- 4 brins de thym
- 2,5 dl de vin blanc sec
- 4 c. à s. d’huile d’olive
- 4 c. à c. d’origan séché
- Poivre et sel

## Preparation

Rincez plusieurs fois les moules à l’eau froide ; grattez-les si nécessaire.

Émincez les oignons en fines rondelles et taillez les poivrons épépinés en
petites lamelles. Retirez la peau du chorizo et taillez-le en petits dés. Faites
revenir ces derniers avec l’huile d’olive dans les casseroles à moules, d’abord
à feu doux, puis à feu vif pour les rissoler. Retirez-les avec une écumoire pour
laisser le gras dans les casseroles. Faites-y revenir 5 min à feu doux les
oignons, les poivrons, du poivre, le thym et l’origan. Ajoutez les moules et le
vin blanc, couvrez et faites chauffer +/- 5 min à feu vif, en secouant les
casseroles de temps en temps, jusqu’à ce que toutes les moules soient ouvertes.
Ajoutez les dés de chorizo et mélangez.

_Delhaize_
