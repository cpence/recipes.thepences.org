---
title: "Lavender-Honey-Cardamom-Lemon Dark Chocolate Tart"
author: "pencechp"
date: "2014-11-19"
categories:
  - dessert
tags:
  - chocolate
  - pie
---

## Ingredients

_for pastry:_

- 260 gr flour
- 1 heaping tsp ground cardamom
- 1 Tbsp freshly ground Meyer lemon zest
- 1/4 cup sugar
- 7 Tbsp butter, cold and cut into pieces
- 1 egg yolk
- 1 Tbsp freshly squeezed lemon juice
- 2 Tbsp water, cold

_for chocolate ganache filling:_

- 10 oz. heavy cream
- 1/4 cup dried lavender buds
- 12 oz. dark chocolate, chopped
- 3 Tbsp honey
- 2 Tbsp butter
- coarse salt
- dried lavender buds

## Preparation

Using a food processor or pastry cutter, mix the flour, cardamom, lemon zest,
and sugar until combined. Cut the cold butter into the flour mixture until the
size of small peas. Add the egg yolk and gradually add the lemon juice and water
just until a dough begins to form when you press it between two fingers. Do not
overmix. Form the pastry into a ball and refrigerate for at least one hour. (or
stick it in the freezer for a shorter amount of time.) Preheat the oven to 425
degrees F. Remove the pastry dough from the fridge and press the dough into the
tart pan. Prick the bottom of the dough all over with a fork, cover with
parchment paper, and weigh down using dry beans or pie weights. Return to
freezer for a few minutes if the dough has softened. Bake for ~13-15 minutes
until the edges just begin to turn golden. Remove the beans and parchment paper
and continue to bake for 3-5 minutes more, until the crust has completely turned
golden brown. Remove from oven and let cool.

In a saucepan with a tight-fitting lid, heat the cream and lavender buds until
just barely a simmer. Cover and remove from heat. Let steep for at least ten
minutes. Place the chocolate and honey in a heat-proof bowl, place a fine mesh
strainer on top, and set aside. Add the butter to the cream and lavender bud
mixture and return to a simmer. Once the cream simmers, remove from heat and
pour the hot cream through the sieve onto the prepared chocolate, making sure to
press all of the liquid from the lavender buds. Discard the lavender buds. Let
the chocolate sit for 2-3 minutes. Whisk until the chocolate is completely
melted and evenly distributed. Immediately pour into the prepared tart crust.
Using your fingers (or a mortar and pestle), rub together an equal but small
amount of coarse salt and dried lavender buds. Sprinkle liberally over the top
of the chocolate. Let the tart set at room temperature until completely cool,
about 1-2 hours. Serve at room temperature.

_Desserts for Breakfast_
