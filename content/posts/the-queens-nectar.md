---
title: "The Queen's Nectar"
author: "pencechp"
date: "2015-10-13"
categories:
  - cocktails
tags:
  - champagne
  - honey
---

## Ingredients

*   1/2 oz. amaro averna
*   2 oz. fresh grapefruit juice
*   3 dashes Peychaud's bitters
*   2 spoons [honey syrup]( {{< relref "honey-simple-syrup" >}} )
*   Prosecco
*   Grapefruit twist

## Preparation

Add amaro, juice, syrup, and bitters into a champagne flute. Top with prosecco. Garnish with twist.

_Apis Restaurant and Apiary's Jose Luis Sapien_
