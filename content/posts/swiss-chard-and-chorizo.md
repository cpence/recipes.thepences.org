---
title: "Quick-Braised Swiss Chard, White Beans, and Chorizo"
date: 2020-01-27T18:33:01+01:00
categories:
    - main course
tags:
    - greens
    - bean
    - sausage
---

## Ingredients

*   1 tbsp. olive oil
*   1/2 c. finely diced onion
*   8 oz. fresh chorizo sausage (Mexican)
*   1 tsp. tomato paste (double-concentrated, or 1 tbsp. normal)
*   8 oz. swiss chard, washed but not dried, each leaf cut into 4 or 5 pieces
*   2 15– or 16-oz. cans white beans, drained and rinsed (around 3 c.)
*   1/2 c. medium-bodied red wine

## Preparation

Heat the oil in a large pan over medium heat. Add the onion and cook, stirring once or twice, for 2 to 3 minutes, until it has softened, then add sausage. Increase the heat to medium-high and cook for 4 to 5 minutes, until the sausage is lightly browned but not cooked through; use a spatula or wooden spoon to break up any large chunks.

Add the tomato paste, stirring to mix well; cook for 1 to 2 minutes. Add the Swiss chard; if the cut pieces are not very wet, add a few tablespoons of water, as needed, to help create steam. Reduce the heat to medium; cover and cook for 2 minutes, until the chard starts to wilt. Add the beans and the red wine; use tongs to fold the ingredients together. Cover and adjust the heat as necessary so the liquid in the pan is bubbling slightly. Cook for 10 to 15 minutes or until the chard is tender.

Remove from the heat; taste and add salt, if desired. Serve warm or at room temperature.

*WaPo*

