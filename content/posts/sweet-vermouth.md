---
title: "Sweet Vermouth"
author: "pencechp"
date: "2018-01-03"
categories:
  - cocktails
tags:
---

## Ingredients

*   3-3 1/4 c. pinot grigio
*   2 tbsp. raisins
*   1 tsp. dried bitter orange peel
*   2 1/2" Ceylon cinnamon stick
*   1 1/2 tsp. wormwood
*   1/2 tsp. gentian root
*   1/2 tsp. angelica root
*   1/2 tsp. ground ginger
*   1/2 tsp. chamomile tea flowers
*   1/2 tsp. ground nutmeg
*   1/4 tsp. coriander seeds
*   1/4 tsp. sassafras bark
*   1/4 tsp. centaury herb
*   1/4 tsp. dried oregano
*   1/4 tsp. dried sage leaves (not ground)
*   1/4 tsp. dried basil
*   1/4 tsp. dried thyme
*   1/8 tsp. fennel seeds
*   1/8 tsp. cardamom seeds
*   1/8 tsp. orris root
*   1/8 tsp. dried rosemary
*   1 1/2 bay leaves
*   6 peppercorns, lightly crushed
*   5 allspice berries, lightly crushed
*   5 whole cloves
*   4 juniper berries, lightly crushed
*   1 star anise
*   1/2 c. turbinado sugar
*   2 tbsp. water
*   1 1/2 c. cognac
*   4 tsp. ruby port

## Preparation

Combine 2 c. wine and remaining ingredients except for sugar, water, cognac, and port, in small saucepan. Bring to simmer over medium heat. Reduce heat to medium-low and simmer gently, uncovered, for 10 minutes. Remove pot from heat, cover, and let sit at room temperature for at least 24h or up to 48h.

Line fine-mesh strainer with triple layer of cheesecloth and set over 4-c. liquid measuring cup; strain and discard solids.

Combine sugar and water in small saucepan and cook over medium heat, swirling occasionally, until sugar is completely dissolved and mixture is bubbling, about 6 minutes. Off the heat, stir in cognac and port. Add sugar mixture to infusion, then add enough of the remaining 1 to 1 1/4 c. wine to measure 3 cups total. Stir well and transfer to bottle.
