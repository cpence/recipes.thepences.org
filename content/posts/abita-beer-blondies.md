---
title: "Abita Beer Blondies"
author: "pencechp"
date: "2014-10-03"
categories:
  - dessert
tags:
  - beer
  - brownie
  - untested
---

## Ingredients

*   1/2 c. butter, softened
*   1 c. light brown sugar
*   2 eggs
*   1 tbsp. vanilla
*   2/3 c. Abita Amber (dunkel lager)
*   1 1/2 c. flour
*   1/4 tsp. salt
*   1/2 tsp. baking powder
*   4 tbsp. butter
*   1 c. brown sugar
*   1/4 c. corn syrup
*   1 tbsp. vanilla
*   3 tbsp. Abita Amber

## Preparation

Heat the oven to 350 and line an 8x8" pan with aluminum foil. Cream butter and brown sugar until fluffy. Beat in the eggs and vanilla. Sift together the flour, baking powder, and salt. Fold the dry ingredients into the wet and add the beer. Stir until combined. Bake for 20-25 minutes until a toothpick comes out clean. Remove from the oven and allow to cool slightly. Meanwhile, melt butter in a saucepan over medium heat. Add brown sugar and corn syrup and bring to a simmer, stirring to prevent from burning. Allow to simmer and keep stirring for 5 minutes. Remove from heat and carefully add vanilla and beer. Be careful, it will try to bubble up. Stir the icing for another minute and carefully pour it over the blondies. Cool completely and cut into squares.

_225 Magazine_
