---
title: "Hot Buttered Rum"
author: "pencechp"
date: "2010-01-08"
categories:
  - cocktails
tags:
  - rum
---

## Ingredients

*   1 tsp. brown sugar
*   1/4 tsp. grated lemon rind
*   3 pinches ground cinnamon
*   1 pinch ground cloves
*   1 pinch ground nutmeg
*   1 shot dark rum
*   boiling water
*   1 tsp. butter

## Preparation

Rinse mug with boiling water to heat.  Place first five ingredients in the bottom of the mug, and add the rum.  Fill the remainder of the mug with boiling water, and add the teaspoon of butter.

_Unknown_
