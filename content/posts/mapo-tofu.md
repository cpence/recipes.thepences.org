---
title: "Mapo Tofu"
author: "pencechp"
date: "2010-12-07"
categories:
  - main course
  - side dish
tags:
  - chinese
  - tofu
---

## Ingredients

*   1 block soft tofu (about 1 pound), drained and cut into 1-inch cubes
*   3 tablespoons peanut oil
*   6 ounces ground pork
*   2 cloves garlic, minced
*   2 leeks, thinly sliced at an angle (or a handful of scallions can be substituted)
*   2 1/2 tablespoons chili bean paste
*   1 tablespoon fermented black beans
*   2 teaspoons ground Sichuan pepper
*   1 cup chicken stock
*   2 teaspoons white sugar
*   2 teaspoons light soy sauce
*   Salt to taste
*   4 tablespoons cornstarch mixed with 6 tablespoons cold water

_Optional garnish:_ 1 tablespoon thinly sliced scallions, or 1/2 teaspoon crushed roasted Sichuan peppercorn

## Preparation

1\. Heat peanut oil in a wok over high heat. Add pork and stir-fry until crispy and starting to brown but not yet dry. Reduce heat to medium, then add the garlic and leeks and stir-fry until fragrant.

2\. Add chili bean paste, black beans, and ground Sichuan pepper, and stir-fry for about 1 minute, until the oil is a rich red color.

3\. Pour in the stock and stir well. Mix in the drained tofu gently by pushing the back of your ladle or wok scoop gently from the edges to the center of the wok or pan; don't stir or the tofu may break up. Season with the sugar, soy sauce, and salt to taste. Simmer for about 5 minutes, allowing the tofu has absorb the flavors of the sauce. Then add the cornstarch mixture in 2 or 3 stages, mixing well, until the sauce has thickened enough to coat the back of a spoon. (Don't add more than you need).

4\. Serve while still hot in a deep plate or wide bowl. Garnish with optional scallions or crushed Sichuan peppercorn.

Serves 4 to 5 as part of a multi-course meal, or 2 to 3 as the main entree

_Appetite for China_
