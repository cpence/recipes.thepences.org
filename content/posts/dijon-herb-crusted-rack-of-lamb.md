---
title: "Dijon & Herb Crusted Rack of Lamb"
author: "pencechp"
date: "2017-05-02"
categories:
  - main course
tags:
  - lamb
---

## Ingredients

*   1 rack of lamb
*   1½ tablespoons olive oil
*   Kosher salt & freshly ground black pepper
*   ½ cup panko breadcrumbs
*   ¼ cup grated Pecorino Romano cheese
*   1 tablespoon chopped flat leaf parsley
*   1 teaspoon chopped fresh thyme leaves
*   ½ teaspoon chopped fresh rosemary leaves
*   2 tablespoons Dijon mustard

## Preparation

Heat a heavy skillet over high heat. While it’s heating, rub rack of lamb with 1 tablespoon of olive oil and season generously with salt and pepper. When the pan is sizzling hot, sear lamb until golden brown on all sides, about 6 minutes total. Place browned lamb on a cutting board to rest until completely cool, about 30 minutes.

Preheat oven to 450 degrees F.

In a small bowl, stir together breadcrumbs, cheese, parsley, thyme, rosemary, and remaining ½ tablespoon olive oil; mix well. Spread mixture on a plate. Coat cooled lamb with Dijon mustard then roll in seasoned bread crumbs. Press crumbs evenly into lamb, creating a nice, thick coating.

Place lamb on a rimmed baking sheet, and roast until an instant-read thermometer inserted into the thickest part of the meat registers 120 degrees F, approximately 20 to 30 minutes. If the crust looks like it’s getting too dark toward the end of cooking, loosely tent with foil.

Remove lamb from oven and rest on a cutting board for 10 minutes before carving into chops and serving.

_D'Artagnan_
