---
title: "Dashi"
author: "pencechp"
date: "2016-12-29"
categories:
  - miscellaneous
tags:
  - japanese
---

## Ingredients

*   4 cups water
*   2 handful of dried bonito flakes (or 4 small packs of dried bonito flakes) \[CP: I used about 30g of bonito; one medium-sized package, and a bit more than 4c. water\]

## Preparation

Boil water and then add dried bonito flakes in a pot. Let it simmer for 2-3 minutes. Strain it. Use it as instructed in recipes.

_Japanese Cooking 101_
