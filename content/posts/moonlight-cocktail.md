---
title: "Moonlight Cocktail"
author: "juliaphilip"
date: "2016-09-13"
categories:
  - cocktails
tags:
  - gin
  - violette
---

## Ingredients

*   1 1⁄2 oz Gin
*   1⁄2 oz Triple sec, Cointreau
*   1⁄2 oz Crème de Violette
*   1⁄2 oz Lime juice

## Preparation

Shake ingredients over ice, strain into a cocktail glass.
