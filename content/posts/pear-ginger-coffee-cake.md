---
title: "Pear Ginger Coffee Cake"
author: "pencechp"
date: "2010-05-08"
categories:
  - breakfast
  - dessert
tags:
  - cake
  - pear
  - untested
---

## Ingredients

*Cake:*

*   1/2 Cup shortening
*   1/2 tsp. salt
*   1 1/2 Cups brown sugar, packed
*   1 egg
*   2 tsp. vanilla extract
*   1 tsp. baking soda
*   1 Cup buttermilk
*   2 Cups flour, sifted
*   1 1/2 Cups pears, peeled and diced into 1/4-1/2 inch cubes

*Topping:*

*   1/4 Cup cinnamon sugar
*   2 TB. crystallized ginger, finely chopped

## Preparation

Preheat oven to 350°. Grease a 9x13 glass pan and set aside. Stir the baking soda into the buttermilk and set aside. Cream the shortening, salt, and brown sugar until light and fluffy. Add the egg and vanilla; mix well. Alternately add the buttermilk mixture and flour to the mixing bowl, beating well after each addition. Fold in the pears and pour into the greased pan. Mix together the cinnamon sugar and chopped crystallized ginger. Sprinkle evenly over the top of the cake. Bake for 35-40 minutes or until a toothpick inserted in the center comes out clean.

_Penzey's Spices_
