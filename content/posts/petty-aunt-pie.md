---
title: "Petty Aunt Pie"
date: 2020-12-08T16:31:53+01:00
categories:
    - dessert
tags:
    - pie
    - apple
---

## Ingredients

*   1 pie crust
*   3 granny smith apples, peeled and grated
*   1/2 stick butter, melted
*   1 1/2 c. sugar
*   2 tbsp. flour
*   1 tsp. cinnamon
*   1 tsp. nutmeg
*   1/2 c. milk
*   2 eggs, beaten

## Preparation

Grate apples into deep-dish pie crust. Drizzle with butter. Mix dry ingredients, add milk, then eggs. Pour cream mixture over apples. Bake at 400 F for 10 minutes, then 350 for 30--40 minutes.

*PostSecret*
