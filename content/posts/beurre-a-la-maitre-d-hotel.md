---
title: "Beurre à la maître d’hotel"
date: 2024-01-10T17:49:57-06:00
categories:
  - miscellaneous
tags:
  - french
---

## Ingredients

- 250 g beurre ramolli en pommade
- une forte cuillerée de persil haché
- sel
- poivre
- un filet de jus de citron

## Preparation

Bien mélanger.

_MC_
