---
title: "522 North Pinckney Cocktail"
author: "pencechp"
date: "2014-06-10"
categories:
  - cocktails
tags:
  - campari
  - champagne
  - stgermain
  - untested
---

## Ingredients

*   2 cups fresh pink grapefruit juice (from 2-3 grapefruits)
*   1/2 cup St-Germain
*   1/4 cup Campari
*   1 750-ml. bottle chilled sparkling wine

## Preparation

Combine grapefruit juice, St-Germain, and Campari in a large pitcher. Cover and chill until cold, about 2 hours. Divide grapefruit mixture among glasses and top off with sparkling wine.

_Bon Appetit, May 2013_
