---
title: "Tourte aux poireaux et au saumon fumé"
date: 2021-08-09T09:59:31+02:00
categories:
  - main course
tags:
  - salmon
  - leek
  - pie
---

## Ingredients

- 1 rouleau de pâte brisée
- 3 blancs de poireau
- 3 càs d'huile d'olive
- 200g saumon fumé
- une poignée de ciboulette fraîche
- 3 œufs
- 2 dl crème
- poivre
- sel

## Preparation

Déroulez la pâte et déposez-la avec le papier de cuisson dans un moule à tarte.
Découpez le papier de cuisson superflue. Piquez le fond.

Coupez les poireaux en fines rondelles et faites-les cuire pendant 2 min dans
l'huile d'olive. Laissez refroidir.

Répartissez les poireaux dans le moule avec les lamelles de saumon fumé et la
ciboulette fraîche hachée. Battez les œufs avec la crème, poivrez, salez, et
versez le tout sur les poireaux et le saumon. Faites cuire la tourte pendant 30
min à 200 C. Garnissez de ciboulette fraîche.

_Pâte brisée Delhaize_
