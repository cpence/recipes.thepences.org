---
title: "Italian Sausage"
author: "pencechp"
date: "2010-10-17"
categories:
  - main course
tags:
  - italian
  - pork
  - sausage
---

## Ingredients 

*   5 lbs well-marbled pork butt
*   1 cup cold wine (red or white)
*   1 cup chopped fresh parsley
*   5 tsp salt
*   5 garlic cloves, chopped
*   1 tbsp fresh ground pepper
*   1 tsp cayenne
*   5 tbsp fennel seed
*   2 tsp crushed chili peppers
*   5 tbsp paprika
*   1 tsp anise, optional (ground)

## Preparation

Cut the pork butt into long, relatively thin strips.  Mix with all the remaining ingredients and marinate overnight.  Process twice through a meat grinder.

_Taken from Emeril Lagasse and Lesley's Recipes_
