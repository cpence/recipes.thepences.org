---
title: "Argentine Spinach Pie (Torta Pascualina)"
author: "pencechp"
date: "2015-06-13"
categories:
  - breakfast
  - side dish
tags:
  - argentinian
  - cheese
  - pie
  - spinach
  - untested
---

## Ingredients

*   1 double-crust pie shell
*   1 1/2 c. frozen chopped spinach or 2 qts. fresh spinach (2 8-oz. bags works well)
*   2 tbsp. olive oil
*   1 onion, chopped
*   1/4 tsp. nutmeg
*   1 tsp. oregano
*   1/2 tsp. salt
*   2 eggs, beaten
*   1-2 c. grated Swiss cheese
*   1 egg yolk for brushing top of pie

## Preparation

Preheat oven to 350. If using frozen spinach, thaw and squeeze. If using fresh, cook, drain, dry, and chop. Heat the oil in a frying pan over medium heat. Add onion and cook until tender. In a bowl combine spinach, onion, nutmeg, salt, eggs, and cheese. Mix well. Pour into the pie shell. Arrange the top crust over the filling and crimp the edges. Brush the top with egg yolk if desired. Bake at 350 for 30-40 minutes.

_Penzey's Spices_
