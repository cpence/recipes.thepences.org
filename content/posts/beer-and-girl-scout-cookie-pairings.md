---
title: "Beer and Girl Scout Cookie Pairings"
author: "pencechp"
date: "2019-03-31"
categories:
  - cocktails
  - dessert
tags:
  - beer
  - cookie
---

- Thin Mints + Black IPA
- Lemonades + American IPA
- Peanut Butter Sandwich + Imperial Hefeweiszen
- Shortbread + Amber
- Thanks-A-Lot + Blonde
- S'Mores + Barrel-Aged Imperial Porter
- Caramel Delites + Hefeweiszen
- Peanut Butter Patties + Imperial Porter

_After Circle Brewing, via Austin American-Statesman_
