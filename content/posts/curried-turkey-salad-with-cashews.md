---
title: "Curried Turkey Salad with Cashews"
author: "juliaphilip"
date: "2009-12-08"
categories:
  - main course
tags:
  - curry
  - turkey
---

## Ingredients

*   2 2-pound turkey breast halves with skin and bones
*   Olive oil 1 1/2 cups mayonnaise
*   1/3 cup dry white wine
*   1/4 cup mango chutney
*   2 tablespoons curry powder \[JP: Penzey's maharajah is really good\]
*   1 tablespoon fresh lemon juice
*   1/2 teaspoon ground ginger
*   3 green onions, chopped
*   2 celery stalks, chopped
*   1/2 cup raisins
*   2/3 cup roasted salted cashews

## Preparation

Preheat oven to 375°F. Place turkey in baking pan. Brush turkey with oil. Sprinkle with salt and pepper. Roast until meat thermometer inserted into thickest part of meat registers 180°F, about 1 hour. Cool. Remove skin and bones. Cut meat into 1/2-inch cubes.

Whisk mayonnaise, wine, chutney, curry powder, lemon juice and ginger in large bowl. Add turkey, onions, celery and raisins; toss to coat. Season with salt and pepper. (Can be made 1 day ahead. Cover; chill.) Mix cashews into salad.

_Bon Appetit, August 2000_
