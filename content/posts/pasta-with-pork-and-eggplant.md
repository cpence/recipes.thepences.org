---
title: "Pasta with Pork and Eggplant"
author: "pencechp"
date: "2018-12-26"
categories:
  - main course
tags:
  - eggplant
  - pasta
  - pork
---

## Ingredients

*   1 medium-size firm eggplant, a little over a pound or so in weight
*   Salt and pepper
*   3 tablespoons Extra Virgin Olive Oil
*   1 pound ground pork
*   1 teaspoon fennel pollen or seed
*   4 large cloves garlic, finely chopped or grated
*   1 small onion, grated or very finely chopped
*   2 tablespoons fresh thyme leaves, finely chopped
*   1 chili pepper – Italian red cherry or Fresno, finely chopped
*   3/4 cup white wine
*   1/2 cup heavy cream
*   a generous handful of flat leaf parsley, chopped
*   1 pound short-cut pasta, whatever shape you like
*   Parmesan cheese

## Preparation

Peel half the skin off the eggplant. Dice very finely into 1/8-inch pieces. Sprinkle with salt and reserve. Place a large pot of water over high heat for the pasta. Add pasta and cook according to package directions. Reserve a half cup of the starchy pasta water for the sauce. Heat the olive oil, 3 turns of the pan, over medium to medium-high heat. Add pork and lightly brown while finely crumbling it into tiny bits. Season with fennel, salt, pepper, garlic, onion, thyme and the chili pepper. Squeeze any liquid from the eggplant then stir it into the pork. Cook about 10-12 minutes, stirring frequently until eggplant is tender. Deglaze with white wine, stir a minute then stir in cream and simmer over low heat while pasta cooks. Toss the cooked pasta with the sauce, adding liquid to combine. Stir in parsley.

_Bobbi's Kozy Kitchen_
