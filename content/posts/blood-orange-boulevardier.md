---
title: "Blood Orange Boulevardier"
date: 2022-03-04T10:48:12+01:00
categories:
  - cocktails
tags:
  - orange
  - bourbon
  - campari
  - untested
---

## Ingredients

- 1 ounce fresh blood orange juice
- 1 ounce Campari
- 1 ounce bourbon
- 1 ounce sweet vermouth
- A few dashes Angostura bitters
- Ice

## Preparation

Stir together the orange juice, Campari, bourbon, vermouth and bitters in a mixing glass.

Fill a highball glass to the top with ice, and then pour in the orange juice mixture. Serve right away.

_WaPo_
