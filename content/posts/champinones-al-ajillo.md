---
title: "Champiñones al ajillo"
author: "pencechp"
date: "2010-07-21"
categories:
  - side dish
tags:
  - spanish
  - tapas
---

## Ingredients

*   1/4 cup (2 fl. oz) olive oil
*   4 cups (8 oz) mushrooms, wiped clean and quartered
*   6 cloves garlic, minced
*   3 tablespoons dry sherry
*   2 tablespoons lemon juice
*   1/2 teaspoon dried red chile, seeded and crumbled
*   1/4 teaspoon Spanish paprika
*   Salt and pepper, to taste
*   2 tablespoons chopped parsley

## Preparation

Heat the oil in a skillet and sauté the mushrooms over high heat for about 2 minutes, stirring constantly.

Lower the heat to medium and add the garlic, sherry, lemon juice, dried chile, paprika, and salt and pepper.

Cook for about 5 minutes or until the garlic and mushrooms have softened.

Remove from the heat, sprinkle with chopped parsley, and serve on small earthenware platters.

_Spain-Recipes.com_
