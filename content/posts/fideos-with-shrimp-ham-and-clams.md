---
title: "Fideos with Shrimp, Ham and Clams"
author: "pencechp"
date: "2012-10-28"
categories:
  - main course
tags:
  - pasta
  - spanish
---

## Ingredients

*   4 smoked ham hocks
*   1/4 teaspoon saffron threads
*   12 scallions
*   1 1/4 pounds fideos or angel hair pasta, broken into 1-inch lengths
*   1 pound large shrimp
*   1/4 cup extra-virgin olive oil, plus more for drizzling
*   1 pound abalone or oyster mushrooms—stems trimmed, caps sliced lengthwise 1/4 inch thick
*   Salt and freshly ground pepper
*   8 garlic cloves, thinly sliced
*   2 large shallots, thinly sliced
*   1 cup Spanish green olives, pitted and chopped
*   1/2 teaspoon crushed red pepper
*   1 cup dry sherry, such as fino
*   2 tablespoons fresh lemon juice
*   2 dozen Manila or littleneck clams, scrubbed
*   1/2 cup chopped cilantro

## Preparation

In a large pot, cover the ham hocks with water and bring to a boil. Simmer over low heat until the meat is tender, about 1 1/2 hours. Transfer the hocks to a large bowl. Strain the broth into a very large glass measuring cup and skim off the fat; you should have about 6 cups of broth. Crumble the saffron threads into the broth and reserve. When the ham hocks are cool enough to handle, remove the meat and coarsely chop it; discard the hocks.

Preheat the oven to 400°. Light a grill or preheat a grill pan. Grill the scallions over high heat until lightly charred all over, about 2 minutes. Coarsely chop the scallions. Spread the fideos on 2 large rimmed baking sheets and bake in the upper and lower thirds of the oven for about 10 minutes, until browned.

Using scissors, cut down the back shell of each shrimp. Remove the dark intestinal veins, leaving the shells intact.

Heat 2 large skillets and add 1 tablespoon of olive oil to each. Add half of the mushrooms to each skillet, season with salt and pepper and cook over moderately high heat, stirring, until browned, about 3 minutes. Add the remaining 2 tablespoons of olive oil to the skillets. Add the garlic and shallots to the skillets and cook over moderate heat, stirring, until fragrant, about 2 minutes; stir in the chopped ham, olives and crushed red pepper, then stir in the fideos. Add the sherry to the skillets and cook over moderately high heat until reduced by three-quarters, about 2 minutes. Stir in the ham hock broth and the lemon juice, then add 1 cup of hot water to each skillet. Season with salt and pepper and bring to a simmer.

Add the shrimp and clams to the skillets and simmer over moderately high heat, turning the seafood a few times, until the fideos have absorbed most of the liquid, the shrimp are cooked and the clams have opened, about 4 minutes. Stir in the grilled scallions and sprinkle with the cilantro. Drizzle the fideos with olive oil and serve right away.

MAKE AHEAD The recipe can be prepared through Step 3 up to two days in advance. Refrigerate the ham hock broth, chopped ham, scallions and shrimp separately. Store the toasted fideos in an airtight container at room temperature. Warm the ham hock broth before using.

_Food & Wine_
