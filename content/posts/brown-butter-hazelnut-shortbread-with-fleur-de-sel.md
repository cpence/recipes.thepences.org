---
title: "Brown Butter Hazelnut Shortbread with Fleur de Sel"
author: "pencechp"
date: "2010-05-09"
categories:
  - dessert
tags:
  - chocolate
  - cookie
  - hazelnut
---

## Ingredients

*Shortbread:*

*   1 cup unsalted butter (2 sticks), softened but still cool, divided
*   1/2 cup granulated sugar
*   1/2 cup lightly packed light brown sugar
*   1/4 teaspoon salt, plus a pinch, divided
*   1 large egg, separated
*   1 1/2 teaspoons pure vanilla extract
*   2 cups all-purpose flour

*Topping:*

*   1 1/2 cups (6 3/4 ounces) chopped hazelnuts
*   2 tablespoons granulated sugar
*   1/2 teaspoon ground cinnamon
*   1/2 teaspoon ground nutmeg
*   1 heaping teaspoon coarse fleur de sel or sea salt
*   6 ounces bittersweet chocolate

## Preparation

Place 1/2 stick of butter (4 tablespoons) in small saucepan over moderate heat. Melt and cook until butter stops foaming, smells toasty and begins to brown, about 10 minutes. The browner the butter, the deeper the flavor, but don't let it blacken or burn. Set aside to cool to room temperature.

Place the remaining butter (12 tablespoons) in a bowl. Using an electric mixer or wooden spoon, beat until creamy. Add the sugars and salt and continue to beat until light and fluffy. Add the egg yolk, vanilla and cooled brown butter. Mix to combine. Then add flour, 1 cup at a time, and stir with a wooden spoon to combine. Chill the batter for 20 to 30 minutes.

Preheat oven to 350 degrees with oven rack in the middle. Butter a 10-by-15-inch jelly roll pan. Divide dough into 8 rough portions and arrange them evenly in the pan. Press dough into an even layer to fill the pan.

In a small bowl, beat the egg white with a pinch of salt. Brush mixture evenly over the dough. Sprinkle hazelnuts over the top; press down lightly. In another small bowl, combine 2 tablespoons sugar with the cinnamon and nutmeg and sprinkle over nuts. Sprinkle fleur de sel or sea salt over the top.

Bake, rotating the pan once halfway through baking, until golden brown and crisp, about 25 minutes. Let cool for 10 minutes and cut into 1- to 2-inch squares or diamonds. Transfer to a cooling rack.

Melt chocolate in a metal bowl set over a pan of simmering water. Using a very small-tipped pastry bag or the tines of a fork, drizzle the chocolate over the tops of the shortbreads.

Makes 3 to 4 dozen cookies.

_Pittsburgh Post-Gazette_
