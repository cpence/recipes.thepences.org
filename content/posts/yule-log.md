---
title: "Yule Log"
date: 2021-01-09T21:19:16+01:00
categories:
  - dessert
tags:
  - cake
---

## Ingredients

- 4 large eggs
- 100g [3.5 oz] caster sugar
- 65g [2.5 oz] self-rising flour
- 40g [1.5 oz] cocoa powder
- 300 ml [1/2 pint] double cream
- 300 g [10.5 oz] dark chocolate chips/pieces
- 300 ml [1/2 pint] double cream, whipped
- powdered sugar, for decorating

## Preparation

Preheat the oven to 200C/400F. Lightly grease a 33x23cm/13x9" jelly-roll pan, and line with parchment paper.

In a large bowl, whisk the eggs and sugar using an electric mixer until pale in colour, light and frothy. Sift the flour and cocoa powder into the bowl and carefully cut and fold together, using a spatula, until incorporated. (Be careful not to beat any air out of the mixture.)

Pour the mixture into the lined tin and spread out into the corners. Bake in the middle of the oven for 8-10 minutes, until well risen and firm to the touch, and the sides are shrinking slightly away from the edge of the tin.

Place a piece of parchment bigger than the sheet on your work surface. Dust generously with powdered sugar. Invert the cake onto the paper (poof). Remove the bottom lining piece of paper.

Cut a score mark, 2.5cm/1" along one of the longer edges. Starting with this edge, begin to tightly roll up the sponge using the paper (with the paper still inside). Sit the roll on top of its outside edge, and cool completely.

While the cake is cooling, make the ganache. Heat the cream in a pan, until you can just keep a finger in it. Remove from the heat and add chocolate, stirring until melted. Cool to room temperature, then put in the fridge to firm up.

Uncurl the roll and remove the paper. Spread the whipped cream on top, and re-roll tightly. Cut a quarter of the cake off from the end, on the diagonal, and place the two pieces on a serving plate to look like a log.

Pipe (star tip) or dollop and spread the icing over the cake. Dust with icing sugar.

_BBC/Mary Berry_
