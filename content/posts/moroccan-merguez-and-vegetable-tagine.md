---
title: "Moroccan Merguez and Vegetable Tagine"
author: "pencechp"
date: "2014-02-17"
categories:
  - main course
tags:
  - mediterranean
  - moroccan
  - sausage
  - untested
---

## Ingredients

*   1 pound merguez sausage
*   2 onions, sliced
*   1 fennel bulb, halved and sliced
*   1 lemon, quartered \[CP: can see no reason why this wouldn't be a preserved lemon\]
*   1/2 pound carrots, peeled, cut into fourths, then sliced lengthwise
*   1 small butternut squash, peeled and chopped
*   3 cloves garlic, chopped
*   1 cup green olives, pitted
*   1/2 pound zucchini, cut into fourths, then sliced lengthwise
*   1 cup cilantro, finely chopped
*   1 cup cooked chickpeas
*   1/2 cup golden raisins
*   1/2 cup dried apricots, roughly chopped
*   2 teaspoons cinnamon
*   1 teaspoon ground coriander
*   2 teaspoons paprika
*   1 teaspoon turmeric
*   3 teaspoons cumin
*   3 cups chicken stock
*   1/4 cup olive oil
*   1 tablespoon flour
*   1 10-ounce box couscous

## Preparation

Heat the olive oil in a large, heavy pot. Use a tagine if you have one, but if not a big dutch oven will do. Add the cinnamon, coriander, paprika, turmeric and cumin to the pot.

Add the lemon, onion, fennel, garlic and green olives to the pot and cook until softened, for about 15 minutes. Taste for salt.

Meanwhile, brown the merguez sausage in a skillet over medium heat and reserve the cooking oils. Set aside.

Add the tablespoon of flour and mix well. Add 3 cups of chicken broth and allow to bubble for 5 minutes.

Place the merguez, plus the oil rendered from the merguez, and the remaining vegetables into the pot. Add the chickpeas, dried apricots and the raisins. Continue to cook over medium for 15 minutes, or until the vegetables are tender and the merguez is cooked through.

For the couscous, follow the directions on the box, and stir in the turmeric before covering the pot to give the couscous a bright yellow hue.

Garnish with cilantro and serve with couscous.

_Laila Gohar, Food Republic_
