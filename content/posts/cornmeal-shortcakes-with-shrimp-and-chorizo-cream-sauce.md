---
title: "Cornmeal Shortcakes with Shrimp and Chorizo Cream Sauce"
author: "juliaphilip"
date: "2009-12-06"
categories:
  - main course
tags:
  - chorizo
  - shrimp
  - spanish
---

## Ingredients

*Cornmeal shortcakes:*

*   1 1/2 cups flour
*   1/2 cup cornmeal
*   1 teaspoon sugar
*   1 tablespoon baking powder
*   1 teaspoon salt
*   1/2 cup butter
*   3/4 cup heavy cream, plus extra for brushing

*Shrimp and chorizo in cream:*

*   1 tablespoon butter
*   1/4 cup diced Spanish chorizo
*   1/2 pound mushrooms, quartered
*   2 tablespoons minced shallot
*   1/2 cup dry white wine
*   1/2 pound small cooked shrimp
*   1/2 cup heavy cream
*   1/2 teaspoon smoked paprika
*   Salt and freshly ground black pepper
*   2 tablespoons minced green onions

## Preparation

*Cornmeal shortcakes:* Heat oven to 375 degrees.

In a food processor bowl, pulse together the flour, cornmeal, sugar, baking powder and salt. Add the cold cubed butter and pulse just until the mixture has the texture of lightly moistened cornmeal. There may be a few pea-sized chunks of butter remaining.

Pour in 3/4 cup whipping cream all at once and then pulse 4 to 6 times just to moisten the dough. Do not overmix or the dough will be tough.

Turn the dough out onto a lightly floured work surface and gather into a crumbly mass. Knead 3 to 4 times to make it cohesive and then pat into a rough circle 6 to 7 inches in diameter and a consistent 3/4 to 1 inch in height.

Using a sharp knife, cut the circle into 6 wedges. Transfer to a cookie sheet, brush with a little cream, and bake until risen and golden brown, 18 to 20 minutes. Turn the pan around halfway through to ensure even cooking. Remove to a cooling rack.

*Shrimp and Chorizo Cream:* Melt the butter in a medium skillet over medium heat. Add the diced chorizo and cook until it begins to brown and crisp, about 3 minutes. Add the mushrooms and cook until they soften, about 5 minutes. Add the shallot and cook until it softens, about 3 minutes. Add the white wine and cook until it reduces to a syrupy glaze, about 5 minutes. Add the shrimp and toss them in the glaze. Add the cream and smoked paprika and simmer until the cream has reduced enough to lightly coat the back of a spoon, about 5 minutes. Season to taste with salt and freshly ground black pepper.

Split each shortcake in half horizontally. Place the bottom half on a plate and spoon the shrimp mixture over. Sprinkle lightly with minced green onion, place the shortcake tops on top and serve immediately.

_Chicago Tribune_
