---
title: "Satsuma Mandarin and Vanilla Upsidedown cake"
author: "juliaphilip"
date: "2017-05-03"
categories:
  - dessert
tags:
  - orange
  - satsuma
---

## Ingredients

*   4 to 5 medium Satsuma mandarins (1 1/2 pounds), thinly sliced and seeded
*   8 ounces (2 sticks) unsalted butter, softened
*   1 vanilla bean, split and scraped, pod reserved for another use
*   1 1/2 cups sugar
*   3 tablespoons freshly squeezed Satsuma mandarin juice (from 1 mandarin)
*   1 1/3 cups plus 1 tablespoon all-purpose flour
*   1 1/2 teaspoons baking powder
*   1/2 teaspoon coarse salt
*   1 tablespoon finely grated mandarin zest
*   2 large eggs, room temperature
*   1/2 cup whole milk

## Preparation

Preheat oven to 350 degrees. Cook mandarins in a large pot of boiling water for 3 minutes. Drain. Arrange slices in a single layer on paper towels.

Place 1 stick of butter in a 9-by-2-inch round cake pan. Mix half the vanilla seeds and 1/2 cup sugar, then sprinkle over butter. Place in oven until butter melts, about 7 minutes. Carefully whisk in 2 tablespoons mandarin juice.

Whisk flour, baking powder, and salt in a medium bowl. Cream zest and remaining 1 stick butter, 1 cup sugar, and vanilla seeds with a mixer until light and fluffy. With mixer running, add eggs, 1 at a time, beating well after each addition. Reduce speed to low. Add half the flour mixture, then the milk and remaining 1 tablespoon juice. Beat in remaining flour mixture.

Arrange mandarin slices in a circle over sugar in pan, starting in the center and spiraling outward, overlapping slices slightly. (Use slices that are completely intact.) Gently spoon batter on top of mandarins, and spread evenly. Bake cake until golden brown and a tester inserted in center comes out clean, 45 to 50 minutes. Let cool in pan on wire rack. Run a knife around edge of pan to loosen cake. Invert onto a serving plate, and let cool before serving.

_Martha Stuart_
