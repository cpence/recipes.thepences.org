---
title: "Yellow Cocktail"
date: 2021-12-23T01:35:34+01:00
categories:
  - cocktails
tags:
  - suze
  - gin
  - chartreuse
---

## Ingredients

- 2/3 oz. gin
- 2/3 oz. Suze
- 2/3 oz. yellow chartreuse
- 2/3 oz. lemon juice

## Preparation

Combine and shake over ice. Strain and serve in a coupé glass.

_CN Traveler_
