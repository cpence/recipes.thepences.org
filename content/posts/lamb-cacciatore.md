---
title: "Lamb Cacciatore"
author: "pencechp"
date: "2010-04-02"
categories:
  - main course
tags:
  - italian
  - lamb
  - untested
---

## Ingredients

*   3 lbs. boneless leg of lamb, cut into 1 1/2" chunks
*   1/4 cup dry white wine
*   2 lg. garlic cloves
*   6 TBSP (or more) olive oil, divided
*   2 TBSP chopped fresh rosemary
*   1 TBSP chopped fresh oregano
*   1 TBSP chopped fresh mint
*   4 anchovy fillets, finely chopped
*   5 to 6 ounces baby arugula
*   lemon wedges

## Preparation

Sprinkle lamb with salt and pepper put in a large zip-lock. Add wine, pressed garlic, 4 TBSP oil, all herbs and anchovies. Mush around to mix and coat well. Chill at least 2 hours or over night, mushing and turning occasionally.

Toss arugula with remaining oil in large platter; sprinkle with salt and pepper.

Put lamb on metal skewers and either grill or broil to desired doneness - about 10 minutes for med. rare. Serve on top of arugula with lemon slices.
