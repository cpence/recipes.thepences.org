---
title: "Wild Mushroom Goulash"
author: "pencechp"
date: "2017-10-30"
categories:
  - main course
tags:
  - hungarian
  - stew
  - vegan
  - vegetarian
---

## Ingredients

*   1/4 cup extra-virgin olive oil
*   2 medium onions, coarsely chopped
*   1 pound Hungarian wax peppers or Italian frying peppers—cored, seeded and chopped
*   1 1/2 pounds wild mushrooms, cut into 1-inch pieces
*   1 1/2 pounds cremini or white button mushrooms, quartered
*   Salt
*   Freshly ground pepper
*   4 garlic cloves, smashed
*   1 teaspoon caraway seeds
*   1/4 cup sweet Hungarian paprika
*   1 tablespoon hot Hungarian paprika
*   One 28-ounce can diced tomatoes
*   2 medium Yukon Gold potatoes, peeled and cut into 1-inch pieces
*   1 pound zucchini, cut into 1-inch pieces
*   6 cups vegetable broth
*   2 bay leaves
*   2 tablespoons fresh bread crumbs
*   Sour cream and chopped parsley, for serving

## Preparation

In a large enameled cast-iron casserole, heat the oil. Add the onions and peppers and cook over moderate heat, stirring, until softened, about 6 minutes. Add all of the mushrooms, season with salt and pepper and cook until browned, about 10 minutes.

Using the side of a chef's knife, mash the garlic to a paste with the caraway seeds and a generous pinch of salt; scrape into the casserole. Stir in both paprikas, the tomatoes, potatoes and zucchini. Add the broth and bay leaves, season with salt and pepper and bring to a boil. Cover and cook over low heat, until the stew is richly flavored, about 1 hour.

Stir the bread crumbs into the stew and cook until slightly thickened, about 10 minutes; serve with sour cream and parsley.

_Food & Wine_
