---
title: "Bestilla"
author: "pencechp"
date: "2010-08-03"
categories:
  - main course
tags:
  - chicken
  - moroccan
---

## Ingredients

_For the Filling:_

*   4 pigeons, about 1 lb. each, with livers and giblets (or doves, cornish game hens, or, most easily, chicken thighs)
*   salt and freshly ground pepper
*   1/2 cup unsalted butter
*   1 cup chopped onion
*   4 TBSP chopped fresh cilantro
*   2 TBSP chopped fresh flat-leaf parsley
*   1 tsp ground ginger
*   1 tsp ground cumin
*   1/2 tsp cayenne pepper
*   1/2 tsp ground turmeric
*   1/2 tsp ground cinnamon
*   1/8 tsp saffron threads, crushed
*   1 cup water
*   2 TBSP fresh lemon juice
*   8 eggs

_For the Almond Layer:_

*   1/4 cup unsalted butter
*   1 1/2 cups slivered blanched almonds
*   2 TBSP sugar
*   1/2 tsp ground cinnamon

_For Assembling the Pie:_

*   1/2 cup unsalted butter, melted and cooled
*   14-16 phyllo sheets, thawed

_For Serving:_

*   3 TBSP confectioner's sugar
*   1 TBSP ground cinnamon
*   whole blanched almonds for garnish (optional)

## Preparation

1\. Cut the birds in quarters and rub with salt and pepper. Melt butter in a large pan over medium heat and saute the bird, turning often, until evenly browned, about 10 minutes. Removed to platter.

2\. To the same pan add onion, liver and giblets and cook until onion is translucent, about 10 minutes. Add the herbs, spices and water and bring to a boil. Return birds, reduce heat to low, cover and simmer until tender, about 40 minutes. Remove meat from pot (leave juices in pan) and, when cool enough, shred meat and chop innards.

3\. Over high heat, reduce pan juices to about 1 3/4 cups. Add lemon juice.

4\. Meanwhile, beat eggs in bowl to blend. Stir the eggs into the pan juices and cook over low heat, stirring constantly, until soft curds form. (this takes a while.) Season with S & P. Remove from heat and drain in a strainer. Set aside.

5\. Fry the almonds in butter over medium heat until golden brown, about 5 minutes. Remove with slotted spoon and drain on paper towel. Chop coarsely, place in small bowl and toss with sugar and cinnamon.

6\. Heat oven to 350. Brush a 14" pizza pan with butter. Arrange 8 phyllo sheets like the spokes of a wheel, brushing each with butter. The sheets should overlap so that all of the pan is covered and they should overhang the edges of the pan generously. Be sure to butter the edges well. Sprinkle 1/3 of the almond mixture in a 10" circle in the center of the pan. Top with half of the eggs. Top with all the meat, then the remaining eggs, then the remaining almonds. Fold the phyllo edges over the filling. Top with phyllo sheets as you did the bottom, this time tucking the edges under the pie.

7\. Bake until golden on top, about 20 minutes. Remove from oven and carefully drain off the excess butter. Invert a second baking sheet over the pie and flip. Return to the oven to brown, 10-15 minutes. Flip again, then bake 5 minutes longer.

8\. Slide onto serving platter. Dust with confectioner's sugar and cinnamon (and almonds).
