---
title: "Cornbread Stuffing"
date: 2023-08-09T22:04:00+02:00
categories:
  - side dish
tags:
  - stuffing
  - pence
---

## Ingredients

- 13x9" or two 8x8" (more or less) pan(s) of cornbread
- 1 stick of butter, melted
- 1 lg. onion
- several ribs of celery
- salt and pepper
- fresh parsley
- fresh sage
- fresh rosemary
- fresh time
- 12 oz / 350g sausage [optional]
- one standard pint-box fresh mushrooms [optional]
- 4 c. chicken broth

## Preparation

If you’re using it, crumble and cook the sausage and set aside. Cook the
mushrooms in the sausage grease, adding extra butter if necessary. If desired,
cook the onion and celery; these may also be left raw.

To make the stuffing, crumble the cornbread, pour over the melted butter, and
add the onion, celery, spices, sausage, and mushrooms. Moisten the stuffing with
as much chicken (or turkey) broth as required. Stuff turkey or bake as required.
