---
title: "Lebanese-Style Peppers"
author: "pencechp"
date: "2014-12-18"
categories:
  - main course
tags:
  - bellpepper
  - lamb
  - mediterranean
  - untested
---

## Ingredients

*   1 tbsp. black pepper
*   1 tbsp. ground allspice
*   1 tbsp. cinnamon
*   1 tsp. nutmeg
*   1 tsp. ground coriander
*   1 tsp. ground cloves
*   1 tsp. powdered ginger
*   2 large green bell peppers
*   1 tbsp. olive oil
*   1 small yellow onion, diced
*   1 small yellow bell pepper, stemmed and seeded, diced
*   1 small orange bell pepper, stemmed and seeded, diced
*   1/2 small eggplant, peeled and diced
*   1/4-1 tsp. salt, to taste
*   1 lb. ground lamb
*   1 garlic clove, minced
*   1 1/2 c. chunky tomato sauce or puree
*   1/4-1/2 tsp. black pepper
*   1/2-1 c. feta cheese, crumbled
*   1 tbsp. oregano
*   1/2 c. plain greek yogurt, optional
*   fresh lemon wedges, optional

## Preparation

Preheat oven to 350. Mix the first seven ingredients and set aside. Remove the tops from the green peppers, slice in half, and remove seeds and ribs. Place in an 8x8 pan, bowl up.

In a large skillet, heat oil at medium heat. Add onion, cook 5 minutes. Add the diced bell peppers, cook until onion is golden and soft. Remove and cool. Sautee the eggplant over medium-high heat until brown but still holding its shape. Add a dash of salt and the spice mix. Cook 1 minute and add to onion/pepper mix.

Brown the lamb, breaking into small pieces. At the end, add garlic, stir, and remove from heat. Cool slightly. Drain any oil or juices and stir in tomato sauce and 1/2 tbsp. spice mix. Remove from heat and cool. Add oregano to the feta, and mix.

Divide the meat/tomato mix between the peppers. Layer with the mix of peppers, onions, and eggplant. Cover with foil and bake for 20-30 minutes until the green peppers  soften. Remove foil for the last 10 minutes. For the last 5 minutes, add the feta. Serve with yogurt and lemons if desired.

_Penzey's Spices_
