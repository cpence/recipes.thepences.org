---
title: "Roasted Cauliflower in Turmeric Kefir"
date: 2021-07-10T11:59:05+02:00
categories:
  - main course
  - side dish
tags:
  - cauliflower
  - vegetarian
  - indian
  - curry
---

## Ingredients

- 910g cauliflower, broken into florets
- 1 tsp. garam masala
- sea salt
- 60 ml vegetable oil
- 150g red onion, minced
- 1/2 tsp. ground turmeric
- 1/2 tsp. red chili powder (optional)
- 30g chickpea flour
- 480 ml fresh kefir or buttermilk
- 1/2 tsp. cumin seeds
- 1/2 tsp. black mustard seeds
- 1 tsp. red chili flakes
- 2 tbsp. cilantro or flat-leaf parsley, chopped

## Preparation

Preheat the oven to 200C fan/gas mark 7.

Place the cauliflower in a roasting pan or baking dish. Sprinkle with the garam
masala, season with salt and toss to coat. Drizzle with 1 tablespoon of the oil
and toss to coat evenly. Roast the cauliflower for 20-30 minutes, until golden
brown and slightly charred. Stir the florets halfway through roasting.

While the cauliflower is roasting, place a deep, medium saucepan or casserole
pot over medium-high heat. Add 1 tablespoon of the oil to the pan. Add the
onions and sauté until they just start to turn translucent – 4-5 minutes. Add
the turmeric and chilli powder and cook for 30 seconds. Lower the heat to
medium-low and add the chickpea flour. Cook, stirring constantly, for 2-3
minutes. Lower the heat to a gentle simmer and fold in the kefir, stirring
constantly. Watch the liquid carefully as it cooks until it thickens slightly –
2-3 minutes. Fold the roasted cauliflower into the liquid and remove from the
heat. Taste and add salt if necessary.

Heat a small frying pan over medium-high heat. Add the remaining 2 tablespoons
of oil. Once the oil is hot, add the cumin and black mustard seeds and cook
until they start to pop and the cumin starts to brown – 30-45 seconds. Remove
from the heat and add the chilli flakes, swirling the oil in the pan until the
oil turns red. Quickly pour the hot oil with the seeds over the cauliflower in
the saucepan. Garnish with the chopped coriander and serve warm with rice or
parathas.

_Nik Sharma / BBC_
