---
title: "Instant Pot Roasted Chicken"
author: "pencechp"
date: "2018-02-07"
categories:
  - main course
tags:
  - chicken
  - instantpot
---

## Ingredients

*   1 tbsp. olive oil
*   1 tbsp. brown sugar
*   1 tbsp. chili powder
*   1 tbsp. smoked paprika
*   1 tsp. chopped thyme leaves
*   kosher salt
*   Freshly ground black pepper
*   1 small chicken (3-4lb)
*   2/3 c. low sodium chicken broth
*   2 tbsp. chopped parsley

## Preparation

In a small bowl, whisk together brown sugar, chili powder, paprika, and thyme. Pat chicken dry with paper towels then season generously with salt and pepper. Rub the brown sugar mixture all over chicken.

Preheat Instant-Pot to sauté setting. When the Instant-Pot says it's hot, add olive oil then add chicken, breast side-down . Let sear until skin is crispy, about 4 minutes. Use very large tongs to flip chicken then pour chicken broth in the bottom of the Instant Pot. Change setting to pressure cook setting and set timer to 25 minutes. Lock the lid into place and set the valve to “Sealing.” Allow the Instant Pot to depressurize naturally, then remove lid and take out chicken. Let chicken reset for 10 minutes before slicing.

_Delish_
