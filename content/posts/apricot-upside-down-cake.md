---
title: "Apricot Upside Down Cake"
date: 2023-08-09T22:04:21+02:00
categories:
  - dessert
tags:
  - apricot
  - cake
  - untested
---

## Ingredients

_For the Topping:_

- 4 tablespoons / 55 grams unsalted butter, melted, plus more for greasing
- Flour, for dusting
- ½ cup / 110 grams light or dark brown sugar, packed
- ½ teaspoon ground cinnamon
- ¼ teaspoon fine sea salt
- 8 medium apricots, rinsed and dried (about 1 pound)

_For the Cake:_

- 1 cup / 130 grams all-purpose flour
- ½ cup / 55 grams almond flour
- 1 teaspoon baking powder
- ¼ teaspoon fine sea salt
- 6 tablespoons / 85 grams unsalted butter, at room temperature
- ⅔ cup / 135 grams granulated sugar
- 1 teaspoon vanilla extract
- 2 large eggs, at room temperature
- ½ cup / 120 milliliters whole milk

## Preparation

1. Heat oven to 350 degrees. Prepare the topping: Lightly grease and flour a
   9-inch cake pan or an 8-inch square baking pan. In a small bowl, stir
   together the melted butter, brown sugar, cinnamon and ¼ teaspoon salt. Spread
   the brown sugary mix in an even layer on the bottom of the pan.
2. Cut the apricots in half and remove the pits. Arrange the apricots,
   flesh-side down, evenly over the bottom of the pan.
3. Prepare the cake batter: Add the flour to a medium bowl. Sift in the almond
   flour, then use your fingertips to break up any clumps that don’t fit through
   the sieve. Add the baking powder and salt to the bowl and whisk to combine.
4. In a large bowl with an electric hand mixer, or in a stand mixer with the
   paddle attached, beat the butter and granulated sugar on medium until
   combined and fluffy, 2 to 3 minutes. Add the vanilla extract, then the eggs,
   one at a time, mixing until smooth. Use a rubber spatula to scrape down the
   bottom and sides of the bowl.
5. Add ⅓ of the flour mixture, then ½ of the milk, mixing to combine. Continue
   alternating the additions of flour and milk and mixing, scraping down the
   sides of the bowl, just until all is combined. (Make sure not to overmix
   during this step.)
6. Spread the batter in an even layer over the apricot halves. Bake until the
   cake is set and doesn’t jiggle when shaken, is golden brown on top, and a
   toothpick inserted into the center comes out clean, about 35 minutes.
7. Remove pan from the oven, and place on a cooling rack for 5 minutes. With the
   cake still in the pan, turn the pan over onto a serving platter. Wait 1
   minute, then remove the pan. This cake is excellent warm, but it’s also
   delicious at room temperature.

_NYT_
