---
title: "Homemade Bisquick"
author: "juliaphilip"
date: "2019-09-21"
categories:
  - bread
  - miscellaneous
tags:
---

## Ingredients

*   4 c (480 g) all purpose flour
*   2 tbsp baking powder
*   1.5 tsp salt
*   2 tsp sugar
*   1/2 c (4 oz) melted butter or shortening

## Preparation

Mix dry ingredients, then add melted butter and mix well.
