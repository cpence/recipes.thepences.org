---
title: "Sauce Espagnole"
date: 2021-08-09T18:11:23+02:00
categories:
  - miscellaneous
tags:
  - sauce
  - french
---

## Ingredients

- 1 bay leaf
- 1/2 teaspoon dried thyme
- 3 to 4 fresh parsley stems
- 7 to 8 whole black peppercorns
- 1 ounce clarified butter
- 1/2 cup diced onion
- 1/4 cup diced carrot
- 1/4 cup diced celery
- 1 ounce all-purpose flour
- 3 cups [brown stock]({{<relref brown-stock.md>}}) (i.e. beef stock)
- 2 tablespoons tomato puree

## Preparation

Fold the bay leaf, thyme, parsley stems, and peppercorns in a square of
cheesecloth and tie the corners with a piece of kitchen twine. Leave one string
long enough so that you can tie it to the handle of your pot to make it easier
to retrieve.

In a heavy-bottomed saucepan, melt the butter over a medium heat until it
becomes frothy. Add the mirepoix—onions, carrots, and celery—and sauté for a few
minutes until lightly browned. Don't let it burn. With a wooden spoon, stir the
flour into the mirepoix a little bit at a time until it is fully incorporated
and forms a thick paste (this is your roux). Lower the heat and cook the roux
for another 5 minutes or so, until it just starts to take on a very light brown
color. Again, don't let it burn. Using a wire whisk, slowly add the stock and
tomato purée to the roux, whisking vigorously to make sure it's free of lumps.

Bring to a boil, lower the heat, and add the sachet. Simmer for about 50
minutes, or until the total volume has reduced by about 1/3, stirring frequently
to make sure the sauce doesn't scorch at the bottom of the pan. Use a ladle to
skim off any impurities that rise to the surface. Remove the sauce from the heat
and retrieve the sachet. For an extra smooth consistency, carefully pour the
sauce through a wire mesh strainer lined with a piece of cheesecloth.

_Spruce Eats_
