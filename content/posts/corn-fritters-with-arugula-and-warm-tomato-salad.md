---
title: "Corn Fritters with Arugula and Warm Tomato Salad"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - side dish
tags:
  - arugula
  - corn
  - tomato
---

## Ingredients

*For tomatoes:*

*   6 scallions, white and pale green parts separated from dark green parts and both finely chopped
*   2 tablespoons olive oil
*   1 lb cherry or grape tomatoes, halved (3 to 4 cups)
*   1/4 teaspoon salt
*   1/4 teaspoon black pepper

*For fritters:*

*   2/3 cup corn (cut from 2 ears)
*   2/3 cup yellow cornmeal
*   3 tablespoons all-purpose flour
*   1/4 teaspoon salt, or to taste
*   1/8 teaspoon baking soda
*   Pinch of sugar
*   1/2 cup whole milk
*   1 large egg
*   1/3 cup vegetable oil

*For arugula:*

*   2 1/2 teaspoons white-wine vinegar
*   1/2 teaspoon whole-grain mustard
*   1/4 teaspoon salt, or to taste
*   1/4 teaspoon black pepper
*   3 tablespoons olive oil
*   1 lb arugula, coarse stems discarded (8 cups)

## Preparation

*Prepare tomatoes:*

Cook white and pale green scallions in oil in a 10- to 12-inch nonstick skillet over moderate heat, stirring, until softened, 1 to 2 minutes. Add tomatoes, salt, and pepper and cook, stirring, until tomatoes begin to soften, 3 to 5 minutes. Remove from heat and stir in scallion greens. Transfer to a bowl and cool to warm.

*Make fritters while tomatoes cool:*

Cook corn in a small saucepan of boiling water until tender, about 3 minutes. Drain in a sieve, then rinse under cold water and pat dry.

Whisk together cornmeal, flour, salt, baking soda, and sugar in a bowl. Whisk together milk and egg in another bowl, then add to dry ingredients and stir until just combined (do not overmix). Stir in corn.

Heat oil in cleaned skillet over moderate heat until hot but not smoking. Working in batches of 4, spoon 1 heaping tablespoon batter per fritter into skillet and fry, turning over once, until lightly browned, about 4 minutes total. Transfer with a spatula to paper towels to drain.

*Prepare arugula:*

Whisk together vinegar, mustard, salt, and pepper in a large bowl, then add oil in a slow stream, whisking until emulsified. Add arugula and toss to coat.

Divide arugula, fritters, and tomatoes among 8 small plates.

_Gourmet, August 2003_
