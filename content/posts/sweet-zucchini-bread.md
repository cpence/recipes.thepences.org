---
title: "Sweet Zucchini Bread"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - bread
  - breakfast
tags:
  - untested
  - zucchini
---

## Ingredients

*   1 1/4 cups flour
*   1 tsp baking soda
*   1 tsp baking powder
*   1/4 tsp cinnamon
*   3/4 cup sugar
*   2 large eggs, beaten
*   1/2 c vegetable oil
*   1 tsp vanilla
*   1/2 tsp salt
*   2 cups grated zucchini, squeezed of excess moisture
*   1 1/2 cups ground pecans or walunts

## Preparation

Preheat oven to 350. Grease a 9 x 5 inch loaf pan.

Whisk together first four ingredients. Blend the next 5 ingredients in a bowl. Stir in the dry ingredients. Blend in the zucchini and nuts with a few swift strokes. Scrape the batter into the greased pan. Bake until the bread pulls away from the sides of the pan, about 45 min. Cool in the pan on a rack for 10 minutes before unmolding to cool completely on the rack.

_Joy of Cooking Calendar_
