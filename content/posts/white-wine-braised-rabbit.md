---
title: "White Wine Braised Rabbit"
author: "pencechp"
date: "2017-03-21"
categories:
  - main course
tags:
  - french
  - rabbit
---

## Ingredients

*   1 small rabbit, about 3 pounds, cut into 6 to 8 pieces
*   Salt and pepper
*   3 tablespoons lard or vegetable oil
*   1 cup all-purpose flour for dredging rabbit, plus 2 tablespoons for sauce
*   1 large onion, diced (about 2 cups)
*   1 cup dry white wine
*   2 ½ cups chicken broth
*   1 tablespoon whole-grain mustard
*   2 thyme branches
*   12 sage leaves
*   ½ cup crème fraîche
*   1 tablespoon Dijon mustard
*   1 teaspoon chopped capers
*   ¼ cup thinly sliced chives
*   1 pound cooked pappardelle pasta or wide egg noodles, for serving (optional)

## Preparation

Lay rabbit pieces on a baking sheet and season each piece generously with salt and pepper. (If you are using a pepper mill, adjust it for coarse grind.)

Heat oven to 375 degrees. Put a deep, heavy-bottomed, oven-safe saucepan or Dutch oven over medium-high heat and add lard or oil.

Put 1 cup flour on a wide plate. Dip seasoned rabbit pieces in flour and dust off excess. Gently set them in the hot oil in one layer without crowding; work in batches if necessary. Adjust heat to keep them from browning too quickly. Cook for about 3 to 4 minutes on each side until nicely browned.

Remove browned rabbit from pan and set aside. Add diced onion to fat remaining in pan. Keep heat brisk and cook onions until softened and lightly browned, stirring occasionally, about 5 to 6 minutes. Season with salt and pepper.

Sprinkle onions with 2 tablespoons flour and stir until well incorporated, then cook for a minute or so, until mixture starts to smell toasty. Add wine and 1 cup broth, whisking as the sauce thickens. Whisk in remaining broth and the whole-grain mustard and bring to a simmer. Taste for salt and adjust.

Return browned rabbit pieces to the sauce. Add thyme and sage. Cover pot and bake for 45 minutes to 1 hour, until meat is fork tender. (Alternatively, simmer over low heat, covered, on the stove top, for about the same amount of time.)

Using tongs, remove rabbit pieces from sauce, set aside, and keep warm. Put saucepan over medium heat and bring contents to a simmer. Whisk in crème fraîche, Dijon mustard and capers and simmer until somewhat thickened, about 5 minutes. Taste sauce and adjust.

Transfer rabbit to a warmed serving bowl and ladle the sauce over. Sprinkle generously with chives and a little freshly ground pepper. Accompany with noodles if desired.

_David Tanis, New York Times_
