---
title: "Bean and Ham Dip with Garlic and Rosemary"
author: "pencechp"
date: "2010-05-08"
categories:
  - side dish
tags:
  - bean
  - dip
  - pork
  - untested
---

## Ingredients

*   4 cloves garlic
*   2 sprigs rosemary, leaves removed and chopped
*   1/2 of a red onion, coarsely chopped
*   1/2 cup coarsely chopped smoked ham
*   1/4 teaspoon red-pepper flakes, optional
*   2 cans (15 ounces each) cannellini or navy beans, rinsed and drained
*   1/2 teaspoon coarse salt
*   Freshly ground pepper
*   1/4 cup extra-virgin olive oil

## Preparation

Place garlic, rosemary, onion, ham and red-pepper flakes in a food processor; pulse to finely chop. Add beans, salt and pepper to taste. Slowly add olive oil with motor running until beans are pureed to a spreadable consistency, about 1 minute. Adjust seasoning.

Note: Serve with thick chips or as a spread on slices of toasted baguette.

Makes 1 1/2 cups.

_Chicago Tribune_
