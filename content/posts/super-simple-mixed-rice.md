---
title: "Super-Simple Mixed Rice"
author: "pencechp"
date: "2010-05-08"
categories:
  - side dish
tags:
  - rice
  - vegan
  - vegetarian
---

## Ingredients

*   1/4 cup dried porcini mushrooms, soaking in hot water
*   2 tablespoons olive oil
*   1 cup short-grain brown rice
*   1 onion, very finely chopped
*   1 teaspoon salt
*   1/2 teaspoon freshly ground pepper
*   1 can (12 ounces) chopped tomatoes, with their liquid
*   8 ounces button mushrooms, sliced
*   1 can (15 ounces) cannellini beans, drained, rinsed
*   1/2 cup chopped fresh basil or parsley
*   1/2 cup grated Parmesan cheese, optional

## Preparation

Heat oil in a stockpot over medium heat. Add rice; cook while stirring, until translucent, about 1 minute. Add onion, salt and pepper; continue to cook, stirring occasionally, until softened, about 1 minute. Add enough water to cover by 1/2 inch, about 3 1/2 cups. Heat to a boil; then simmer. Cook, stirring occasionally, 10 minutes.

Remove porcini from water, reserving liquid; coarsely chop. Pour liquid (without sediment) into rice. Stir in porcini, tomatoes and button mushrooms. Heat to a boil; then simmer. Cook, stirring occasionally, until tomatoes break down, about 30 minutes, adding water if needed.

When rice is tender, add beans. Cook, stirring occasionally, until mixture is no longer soupy but not yet dry, about 2 minutes. Add basil and cheese; serve.

_Chicago Tribune_
