---
title: "Savory Polenta"
author: "pencechp"
date: "2013-03-13"
categories:
  - side dish
tags:
  - vegetarian
---

## Ingredients

*   2 tablespoons olive oil, plus extra for grilling or sauteing if desired
*   3/4 cup finely chopped red onion
*   2 cloves garlic, finely minced
*   optional: 1/2 medium jalapeño, or a few tsp. hot paprika
*   1 quart chicken stock or broth
*   1 cup coarse ground cornmeal
*   3 tablespoons unsalted butter
*   1 1/2 teaspoons kosher salt
*   1/4 teaspoon freshly ground black pepper
*   2 ounces Parmesan, grated

## Preparation

Preheat oven to 350 degrees F.

In a large, oven-safe saucepan heat the olive oil over medium heat. Add the red
onion and salt and sweat until the onions begin to turn translucent,
approximately 4 to 5 minutes. Reduce the heat to low, add the garlic (and
peppers or paprika, if using), and saute for 1 to 2 minutes, making sure the
garlic does not burn.

Turn the heat up to high, add the chicken stock, bring to a boil. Gradually add
the cornmeal while continually whisking. Once you have added all of the
cornmeal, cover the pot and place it in the oven. Cook for 35 to 40 minutes,
stirring every 10 minutes to prevent lumps. Once the mixture is creamy, remove
from the oven and add the butter, salt, and pepper. Once they are incorporated,
gradually add the Parmesan.

Serve as is, or pour the polenta into 9 by 13-inch cake pan lined with parchment
paper. Place in the refrigerator to cool completely.

Once set, turn the polenta out onto a cutting board and cut into squares,
rounds, or triangles. Brush each side with olive oil and saute in a nonstick
skillet over medium heat, or grill.

_Alton Brown, Food Network_
