---
title: "Lamb Ribs Masala with Cucumber Raita"
author: "pencechp"
date: "2012-07-16"
categories:
  - main course
tags:
  - curry
  - lamb
  - untested
---

## Ingredients

*   1 c. thick plain yogurt
*   1 t. ginger
*   2 t. fennel seeds, minced
*   ½ t. turmeric
*   1 t. cinnamon
*   2 t. cardamom
*   Lamb ribs

_Cucumber Raita_

*   1 cucumber, seeds removed, or 1 bunch radishes
*   Salt
*   1 ½ c. plain yogurt
*   2 T. sour cream
*   4 T. minced cilantro

## Preparation

Combine the yogurt with spices and salt and coat entire surface of ribs. Marinate for 4 to 6 hours. Place ribs in a wide Dutch oven, pour in water and bring to a simmer. Cook, covered, at a very low simmer for about 1 hour, until tender. Remove ribs, reserving sauce in pot. Heat grape seed oil or ghee in a large skillet until almost smoking, then sear ribs to form a crust. Let meat rest briefly, then cut into individual ribs and serve with rice or roasted potatoes and raita.

For the Cucumber Raita: Grate cucumber or radishes into small bowl. If using cucumber, toss with salt and set aside until moisture is released. Squeeze out excess moisture and drain. Combine with remaining ingredients and adjust seasoning to taste.

_Edible Austin_
