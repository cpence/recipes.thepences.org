---
title: "Garlic-Lemon Roasted Cauliflower"
author: "pencechp"
date: "2016-04-28"
categories:
  - side dish
tags:
  - cauliflower
  - lemon
---

## Ingredients

*   1 head cauliflower
*   1 slice white bread, torn into 1-inch pieces
*   5 tbsp. olive oil
*   Salt and pepper
*   1 garlic clove, minced
*   1 tsp. lemon zest, plus lemon wedges for serving
*   ¼ c. chopped fresh parsley

## Preparation

1\. Trim outer leaves of cauliflower and cut stem flush with bottom of head. Turn head so stem is facing down and cut head into 3/4-inch-thick slices. Cut around core to remove florets; discard core. Cut large florets into 1 1/2-inch pieces. Transfer florets to bowl, including any small pieces that may have been created during trimming, and set aside.

2\. Pulse bread in food processor to coarse crumbs, about 10 pulses. Heat bread crumbs, 1 tablespoon oil, pinch salt, and pinch pepper in 12-inch nonstick skillet over medium heat, stirring frequently, until bread crumbs are golden brown, 3 to 5 minutes. Transfer crumbs to bowl and wipe out skillet.

3\. Combine 2 tablespoons oil and cauliflower florets in now-empty skillet and sprinkle with 1 teaspoon salt and 1/2 teaspoon pepper. Cover skillet and cook over medium-high heat until florets start to brown and edges just start to become translucent (do not lift lid), about 5 minutes.

4\. Remove lid and continue to cook, stirring every 2 minutes, until florets turn golden brown in many spots, about 12 minutes.

5\. Push cauliflower to edges of skillet. Add remaining 2 tablespoons oil, garlic, and lemon zest to center and cook, stirring with rubber spatula, until fragrant, about 30 seconds. Stir garlic mixture into cauliflower and continue to cook, stirring occasionally, until cauliflower is tender but still firm, about 3 minutes longer.

6\. Remove skillet from heat and stir in parsley. Transfer cauliflower to serving platter and sprinkle with bread crumbs. Serve, passing lemon wedges separately.

_CI_
