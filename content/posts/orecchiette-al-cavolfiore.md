---
title: "Orecchiette al Cavolfiore"
author: "pencechp"
date: "2016-08-09"
categories:
  - main course
  - side dish
tags:
  - cauliflower
  - italian
  - pasta
---

## Ingredients

*   1 medium cauliflower
*   1 clove garlic
*   1 chili (red?)
*   2 tbsp olive oil
*   500g (just over 1 lb) orecchiette pasta
*   3 anchovy fillets, roughly chopped
*   salt
*   parmesan, for serving, grated

## Preparation

Cut the cauliflower into little florets, cook in salted boiling water for 5 minutes, drain. Peel the garlic and deseed and chop the chili. Heat the oil and gently cook both for a few minutes. Add anchovies, cook two more minutes. Add cauliflower, cook until it just begins to brown on the edges. Meanwhile, cook pasta to package directions. Drain and toss with the cauliflower. Serve with generous parmesan and more olive oil.

_Nudo_
