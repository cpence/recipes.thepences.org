---
title: "Clementine and Sardine Salad"
author: "pencechp"
date: "2014-01-15"
categories:
  - side dish
tags:
  - fish
  - orange
  - salad
  - spanish
  - untested
---

## Ingredients

_For the dressing:_

*   1 c. olive oil
*   1/4 c. sherry vinegar
*   zest and juice of 2 clementines
*   1 shallot, thinly sliced
*   12 nice black olives, pitted and halved
*   sea salt to taste

_For the salad:_

*   1/2 head romaine lettuce
*   1/2 head bibb lettuce
*   1/2 head red lettuce
*   8 oil-packed sardines
*   4 clementines, peeled and sliced into 1/4" rounds
*   sea salt to taste

## Preparation

To make the dressing: whisk together olive oil, vinegar, clementine juice, zest, and shallots in a medium bowl. Add olives, season with salt, set aside.

To make the salad: Divide the heads of lettuce into individual leaves and distribute among 4 plates or bowls.  Place two sardines and four clementine slices on each salad.  Drizzle each salad with dressing and season with sea salt.

_Jose Andres Foods_
