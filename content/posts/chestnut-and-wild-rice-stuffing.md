---
title: "Chestnut and Wild Rice Stuffing"
author: "pencechp"
date: "2012-12-16"
categories:
  - side dish
tags:
  - chestnut
  - rice
  - stuffing
---

## Ingredients

*   6 slices bacon, chopped
*   1 yellow onion, chopped
*   2 cups sliced celery
*   ½ cup chopped parsley
*   1 can whole chestnuts, drained and chopped (or something over a pound of raw chestnuts, boiled and then peeled)
*   4 cups cooked wild rice (about 1.5 cups dry wild rice, plus water or beef or chicken broth according to directions)
*   1 tsp crumbled dried sage
*   1 tsp dried thyme
*   1 tsp dried marjoram

## Preparation

If you’re doing the chestnuts fresh, do them first–give yourself between 30 minutes and an hour to get them ready. Probably an hour. Boil the chestnuts for 5-10 minutes, then let them drain and cool, and peel (making sure to remove the skins). \[CP: Doing the chestnuts yourself fresh makes for a much better flavor -- the chestnuts are firmer and tastier. But it takes _forever,_ and is really annoying work.\]

Cook wild rice according to the package directions.

Fry bacon in a skillet until browned.

Pour off some of the grease if you wish, and add the onion, celery, and parsley, cooking for about 5 minutes, or until the onion and celery is soft.

Stir in chestnuts, rice, and herbs, and season to taste with salt and/or pepper.

_Yankee Magazine, via RoastGoose.com, via The Taste Place_
