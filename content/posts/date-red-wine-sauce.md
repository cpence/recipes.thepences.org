---
title: "Date & Red Wine Sauce"
author: "pencechp"
date: "2012-12-16"
categories:
  - miscellaneous
tags:
  - sauce
---

## Ingredients

*   600ml chicken stock
*   250ml red wine
*   3 tbsp port
*   3 tbsp balsamic vinegar
*   200g (7 oz) halved, stoned dates

## Preparation

Boil down the chicken stock, red wine, port and balsamic vinegar by two-thirds. Meanwhile, cover the dates with boiling water, leave for 5 mins and then drain. Strain the reduced stock and return to the pan with the soaked dates. Simmer sauce for 3-5 mins, then check seasoning and keep warm.

_Gordon Ramsay, BBC Good Food, December 2005_
