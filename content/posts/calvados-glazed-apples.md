---
title: "Calvados-Glazed Apples"
author: "pencechp"
date: "2012-12-16"
categories:
  - side dish
tags:
  - apple
---

## Ingredients

*   4 Granny Smiths
*   50g (3-4 tbsp.) butter
*   a sprig of sage
*   25g (around 1/8 c.) golden caster sugar (or raw sugar)
*   2 tbsp Calvados

## Preparation

Quarter the apples, cut out the cores and trim the ends. Heat the butter in a frying pan and toss in the apple wedges and sage. Cook for 2 mins, then sprinkle in the golden caster sugar and continue cooking for about 5 mins, stirring gently until golden and softened. Pour in the Calvados and cautiously flambé. Season lightly and keep warm until ready to serve.

_Gordon Ramsay, BBC Good Food, December 2005_
