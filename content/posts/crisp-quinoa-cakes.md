---
title: "Crisp Quinoa Cakes"
author: "pencechp"
date: "2014-06-10"
categories:
  - main course
  - side dish
tags:
  - quinoa
  - untested
  - vegan
  - vegetarian
---

## Ingredients

*   1 cup quinoa
*   Salt
*   1/4 cup chopped almonds
*   2 tablespoons minced shallots
*   1 tablespoon minced fresh rosemary
*   1 tablespoon Dijon mustard
*   Freshly ground black pepper
*   2 tablespoons olive oil
*   Lemon wedges for serving

## Preparation

Put the quinoa, a large pinch of salt and 2 1/4 cups water in a medium saucepan. Bring to a boil, and then adjust the heat so that the mixture bubbles gently. Cover, and cook, stirring once, until the grains are very tender and begin to burst, 25 to 30 minutes. When they are starchy and thick, transfer them to a large bowl to cool for a few minutes.

Heat the oven to 200. Fold the almonds, shallots, rosemary and mustard into the quinoa, and add a generous sprinkle of salt and pepper. With your hands, form the mixture into 8 patties.

Put 1 tablespoon olive oil in a large skillet over medium heat. When the oil is warm, cook 4 cakes at a time until the bottoms are nicely browned and crisp, about 4 minutes. Flip, and brown on the other side, another 4 minutes. Transfer the cakes to the oven to keep warm while you cook the second batch with another tablespoon of oil. Serve with lemon wedges.

_Alternatives:_ Instead of the almonds, shallots, rosemary and Dijon -- 1/4 cup pine nuts, 1/4 cup raisins, 1/4 cup chopped parsley, and 1/2 cup grated Parmesan, or -- 1/2 cup chopped cilantro, 1/4 cup chopped scallions, and 1 tbsp Sriracha.

_The New York Times_
