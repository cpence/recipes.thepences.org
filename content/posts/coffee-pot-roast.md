---
title: "Coffee Pot Roast"
author: "pencechp"
date: "2010-01-08"
categories:
  - main course
tags:
  - beef
  - crockpot
  - untested
---

## Ingredients

*   2 large onions, thinly sliced
*   1 beef rump roast, about 3 pounds, or 1 chuck roast, about 4 pounds
*   1 can (14 1/2 ounces) chicken broth
*   2 cloves garlic, minced
*   2 bay leaves
*   1 cup strong brewed coffee
*   1/4 cup soy sauce
*   1 teaspoon dried oregano leaves
*   Freshly ground pepper

## Preparation

1\. Place half of the onions in a slow cooker; top with roast. Top roast with remaining onions, broth, garlic, bay leaves, coffee, soy sauce, oregano and pepper to taste.

2\. Cover; cook on low until beef is tender, 6-8 hours, turning roast once or twice. Slice beef; serve with broth and onions.

_Chicago Tribune_
