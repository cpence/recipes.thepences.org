---
title: "Spinach Salad with Apples, Avocado, and Bacon"
author: "pencechp"
date: "2012-07-18"
categories:
  - side dish
tags:
  - avocado
  - bacon
  - salad
  - spinach
---

## Ingredients

*   1 cup vegetable oil
*   1/4 cup powdered sugar
*   1/4 cup apple cider vinegar
*   1 tablespoon fresh lemon juice
*   1 1/2 teaspoons dry mustard
*   1 1/2 teaspoons paprika
*   1/2 teaspoon ground ginger
*   1/2 pound bacon slices, chopped
*   2 6-ounce bags baby spinach leaves
*   2 medium-size red apples (such as Red Delicious or Gala), halved, cored, thinly sliced \[CP: replace this with Granny Smith\]
*   1 cup very thinly sliced red onion
*   1 ripe avocado, halved, pitted, peeled, cubed

## Preparation

Whisk oil, powdered sugar, vinegar, lemon juice, dry mustard, paprika and ground ginger in small bowl to blend. Season to taste with salt and pepper.

Sauté chopped bacon slices in heavy large skillet over medium-high heat until brown and crisp. Using slotted spoon, transfer chopped bacon to paper towel and drain. Combine baby spinach leaves, apple slices, red onion, avocado and bacon in large bowl. Toss salad with enough dressing to coat. Divide salad among plates and serve.

_Bon Appetit, December 2000_
