---
title: "Curried Cod and Mussels"
date: 2020-01-27T18:44:32+01:00
categories:
    - main course
tags:
    - mussels
    - fish
    - curry
---

## Ingredients

*   1/4 cup dried porcini mushrooms
*   3/4 cup boiling water
*   1 tablespoon extra-virgin olive oil
*   1/4 cup minced shallots (about 1 large)
*   1 Granny Smith apple, finely diced
*   1 garlic clove, minced
*   1 tablespoon Madras curry powder
*   2 thyme sprigs
*   Salt and freshly ground pepper
*   2 pounds mussels, scrubbed
*   1/2 cup dry white wine
*   1/2 cup heavy cream
*   1 pound skinless cod fillets—bones removed, fish cut into 2-inch chunks
*   Crusty bread, for serving 

## Preparation

Soak the porcini in the boiling water until softened, 10 minutes. Strain the mushrooms, reserving the soaking liquid, and rinse to remove any grit. Finely chop.

In a large pot, heat the oil. Add the shallots, apple, garlic, curry powder, thyme sprigs and porcini and season with salt and pepper. Cook over moderate heat, stirring, until the shallots are softened, about 5 minutes.

Add the mussels and toss. Add the wine. Bring to a boil, cover and cook over high heat until the mussels have opened, 3 minutes. Add the cream and 1/2 cup of the porcini soaking liquid, stopping before you reach the grit. Bring to a simmer. Nestle the cod in the broth, cover and cook until the fish lightly flakes, 4 minutes. Discard the thyme sprigs. Transfer the cod and mussels to large bowls and spoon the broth on top. Serve with crusty bread.

*Food & Wine*

