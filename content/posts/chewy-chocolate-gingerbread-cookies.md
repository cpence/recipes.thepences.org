---
title: "Chewy Chocolate Gingerbread Cookies"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - dessert
tags:
  - chocolate
  - cookie
---

## Ingredients

*   7 ounces best-quality semisweet chocolate
*   1 1/2 cups plus 1 tablespoon all-purpose flour
*   1 1/4 teaspoons ground ginger
*   1 teaspoon ground cinnamon
*   1/4 teaspoon ground cloves
*   1/4 teaspoon ground nutmeg
*   1 tablespoon cocoa powder
*   8 tablespoons (1 stick) unsalted butter
*   1 tablespoon freshly grated ginger
*   1/2 cup dark-brown sugar, packed
*   1/2 cup unsulfured molasses
*   1 teaspoon baking soda
*   1/4 cup granulated sugar

## Preparation

Line two baking sheets with parchment. Chop chocolate into 1/4-inch chunks; set aside. In a medium bowl, sift together flour, ground ginger, cinnamon, cloves, nutmeg, and cocoa.

In the bowl of an electric mixer, fitted with the paddle attachment, beat butter and grated ginger until whitened, about 4 minutes. Add brown sugar; beat until combined. Add molasses; beat until combined.

In a small bowl, dissolve baking soda in 1 1/2 teaspoons boiling water. Beat half of flour mixture into butter mixture. Beat in baking-soda mixture, then remaining half of flour mixture. Mix in chocolate; turn out onto a piece of plastic wrap. Pat dough out to about 1 inch thick; seal with wrap; refrigerate until firm, 2 hours or more.

Heat oven to 325 degrees. Roll dough into 1 1/2- inch balls; place 2 inches apart on baking sheets. Refrigerate 20 minutes. Roll in granulated sugar. Bake until the surfaces crack slightly, 10 to 12 minutes. Let cool 5 minutes; transfer to a wire rack to cool completely.

_Martha Stewart_
