---
title: "Miso-Glazed Salmon Steaks"
author: "pencechp"
date: "2011-11-12"
categories:
  - main course
tags:
  - fish
  - japanese
---

## Ingredients

*   Vegetable oil
*   4 10-to 12-ounces salmon steaks, bone in [CP: or other fish steaks, such as
    tuna, adjusting the cooking time/method below]
*   1/3 cup white miso
*   2 tablespoons mirin (sweet Japanese rice wine)
*   2 tablespoons unseasoned rice vinegar
*   2 teaspoons minced peeled fresh ginger
*   1 teaspoon Asian sesame oil
*   Kosher salt
*   4 lime wedges (for serving)

## Preparation

Line a rimmed baking sheet with foil; brush lightly with vegetable oil. Place
salmon steaks on prepared baking sheet. Whisk miso, mirin, vinegar, ginger, and
sesame oil in a small bowl to blend. Spread half the miso mixture over salmon
steaks; season lightly with salt. Turn salmon steaks over and spread with
remaining miso mixture; season lightly with salt. Cover with plastic wrap and
let marinate at room temperature for at least 15 minutes, or refrigerate for up
to 1 hour.

Position an oven rack 6"–8" from broiler and preheat. Broil salmon, turning
once, until golden brown and just opaque in center, 10–12 minutes total.

Transfer salmon steaks to plates and serve with lime wedges.

_Bon Appetit, May 2010_
