---
title: "Iraqi Lamb Stew"
date: 2021-01-09T22:34:27+01:00
categories:
  - main course
tags:
  - lamb
  - stew
  - mediterranean
---

## Ingredients

- Kosher salt
- 1 large eggplant (1 1/2 pounds), sliced crosswise 1/2 inch thick
- 1/4 cup plus 2 1/2 tablespoons vegetable oil
- 4 lamb shanks (about 1 1/4 pounds each)
- Freshly ground pepper
- 1 large white onion, chopped
- 1/2 cup pomegranate molasses
- 1/2 cup dried yellow split peas (3 ounces)
- 4 dried red chiles
- 2 teaspoons baharat spice blend or garam masala
- 2 teaspoons ground coriander
- 8 small pita breads, warmed and torn into large pieces

## Preparation

In a bowl, dissolve 2 teaspoons of salt in 1 quart of water. Add the eggplant, cover with a small plate to keep the slices submerged and let soak for 30 minutes.

Meanwhile, in a large enameled cast-iron casserole, heat 2 tablespoons of the oil. Season the lamb with salt and pepper and cook over high heat, turning once, until well browned, about 5 minutes per side. Add the onion and cook over moderate heat, stirring, until softened, 10 minutes. Add 3 quarts of water, the pomegranate molasses, split peas, dried chiles, baharat and coriander and bring to a boil. Reduce the heat to low and simmer for 45 minutes, skimming and stirring a few times.

Drain the eggplant and pat dry. In a large nonstick skillet, heat 11/2 tablespoons of the oil. Add one third of the eggplant and cook over moderately high heat until browned, 3 minutes per side; transfer to paper towels to drain. Brown the remaining eggplant in the 3 tablespoons of oil. Add the eggplant to the lamb and simmer over low heat, stirring, until the lamb is tender, 45 minutes.

Transfer the lamb to a rimmed baking sheet. Remove the meat from the bones and cut into 1-inch pieces. Boil the stew over high heat, skimming, until the liquid has reduced to 3 cups, about 40 minutes.

Return the lamb to the stew and season with salt and pepper. Divide the pita among 4 bowls. Ladle the stew on top and serve.

_Food & Wine_
