---
title: "Balsamic and Rosemary Steak Florentine"
date: 2020-09-28T12:18:52+02:00
categories:
    - main course
tags:
    - steak
---

## Ingredients

*   1 cup balsamic vinegar
*   1/2 cup plus 2 tablespoons extra-virgin olive oil
*   1/4 cup finely chopped rosemary
*   one 3-pound porterhouse steak, about 4 inches thick
*   2 teaspoons kosher salt
*   2 teaspoons coarsely ground pepper

## Preparation

In a sturdy resealable plastic bag, combine the vinegar with 1/2 cup of the
olive oil and the rosemary. Add the steak, seal the bag and refrigerate
overnight, turning the bag several times.

Preheat the oven to 425° and bring the steak to room temperature. Heat a grill
pan. Remove the steak from the marinade and season with the salt and pepper. Rub
the side with the remaining 2 tablespoons of olive oil. Grill over moderately
high heat until nicely charred on the top and bottom, about 5 minutes per side.
Transfer the steak to a rimmed baking sheet and roast for about 30 minutes,
until an instant-read thermometer inserted into the tenderloin (the smaller
section) registers 125°. Alternatively, build a fire on one side of a charcoal
grill or light a gas grill. Grill the steak over moderate heat for 5 minutes on
each side. Transfer the steak to the cool side of the grill, close the lid and
cook for 30 minutes longer. Transfer the steak to a carving board and let rest
for 10 minutes. Slice the steak across the grain and serve immediately.

*Food & Wine*

