---
title: "English Muffin Bread"
date: 2020-04-11T13:56:06+02:00
categories:
    - bread
---

## Ingredients

*   2 1/4 teaspoons instant yeast
*   1/3 cup  warm water
*   1 tablespoon granulated sugar, divided
*   3 cups (384g) all-purpose flour
*   1 1/2 teaspoon kosher salt
*   1/4 teaspoon fresh cracked black pepper
*   1/4 teaspoon baking soda
*   1/2 cup chopped scallions, whites and greens
*   1 cup grated sharp cheddar cheese
*   1 cup (225g) thick buttermilk, at room temperature
*   2 tablespoons plus 1 teaspoon vegetable oil

## Preparation

Lightly grease or spray a 9x5-inch metal loaf pan with shortening or nonstick cooking spray. Wipe away any excess that pools in the corners. Line the pan with a piece of parchment paper that hangs over the edges. Grease the parchment paper. Dust the pan with 2 tablespoons coarse cornmeal and tap around the pan and paper to coat.

In a small bowl whisk together yeast, warm water, and a pinch of sugar. Let rest and froth for a few minutes.

In the bowl of an electric stand mixer fitted with a paddle attachment, whisk together the flour, remaining sugar, salt, black pepper and baking soda. Add the scallions and cheddar and stir to combine.

Add the buttermilk, oil, and yeast mixture. Stir with a spatula to lightly incorporate. Place on the mixer with the dough hook and mix on low speed for 4 to 6 minutes. Until all of the ingredients are combined and the mixture is soft and sticky.

Lightly grease your fingers and scrape the dough out of the bowl into the prepared pan. With oiled fingers gently press the dough evenly into the pan, corner to corner. Sprinkle the top of the dough with remaining cornmeal.

Cover loosely with plastic wrap and allow to rest in a warm place until doubled in size, about an hour. The dough will come to about 1 inch or a smidge closer to the top of the pan.

Place a rack in the center of the oven and preheat oven to 400 degrees F. When bread has doubled, place it in the oven and allow to bake for 22 to 25 minutes, until golden brown and the bread sounds hollow when tapped in the center.

*Joy the Baker*

