---
title: "Mexican White Rice"
author: "juliaphilip"
date: "2010-04-26"
categories:
  - side dish
tags:
  - chile
  - mexican
  - rice
  - vegan
  - vegetarian
---

## Ingredients

*   2 tbsp. vegetable oil
*   3 cloves garlic, halved
*   3/4 c chopped onion
*   1 1/2 c white rice
*   3 c water
*   2 sprigs parsley
*   2 whole serrano chiles
*   1 1/4 tsp salt

## Preparation

Saute  the garlic until deep brown, then discard. Add the onion and saute until tender. Add rice and stir 5 min. Add hot water, parsley, serrano chilis, and salt. Bring to a boil. Reduce heat to med-low, cover and simmer until all almost all liquid is absorbed (15 min). Stir, cover, and simmer 5 more minutes until all liquid is absorbed. Remove from heat, let stand 10 min. Discard parsley and serranos.
