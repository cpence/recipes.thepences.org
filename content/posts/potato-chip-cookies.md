---
title: "Potato Chip Cookies"
author: "pencechp"
date: "2018-02-07"
categories:
  - dessert
tags:
  - cookie
  - untested
---

## Ingredients

*   2 cups butter, softened
*   1 cup sugar
*   1 teaspoon vanilla extract
*   3-1/2 cups all-purpose flour
*   2 cups crushed potato chips
*   3/4 cup chopped walnuts

## Preparation

Preheat oven to 350°. In a large bowl, cream butter and sugar until light and fluffy. Beat in vanilla. Gradually add flour to creamed mixture and mix well. Stir in potato chips and walnuts. Drop by rounded tablespoonfuls 2 in. apart onto ungreased baking sheets. Bake 10-12 minutes or until lightly browned. Cool 2 minutes before removing from pans to wire racks. Yield: 4-1/2 dozen.

_Taste of Home_
