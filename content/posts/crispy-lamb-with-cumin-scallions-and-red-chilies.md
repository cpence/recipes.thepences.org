---
title: "Crispy Lamb With Cumin, Scallions and Red Chilies"
author: "pencechp"
date: "2016-02-02"
categories:
  - main course
tags:
  - chinese
  - lamb
---

## Ingredients

*   1 tablespoon egg white
*   1 tablespoon rice wine or dry sherry
*   2 teaspoons cornstarch
*   1 teaspoon salt, more to taste
*   1/2 teaspoon black pepper
*   1 pound boneless leg of lamb or lamb shoulder, cut into strips about 1/2 inch by 2 inches
*   3 tablespoons vegetable oil
*   2 tablespoons cumin seeds, lightly cracked in a mortar or grinder
*   2 tablespoons whole dried red chili peppers, about 2 inches long
*   4 scallions, white and green parts only, cut on diagonal into 1-inch lengths
*   Sesame oil, for seasoning

## Preparation

1\. In a bowl combine egg white, wine, cornstarch, salt and pepper. Add lamb and set aside to marinate 1 hour.

2\. Heat a large wok or skillet over high heat until a drop of water sizzles on contact. Swirl half the oil into wok and carefully add lamb, spreading it in a single layer. Let sear a moment, then stir-fry briskly just until lamb is no longer pink. Transfer to a plate. (If your wok is not large enough to hold all the lamb, do this in 2 batches, using extra oil.)

3\. Swirl remaining 1 1/2 tablespoons oil into empty wok, add cumin seeds and chilies and stir-fry a few seconds until cumin seeds start to pop. Press chilies against sides of wok to char their skins.

4\. Add scallions and stir-fry 1 minute. Then return lamb to wok and stir-fry 1 to 2 minutes more until lamb is cooked through. Turn off heat, sprinkle with salt and drops of sesame oil, and serve immediately.

_New York Times_
