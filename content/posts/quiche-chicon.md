---
title: "Quiche aux chicons, noix de pécan et emmental"
date: 2021-08-09T10:29:00+02:00
categories:
  - main course
tags:
  - vegetarian
  - endive
  - belgian
  - untested
---

## Ingredients

- 3 œufs
- 650 g de chicons
- 1/2 citron
- 80 g de noix de pécan
- 1 pâte brisée
- 100 g d’emmental râpé
- 25 cl de crème fraîche light à 20 % de mg
- 1 càs huile d’olive
- 4 brins de persil
- 1 c à c de curry Madras (facultatif)
- Poivre & Sel

## Preparation

Emincez les chicons, mettez-les dans une grande poêle contenant l’huile d’olive
et arrosez-les d’un trait de jus de citron. Couvrez et laissez suer 10 min à feu
doux. Poivrez, salez, retirez le couvercle et faites revenir 5 min à bon feu,
jusqu’à évaporation de toute l’eau rendue.

Pendant ce temps, préchauffez le four sur th. 6 -180 °C. Faites griller
légèrement les noix de pécan à sec, dans une poêle anti-adhésive. Hachez-les
grossièrement.

Garnissez un grand moule à tarte avec la pâte brisée. Battez les œufs avec la
crème dans un saladier. Ajoutez l’emmental râpé, les ¾ des noix de pécan et les
chicons ; mélangez et rectifiez l’assaisonnement (ajoutez éventuellement le
curry, selon vos goûts). Versez le tout dans le fond de tarte et faites cuire 20
min au four. Démoulez et déposez la quiche avec son papier de cuisson
directement sur la grille du four. Poursuivez la cuisson 10 min supplémentaires.

Saupoudrez avec le persil haché et le reste des noix de pécan. Servez chaud,
accompagné d’une salade.

_Delhaize_
