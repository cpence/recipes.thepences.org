---
title: "Mediterranean Braised Green Beans"
author: "pencechp"
date: "2016-04-28"
categories:
  - main course
  - side dish
tags:
  - greenbean
  - mediterranean
  - vegan
  - vegetarian
---

## Ingredients

*   5 tbsp. olive oil
*   1 onion, chopped fine
*   4 garlic cloves, minced
*   Pinch cayenne pepper
*   1½ c. water
*   ½ tsp. baking soda
*   1½ lb. green beans, trimmed and cut into 2- to 3-inch lengths
*   1 tbsp. tomato paste
*   14.5 oz can diced tomatoes, drained with juice reserved, chopped coarse
*   1 tsp. salt
*   ¼ tsp. pepper
*   ¼ c. chopped parsley
*   red wine vinegar

## Preparation

1. Adjust oven rack to lower-middle position and heat oven to 275 degrees. Heat
   3 tablespoons oil in Dutch oven over medium heat until shimmering. Add onion
   and cook, stirring occasionally, until softened, 3 to 5 minutes. Add garlic
   and cayenne and cook until fragrant, about 30 seconds. Add water, baking
   soda, and green beans and bring to simmer. Reduce heat to medium-low and
   cook, stirring occasionally, for 10 minutes. Stir in tomato paste, tomatoes
   and their juice, salt, and pepper.

2. Cover pot, transfer to oven, and cook until sauce is slightly thickened and
   green beans can be easily cut with side of fork, 40 to 50 minutes (or
   possibly significantly longer). Stir in parsley and season with vinegar to
   taste. Drizzle with remaining 2 tablespoons oil and serve warm or at room
   temperature.

_CI_
