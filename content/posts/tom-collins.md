---
title: "Tom Collins"
author: "pencechp"
date: "2009-12-05"
categories:
  - cocktails
tags:
  - gin
---

## IBA Standard Ingredients

*   2 meas. gin
*   1 meas. lemon juice \[CP: better w/ lime juice\]
*   drizzle simple syrup
*   soda to taste
