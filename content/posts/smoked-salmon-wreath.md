---
title: "Smoked Salmon Wreath"
date: 2020-01-27T18:26:33+01:00
categories:
    - main course
    - side dish
    - breakfast
tags:
    - salmon
---

## Ingredients

*   1 lg. fennel bulb, trimmed, cored
*   3 limes
*   2 green onions
*   3 avocados
*   250g baby cucumbers, cut into ribbons with a peeler
*   200g (1 bunch) radishes, trimmed, washed, thinly sliced
*   400g smoked salmon
*   olive oil
*   crème fraîche, for serving
*   salmon roe, for serving
*   fresh dill, for serving
*   crackers or bread, for serving

## Preparation

Use a mandoline to thinly slice the fennel lengthwise. Place in a large bowl with the juice of 1 lime. Add a large pinch of salt, toss to combine, and set aside to pickle.

Cut green onions into 8cm lengths, and thinly slice lengthwise. Place in a bowl of cold water for 2–3 minutes or until lightly curled. Drain well.

Mash the avocados in a bowl, and add the juice of the last two limes. Combine and season with salt and pepper.

Drain the fennel. Arrange the avocado in a heaped circle around the edge of a large plate. Arrange fennel, cucumber, radishes, green onions, and salmon over the top. Drizzle with olive oil, and season with pepper. Dollop on crème fraîche and salmon roe. Scatter with dill. Serve immediately with bread or crackers.

*Taste.com.au*

