---
title: "Apple Pandowdy"
author: "juliaphilip"
date: "2009-12-06"
categories:
  - dessert
tags:
  - apple
  - untested
---

## Ingredients

*   4 apples, Granny Smith, peeled, cored and sliced
*   1 lemon, juiced
*   2 teaspoons cornstarch
*   Pinch salt
*   1/2 cup plus 1 tablespoon sugar or maple syrup
*   1/8 teaspoon ground nutmeg
*   1/2 teaspoon ground cinnamon
*   1/4 cup orange liqueur (recommended: Grand Marnier)
*   Pie crust
*   Whipped cream, for serving
*   Caramel ice cream, for serving

## Preparation

Preheat the oven to 400 degrees F.

In a large mixing bowl combine the apples with the lemon juice, cornstarch, and salt and toss to combine. Add 1/2 cup sugar, nutmeg, and cinnamon and stir well. Toss with the orange liqueur. Transfer to a heated, large skillet.

On a lightly floured surface, roll the pie crust to a thickness of 1/8-inch and transfer to the top of the fruit mixture. Trim the edges flush with the edges of the skillet. Using the tip of a sharp knife, cut several steam vents in the top of the pie crust. Bake the pandowdy uncovered for 30 minutes.

Remove the pandowdy from the oven and reduce the oven temperature to 350 degrees F. Use the edge of a metal spatula to cut the crust into 1-inch squares, then press the crust down into the filling. Return the pandowdy to the oven and bake until golden brown, about 30 minutes longer. Let cool 15 to 20 minutes before serving.

Serve the pandowdy warm, with a dollop of whipped cream and a scoop of caramel ice cream.

_Emeril Lagasse_
