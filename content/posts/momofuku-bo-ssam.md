---
title: "Momofuku Bo Ssam"
author: "pencechp"
date: "2012-02-23"
categories:
  - main course
tags:
  - korean
  - pork
---

## Ingredients

_Pork Butt_

*   1 whole bone-in pork butt or picnic ham (8 to 10 pounds)
*   1 cup white sugar
*   1 cup plus 1 tablespoon kosher salt
*   7 tablespoons brown sugar

_Ginger-Scallion Sauce_

*   2½ cups thinly sliced scallions, both green and white parts
*   ½ cup peeled, minced fresh ginger
*   ¼ cup neutral oil (like grapeseed) 
*   ½ teaspoons light soy sauce
*   1 scant teaspoon sherry vinegar
*   ½ teaspoon kosher salt, or to taste

_Ssam Sauce_

*   2 tablespoons fermented bean-and-chili paste (ssamjang, available in many Asian markets, and online)
*   1 tablespoon chili paste (kochujang, available in many Asian markets, and online)
*   ½ cup sherry vinegar
*   ½ cup neutral oil (like grapeseed)

_Accompaniments_

*   2 cups plain white rice, cooked
*   3 heads bibb lettuce, leaves separated, washed and dried
*   1 dozen or more fresh oysters (optional)
*   Kimchi (available in many Asian markets, and online)

## Preparation

1\. Place the pork in a large, shallow bowl. Mix the white sugar and 1 cup of the salt together in another bowl, then rub the mixture all over the meat. Cover it with plastic wrap and place in the refrigerator for at least 6 hours, or overnight.

2\. When you’re ready to cook, heat oven to 300. Remove pork from refrigerator and discard any juices. Place the pork in a roasting pan and set in the oven and cook for approximately 6 hours, or until it collapses, yielding easily to the tines of a fork. (After the first hour, baste hourly with pan juices.) At this point, you may remove the meat from the oven and allow it to rest for up to an hour.

3\. Meanwhile, make the ginger-scallion sauce. In a large bowl, combine the scallions with the rest of the ingredients. Mix well and taste, adding salt if needed.

4\. Make the ssam sauce. In a medium bowl, combine the chili pastes with the vinegar and oil, and mix well.

5\. Prepare rice, wash lettuce and, if using, shuck the oysters. Put kimchi and sauces into serving bowls.

6\. When your accompaniments are prepared and you are ready to serve the food, turn oven to 500. In a small bowl, stir together the remaining tablespoon of salt with the brown sugar. Rub this mixture all over the cooked pork. Place in oven for approximately 10 to 15 minutes, or until a dark caramel crust has developed on the meat. Serve hot, with the accompaniments.

_Momofuku, via the New York Times_
