---
title: "Farro Risotto"
author: "pencechp"
date: "2010-05-08"
categories:
  - side dish
tags:
  - farro
  - vegetarian
---

## Ingredients

*   2 tbsp olive oil
*   1 small white onion, finely chopped
*   1-1/2 cups farro
*   1/4 cup white wine
*   3 cups water
*   1/2 cup heavy cream
*   1/4 cup parmesan
*   2 tsp butter
*   salt & pepper

## Preparation

1\. Saute onion til soft. Add farro, saute 1 minute to coat with oil. Add wine and cook until it is absorbed.

2\. Stirring often, add water 1 cup at a time as each addition is completely absorbed, about 30 minutes. (Farro should be softly al dente.)

3\. Stir in cream, cheese and butter and cook until thick, about 5 minutes. Season to taste with salt and pepper.
