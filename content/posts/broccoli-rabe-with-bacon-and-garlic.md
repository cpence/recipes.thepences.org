---
title: "Broccoli Rabe with Bacon and Garlic"
author: "juliaphilip"
date: "2010-04-05"
categories:
  - side dish
tags:
  - broccoli
  - untested
---

## Ingredients

*   1 lb broccoli rabe, trimmed
*   4 strips bacon
*   3 cloves garlic, thinly sliced lengthwise
*   1/4 tsp crushed red pepper flakes

## Preparation

Cook the broccoli rabe in boiling water 3 to 5 min until bright green and crisp-tender. Drain.

Cook bacon in a large skillet; remove from skillet, drain on a plate lined with paper towel, and crumble when cooled.

Add garlic to skillet and cook until lightly browned, about 1 min. Add the drained broccoli rabe and red pepper flakes and cook until warmed through. Stir in the crumbled bacon, taste and add salt and pepper as desired.

_Marge Perry_
