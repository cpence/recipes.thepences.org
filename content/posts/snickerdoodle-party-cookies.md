---
title: "Snickerdoodle Party Cookies"
author: "juliaphilip"
date: "2019-01-26"
categories:
  - dessert
tags:
  - cookie
---

## Ingredients

*   1 cup (2 sticks) unsalted butter
*   2 cups all-purpose flour
*   1 tsp. baking soda
*   1 tsp. kosher salt
*   2½ tsp. ground cardamom, divided
*   2 tsp. ground cinnamon, divided
*   1 cup (packed) light brown sugar
*   ½ cup granulated sugar, divided
*   2 large eggs
*   1 tsp. vanilla extract or paste
*   2 1.4-oz. chocolate toffee bars (preferably Skor), finely chopped
*   3 cups cornflakes, lightly crushed
*   Gold or rainbow disco or luster dust (for serving; optional)

## Preparation

Place racks in upper and lower thirds of oven; preheat to 350°. Cook butter in a medium saucepan over medium heat, stirring often, until it foams, then browns, 5–8 minutes. Scrape into the bowl of a stand mixer. Let brown butter cool slightly. Meanwhile, whisk flour, baking soda, salt, 1½ tsp. cardamom, and 1 tsp. cinnamon in a medium bowl. Add brown sugar and ¼ cup granulated sugar to brown butter. Fit bowl onto stand mixer fitted with paddle attachment and beat on medium speed until thick, about 1 minute. Add eggs one at a time, beating well after each addition. Continue to mix just until smooth, about 1 minute longer. Beat in vanilla. Reduce speed to low and add dry ingredients; beat until nearly combined. Add Skor bars and beat just long enough to bring dough together (it will be a little loose). Cover and let rest 5–10 minutes to let flour hydrate (dough will thicken). Place cornflakes in a small bowl. Mix remaining ¼ cup granulated sugar, 1 tsp. cardamom, and 1 tsp. cinnamon in another small bowl. Working one at a time, scoop out 2-tablespoonfuls of dough (they don’t need to be perfect balls) and toss in cornflakes to coat well; squeeze lightly to adhere. Move to bowl with spiced sugar and toss to coat. Divide between 2 parchment-lined baking sheets, spacing 3" apart. Bake cookies, rotating baking sheets top to bottom and front to back halfway through, until edges are set, 13–16 minutes. Let cool on baking sheets. Brush with gold dust if desired.

_Bon Appétit, December 2018_
