---
title: "Elderfashion"
author: "pencechp"
date: "2011-11-12"
categories:
  - cocktails
tags:
  - gin
  - stgermain
---

## Ingredients

*   3 parts gin
*   1/2 part St. Germain
*   dash bitters

## Preparation

Stir in a shaker, pour into an ice-filled cocktail glass. Garnish w/ a twist (pref. grapefruit).

_St. Germain_
