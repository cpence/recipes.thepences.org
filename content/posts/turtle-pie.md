---
title: "Turtle Pie"
author: "pencechp"
date: "2017-12-31"
categories:
  - dessert
tags:
  - caramel
  - chocolate
  - pecan
  - pie
---

## Ingredients

_for the crust:_

*   2 cups ground pecans
*   1/4 cup sugar
*   1/4 cup melted butter

_for the filling:_

*   14 oz. Kraft caramels
*   1/4 cup milk
*   1 cup chopped pecans
*   1 pkg. (8 squares) Baker's Bittersweet chocolate
*   1/3 cup milk \[it may need a lot more\]
*   1/4 cup powdered sugar
*   1/2 tsp. vanilla

## Preparation

Combine the crust ingredients and bake at 350 degrees for 12-15 minutes, or until lightly browned.

Melt caramels in milk.  Pour over crust, sprinkle with pecans.  Heat chocolate, 1/3 cup milk, powdered sugar and vanilla, stirring, just until melted.  Pour over pecans and refrigerate.  Serves 10-12.
