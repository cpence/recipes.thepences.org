---
title: "Sauteed Okra with Cashews"
author: "pencechp"
date: "2011-11-12"
categories:
  - side dish
tags:
  - okra
---

## Ingredients

*   1/4 cup extra-virgin olive oil
*   2 pounds small okra
*   Salt and freshly ground pepper
*   6 garlic cloves, thinly sliced
*   1/2 cup salted roasted cashews, chopped
*   1 teaspoon finely grated lime zest, plus lime wedges, for serving

## Preparation

In a large skillet, heat the oil until shimmering. Add the okra, season with salt and pepper and cook over high heat, stirring, until tender, about 5 minutes.

Add the garlic to the skillet and cook over moderate heat until softened and fragrant, 3 minutes. Stir in the cashews and lime zest. Transfer the okra to a platter, garnish with the lime wedges and serve.

_Daniel Boulud, via Food & Wine_
