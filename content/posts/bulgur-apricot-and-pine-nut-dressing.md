---
title: "Bulgur, Apricot, and Pine Nut Dressing"
author: "pencechp"
date: "2010-08-18"
categories:
  - side dish
tags:
  - bulgur
  - stuffing
---

## Ingredients

*   1/4 cup extra-virgin olive oil
*   2/3 cup pine nuts (3 1/2 oz)
*   1 large onion, finely chopped (1 1/2 cups)
*   1 cup coarse bulgur (5 1/2 oz)
*   1 3/4 cups water
*   1/2 cup dried apricots (preferably California; 3 oz), finely chopped
*   1/2 teaspoon salt
*   1/4 teaspoon black pepper
*   4 oz feta, coarsely crumbled (3/4 cup)
*   1/2 cup chopped fresh flat-leaf parsley

## Preparation

Heat oil in a 3- to 4-quart heavy saucepan over moderate heat until hot but not
smoking, then cook pine nuts, stirring constantly, until golden, 2 to 3
minutes. Transfer with a slotted spoon to paper towels to drain.

Add onion to pan, then reduce heat to moderately low and cook, stirring
occasionally, until softened but not browned, about 6 minutes. Add bulgur and
cook, stirring, 1 minute. Stir in water, apricots, salt, and pepper and bring to
a boil. Remove pan from heat and let stand, covered, until bulgur is tender,
about 30 minutes. Stir in nuts, feta, parsley, and salt and pepper to taste.

[CP: Note that while this calls itself a dressing, it is also a great bulgur
side dish.]

_Gourmet, November 2005_
