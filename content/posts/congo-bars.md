---
title: "Congo Bars"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - dessert
tags:
  - cookie
  - philip
---

## Ingredients

*   1 lb box brown sugar
*   3 eggs
*   2/3 cup shortening
*   1 cup chopped nuts (optional)
*   1 12 oz bag chocolate chips (or other kinds of chips)
*   2 ¾ cups flour (11 oz.)
*   2 ½ tsp baking soda
*   ½ tsp salt

## Preparation

Melt shortening, add sugar, and cool.  Beat 1 egg at a time into sugar mixture.  Add dry ingredients, blend well.  Then add chips and nuts.

Use a well greased cookie sheet (with sides).  Spread dough evenly with greased hands into pan.

Bake at 350 for 15 minutes.

_Ott family recipe_
