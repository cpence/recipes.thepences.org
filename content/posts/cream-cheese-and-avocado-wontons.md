---
title: "Cream Cheese and Avocado Wontons"
author: "pencechp"
date: "2018-10-01"
categories:
  - appetizer
  - side dish
tags:
  - avocado
  - chinese
  - untested
---

## Ingredients

_Cilantro Lime Dipping Sauce_

*   1/2 cup soy sauce
*   2 teaspoons fresh ginger, minced
*   2 cloves garlic, minced
*   2 tablespoons rice vinegar
*   2 tablespoons honey
*   1/3 cup chopped fresh cilantro
*   2 limes, juiced

_Wontons_

*   2 cups vegetable oil (for frying)
*   1 (8 oz) package cream cheese
*   2 ripe avocados, peeled, seed removed, and diced
*   1 package won ton wrappers
*   1 egg + 1 tablespoon water, lightly beaten

## Preparation

1.  In a medium bowl, whisk together cilantro lime dipping sauce ingredients. Set aside.

2.  Heat vegetable oil in a medium saucepan over medium heat (should reach 350 degrees).

3.  While oil heats up, whip the cream cheese with a hand mixer until fluffy and smooth. Add in the diced avocado and gently fold in with a spoon.

4.  Lightly brush both sides of the wonton wrapper with the egg wash (1 egg + 1 tablespoon water). Put about a teaspoon of the avocado cream cheese filling in the center of each wonton. Fold the wonton wrappers in half, forming a triangle, and seal the edges. Fold the very ends of each point of the triangle over.

5.  Fry wontons in the hot oil 4 at a time until golden brown. This will only take about a minute.or two. Using a slotted spoon, remove the golden brown wontons from the oil and put on paper towels to drain.

6.  Divide the dipping sauce into individual bowls and serve with hot wontons.

_The Stay at Home Chef_
