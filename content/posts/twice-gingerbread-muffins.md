---
title: "Twice Gingerbread Muffins"
author: "juliaphilip"
date: "2010-11-18"
categories:
  - dessert
tags:
  - cupcake
  - untested
---

## Ingredients

*   2 1/4 cups flour
*   2 teaspoons ground ginger
*   1 teaspoon each: baking soda, cinnamon
*   1/2 teaspoon salt
*   1/4 teaspoon each, ground: cloves, nutmeg
*   1 1/2 sticks (3/4 cup) unsalted butter, at room temperature
*   3/4 cup packed dark brown sugar
*   2 large eggs
*   3/4 cup unsulfured molasses
*   1 cup boiling water
*   1/3 cup coarsely chopped crystallized ginger

## Preparation

Sift together flour, ground ginger, baking soda, cinnamon, salt, cloves and nutmeg into a bowl. Mix the butter and brown sugar together in another bowl with an electric mixer on medium-high speed until well blended and smooth, about 3 minutes. Add eggs, one at a time, blending after each. Reduce speed to low; beat in dry ingredients. Add the molasses; beat several seconds until blended. Add the water; beat a few seconds until blended.

Fill two standard-size muffin tins, coated with nonstick spray, about 3/4 full (batter should fill 18 or 19 molds). Bake until tops are set but mixture beneath is jiggly, 10 minutes. Remove pans from oven; sprinkle muffins with crystallized ginger. Return to oven; bake until a tester inserted into the center comes out clean, 9-10 minutes. Cool muffins in tins, 15 minutes. Serve warm or at room temperature.
