---
title: "Tortilla Soup"
author: "pencechp"
date: "2012-07-31"
categories:
  - main course
tags:
  - mexican
  - soup
---

## Ingredients

*   2 1/2 pounds bone-in chicken thighs or legs
*   1 pound beef bones, or a cut of beef with a lot of bone in it (like short ribs), optional
*   1 medium onion, quartered (leave the skin on)
*   1 head garlic, halved across the equator (leave the skin on)
*   1/4 cup vegetable oil, or more as needed
*   6 corn tortillas
*   Salt
*   2 tablespoons canned chipotle chilies in adobo, or to taste
*   1/2 cup chopped fresh cilantro
*   2 avocados, pitted, peeled and cubed
*   4 to 8 ounces plain melting cheese, like mozzarella (not fresh), Oaxaca or Jack, shredded or cubed
*   Lime wedges for serving, optional

## Preparation

1\. Put the chicken, the beef bones if you’re using them, 3 of the onion quarters and the garlic in a large pot. Add water just to cover (about 10 cups) and bring to a boil. Reduce the heat so the liquid bubbles gently. Cook, skimming the foam off the surface every now and then, until the chicken is very tender, 45 minutes to 1 hour.

2\. Meanwhile, put the vegetable oil in a large skillet over medium heat. When the oil is hot but not smoking, fry 2 of the tortillas (one at a time if necessary), turning once, until crisp and golden, 2 to 3 minutes per tortilla. Drain on paper towels. Cut the 4 remaining tortillas into strips, add them to the skillet and fry, stirring to separate them, until crisp and golden, another 2 to 3 minutes. Drain on paper towels and sprinkle with salt while they’re still warm.

3\. When the chicken is tender, transfer it to a plate or cutting board with tongs or a slotted spoon (or put it in the fridge or freezer so it cools faster). When it’s cool enough to handle, shred the meat with your fingers, discarding the bones and the skin. (If you used beef, discard it or save it for another use.)

4\. While the chicken is cooling, strain the stock and discard the solids. Peel the remaining quarter of an onion and put it in a blender with the chipotle, 1/4 cup of the cilantro and a sprinkle of salt. Crumble in the two whole fried tortillas and add enough stock to fill the blender a little more than halfway. Purée until the mixture is as smooth as possible.

5\. Pour the purée and remaining stock back into the pot and bring to a boil. Reduce the heat so the mixture bubbles gently and cook for 5 to 10 minutes. Stir in the shredded chicken, taste and add more salt if necessary. Divide the avocados, the cheese and the remaining 1/4 cup cilantro among 4 to 6 bowls. Ladle the soup into the bowls and garnish with the fried tortilla strips. Serve immediately with lime wedges if you like.

_Rose Garden Restaurant, via Mark Bittman, NYT_
