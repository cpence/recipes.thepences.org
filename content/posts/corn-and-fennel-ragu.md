---
title: "Corn and Fennel Ragu"
author: "pencechp"
date: "2010-05-08"
categories:
  - side dish
tags:
  - corn
  - fennel
  - untested
  - vegan
  - vegetarian
---

## Ingredients

*   2 tablespoons olive oil
*   1/4 cup finely chopped shallots
*   2 cups finely chopped fennel, fronds chopped and reserved
*   1/4 teaspoon ground cumin
*   1 teaspoon yellow mustard seeds
*   Salt and freshly ground black pepper
*   3 cups corn kernels (from about 6 ears)

## Preparation

1\. Pour the olive oil into a large skillet and place over medium-high heat. When hot, add the shallots and fennel. Season with cumin, mustard seeds and a pinch of salt and pepper and cook until softened, about 4 minutes. Add the corn and sauté until tender, about 4 minutes more. Add 1 1/4 cups water and bring to a boil. Simmer for 8 minutes. Remove from the heat.

2\. Transfer 1 1/2 cups of the corn mixture and liquid to a blender and purée. Strain the pure,é pressing the solids; discard the solids. Pour the corn purée back into the skillet and simmer to a ragu consistency, about 8 minutes. Season with salt and pepper. Garnish with fennel fronds and serve with the gently smoked salmon.

_New York Times Magazine_
