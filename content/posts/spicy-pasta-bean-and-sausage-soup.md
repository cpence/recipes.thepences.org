---
title: "Spicy Pasta, Bean, and Sausage Soup"
author: "juliaphilip"
date: "2010-04-03"
categories:
  - main course
tags:
  - bean
  - sausage
  - stew
---

## Ingredients

*   2 15- to 16-ounce cans garbanzo beans (chickpeas)
*   2 tablespoons olive oil
*   1 pound Italian hot sausages, casings removed
*   4 teaspoons chopped fresh rosemary
*   2 large garlic cloves, chopped
*   1/4 teaspoon dried crushed red pepper
*   1/4 cup tomato paste
*   5 cups canned low-salt chicken broth
*   8 ounces (about 2 1/3 cups) orecchiette (little ear-shaped pasta) or other small pasta
*   1 1/2 cups grated Romano cheese

## Preparation

Strain liquid from canned beans into blender. Add 1 cup beans and puree until smooth. Heat oil in large pot over medium heat. Add sausages, rosemary, garlic, and crushed red pepper. Sauté until sausages are cooked through, breaking up with fork, about 8 minutes. Mix in tomato paste. Add bean puree, remaining beans, broth, and pasta. Simmer until pasta is tender and mixture is thick, stirring occasionally, about 30 minutes. Mix in 1/4 cup cheese. Season with salt and pepper. Serve, passing remaining 1 1/4 cups cheese.

_Bon Appetit, February 2001_
