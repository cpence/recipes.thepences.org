---
title: "Apricot Pie"
author: "pencechp"
date: "2010-07-28"
categories:
  - dessert
tags:
  - apricot
  - pie
---

## Ingredients

*   Pastry dough / Pie crust
*   2 lbs fresh apricots [or any other fruit-pie fruit]
*   1 stick unsalted butter
*   3/4 cup sugar
*   3/4 cup all-purpose flour
*   1/2 teaspoon grated nutmeg

## Preparation

Preheat oven to 400ºF with rack in lower third.

Roll out dough on a lightly floured surface with a lightly floured rolling pin
into a 13-inch round. Fit into a 9-inch pie plate. Trim excess dough, leaving a
1/2-inch overhang. Fold overhang under and press against rim of pie plate, then
crimp decoratively. Chill pie shell in freezer 15 minutes.

Meanwhile, pull apricots (with skins) apart into halves, discarding pits. Melt
butter in a small heavy saucepan over medium heat, then stir in sugar, flour,
and nutmeg and remove from heat. Cool mixture until firm enough to crumble, 10
to 15 minutes.

Put apricots in pie shell and crumble butter mixture over them. Bake pie, with a
foil-lined baking pan on rack below it (to catch drips), 10 minutes.

Reduce oven temperature to 350ºF and continue to bake until top is golden, 50
minutes to 1 hour more.

Cool pie to warm or room temperature on a rack.

_Gourmet.com_
