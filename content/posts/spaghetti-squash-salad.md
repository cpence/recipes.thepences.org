---
title: "Spaghetti Squash Salad"
author: "pencechp"
date: "2017-11-15"
categories:
  - side dish
tags:
  - salad
  - squash
  - vegetarian
---

## Ingredients

*   1 (2 1/2-pound) spaghetti squash, halved lengthwise and seeded
*   6 tablespoons extra-virgin olive oil, plus extra for drizzling 
*  Salt and pepper
*  2 teaspoons grated lemon zest plus 7 teaspoons juice
*  1 (15-ounce) can chickpeas, rinsed
*  2 ounces feta cheese, crumbled (1/2 cup)
*  ½ cup coarsely chopped fresh parsley
*  4 scallions, sliced thin on bias
*  2 tablespoons chopped toasted pistachios

## Preparation

1\. Adjust oven rack to middle position and heat oven to 375 degrees. Brush cut sides of squash with 2 tablespoons oil and season with salt and pepper. Place squash, cut side down, on rimmed baking sheet. Roast squash until just tender and tip of paring knife can easily be slipped into flesh, 40 to 45 minutes. Transfer squash to wire rack, cut side up, and let cool completely, about 1 hour.

2\. Combine lemon zest and juice, remaining 1/4 cup oil, 1/2 teaspoon salt, and 1/2 teaspoon pepper in large bowl. Holding squash over bowl, use fork to scrape flesh from skin into strands; discard skin.

3\. Add chickpeas to bowl with squash and toss gently to coat with dressing. Transfer to serving platter and sprinkle with feta, parsley, scallions, and pistachios. Drizzle with extra oil before serving.
