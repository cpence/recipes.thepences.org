---
title: "Lemon Chicken Tagine"
author: "pencechp"
date: "2014-03-10"
categories:
  - main course
tags:
  - chicken
  - lemon
  - moroccan
  - untested
---

## Ingredients

*   1 tbsp. olive oil
*   1 medium onion, thinly sliced
*   2 garlic cloves, crushed
*   4 chicken quarters
*   1 teaspoon ground turmeric
*   2 tbsp. cilantro
*   3/4 c. chicken stock
*   1 preserved lemon
*   salt pepper

## Preparation

Heat the oil in a tagine on medium. Fry the onion and garlic for 3-4 minutes without coloring. Cut the chicken quarters into two pieces each. Add to the tagine and brown, turning frequently. Lower heat if necessary. Add turmeric, 1 tbsp. cilantro, stock, salt and pepper. Bring to a simmer. Rinse the preserved lemon, discard the pulp, and cut the peel into thin strips. Stir into the chicken. Cover the tagine and simmer for 2-2 1/2 hours. Remove the lid and, if needed, boil rapidly for a few minutes to thicken. Garnish with remaining cilantro.

_Le Creuset tagine booklet_
