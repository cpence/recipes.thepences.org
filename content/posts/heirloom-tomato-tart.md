---
title: "Heirloom Tomato Tart"
date: 2021-08-09T18:05:53+02:00
categories:
  - main course
  - side dish
tags:
  - tomato
  - cheese
  - pie
---

## Ingredients

- 2 sleeves Ritz crackers (about 7.5 ounces or 1 3/4 cup crushed crackers)
- 1/2 cup grated Parmesan cheese
- 6 tablespoons butter, melted
- 1 large egg white
- 6 ounces cream cheese, at room temperature
- 1/2 cup good quality mayonnaise
- 1/2 teaspoon garlic powder
- 1 teaspoon apple cider vinegar
- 3 cups freshly shredded sharp cheddar cheese
- 1 (4-ounce) jar diced pimentos, drained and diced
- 3 or 4 large heirloom tomatoes, sliced
- Fresh basil leaves and minced chives, to taste
- Kosher salt
- Flaky sea salt

## Preparation

1. Preheat oven to 350 degrees Fahrenheit.

2. Add crackers to bowl of food processor. Process until crackers are a fine
   crumb. Add Parmesan cheese, melted butter, egg white and 1/2 teaspoon kosher
   salt. Process until evenly combined and mixture holds together when pressed
   between fingers. If it doesn’t hold together, add an additional tablespoon of
   melted butter.

3. Tightly press mixture into bottom and sides of 9-inch tart pan with removable
   bottom. Tip: If the crust is not packed tightly, it will crumble when sliced.
   Use the back of a measuring cup to help really pack it down, as seen here.

4. Place tart pan on a baking sheet and bake until golden brown, about 12 to 15
   minutes. Set aside and cool completely. Then place in fridge for one hour to
   help crust set.

5. Meanwhile, add tomato slices to paper towel-lined plate. Season with kosher
   salt and let sit for a few minutes to remove excess moisture.

6. Mix together softened cream cheese, mayonnaise, garlic powder, apple cider
   vinegar and 1/2 teaspoon kosher salt. Whisk until evenly combined.

7. Stir in cheddar cheese and pimentos. Check for seasoning and add more salt if
   needed.

8. Spread pimento cheese mixture evenly on bottom of chilled tart crust.

9. Top with sliced tomatoes. Garnish with chives, basil leaves and flaky sea
   salt. Serve immediately.

_HuffPo_
