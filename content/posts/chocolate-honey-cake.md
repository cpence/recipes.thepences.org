---
title: "Chocolate Honey Cake"
author: "pencechp"
date: "2010-05-09"
categories:
  - dessert
tags:
  - cake
  - chocolate
  - untested
---

## Ingredients

*   1 c. vegetable oil plus oil for the pan
*   1 c. sugar
*   2/3 c. brown sugar
*   1 c. light honey, such as clover
*   1 tsp. vanilla extract
*   4 eggs
*   1/2 c. unsweetened cocoa powder
*   2 3/4 c. all-purpose flour
*   1/2 tsp. baking soda
*   1 tbsp. baking powder
*   1/2 tsp. salt
*   1/4 tsp. ground cinnamon
*   1 c. flat cola or brewed coffee, room temperature
*   1/3 c. slivered or sliced almonds
*   1/2 c. coarsely chopped semisweet chocolate or chips
*   confectioners' sugar or cocoa powder, for dusting

## Preparation

Preheat the oven to 350 degrees.  Oil the bottom and tube of a 9- or 10-inch angel-food-cake or tube pan.  Cut out a circle of parchment or baking paper and line the bottom.

In a medium-size bowl, blend the oil with the sugars, honey and vanilla.  Blend in the eggs and mix well.  In a separate bowl, mix together the cocoa powder, flour, baking soda, baking powder, salt, and cinnamon.  Make a well in the center of the dry ingredients and slowly stir in the wet ingredients, slowly adding the cola (or coffee).  Blend well to make a smooth batter.

Spoon the batter into the pan and sprinkle the top with slivered almonds.

Bake the cake for 15 minutes, then reduce the heat to 325 degrees and bake for an hour, or until the cake springs back when touched.  (Make sure the center of the cake, near the tube, springs back as well as the outer edge, or the center will remain undercooked.)  Remove the cake from the oven.

Sprinkle on the chopped chocolate.  Refrigerate the cake to set the chocolate.  Once the chocolate is set, invert and remove the cake from the pan.  To serve, dust the top with cocoa and/or confectioners' sugar.
