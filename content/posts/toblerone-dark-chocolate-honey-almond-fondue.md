---
title: "Toblerone Dark Chocolate Honey Almond Fondue"
author: "pencechp"
date: "2010-07-21"
categories:
  - dessert
tags:
  - almond
  - chocolate
  - fondue
  - honey
---

## Ingredients

*   6 tablespoons whipping cream
*   3 tablespoons honey
*   2 3.52-ounce bars Toblerone bittersweet chocolate or 7 ounces semisweet chocolate, chopped
*   1 tablespoon kirsch (clear cherry brandy)
*   1/4 teaspoon almond extract
*   Assorted fresh fruit (such as whole strawberries, 1-inch-thick slices peeled banana, peeled pear wedges and orange segments)

## Preparation

Bring cream and honey to simmer in heavy medium saucepan. Add chocolate; whisk until melted. Remove from heat. Whisk in kirsch and extract. Pour fondue into bowl; place on platter. Surround with fruit. Serve with skewers.

_Bon Appetit, December 1997_
