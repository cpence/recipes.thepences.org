---
title: "Ukrainian Borscht"
author: "pencechp"
date: "2019-10-18"
categories:
  - main course
tags:
  - beet
  - pence
  - sausage
  - soup
  - stew
---

## Ingredients

*   6 tbsp. butter
*   3 medium onions, finely chopped (2 c.)
*   2 cloves garlic, finely chopped (1 tbsp.)
*   2 lbs. beets, coarsely grated (4 c. packed)
*   2 med. turnips, coarsely grated
*   1 celery root, coarsely grated
*   1 parsley root or parsnip, coarsely grated
*   4 med. tomatoes, coarsely chopped (4 c.)
*   1 tsp. sugar
*   2 tsp. salt pepper
*   1/2 c. red wine vinegar
*   10 c. (2 1/2 quarts) beef stock
*   2 lb. head of green cabbage, cut into 2" squares and then shredded
*   1 lb. potatoes, in 2" squares
*   1 lb. polish sausage, in 1/2" slices, browned
*   1 med. raw beet, grated, with 3 tbsp. water
*   2 c. sour cream
*   1/2 c. fresh dill

## Preparation

In a 4 quart heavy dutch oven, melt butter and cook onions and garlic until slightly colored (8–10 minutes). Add the next nine ingredients (from beets to vinegar), and 2 c. of the beef stock. Bring to a boil, stirring constantly, then lower the heat and cook partially covered for 45 minutes.

Meanwhile, bring the remaining 8 c. of beef stock to a boil in a large soup pot. Add the cabbage and potatoes. Simmer for 15 minutes, or until the potatoes are almost tender.

When the beet mixture has finished cooking, pour it into the veggies. Add sausage. Strain the water from the soaking beets and add it to the soup to brighten its color.

Serve with dill and sour cream.

_Alternative:_ You can add some dill and bay leaves to the beet mixture, and then add browned stew meat when you add the sausage.

_From an exchange student who lived with my grandparents, 1960s_
