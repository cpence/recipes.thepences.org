---
title: "Mediterranean Pie Dough"
author: "juliaphilip"
date: "2010-04-03"
categories:
  - miscellaneous
tags:
  - mediterranean
---

## Ingredients

*   1 tbl active dry yeast
*   3/4 c warm water
*   2 1/2 c (390 g) all-purpose flour
*   1 1/2 tsp salt
*   1 tbl olive oil

## Preparation

In a small bowl dissolve the yeast in warm water and let stand until bubbly, about 5 min.

In a large bowl stir together the flour and salt. Add the yeast mixture and oil, mix well and knead briefly on a lightly floured work surface until smooth and elastic.

Shape the dough into a ball and place in an oiled bowl. Turn the ball to coat all surfaces with oil. Cover the bowl with a damp kitchen towel and or plastic wrap and let rise in a warm place until almost double in bulk, about 1 1/2 hours. Punch down the dough before use.

Use dough for spinach pies or meat pies.
