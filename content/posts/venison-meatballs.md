---
title: "Venison Meatballs"
author: "pencechp"
date: "2011-10-17"
categories:
  - main course
tags:
  - pasta
  - venison
---

## Ingredients

*   Ground venison and ground pork, in a 3-to-1 ratio \[CP: I used 1 lb. / 1/3 lb.\]
*   3/4 cup bread crumbs
*   1 cup Parmesan cheese, shredded
*   1 medium onion, diced
*   2 teaspoons basil
*   2 teaspoons oregano
*   3 cloves garlic, minced
*   1 teaspoon salt
*   1 tablespoon pepper
*   Olive oil

## Preparation

Sautee the onion until translucent, cool.  Mix all ingredients.  Sautee meatballs in olive oil, browning on 4 or so sides to make sure they're cooked through.  Serve with pasta.  \[CP: I did this with [white wine cream sauce]( {{< relref "white-wine-cream-sauce" >}} ), and it was wonderful – reminiscent of all the good things about Swedish meatballs.\]

_T. Edward Nickens, Field and Stream_
