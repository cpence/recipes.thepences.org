---
title: "Bengali Fish Curry"
date: 2021-07-10T11:26:08+02:00
categories:
  - main course
tags:
  - indian
  - fish
  - curry
---

## Ingredients

- 1.5 kg fish fillets (cod, halibut, etc.), skinless
- 3 tsp. salt
- 1/2 tsp. ground turmeric
- 6 tbsp. vegetable oil
- 1 lg. white onion, finely grated
- 4 cloves garlic, crushed
- 2.5cm piece ginger, crushed
- 1 tbsp. ground coriander
- 1 tsp. ground cumin
- 1 tsp. chili powder
- 3 tbsp. tomato puree
- 200g tomatoes, cut into 1" cubes
- 600ml warm water
- 1/2 tsp. sugar
- green chiles
- a few sprigs cilantro

## Preparation

Cut the fish fillets into 12 equal portions. Mix 1 teaspoon of the salt and 1
teaspoon of the turmeric, then rub on all sides of the fish and set aside for 30
minutes.

In a shallow saucepan, heat 5 tablespoons of the oil over a medium-high heat. If
you are using mustard oil, heat the oil until it is smoking hot – this removes
its bitter pungency – then bring it down to a medium-high heat. Add the fish to
the pan and fry to seal each piece, but do not let the fillets cook through.
Remove from the pan to a plate and set aside.

Add the onion, garlic and ginger to the pan and cook, stirring, for 2 minutes
over a medium-high heat. If the paste is burning or sticking to the base of the
pan, add a splash of water. Add the remaining salt and turmeric, followed by the
coriander, cumin, chilli powder, tomato puree and diced tomatoes. Pour in the
warm water and cook for 5 minutes. Let the liquid reduce for 15 minutes or until
the oil comes to the surface and seeps to the sides of the pan.

Gently return the fish fillets to the pan and cover with the gravy, ensuring all
sides of each fillet are cooking evenly. If possible, cook the fish fillets in a
single layer in the pan as this will prevent them from breaking up into flakes.
Lower the heat, add the sugar and cook, covered, until the fillets are cook
through – this should take no longer than 5 minutes.

To serve, garnish the fish with whole green chillies and sprigs of fresh
coriander leaves.

_Asma Khan / BBC_
