---
title: "Slow-Roasted Strawberries"
date: 2020-10-29T10:26:16+01:00
categories:
    - dessert
    - breakfast
tags:
    - strawberry
    - untested
---

## Ingredients

*   6 cups (900g) fresh, ripe strawberries
*   1/2 cup to 1/2 cup plus 2 tablespoons (100g to 125g) sugar, depending on the
    strawberries’ sweetness

## Preparation

Heat the oven to 250° F (120° C). Rinse and hull the berries. Leave any tiny
ones whole and quarter or halve the rest so they all cook at about the same
rate.

In a nonreactive baking pan that will hold the strawberries closely packed in a
single layer, gently toss the strawberries with the sugar, then spread in an
even layer.

Roast slowly in the oven, uncovered, for 3 to 6 hours, shaking occasionally but
not stirring. If they start to look dry on top, gently flip them over with a
wide spatula. They are done when their juices have reduced to a syrup, but not
darkened into caramel, and the berries are very jammy but not dry. Store in an
airtight container in the refrigerator.

*Food52*

