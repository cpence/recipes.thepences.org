---
title: "Lemon Pudding Cakes"
author: "pencechp"
date: "2016-04-28"
categories:
  - dessert
tags:
  - lemon
  - untested
---

## Ingredients

*   1 c. milk
*   ½ c. heavy cream
*   3 tbsp. lemon zest plus ½ cup juice (3 lemons)
*   1 c. sugar
*   ¼ c. all-purpose flour
*   ½ tsp. baking powder
*   ⅛ tsp. salt
*   2 eggs, separated, plus 2 egg whites
*   ½ tsp. vanilla extract

## Preparation

1\. Adjust oven rack to middle position and heat oven to 325 degrees. Bring milk and cream to simmer in medium saucepan over medium-high heat. Remove pan from heat, whisk in lemon zest, cover pan, and let stand for 15 minutes. Meanwhile, fold dish towel in half and place in bottom of large roasting pan. Place six 6-ounce ramekins on top of towel and set aside pan.

2\. Strain milk mixture through fine-mesh strainer into bowl, pressing on lemon zest to extract liquid; discard lemon zest. Whisk 3/4 cup sugar, flour, baking powder, and salt in second bowl until combined. Add egg yolks, vanilla, lemon juice, and milk mixture and whisk until combined. (Batter will have consistency of milk.)

3\. Using stand mixer fitted with whisk, whip egg whites on medium-low speed until foamy, about 1 minute. Increase speed to medium-high and whip whites to soft, billowy mounds, about 1 minute. Gradually add remaining 1/4 cup sugar and whip until glossy, soft peaks form, 1 to 2 minutes.

4\. Whisk one-quarter of whites into batter to lighten. With rubber spatula, gently fold in remaining whites until no clumps or streaks remain. Ladle batter into ramekins (ramekins should be nearly full). Pour enough cold water into pan to come one-third of way up sides of ramekins. Bake until cake is set and pale golden brown and pudding layer registers 172 to 175 degrees at center, 50 to 55 minutes.

5\. Remove pan from oven and let ramekins stand in water bath for 10 minutes. Transfer ramekins to wire rack and let cool completely. Serve.

_CI_
