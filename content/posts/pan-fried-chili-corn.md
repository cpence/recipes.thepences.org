---
title: "Pan-Fried Chili Corn"
author: "juliaphilip"
date: "2010-04-30"
categories:
  - side dish
tags:
  - bellpepper
  - corn
  - vegan
  - vegetarian
---

## Ingredients

*   1 1/2 c cooked corn kernels (you don't need to cook frozen or canned corn in advance)
*   1/2 c coarsely chopped red or green bell pepper
*   1 to 2 tsp chili powder
*   1 tbsp butter
*   1 tbsp canola oil
*   sea salt

## Preparation

Saute all ingredients over medium-high heat until corn in lightly browned. Sprinkle with sea slat.
