---
title: "Lemon Garlic Chicken Thighs"
author: "juliaphilip"
date: "2010-03-31"
categories:
  - main course
tags:
  - chicken
  - lemon
---

## Ingredients

*   8 chicken thighs with bones and skin, about 3 1/2 pounds
*   1 3/4 teaspoons salt
*   1 teaspoon freshly ground black pepper
*   1/2 cup all-purpose flour
*   1/4 cup olive oil
*   3 cups thinly sliced onions
*   30 cloves (about 2 heads) garlic, peeled and smashed
*   1/2 teaspoon crushed red pepper
*   1 bay leaf
*   1/2 cup freshly squeezed lemon juice
*   1 1/2 cups chicken broth
*   1 pound angel hair pasta
*   3 tablespoons chopped fresh parsley leaves, plus more for garnish
*   3 tablespoons unsalted butter

## Preparation

Preheat the oven to 350 degrees F.

Season the chicken thighs with 1 1/4 teaspoons of the salt and 1 teaspoon of the freshly ground pepper.

Place the flour in a shallow dish. Lightly dredge the chicken in the flour and set aside.

In a large wide saute pan with 2 to 3-inch sides, heat the oil over medium-high heat. When the oil is hot but not smoking, add the thighs and brown well on both sides, about 6 minutes. Remove the chicken from the pan and set aside.

Add the onions to the oil and cook, stirring to scrape up any browned bits from the chicken until wilted, about 3 minutes. Add the garlic, crushed red pepper, remaining 1/2 teaspoon salt, and the bay leaf and cook 1 minute. Add the lemon juice, chicken broth, and browned chicken thighs to the pan. Bring the mixture to a simmer, cover tightly, and place in the oven. Bake for 20 minutes, remove the lid and cook an additional 15 minutes, or until cooked through and tender.

While the chicken is baking, cook the pasta until al dente, 4 to 5 minutes for dry pasta. Drain well and toss in a large bowl with 3 tablespoons of the parsley and the butter.

To serve, place a portion of pasta in the center of 4 large plates. Arrange 2 thighs around each serving of pasta and drizzle lemon garlic cooking liquid over the pasta. Garnish with additional parsley and serve immediately.
