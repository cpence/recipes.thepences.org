---
title: "Tea-Braised Chuck Roast"
author: "pencechp"
date: "2018-01-01"
categories:
  - main course
tags:
  - beef
  - instantpot
  - untested
---

## Ingredients

*   One 3-pound boneless beef chuck roast
*   1 teaspoon salt
*   1/2 teaspoon freshly ground black pepper
*   2 tablespoons unsalted butter
*   2 medium yellow onions, halved and then thinly sliced into half-moons
*   2 tablespoons white balsamic vinegar
*   2 tablespoons finely diced orange zest
*   2 tablespoons finely diced peeled fresh ginger root (from a 3-inch piece)
*   1 teaspoon ground allspice
*   1 1/2 cups strongly brewed lapsang souchong tea

## Preparation

Season the meat on both sides with the salt and pepper.

Melt the butter in the electric pressure cooker turned to its sauteing function.

Add the onions and cook for 4 to 6 minutes, stirring often, until they begin to brown just a bit at the edges.

Push the onions to line the inside perimeter of the pot; set the meat in the cleared space. Brown well on both sides, turning once and stirring the onions a couple of times, 8 to 10 minutes. The onions will brown deeply and may even blacken in parts.

Sprinkle the vinegar over the onions; use a wooden spoon to dislodge any browned bits.

Sprinkle the orange zest, ginger and allspice over the meat and onions. Pour 1 1/2 cups of the tea over everything.

Lock the lid onto the pot and set it for high pressure (9 to 11 psi). Cook at high pressure for 75 minutes. Unplug the machine or turn it off to bring the pressure back to normal naturally, about 20 minutes. Unlock and remove the lid.

Transfer the chuck roast to a cutting board. Skim as much surface fat as you can from the sauce in the pot. Carve the meat into chunks, transfer to serving bowls, and pour the sauce on top.

_Washington Post_
