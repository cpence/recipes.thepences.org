---
title: "Instant Pot Roasted Potatoes"
author: "pencechp"
date: "2018-02-07"
categories:
  - side dish
tags:
  - instantpot
  - potato
  - untested
---

## Ingredients

*   2 pounds baby yukon golds and red potatoes, about 1 to 1 1/2 inches
*   1/2 teaspoon dried rosemary
*   1/2 teaspoon dried thyme
*   1/2 teaspoon dried marjoram
*   1/2 teaspoon dried oregano 
*   1/2 teaspoon garlic powder (or: 1 tbsp. of a spice mix)
*   1 teaspoon kosher salt
*   1/4 teaspoon pepper
*   3 tablespoons olive oil
*   1/2 cup water or chicken stock

## Preparation

Wash the potatoes and pat dry. Set Instant Pot to Sautee mode. Add oil and heat. In batches, add the potatoes and cook on all sides, for about 5-6 minutes, until browned and crisp. Pierce each potato with a fork. Add seasonings, water, and stock. Close and lock the lid, and cook for 7 minutes on Manual. Quick release the pressure.

_Adapted from Onion Rings and Things_
