---
title: "Swiss Chard Pesto with Almonds"
author: "pencechp"
date: "2016-12-29"
categories:
  - miscellaneous
tags:
  - greens
  - pasta
  - sauce
---

## Ingredients

*   Kosher salt
*   3 bunches Swiss chard, stems removed
*   3/4 cup toasted almonds
*   3/4 cup grated aged Pecorino Romano cheese
*   Pinch nutmeg
*   Pinch ground cloves
*   2 lemons, zested and juiced
*   1 clove garlic, grated on a rasp grater
*   About 3/4 cup extra-virgin olive oil
*   Freshly ground black pepper

## Preparation

Bring a large pot of water to a boil; season the water generously with kosher salt. Prepare an ice water bath with a colander. Blanch the Swiss chard in the boiling water until tender, about 2 minutes. Shock immediately in the ice water. Drain by squeezing the leaves in a ball in a clean kitchen cloth.

Put the cooked chard, almonds, cheese, nutmeg, cloves, lemon zest and juice and garlic into the bowl of a food processor and pulse until the mixture begins to break down and come together. Add enough olive oil to the mixture to just get it to come together, about 3/4 cup. Season with salt and pepper.

_Food Network_
