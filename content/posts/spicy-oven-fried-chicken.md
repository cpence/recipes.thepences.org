---
title: "Spicy Oven-Fried Chicken"
author: "pencechp"
date: "2013-05-28"
categories:
  - main course
tags:
  - chicken
  - untested
---

## Ingredients

*   1 1/4 cups buttermilk
*   1/4 cup extra-virgin olive oil
*   3 tablespoons hot pepper sauce \[don't worry, it won't get too hot\]
*   2 tablespoons Dijon mustard
*   2 garlic cloves, minced
*   2 teaspoons salt
*   1/2 teaspoon ground black pepper
*   1 large onion, sliced
*   12 chicken pieces (breasts, thighs and drumsticks) with skin and bones
*   1 cup dry unseasoned breadcrumbs
*   1/3 cup freshly grated Parmesan cheese
*   1/4 cup all purpose flour
*   2 teaspoons dried thyme
*   1/2 teaspoon paprika
*   1/2 teaspoon cayenne pepper
*   3 tablespoons butter, melted

## Preparation

Whisk buttermilk, oil, hot pepper sauce, mustard, garlic, 1 teaspoon salt and 1/2 teaspoon pepper in large bowl to blend well. Add onion, then chicken and turn to coat. Cover; chill at least 3 hours or up to 1 day, turning chicken occasionally.

Place racks on 2 large rimmed baking sheets. Whisk breadcrumbs, cheese, flour, thyme, paprika, cayenne and 1 teaspoon salt in large baking dish to blend. Remove chicken from marinade, allowing excess to drip off. Add chicken to breadcrumb mixture and turn to coat completely. Arrange chicken, skin side up, on racks on baking sheets. Let stand 30 minutes.

Preheat oven to 425°F. Drizzle butter over chicken. Bake until crisp, golden and cooked through, about 50 minutes. Serve warm or at room temperature.

_Serves 6._

_Bon Appetit, June 2000_
