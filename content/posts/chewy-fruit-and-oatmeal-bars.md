---
title: "Chewy Fruit and Oatmeal Bars"
author: "pencechp"
date: "2013-03-13"
categories:
  - breakfast
tags:
---

## Ingredients

*   3/4 cup firmly packed brown sugar
*   1/2 cup granulated sugar
*   1 (8 ounce) container low-fat vanilla yogurt (or plain)
*   2 egg whites, lightly beaten
*   2 tablespoons vegetable oil
*   2 tablespoons nonfat milk
*   2 teaspoons vanilla
*   1 1/2 cups all-purpose flour
*   1 teaspoon baking soda
*   1 teaspoon ground cinnamon
*   1/2 teaspoon salt (optional)
*   3 cups oats (quick or old fashioned)
*   1 cup diced dried mixed fruit (or raisins or dried cranberries)

## Preparation

Heat oven to 350°F. In large bowl combine sugars, yogurt,egg whites, oil, milk and vanilla; mix well. In medium bowl combine flour baking soda, cinnamon and salt; mix well. Add dry ingredients to wet mix; mix well. Stir in oats and dried fruit. Spread dough onto bottom of ungreased 13x9 baking pan. Bake 28-32 minutes or until golden brown. Cool completely on wire rack. Cut into bars. Store tightly covered.

_Easy Home Cooking Magazine, via Food.com_
