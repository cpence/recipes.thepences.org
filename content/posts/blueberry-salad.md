---
title: "Blueberry Salad"
author: "juliaphilip"
date: "2010-04-03"
categories:
  - main course
tags:
  - chicken
  - salad
  - untested
---

## Ingredients

*   4 boneless, skinless chicken breasts
*   Olive oil for grill
*   Salt and freshly ground black pepper
*   [1/2 c. blueberry vinaigrette]( {{< relref "blueberry-vinagrette" >}} )
*   1 large head of romaine lettuce, shredded
*   1/3 c. toasted slivered almonds
*   3/4 c. blueberries
*   8 to 10 strawberries, sliced
*   1/2 c. shredded aged gouda

## Preparation

Prepare grill. Rinse chicken breasts under cold running water, then drain and blot dry with paper towels. When ready to grill, brush grill grate with olive oil. Season chicken breasts with salt and pepper and arrange breasts in same direction on hot grate.

Grill for 2 minutes. Using tongs, rotate breasts 45 degrees and grill for 2 to 4 additional minutes. Turn breasts, brush cooked side with blueberry vinaigrette and grill, rotating breasts 45 degrees after 2 minutes. Remove breasts to a platter and cool.

When ready to serve, toss lettuce with vinaigrette in a large salad bowl. Arrange almonds, blueberries, strawberries, cheese and chicken breast on lettuce, season with salt and pepper and serve.

_Mike Grossmann_
