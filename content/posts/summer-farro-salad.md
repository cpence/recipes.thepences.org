---
title: "Summer Farro Salad"
author: "pencechp"
date: "2017-06-20"
categories:
  - main course
  - side dish
tags:
  - farro
  - salad
---

## Ingredients

*   1/3 cup plus 2 tablespoons extra-virgin olive oil
*   1 small yellow onion, quartered 
*  1 small carrot, halved
*  1 celery rib, halved
*  12 ounces farro (1 3/4 cups)
*  5 cups water
*  Kosher salt
*  3 tablespoons red wine vinegar
*  Freshly ground pepper
*  1/2 small red onion, thinly sliced
*  1 small seedless cucumber, halved lengthwise and thinly sliced crosswise
*  1 pint grape tomatoes, halved
*  1/4 cup chopped fresh basil

## Preparation

In a large saucepan, heat 2 tablespoons of the oil. Add the yellow onion, carrot and celery, cover and cook over moderately low heat until barely softened, about 5 minutes. Add the farro and stir to coat with oil. Add the water and bring to a boil. Cover and simmer over low heat until the farro is barely tender, about 10 minutes; season with salt. Cover and simmer until the farro is al dente, about 10 minutes longer. Drain the farro and discard the onion, carrot and celery. Let cool completely.

In a large bowl, whisk the remaining 1/3 cup of olive oil with the vinegar and season with salt and pepper. Fold in the farro, red onion, cucumber, tomatoes and basil, season with salt and pepper and serve.

_Food & Wine_
