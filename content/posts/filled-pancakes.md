---
title: "Filled Pancakes"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - breakfast
tags:
  - pancake
---

## Ingredients

\[CP: If making this recipe for two, _halve it!_\]

_For the pancakes:_

*   2 cups flour
*   1 tsp. baking powder
*   1/2 tsp. salt
*   1 tbsp. sugar
*   4 eggs, separated
*   2 cups milk
*   4 tbsp unsalted butter, melted plus more for cooking
*   fillings
*   syrup

_For the cinnamon filling (optional):_

*   1/2 cup granulated sugar
*   2 Tbs. all-purpose flour
*   1 1/2 Tbs. ground cinnamon
*   1/4 tsp. salt
*   4 Tbs. (1/2 stick) unsalted butter, cut into 1/2-inch cubes, at room temperature

_For the cream cheese frosting (optional):_

*   3 oz. cream cheese, at room temperature
*   4 Tbs. (1/2 stick) unsalted butter, at room temperature
*   1 1/2 cups confectioners' sugar
*   3 to 4 Tbs. milk

## Preparation

_If making the cinnamon filling:_ In a bowl, stir together the granulated sugar, flour, cinnamon and salt. Add the butter and, using the back of a spoon, mash the butter into the flour mixture until all of it is absorbed into the butter, forming a paste. Set the cinnamon filling aside.

_If making the cream cheese frosting:_ In the bowl of an electric mixer fitted with the flat beater, beat together the cream cheese and butter on medium speed until light and fluffy, 2 to 3 minutes. Add the confectioners' sugar, reduce the speed to low and beat until combined, 1 to 2 minutes, stopping the mixer occasionally to scrape down the sides of the bowl. Add 3 Tbs. of the milk and beat until combined, about 1 minute. The frosting should be thick but still pourable; add more milk if needed to thin it. Transfer the frosting to a small bowl; set aside.

_For the pancakes: _In a bowl, whisk together the flour, baking powder, salt and sugar. In a small bowl, lightly whisk the egg yolks. Then whisk in the milk and the 4 tbsp melted butter. Whisk the yolk mixture into the flour mixture until well combined; the batter will be lumpy.

In another bowl, using an electric mixer fitted with the whisk attachment, beat the egg whites on high speed until stiff but not dry peaks form, 2-3 min. Using a rubber spatula, gently stir the whites into the batter in 2 additions.

Put 1/4 tsp butter in each well of the filled pancake pan. Set over medium heat and heat until the butter begins to bubble. Pour 1 tbsp batter into each well. Put 1 tsp of the filling of your choice in the center of each pancake and top with 1 tbsp batter. (Good fillings other than the cinnamon include cheese/bacon, fresh berries, or banana.) Cook until the bottoms are golden brown and crispy, 3-5 min. Using 2 wooden skewers (or chopstick), flip the pancakes over and cook until golden and crispy, about 3 min more. Transfer to a plate. Repeat with the remaining batter and filling. Serve immediately with syrup or whipped cream, or topped with the frosting. Makes about 40.

_Williams-Sonoma_
