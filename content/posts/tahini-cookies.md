---
title: "Tahini Cookies"
author: "pencechp"
date: "2019-12-15"
categories:
  - dessert
tags:
  - cookie
---

## Ingredients

*   2 cups all-purpose flour
*   1 teaspoon baking powder
*   ½ teaspoon kosher salt
*   ¾ cup (1½ sticks) unsalted butter, room temperature
*   ¾ cup sugar
*   3 tablespoons honey
*   ¾ cup tahini
*   ¼ cup toasted sesame seeds

## Preparation

Place racks in upper and lower thirds of oven and preheat to 350°. Whisk flour, baking powder, and salt in a medium bowl. Using an electric mixer on medium speed, beat butter, sugar, and honey in a large bowl until light and fluffy, about 3 minutes. Beat in tahini, then add dry ingredients in 2 batches, beating after each addition until fully combined. Dough will be slightly sticky.

Place sesame seeds in a small bowl. Scoop out heaping tablespoons of dough (about 1 oz.) and roll into balls. Dip tops of balls in sesame seeds, pressing to adhere, and place, sesame side up, on 2 parchment-lined baking sheets, spacing about 2" apart. Bake cookies, rotating baking sheets halfway through, until golden brown, 13–15 minutes. Let cool on baking sheets (cookies will firm as they cool).

To make with already-sweetened tahini (e.g., flavored), just omit the honey.

_BA, edited_
