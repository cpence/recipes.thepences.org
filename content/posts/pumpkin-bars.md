---
title: "Pumpkin Bars"
author: "pencechp"
date: "2010-05-08"
categories:
  - dessert
tags:
  - pumpkin
  - untested
---

## Ingredients

*   1/2 Cup butter, soft (1 stick)
*   1 Cup brown sugar
*   2 eggs
*   2/3 Cup canned pumpkin
*   1 tsp. vanilla extract
*   1 Cup flour
*   1/2 tsp. baking powder 
*  1/2 tsp. baking soda
*  1 tsp. cinnamon
*  1/4 tsp. ground ginger
*  1/4 tsp. ground nutmeg

*Brown Butter Icing:*

*   1/2 Cup butter (1 stick)
*   3 Cups powdered sugar
*   1 tsp. vanilla extract
*   2 TB milk

## Preparation

Preheat oven to 350°. Cream the butter. Add the brown sugar, eggs, pumpkin, and VANILLA and mix well. In a separate bowl, sift all of the dry ingredients together. Gradually add to the wet mixture and mix well. Pour into a greased 9x13 inch pan and bake at 350° for about 15-20 minutes. Let cool and top with Brown Butter Icing.

*Brown Butter Icing*

In a heavy-bottomed saucepan, heat the butter over low heat until it is a light brown. Remove from heat and add the powdered sugar, VANILLA, and milk. Whisk until smooth and well blended.

_Penzey's Spices_
