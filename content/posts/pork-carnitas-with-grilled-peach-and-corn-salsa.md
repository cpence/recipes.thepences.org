---
title: "Pork Carnitas with Grilled Peach and Corn Salsa"
author: "pencechp"
date: "2011-09-03"
categories:
  - main course
tags:
  - mexican
  - pork
---

## Ingredients

*   3 lb. pork butt, cut into 1- to 2-inch cubes
*   1/2 cup olive oil
*   1/2 tsp. paprika
*   1/2 tsp. kosher salt
*   1/2 tsp. pepper
*   1/2 tsp. garlic powder
*   1/2 tsp. chili powder
*   8 ounces cola, plus more for cooking
*   1/8 cup soy sauce or teriyaki
*   1/4 cup pineapple juice
*   Juice and zest of 1 lime
*   Juice and zest of 2 oranges
*   1/4 cup minced garlic
*   2 Tbsp. cumin
*   1 dried habanero or ancho chile
*   4 flour tortillas
*   1 avocado, sliced
*   1 recipe [Grilled Peach and Corn Salsa]( {{< relref "grilled-peach-and-corn-salsa" >}} )

## Preparation

Combine paprika, salt, pepper, garlic powder and chili powder and rub into pork cubes.

Mix cola, soy sauce, pineapple juice, lime juice and zest, orange juice and zest, garlic and cumin and cover pork.  Marinate pork in the refrigerator 2 to 6 hours.

Remove pork from marinade (reserve liquid), and sear meat in olive oil on all sides in a heavy-bottom pot.  Add reserved marinating liquid and dried chili and simmer for approximately 2 hours.  Liquid should reduce to a glaze by the 1 1/2 to 2-hour mark.  To keep meat moist during earlier cooking time, add additional cola as needed.  Meat should be falling apart tender.  Can keep up to 5 days in the refrigerator.  Saute to reheat.

Serve in flour tortillas with slices of avocado and grilled peach and corn salsa.  Serves 4.

_Rachel Mabb_
