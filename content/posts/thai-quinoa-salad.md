---
title: "Thai Quinoa Salad"
author: "pencechp"
date: "2015-07-31"
categories:
  - main course
  - side dish
tags:
  - quinoa
  - salad
  - thai
  - untested
---

## Ingredients

_For the Salad_

*   1 cup quinoa, rinsed
*   1/2 teaspoon salt
*   1 red bell pepper, cut into bite-sized strips
*   1 carrot, peeled and grated
*   1 English cucumber, seeded and diced
*   2 scallions, white and green parts, finely sliced
*   1/4 cup freshly chopped cilantro
*   2 tablespoons fresh chopped mint or basil

_For the Dressing_

*   1/4 cup freshly squeezed lime juice, from 3-4 limes
*   2 1/2 teaspoons Asian fish sauce
*   1 1/2 tablespoons vegetable oil
*   2 tablespoons sugar
*   1/4 teaspoon crushed red pepper flakes

## Preparation

Add quinoa, salt and 1-2/3 cups water to a medium saucepan. Bring to a boil and then reduce heat to low, cover, and cook for 15 minutes, until the water is absorbed and the quinoa is cooked. (If necessary add 1-2 tablespoons more water in cooking.) Transfer to a serving bowl and let cool in the refrigerator.

In the meantime, make the dressing by combining the lime juice, fish sauce, vegetable oil, sugar and crushed red pepper flakes in a medium bowl. Whisk until the sugar is dissolved.

Once the quinoa is cool, add the red bell peppers, carrots, cucumbers, scallions, fresh herbs and dressing. Toss well, then taste and adjust seasoning with more salt, sugar and lime juice if necessary. Chill.

_Once Upon a Chef_
