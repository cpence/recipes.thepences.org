---
title: "Chocolate Chip Zucchini Cake"
author: "pencechp"
date: "2013-04-29"
categories:
  - dessert
tags:
  - cake
  - chocolate
  - untested
---

## Ingredients

*   1/2 c. butter (1 stick), softened
*   1 3/4 c. sugar
*   1/2 c. vegetable oil
*   2 eggs
*   1 tsp. vanilla
*   2 1/2 c. flour
*   2 tbsp. cocoa powder
*   1 tsp. baking soda
*   1/2 tsp. baking powder
*   1/2 tsp. cinnamon
*   1/4 tsp. cloves
*   1/2 c. buttermilk
*   2 c. peeled, shredded zucchini
*   2 c. chocolate chips, semi-sweet

## Preparation

Preheat oven to 350.  In a large mixing bowl, cream together the butter and sugar.  Beat in the oil, eggs, and vanilla.  Combine the dry ingredients in a separate bowl and add to the creamed mixture alternately with the buttermilk.  Stir in the zucchini.  Pour into a greased 9x13" pan.  Sprinkle with the chocolate chips.  Bake at 350 for 45-50 minutes or until a toothpick comes out clean.  Cool.

_Penzey's_
