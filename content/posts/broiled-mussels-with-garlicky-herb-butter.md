---
title: "Broiled Mussels with Garlicky Herb Butter"
author: "pencechp"
date: "2014-01-04"
categories:
  - main course
tags:
  - italian
  - mussels
---

## Ingredients

*   1/2 cup parsley leaves
*   2 large garlic cloves
*   1/2 teaspoon kosher salt
*   1/4 teaspoon black pepper
*   10 tablespoons unsalted butter, softened
*   1/4 cup plus
*   1 1/2 tablespoons
*   Pernod or pastis
*   2 pounds mussels, scrubbed
*   1/3 cup bread crumbs

## Preparation

In a food processor, pulse together parsley, garlic, salt and pepper until finely chopped. Pulse in butter and 1 1/2 tablespoons Pernod until mixture is combined. Scrape into a bowl.

In a soup pot with a tightfitting lid, combine mussels, 1/4 cup pastis and 1/4 cup water. Cover and cook over medium-high heat until mussels have opened, 5 to 10 minutes. Transfer mussels to a bowl until cool enough to handle; remove meat from the shells (reserving shells) and transfer to a bowl.

Pry apart mussel shells and arrange half the shells on one or two large baking sheets; discard remaining shells. Place one mussel in each shell. Top each with a small spoonful of herb butter and a sprinkling of bread crumbs. Mussels may be made up to 1 day ahead up to this point; wrap baking sheets and mussels in plastic wrap and refrigerate. When ready to serve, heat broiler to high and arrange a rack 4 inches from the heat. Transfer tray(s) to the oven and broil until bread crumbs are golden, 1 to 2 minutes.

_Melissa Clark, New York Times_
