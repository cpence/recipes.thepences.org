---
title: "Carnitas Caldo (Soup)"
author: "pencechp"
date: "2013-09-29"
categories:
  - main course
tags:
  - mexican
  - pork
  - soup
---

## Ingredients

- 1 tbsp. olive or vegetable oil
- 1 large white onion, peeled and diced
- 1 chayote, skin and core removed, then sliced into matchsticks
- 8 cloves garlic, thinly sliced
- 8 cups chicken broth
- 5-6 cups cooked [pork carnitas]( {{< relref "slow-cooked-carnitas" >}} )
- 1-3 roasted serrano peppers (add to taste, instructions below)
- salt and pepper
- 2 cups potato-masa dumplings (below)
- 2 avocados, diced
- 1 cup fresh cilantro, chopped
- 6-8 cups fresh baby arugula
- lime wedges for garnish

_for dumplings:_

- 1 cup mashed potatoes
- 1/2 cup masa harina
- 1 egg
- 1/4 tsp. salt

## Preparation (Dumplings)

Combine last 4 ingredients in a bowl and stir together until blended. Use your
hands to knead the mixture a few times until smooth. Then pinch off about 1/4
cup of the mixture and roll it into a long tube about 1/2-inch wide in diameter.
Use a knife to slice the tube into small coins, about 1/8-inch wide. Repeat with
remaining dough to make dumplings. Cover and refrigerate until ready to use.

## Preparation (Soup)

Heat oil in a large stockpot over medium high heat. Add the white onion and
chayote, and saute for 5 minutes until cooked and the onion is translucent. Add
garlic and cook for an additional 1-2 minutes until fragrant. Add the chicken
broth, carnitas, and serrano peppers (add one at a time to test out the level of
heat) and bring to a boil. Reduce heat to medium-low, cover and simmer for at
least 30 minutes.

Season generously with salt and pepper. Then ladle the soup into serving bowls,
filling them about half full. Then add in a small handful of potato dumplings,
avocados and fresh cilantro to each bowl. Then top each with a large handful of
baby arugula. Serve with lime wedges for garnish.

_Recipe from Gimme Some Oven, after XOCO_
