---
title: "Goat Cheese and Lemon Cheesecake"
author: "pencechp"
date: "2016-04-28"
categories:
  - dessert
tags:
  - cake
  - chevre
  - lemon
  - untested
---

## Ingredients

_For the crust:_

*   2 oz. hazelnuts, toasted, skinned, and cooled
*   3 tbsp. sugar
*   5 oz. animal crackers
*   3 tbsp. melted warm butter

_For the filling:_

*   1 tbsp. lemon zest
*   ¼ c. lemon juice
*   1¼ c. sugar
*   1 lb. cream cheese, cut into rough 1-inch chunks at room temp.
*   4 eggs, room temp.
*   8 oz. mild goat cheese, room temp.
*   2 tsp. vanilla extract
*   ½ c. heavy cream

_For the lemon curd:_

*   ⅓ c. lemon juice
*   2 eggs
*   1 egg yolk
*   ½ c. sugar
*   2 tbsp. butter, cut into ½-inch cubes and chilled
*   1 tbsp. heavy cream
*   ¼ tsp. vanilla extract
*   pinch salt

## Preparation

1\. FOR THE CRUST: Adjust oven rack to lower-middle position and heat oven to 325 degrees. In food processor, process hazelnuts with 3 tablespoons sugar until finely ground and mixture resembles coarse cornmeal, about 30 seconds. Add Nabisco Animal Crackers or Social Tea Biscuits and process until mixture is finely and evenly ground, about 30 seconds. Add warm melted butter in slow, steady stream while pulsing; pulse until mixture is evenly moistened and resembles wet sand, about ten 1-second pulses. Transfer mixture to 9-inch springform pan; using bottom of ramekin or dry measuring cup, press firmly and evenly into pan bottom, keeping sides as clean as possible. Bake until fragrant and golden brown, 15 to 18 minutes. Cool on wire rack to room temperature, about 30 minutes. When cool, wrap outside of pan with two 18-inch square pieces heavy-duty foil; set springform pan in roasting pan.

2\. FOR THE FILLING: While crust is cooling, process 1/4 cup sugar and lemon zest in food processor until sugar is yellow and zest is broken down, about 15 seconds, scraping down bowl if necessary. Transfer lemon sugar to small bowl; stir in remaining 1 cup sugar.

3\. In standing mixer fitted with paddle attachment, beat cream cheese and goat cheese on low to break up and soften slightly, about 5 seconds. With machine running, add sugar mixture in slow steady stream; increase speed to medium and continue to beat until mixture is creamy and smooth, about 3 minutes, scraping down bowl with rubber spatula as needed. Reduce speed to medium-low and add eggs 2 at a time; beat until incorporated, about 30 seconds, scraping sides and bottom of bowl well after each addition. Add lemon juice and vanilla and mix until just incorporated, about 5 seconds; add heavy cream and mix until just incorporated, about 5 seconds longer. Give batter final scrape, stir with rubber spatula, and pour into prepared springform pan; fill roasting pan with enough hot tap water to come halfway up sides of springform pan. Bake until center jiggles slightly, sides just start to puff, surface is no longer shiny, and instant-read thermometer inserted in center of cake registers 150 degrees, 55 to 60 minutes. Turn off oven and prop open oven door with potholder or wooden spoon handle; allow cake to cool in water bath in oven for 1 hour. Transfer springform pan without foil to wire rack; run small paring knife around inside edge of pan to loosen sides of cake and cool cake to room temperature, about 2 hours.

4\. FOR THE LEMON CURD: While cheesecake bakes, heat lemon juice in small nonreactive saucepan over medium heat until hot but not boiling. Whisk eggs and yolk in medium nonreactive bowl; gradually whisk in sugar. Whisking constantly, slowly pour hot lemon juice into eggs, then return mixture to saucepan and cook over medium heat, stirring constantly with wooden spoon, until mixture registers 170 degrees on instant-read thermometer and is thick enough to cling to spoon, about 3 minutes. Immediately remove pan from heat and stir in cold butter until incorporated; stir in cream, vanilla, and salt, then pour curd through fine-mesh strainer into small nonreactive bowl. Cover surface of curd directly with plastic wrap; refrigerate until needed.

5\. TO FINISH THE CAKE: When cheesecake is cool, scrape lemon curd onto cheesecake still in springform pan; using offset icing spatula, spread curd evenly over top of cheesecake. Cover tightly with plastic wrap and refrigerate for at least 4 hours or up to 24 hours. To serve, remove sides of springform pan and cut cake into wedges. (If desired, to remove cheesecake from springform pan bottom, slide thin metal spatula between crust and pan bottom to loosen, then slide cake onto serving plate.)

_CI_
