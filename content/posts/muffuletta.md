---
title: "Muffuletta"
author: "pencechp"
date: "2018-10-01"
categories:
  - main course
tags:
  - cajun
  - sandwich
  - untested
---

## Ingredients

_for the bread_

*   1 c. warm water
*   1 Tbsp yeast
*   1 Tbsp sugar
*   2 c. all purpose flour
*   1 c. bread flour
*   1 1/2 tsp salt
*   2 Tbsp lard or vegetable shortening
*   sesame seeds
*   3 Tbsp olive oil
*   1 egg
*   2 Tbsp cold water

_for the olive salad_

*   1 1/2 c. green olives, pitted
*   1/2 c. kalamata olives, pitted
*   1 c. giardiniera
*   1 Tbsp. capers
*   3 cloves garlic, thinly sliced
*   1/8 c. celery, thinly sliced
*   1 Tbsp parsley, finely chopped
*   1 Tbsp fresh oregano
*   1 tsp. crushed red pepper flakes
*   3 Tbsp red wine vinegar
*   1/4 c. roasted red peppers
*   1 Tbsp green onions, thinly sliced
*   salt and pepper to taste
*   1 to 1 1/2 c. olive oil

_for the sandwich_

*   1/4 lb genoa salami
*   1/4 lb hot capicola (or regular ham)
*   1/4 lb mortadella
*   1/8 lb sliced mozzarella
*   1/8 lb sliced provolone

## Preparation

_to make the bread_ Combine the water, yeast and sugar in the workbowl of a stand mixer, stir well and let stand for 5-10 minutes or until good and foamy. Meanwhile, combine the flours, salt, and lard in a bowl and work in the fat with your hands until broken up into very small pieces. When the yeast is foamy, fit the mixer with a dough hook attachment and gradually add the flour on low speed until its all incorporated. Scrape the sides down between additions. When the dough comes together, turn it onto a floured work surface and knead until smooth and elastic, 5-10 minutes, adding more flour if necessary.. Alternatively, you can let the machine do the work, but for me, bread is a touch thing. Coat a large bowl with the Olive Oil, then put the dough in, turning once to coat both sides. Cover loosely with a clean dry towel, or plastic wrap. Let the dough rise in a warm place until doubled in bulk, about 1-1/2 hours. Punch the dough down and shape into a flat round about 9 inches across (it will expand to about 10″.) Place the dough on a lightly oiled baking sheet. Sprinkle the top with sesame seeds, about 2-3 Tbsp should do it, then press them lightly into the dough. Loosely cover the loaf and let rise until doubled in bulk, about 1 hour. When the dough has risen, remove the cover, gently brush with the egg wash then gently place into a preheated 425 degree F oven for 10 minutes. Turn the heat down to 375 degrees F for an additional 25 minutes or until it’s golden brown and sounds hollow when tapped.

_to make the olive salad_ Crush each olive on a cutting board with your hand. Combine all ingredients. Cover with oil. Put into a bowl or jar, cover and let the flavors marry for about one week.

_to assemble the sandwich_ Cut the bread in half lengthwise. Brush both sides with the oil from your 1 week old Olive Salad, go a little heavier on the bottom. Layer half of the salami on the bottom half of the bread. Then the mortadella. Then the mozzarella, then the capicola/ham, provolone, and the remainder of the salami. Top this with the olive salad. Put the lid on and press it down without smashing the bread. Quarter it.

_NOLA Cuisine_
