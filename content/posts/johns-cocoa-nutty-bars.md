---
title: "John's Cocoa Nutty Bars"
author: "pencechp"
date: "2010-05-09"
categories:
  - dessert
tags:
  - chocolate
  - cookie
  - peanutbutter
  - untested
---

## Ingredients

*   3 cups semisweet chocolate chips
*   3/4 cup butter cut into tablespoons
*   1 1/2 cups flour
*   3/4 cup unsweetened cocoa powder
*   1/2 teaspoon each: baking powder, salt
*   4 large eggs
*   1 1/2 cups sugar
*   2 teaspoons vanilla
*   3/4 cup creamy peanut butter, but not natural peanut butter
*   1/2 to 2/3 cups chopped peanuts

## Preparation

1\. Heat oven to 375 degrees. Melt 1 1/2 cups of the chocolate chips and the butter in a large saucepan over low heat, stirring constantly; let cool. Combine the flour, cocoa powder, baking powder and salt in small bowl; set aside.

2\. Beat the eggs, sugar and vanilla in a large bowl with a mixer on medium speed until combined; gradually add the dry ingredients, beating well. Add the reserved chocolate mixture; beat well. Spread the dough onto a greased 15-by-10 inch rimmed baking sheet; bake until firm, about 12-15 minutes. Cool until barely warm, about 20 minutes.

3\. Spread the peanut butter over the chocolate layer; sprinkle with the peanuts. Melt the remaining 1 1/2 cups of the chocolate chips; spread while warm over the nut layer. Let cool completely; cut into squares.

Note: Sue Manos says she microwaves the peanut butter for 5-10 seconds to soften it for spreading.

_Chicago Tribune_
