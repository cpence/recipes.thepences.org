---
title: "Jocon"
date: 2024-03-10T18:22:19+01:00
categories:
  - main course
tags:
  - untested
  - mexican
  - stew
  - tomatillo
  - chicken
---

## Ingredients

- 1 cup roasted, unsalted pumpkin seeds
- ¼ cup white sesame seeds
- 1 pound tomatillos (about 9 medium), husked and washed
- 3 medium green bell peppers, halved and destemmed
- 1 sweet onion, peeled and quartered
- 1 jalapeño, split lengthwise
- 6½ cups chicken broth
- 2 pounds boneless, skinless chicken thighs
- 1 tablespoon olive oil
- 2 garlic cloves, minced
- Salt
- 3 scallions, trimmed and roughly chopped
- 1 large bunch cilantro (leaves and tender stems), roughly chopped
- 3 large russet potatoes, peeled and diced into ⅓-inch cubes
- 4 medium carrots, peeled and diced into ⅓-inch cubes
- Cooked white rice, for serving

## Preparation

Toast the pumpkin and sesame seeds in a medium pan over medium-high heat,
stirring occasionally, until fragrant and the sesame seeds start to turn light
golden, about 3 minutes. Transfer to a blender and pulse until sandy, scraping
the sides and stirring as needed. Set aside ground seed mixture.

Set the broiler to high. In a large sheet pan, arrange the whole tomatillos,
halved peppers, quartered onion and halved jalapeño, cut sides down, in an even
layer. Broil until the skin of the vegetables is soft and blistered, rotating
the pan as needed, 6 to 8 minutes.

To a large pot, add the chicken broth, chicken thighs and the blistered
jalapeño, bell peppers, onion and tomatillos; bring the mixture to a boil over
high. Once the soup is boiling, reduce the heat to medium and simmer it,
stirring occasionally, for 35 minutes.

While the soup cooks, heat the olive oil in a small skillet over low heat. Add
the minced garlic and 2 teaspoons salt and fry just until golden, swirling the
pan, 2 to 3 minutes. Set aside.

Remove the chicken from the broth and set aside in a medium bowl. Add the
scallions and cilantro to the broth. Purée the broth in a blender, or directly
in the pot using an immersion blender.

Add the garlic mixture to the large pot with the soup, along with the potatoes,
carrots and the ground seed mixture. Cook on medium heat, stirring occasionally,
for 15 to 20 minutes, until the vegetables are fork-tender and the soup
thickens.

Using two forks, pull the chicken into small chunks or bite-size pieces. Stir
the chicken back into the soup to warm it, and add more salt to taste.

To serve, scoop cooked rice into a small cup and invert it into a bowl; surround
with soup. The soup can be stored in the fridge for up to 2 days or frozen for
up to 3 months.

_NYT_
