---
title: "Whole Pumpkin Pie Soup"
author: "pencechp"
date: "2010-11-30"
categories:
  - main course
tags:
  - pumpkin
  - soup
---

## Ingredients

*   1 whole baking pumpkin, approximately 4 pounds, rinsed
*   2 teaspoons vegetable oil
*   1 tablespoon unsalted butter
*   1/2 small yellow onion, diced
*   1 teaspoon kosher salt
*   1 clove garlic, minced
*   1 small apple, peeled, cored, and diced
*   1 cup low-sodium chicken broth
*   1/2 cup heavy cream
*   2 ounces goat cheese
*   1 teaspoon fresh thyme leaves

## Preparation

Heat the oven to 375 degrees F.

Make a lid on the top of the pumpkin by cutting around the stem at a 45 degree angle. Make sure the opening is large enough to work within. Remove the seeds and fibers with a metal spoon or ice cream scoop and kitchen shears. Reserve the seeds for another use. Brush the exterior of the pumpkin and the lid with vegetable oil. Oil a round casserole dish large enough to hold the pumpkin and place the pumpkin inside.

Combine the butter, onion, salt, garlic, apples, chicken broth, and heavy cream in the hollow pumpkin. Replace the lid of the pumpkin to cover. Bake for 1 1/2 hours.

Remove the lid. Add the goat cheese and thyme and bake an additional 30 minutes, uncovered. Remove the pumpkin from the oven, and gently scrape some of the flesh into the soup mixture. Puree with an immersion blender to desired consistency, being careful to avoid the sides and bottom of the pumpkin. Serve immediately.

_Alton Brown_
