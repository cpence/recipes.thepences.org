---
title: "Vidalia Onion Pie"
author: "pencechp"
date: "2013-06-17"
categories:
  - main course
  - side dish
tags:
  - onion
  - pie
  - untested
---

## Ingredients

*   3 vidalia onions, thinly sliced
*   2 eggs, beaten
*   1/2 c. sour cream
*   salt and pepper to taste
*   1/4 c. unsalted butter
*   2 tbsp. grated Parmesan cheese
*   pinch paprika
*   1 tbsp. hot sauce
*   9" pie shell, baked
*   1/4 c. grated Parmsean for topping

## Preparation

Preheat oven to 375 degrees F (190 degrees C).

In a medium skillet, cook onions in butter for about 10 minutes, or until clear and soft; stir often. Reserve juices.

In a large bowl, mix onions with eggs and sour cream. Stir in onion juices, butter and cheese. Add salt and pepper and hot sauce to taste. Make sure all ingredients are well blended and then pour into the pie shell.

Sprinkle grated cheese and paprika on top of pie. Bake in preheated oven for 20 minutes. Lower temperature to 350 degrees F (175 degrees C) and bake for an additional 30 to 40 minutes, or until lightly browned on top. Let cool for a few minutes to settle before slicing.

_Serves 8, one 9" pie_

_Allrecipes.com_
