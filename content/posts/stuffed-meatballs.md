---
title: "Stuffed Meatballs"
author: "pencechp"
date: "2012-07-24"
categories:
  - main course
tags:
  - cheese
  - italian
  - pasta
  - sausage
---

## Ingredients

*   2 lb. italian sausage
*   12 small mozzarella balls
*   1 large (28 oz.) can diced tomatoes
*   1 container store bought pesto
*   1/4 cup white wine
*   pinch crushed red pepper flakes
*   fresh basil (optional)
*   1 lb. whole wheat pasta

## Preparation

Divide sausage into 12 parts, wrap it around the cheese balls. Cover a baking sheet with olive oil. Bake meatballs at 350 for 45 minutes (or so) – keep an eye on them, you're looking for browned but not overcooked. Heat tomatoes w/ pesto, wine, red pepper flakes, heat through and cook gently for a while. Gently blend the whole thing with an immersion blender to break up the big chunks. Optionally top with a little chopped fresh basil. Serve the whole thing with whole wheat pasta. Oven-roasted broccoli is great on the side, as are fresh greens.

_Mom_
