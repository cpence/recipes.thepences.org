---
title: "Green Bean Potato Curry"
author: "pencechp"
date: "2017-03-30"
categories:
  - main course
tags:
  - curry
  - greenbean
  - potato
  - vegan
  - vegetarian
---

## Ingredients

*   3 tablespoons olive oil
*   1 1/2 tablespoons mustard seed
*   2 medium potatoes, diced
*   1 teaspoon salt
*   1 teaspoon turmeric powder
*   1 pound green beans, snapped in to 2-3 inch piece and remove any tough string
*   2 tomatoes, diced
*   1 teaspoon ginger, minced
*   1 teaspoon cumin powder
*   1 teaspoons coriander powder
*   1/2 teaspoons red pepper flakes

## Preparation

In a large skillet or Dutch Oven, heat the oil. Add the mustard seeds, and fry until they pop. Add the potatoes, salt, and turmeric, and stir to combine. Toss in the beans, cover and cook for 3 minutes. Add the remaining ingredients and stir to combine well. Cover and allow to cok until potatoes are tender and beans still partially firm. Check often to make sure dish is not sticking. Serves 6.

_Edible Nashville_
