---
title: "Sugar Snap Peas with Mint and Orange"
author: "juliaphilip"
date: "2010-04-30"
categories:
  - side dish
tags:
  - peas
  - untested
  - vegan
  - vegetarian
---

## Ingredients

*   12 ounces sugar snap peas, trimmed (about 3 1/2 cups)
*   2 tablespoons (1/4 stick) butter
*   1 tablespoon water
*   1 1/2 tablespoons thinly sliced fresh mint leaves
*   1/2 teaspoon finely grated orange peel

## Preparation

Cook peas in large saucepan of boiling salted water for 2 minutes; drain. DO AHEAD: Can be made 2 hours ahead. Let stand at room temperature. Melt butter in medium skillet over medium-high heat. Whisk in 1 tablespoon water. Stir in mint and orange peel; add sugar snap peas and sauté just until heated through, about 1 minute. Season with salt and pepper. Transfer to bowl and serve.

_Bon Appetit, June 2008_
