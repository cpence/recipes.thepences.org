---
title: "Moscow Mule"
author: "juliaphilip"
date: "2016-04-30"
categories:
  - cocktails
tags:
  - vodka
---

## Ingredients

*   2 oz vodka
*   1 oz lime juice
*   chilled ginger beer
*   sprig of mint
*   lime wedge

## Preparation

Combine vodka and lime juice. Pour into ice filled Moscow Mule lug. Top with ginger beer. Garnish with mint and lime wedge.
