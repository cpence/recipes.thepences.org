---
title: "Jollof Rice (Ghana Style)"
date: 2022-09-25T19:45:49+02:00
categories:
  - main course
tags:
  - rice
  - african
  - untested
---

## Ingredients

- 6 roma tomatoes
- 2 habañero chiles (optional)
- 1/4 c. vegetable oil
- 500 g meat (beef, chicken, or lamb)
- 4 large onions, sliced
- 6 cloves garlic, pressed
- 1.5 l of water or appropriate stock
- 2 tbsp. tomato paste
- 800 g long grain rice
- 2 carrots, steamed, chopped in large pieces

## Preparation

Put tomatoes and chiles (if using) into the blender and blend until smooth.

Brown your meat, which should be already cooked (best seems to be leftover stew meat) in the oil, and set aside.

Add onions to the oil, and fry until soft, then add garlic and fry briefly. Add the tomato mixture, the stock, the tomato paste, and 1 tsp. each of salt and pepper. The seasoning should be strong, since the rice will cook in this. Continue simmering the liquid for around 10 minutes on medium heat. If it's still under-seasoned, you can add some more of a stock cube.

Rinse the rice and add it to the pot, stir, cover, and cook on low heat for 20 minutes. It shouldn't be entirely dried out yet. Add the meat and carrots and mix well. Add 1 c. more of water, and continue to cook on low heat until the rice is cooked.

_My African Food Map, adapted_
