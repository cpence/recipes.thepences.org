---
title: "Brandied Apricot Butter"
author: "pencechp"
date: "2018-02-07"
categories:
  - miscellaneous
  - side dish
tags:
---

## Ingredients

*   1/2 cup dried apricots, chopped
*   1/4 cup Cognac or other brandy
*   2 tablespoons light brown sugar
*   2 sticks unsalted butter, softened
*   Salt

## Preparation

In a small saucepan, soak the apricots in the Cognac for 10 minutes. Bring to a boil, then carefully ignite with a long match. When the flames subside, add the brown sugar and cook over moderate heat until the sugar is dissolved. Transfer to a food processor and let cool. Add the butter and process until fairly smooth. Season the butter with salt. Scrape the apricot butter into a large ramekin and serve with crusty bread.

_Food & Wine_
