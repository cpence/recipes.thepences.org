---
title: "Scotch Shortbread"
author: "pencechp"
date: "2016-09-06"
categories:
  - dessert
tags:
  - caramel
  - chocolate
  - untested
---

## Ingredients

*   7/6 c. butter
*   1/4 c. white sugar
*   1 1/4 c. flour
*   1/2 c. brown sugar
*   2 tbsp. light corn syrup
*   1/2 c. sweetened condensed milk
*   1 1/4 c. dark chocolate chips

## Preparation

Preheat oven to 350 F. Mix together sugar, flour, and 2/3 c. of the butter until evenly crumbly. Press into a 9" square baking pan. Bake for 20 minutes. In a saucepan, combine remaining 1/2 c. butter, brown sugar, corn syrup, and condensed milk. Bring to a boil, and continue to boil for 5 minutes. Remove from heat and beat vigorously with a wooden spoon for about 3 minutes. Pour over crust (warm or cool). Cool until it begins to firm. Melt chocolate chips, pour over the top of the caramel, and spread. Chill and cut into squares.

_Facebook_
