---
title: "Almond Strawberry Crisp [Cobbler]"
author: "pencechp"
date: "2013-07-05"
categories:
  - dessert
tags:
  - almond
  - strawberry
---

## Ingredients

*   1/4 c. unsalted butter
*   1/3 c. lightly packed brown sugar
*   1/4 c. flour
*   1 c. rolled oats
*   1/4 c. sliced almonds
*   4 c. fresh strawberries
*   1 tbsp. granulated sugar
*   1 tbsp. ground almonds
*   1 tsp. vanilla

## Preparation

Preheat your oven to 375ºF.

In a medium bowl, cream the butter and brown sugar. Add the flour and
incorporate well. Then throw in your oats and your almonds, scrunch with your
hands until evenly distributed.

In a small bowl, combine granulated sugar and ground almonds. Toss this in a
medium bowl with the strawberries and vanilla.

Dump the fruit mixture into an eight or nine inch baking dish, then evenly
distribute the topping over it. Pop it in the oven and bake for 45 minutes, or
until you can see juice bubbling up around the sides. It's pretty juicy and it
isn't very sweet, so it would be lovely with some vanilla ice cream.

[Note: This is our pick for best base cobbler/crisp recipe. You can also easily
swap around the fruits/nuts for others.]

_unhip squirrel_
