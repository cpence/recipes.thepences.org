---
title: "Roasted Broccoli with Ginger and Garlic"
author: "pencechp"
date: "2010-05-08"
categories:
  - side dish
tags:
  - broccoli
  - chinese
  - vegan
  - vegetarian
---

Cut the big stem(s) off the brocolli, but leave a couple of inches of stem.

Then cut the brocolli into thin spears, about three or four inches long and 1/2 inch wide.

Place in a single layer on a baking sheet.

Sprinkle with salt, ginger root sliced into thin rounds (say 1/4 cup), and two large garlic cloves sliced in thin rounds. Drizzle generously with olive oil and soy sauce.

Cook the broccoli 20-30 minutes, or until tender.
