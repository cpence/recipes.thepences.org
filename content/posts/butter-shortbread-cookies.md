---
title: "Butter Shortbread Cookies"
author: "pencechp"
date: "2014-01-02"
categories:
  - dessert
tags:
  - cookie
---

## Ingredients

*   1 1/2 cups (3 sticks) unsalted butter, at room temperature
*   1 1/3 cups sugar
*   2 teaspoons kosher salt
*   3 egg yolks
*   2 tablespoons vanilla extract
*   Interior scrapings of 1/2 split vanilla bean, preferably Tahitian
*   3 3/4 cups all-purpose flour

_For Classic Shortbread:_

*   1 cup granulated, raw, or turbinado sugar

_For Five-Spice Shortbread:_

*   2 teaspoons ground ginger
*   2 teaspoons ground cinnamon
*   2 teaspoons ground cardamom
*   1/2 teaspoon ground cloves
*   1/2 teaspoon ground star anise
*   1/4 cup turbinado or other granulated sugar

_For Double Chocolate-Ginger Shortbread_

*   1/4 cup turbinado or other granulated sugar
*   1/4 cup extra-brut cocoa or regular unsweetened cocoa
*   1 tablespoon peeled and grated fresh ginger
*   1/4 cup minced candied ginger
*   3/4 cup semisweet chocolate chips, or bittersweet chocolate chopped into small chunks

_For Caramel Macadamia Nut Shortbread_

*   1 cup sugar
*   1 cup heavy cream
*   1 pound macadamia pieces, or whole nuts, roughly chopped

## Preparation

Preheat the oven to 325°F. In the bowl of a mixer, combine butter, sugar and salt and cream on medium speed until blended, about 2 minutes. One by one, add the egg yolks, mixing until incorporated. Add the vanilla extract and the scrapings of the vanilla bean. Scrape down the bowl.

Turn the mixer off and add the flour. Turn the machine to low and mix until the flour is completely incorporated. Remove the dough from the bowl. Working on parchment or wax paper, form dough into 4 logs 10 inches long and 1 1/4 inches in diameter; wrap and chill.

_For the Classic, Five-Spice, or Double Chocolate-Ginger Cookies:_ In a small bowl, combine the ingredients listed.

Cut the chilled log into twenty 1/2-inch rounds. Dip one cut surface of each round into the mixture and arrange the rounds 2 inches apart on all sides on a parchment-lined or nonstick cookie sheet or sheets.

Bake until golden brown, 15 to 20 minutes. Remove the cookies with a spatula and cool on a wire rack.

_For the Caramel Macadamia Cookies:_ Grease a baking sheet, preferably rimmed.

In a medium saucepan, combine sugar and 1/2 cup of water. Heat over medium heat until mixture begins to simmer, about 3 minutes. Cover the pan and continue to simmer, allowing steam from cooking mixture to wash down the sides of the pan. Remove cover and continue to simmer until the mixture turns golden, 7 to 8 minutes. Immediately remove from the stove, and, to avoid mixture bubbling over, carefully pour cream in a slow, steady stream into the pan; do not stir. Return the pot to the stove, reduce heat to low, and, using a wooden spoon, gently stir mixture until cream is completely incorporated.

Transfer the caramel to a medium heat-proof bowl, add the nuts, and stir to coat the nuts evenly. Transfer the mixture to a baking sheet, spreading it evenly, and bake until dark brown, 12 to 15 minutes. Cool the brittle to room temperature and chop mixture coarsely using a food processor, a cleaver or by hand.

Cut the log into twenty 1/2-inch rounds, press one cut side of each round into the brittle, and arrange the rounds 2 inches apart on all sides on a parchment-lined or nonstick cookie sheet or sheets.

Bake until golden brown, 15 to 20 minutes. Remove the cookies with a spatula and cool on a wire rack.

_Ming Tsai, via Epicurious_
