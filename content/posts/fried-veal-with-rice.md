---
title: "Fried Veal with Rice"
date: 2020-01-27T18:45:55+01:00
categories:
    - main course
tags:
    - rice
    - portuguese
    - veal
---

## Ingredients

*   450 grams (1 pound) veal cut into cubes
*   1 medium onion
*   100 ml (1/2 cup) olive oil
*   Nutmeg (to taste)
*   Pepper (to taste)
*   Coriander (to taste)
*   1 teaspoon paprika
*   100 ml (1/2 cup) white wine
*   3 cloves of garlic
*   1 tablespoon Worcestershire sauce
*   200 grams (1 cup) rice
*   3 bay leaves
*   Salt (to taste)

## Preparation

Season the meat with salt, nutmeg, pepper, paprika, two chopped garlic cloves, white wine, Worcestershire sauce and bay leaf. Let marinate for 1 hour.

Meanwhile, place 50 ml (1/4 cup) olive oil and one chopped garlic clove in a saucepan and saute over low heat for 2 to 3 minutes, stirring occasionally. Add the rice and season with a little salt. Stir and fry the rice for 1 minute. Pour the water (twice the rice volume) and boil over medium high heat. When starts boiling, reduce to low heat and cook for about 10 minutes. Turn off the heat and set aside.

In a frying pan, place the remaining olive oil, a chopped onion and saute over low heat until the onion start to turn golden brown. Add the meat together with the marinade and fry over medium-low heat for 15 to 20 minutes, stirring occasionally.

Add the rice to the frying pan, mix everything nicely and turn off the heat. Sprinkle with chopped coriander and serve.

*Food From Portugal*

