---
title: "Chickpea Salad"
date: 2024-06-10T11:07:43+02:00
categories:
  - main course
  - side dish
tags:
  - mediterranean
  - salad
  - untested
---

## Ingredients

- ½ cup plain full-fat Greek yogurt
- 3 tablespoons mayonnaise
- 2 tablespoons lemon juice (from 1 lemon)
- 1½ teaspoons Dijon mustard
- 1 teaspoon kosher salt
- ½ teaspoon black pepper
- 2 tablespoons minced fresh dill, plus more for serving
- 2 tablespoons minced fresh parsley, plus more for serving
- 3 (15-ounce) cans chickpeas, rinsed
- 1 cup finely diced celery (about 3 stalks)
- ½ cup thinly sliced scallions, white and green parts (2 to 3 scallions)

## Preparation

1. In a small bowl, combine the yogurt, mayonnaise, lemon juice, mustard, salt
   and pepper. Whisk until smooth, then add the dill and parsley and stir to
   combine. Set aside.
2. Place the chickpeas in a large bowl and using a fork, lightly mash about ⅓ of
   them. Add the celery and scallions and toss.
3. Pour the dressing over the salad, toss well, and set aside at room
   temperature for at least 30 minutes before serving. Sprinkle with more dill
   and parsley and serve. (If you’re not serving the dish immediately, you can
   store it in the refrigerator for up to 2 days. Let sit at room temperature
   for 30 minutes before serving.)

_NYT_
