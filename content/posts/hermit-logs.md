---
title: "Hermit Logs"
author: "pencechp"
date: "2010-05-08"
categories:
  - dessert
tags:
  - cookie
  - untested
---

## Ingredients

*   1/2 C shortening (no substitutes)
*   1 egg
*   1/2 C sugar
*   1/4 C molasses
*   1 3/4 C unsifted flour
*   1 1/2 tsp Cinnamon
*   3/4 tsp Powdered Ginger
*   3/4 tsp baking soda
*   1/2 tsp salt
*   1/2 C seedless raisins
*   1/2 C chopped walnuts
*   1 egg white (optional)
*   2 T Vanilla Sugar or sugar (optional)

## Preparation

1\. Preheat oven to 375°F.

2\. In a medium bowl, cream together the shortening, egg and the sugar. Gradually add the molasses and continue beating until fluffy.

3\. In a separate bowl, combine the flour, Cinnamon, Ginger, baking soda and salt. Add the dry ingredients to the creamed mixture, beating just until blended. Fold in the raisins and walnuts.

4\. Divide the dough into two equal portions. Place on a cookie sheet and form each half into a 12x2" rectangle, leaving 3" between the halves.

5\. Bake 13-15 minutes. The dough should be slightly soft in the center.

6\. In a small bowl, lightly beat the remaining egg. Remove the dough from the oven; brush each half with egg white and sprinkle with Vanilla Sugar if desired (if you are concerned about using raw egg, use pasteurized eggs or simply omit). While still warm, cut diagonally. Cool on a wire rack.

_Penzey's Spices_
