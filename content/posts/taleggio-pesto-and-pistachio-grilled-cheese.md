---
title: "Taleggio, Pesto, and Pistachio Grilled Cheese"
author: "pencechp"
date: "2015-05-06"
categories:
  - main course
tags:
  - cheese
  - sandwich
  - untested
---

## Ingredients

*   1 tablespoon butter, softened
*   2 slices white sandwich bread
*   2 tablespoons grated Parmesan cheese
*   2 tablespoons basil pesto
*   3 ounces Taleggio cheese, sliced thin
*   2 tablespoons shelled and crushed pistachios

## Preparation

Heat a nonstick skillet over medium-low. Spread butter evenly on both sides of the bread slices. Place bread in skillet and toast until golden, about 2 minutes. Evenly sprinkle Parmesan on top of the slices and flip. Spread pesto on both slices and top with Taleggio. When cheese melts, turn off heat and sprinkle pistachios on one slice. Top it with the other slice with fillings facing in.

_Thrillist_
