---
title: "Peach, Bourbon, and Molasses Tart"
author: "pencechp"
date: "2017-03-30"
categories:
  - dessert
tags:
  - peach
  - untested
  - vegan
  - vegetarian
  - whiskey
---

## Ingredients

_CRUST_

*   1 cup walnuts 1/2 cup raw unsalted sunflower seeds 1/2 cup medjool dates 1/4 teaspoon sea salt

_FILLING_

*   4 local ripe peaches
*   1 cup multicolored raisins
*   5 medjool dates
*   1/4 cup bourbon
*   1/4 cup blackstrap molasses
*   1/4 cup maple syrup
*   1/2 cup nuts (walnuts or almonds are best)
*   2 tablespoons lemon juice

## Preparation

Pulse all crust ingredients in food processor until crumbly dough forms. Press into pie pan and set aside.

Large dice ripe peaches and transfer to mixing bowl. In food processor, pulse pitted dates, lemon, bourbon, and maple until mostly smooth. Pour into mixing bowl and toss peaches, raisins, 1/2 cup of nuts, until well-covered. Pour into pie pan over crust and drizzle molasses.

_Edible Nashville_
