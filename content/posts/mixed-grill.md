---
title: "Mixed Grill"
author: "pencechp"
date: "2015-05-06"
categories:
  - main course
tags:
  - bellpepper
  - chicken
  - pence
  - sausage
---

## Ingredients

- olive oil
- 4 chicken thighs
- 1 lb. fresh sausage links (Italian or Bratwust)
- 1 onion, halved and thinly sliced
- 1 garlic clove, thinly sliced
- 1 each red, orange, and yellow bell peppers, sliced
- 1 small container brown mushroom caps
- 1/4 c. Worcestershire sauce
- 1/4 c. soy sauce brown rice

## Preparation

Salt & pepper the chicken thighs. Heat a little olive oil in your largest skillet, add thighs skin-side down, cover, and cook until lightly browned. Cut sausage links into thirds. Flip chicken, add sausage, cover, and cook until sausage starts to brown, turning once to cook evenly. Add onion and garlic. Cook (without lid) and stir until onion begins to wilt. Add peppers and mushrooms. Stir occasionally while veggies cook.

When peppers and mushrooms are done to your liking, add sauces and stir until the pan juices thicken and glaze the meats and veggies.

Serve with brown rice.
