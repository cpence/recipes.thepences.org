---
title: "Farro with Creamy Root Vegetables"
author: "pencechp"
date: "2015-01-27"
categories:
  - main course
  - side dish
tags:
  - carrot
  - farro
  - turnip
  - untested
  - vegetarian
---

## Ingredients

*   2 1/2 c. water
*   2 1/2 c. chicken stock
*   2 c. farro
*   4 tbsp. olive oil
*   2 medium shallots
*   1 1/2 lb. turnips (3-4), peeled and cut into bite-sized pieces
*   1/2 lb. carrots, chopped (4 medium)
*   2 cloves garlic, thinly sliced
*   1/2 c. white wine
*   1/2-1 c. chicken stock
*   1 c. parmesan cheese, grated
*   3 tbsp. chopped parsley
*   salt
*   pepper

## Preparation

Cook the farro according to package directions, cooking 1 minute short of the time used on the package, using half water and half chicken stock (5 cups liquid to 2 cups farro). Drain. The grain should have a slightly chewy texture. In a large frying pan, heat 3 tbsp. olive oil. Add shallots and cook until caramelized and still slightly firm, about 6-8 minutes. Add turnips, carrots, and garlic and cook for about 1 minute, just until fragrant. Stir in the farro and the white wine, scraping up the browned bits. Add enough stock to barely cover the veggies and farro and cook over medium until reduced by half and the turnips are tender. Remove from heat and stir in cheese, parsley, salt and pepper. Drizzle with olive oil and serve.

_Penzey's Spices_
