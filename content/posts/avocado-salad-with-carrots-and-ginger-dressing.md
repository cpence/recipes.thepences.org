---
title: "Avocado Salad with Carrots and Ginger Dressing"
author: "pencechp"
date: "2013-09-29"
categories:
  - side dish
tags:
  - japanese
  - salad
  - untested
---

## Ingredients

*   1 large carrot, peeled and roughly chopped
*   1 small shallot, peeled and roughly chopped
*   2 tablespoons roughly chopped fresh ginger
*   2 tablespoons sweet white miso
*   2 tablespoons rice vinegar
*   2 tablespoons toasted sesame seed oil
*   1/4 cup grapeseed or another neutral oil
*   2 tablespoons water

*   1 small/medium head of lettuce (I used Bibb) or mixed greens of your choice
*   1/4 red onion, thinly sliced
*   1 avocado, quartered

## Preparation

Whiz the carrots, shallot and ginger in a blender or food processor until finely chopped. Scrape down the sides, then add the miso, vinegar and sesame oil. While the machine running, slowly drizzle in the grapeseed oil and the water.

Divide the lettuce among four bowls, add some of the onion and a quarter of the avocado. Drizzle with plenty of dressing and serve.

_Smitten Kitchen_
