---
title: "Creamed Cabbage with Red Onion"
author: "pencechp"
date: "2010-10-18"
categories:
  - side dish
tags:
  - cabbage
  - swedish
---

## Ingredients

*   4 c cabbage
*   1 red onion
*   1 clove garlic, minced
*   1 1/2 c cream
*   1-2 tsp salt
*   white pepper to taste
*   1-2 tsp sugar
*   1 Tbsp lemon juice

## Preparation

Chop cabbage into rough dice. Shred onion.

In a saucepan or large skillet, combine cabbage, onion, garlic and cream. Bring to a boil.

Reduce heat, allow to simmer until tender.

Correct seasoning with salt, pepper, sugar and lemon juice to taste.

Serve with Meatballs and a spoonful of lingonberry jam.

_Gretchen's Cookbook_
