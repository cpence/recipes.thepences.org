---
title: "Country-Style Cabrito Pate"
author: "pencechp"
date: "2010-05-08"
categories:
  - side dish
tags:
  - cabrito
  - untested
---

## Ingredients

*   2 pounds cabrito, cut into 2-inch cubes
*   1 pound pork butt, cut into 2-inch cubes
*   1 tablespoon, plus 1 teaspoon, kosher salt
*   1 tablespoon, plus 1 teaspoon, medium ground black pepper
*   2 tablespoons olive oil, plus more for greasing loaf pan
*   3 shallots, diced
*   5 cloves garlic, chopped
*   5 sage leaves, chopped
*   ¾ cup red wine
*   2 teaspoons whole coriander seed
*   2 teaspoons whole fennel seed
*   1 tablespoon, plus
*   1 teaspoon, paprika
*   ½ teaspoon crushed red pepper flakes
*   4 strips of bacon

## Preparation

1\. Season the cabrito and pork with the salt and pepper and refrigerate. In a skillet, heat the olive oil over medium-high heat, add the shallots and garlic and cook until softened, about 5 minutes. Add the sage and cook for a minute. Add the red wine and simmer until reduced by one-third. Transfer to a small bowl and chill until very cold.

2\. Toast the coriander, fennel, paprika and pepper flakes in a dry skillet until fragrant, 2 minutes. Grind the spices and add them to the meat. Stir in the shallot-wine mixture. Freeze the mixture for 15 minutes.

3\. Preheat the oven to 325 degrees. Using a coarse attachment on a meat grinder, grind the meat into a mixing bowl. Using the paddle attachment on an electric mixer, whip the meat at medium speed until the meat pulls away from the sides of the bowl. Lightly oil an 8-by-4-inch loaf pan. Pat the meat into the pan, then bang the pan on the counter to remove air pockets. Top the paté with the bacon, tucking in the ends.

4\. Place the pan in a larger baking dish and add hot (not boiling) water, enough to come halfway up the pan. Bake until the paté reaches an internal temperature of 165 degrees, about 1 hour; begin checking after 45 minutes, since cooking times may vary. Cool the paté for an hour, wrap in plastic and refrigerate overnight. Serves 12. Adapted from Lou Lambert, chef and owner of Lambert’s Downtown Barbecue.

_New York Times_
