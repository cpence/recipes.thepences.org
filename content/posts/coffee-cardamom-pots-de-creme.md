---
title: "Coffee-Cardamom Pots de Creme"
author: "pencechp"
date: "2013-11-29"
categories:
  - dessert
tags:
  - coffee
  - custard
---

## Ingredients

*   3 oz (1 c.) coffee beans, preferably dark roast
*   2 tbsp. cardamom pods
*   3/4 c. sugar
*   2 c. (approx) heavy cream
*   1 c. whole milk
*   7 large egg yolks

## Preparation

Put the coffee beans and cardamom pods in a food processor or coffee grinder and pulse to roughly chop (not grind) them. Turn the chopped beans and pods into a medium saucepan and add 1/2 c. of the sugar. Put the pan over medium heat and cook, stirring constantly with a wooden spoon, until the sugar starts to melt. Once the sugar has melted, continue to cook, stirring constantly, until the sugar caramelizes (to deep amber, almost mahogany). Now, standing back, slowly pour in 1 c. of the cream and the milk. The caramel will immediately seize and harden, but it will smooth out as the liquids warm again. Bring to a boil, and pull the pan from the heat once everything is smooth. Cover the pan and allow it to infuse for 20 minutes.

Preheat the oven to 300 F.

In a large bowl, whisk the yolks and the 1/4 c. remaining sugar until the mixture is pale and thick. Strain the coffee-cardamom liquid into a measuring cup (discard the beans and pods) and add enough cream to bring the liquid to 2 cups. Gently (without creating air bubbles) mix the liquid into the egg mixture; skim off the foam, if there is any.

Arrange 6 4-oz espresso cups or ramekins in a roasting pan, leaving space between them, and fill them with the custard mixture. Put the pan in the oven, then fill the pan with enough hot water to come halfway up the sides. Cover the pan with foil and poke two holes in opposite corners. Bake for 30-40 minutes, until the edges slightly darken and the custards are set but still jiggle in the middle.

Remove the pan and let the custards sit for 10 minutes. Remove cups from the pan and cool in the refrigerator. Let them warm for 20 minutes to room temperature before serving.

_Daniel Boulud, Cafe Boulud Cookbook_
