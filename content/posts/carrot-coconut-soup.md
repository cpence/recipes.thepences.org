---
title: "Carrot-Coconut Soup"
author: "pencechp"
date: "2013-11-26"
categories:
  - main course
  - side dish
tags:
  - carrot
  - soup
---

## Ingredients

*   1/4 cup (1/2 stick) unsalted butter
*   1 pound carrots, peeled, chopped
*   1 medium onion, chopped
*   Kosher salt, freshly ground pepper
*   2 cups low-sodium chicken broth
*   1 13.5-ounce can unsweetened coconut milk
*   2 tablespoons Thai-style chili sauce, plus more for serving
*   Fresh cilantro leaves (for serving)

## Preparation

Melt butter in a large saucepan over medium-high heat. Add carrots and onion, season with salt and pepper, and cook, stirring often, until carrots are softened, 15–20 minutes. Stir in broth, coconut milk, and 2 tablespoons chili sauce. Bring to a boil, reduce heat, and simmer, stirring occasionally, until vegetables are very soft and liquid is slightly reduced, 40–45 minutes.

Let soup cool slightly, then purée in a blender until smooth. Reheat in a clean saucepan, thinning with water to desired consistency; season with salt and pepper.

Divide soup among bowls, drizzle with chili sauce, and top with cilantro.

_Bon Appetit, November 2013_
