---
title: "Shrimp Barbecue"
author: "juliaphilip"
date: "2010-04-26"
categories:
  - main course
tags:
  - pence
  - shrimp
---

## Ingredients

*   2 1/2 lb shrimp, in shells
*   1/3 c lemon juice
*   1 lb butter
*   1 clove minced garlic
*   1 1/2 tbsp soy sauce
*   1/8 c coarse ground black pepper
*   3 tbsp tobasco
*   sprinkle salt
*   French bread for serving

## Preparation

Combine all ingredients except shrimp in a large pot. Heat to bubbling. Add shrimp and cook until done. Serve with French bread.
