---
title: "Coffee-Coconut Tart"
author: "juliaphilip"
date: "2009-12-06"
categories:
  - dessert
tags:
  - coconut
  - coffee
  - untested
---

## Ingredients

*Crust:*

*   3/4 cup all purpose flour
*   1/2 cup sweetened shredded coconut, toasted, cooled
*   7 tablespoons chilled unsalted butter, cut into 1/2-inch pieces
*   1/3 cup powdered sugar
*   1 teaspoon instant coffee crystals
*   1/4 teaspoon salt

*Filling:*

*   1/4 cup sugar
*   6 1/4 teaspoons instant coffee crystals
*   2 tablespoons cornstarch
*   1/2 cup plus 2 tablespoons whipping cream
*   6 tablespoons cream of coconut (such as Coco Lopez)
*   4 large egg yolks
*   1/2 teaspoon vanilla extract

*Topping:*

*   3/4 cup chilled whipping cream
*   1/4 cup cream of coconut (such as Coco Lopez)
*   2 tablespoons powdered sugar
*   Sweetened shredded coconut, toasted

## Preparation

*For crust:* Preheat oven to 350°F. Combine all ingredients in processor. Process until moist clumps form, about 1 minute. Press dough onto bottom and up sides of 9-inch-diameter tart pan with removable bottom. Freeze crust until firm, about 10 minutes.

Place crust on baking sheet. Pierce all over with toothpick. Bake crust until golden brown, about 25 minutes. Cool.

*For filling:* Whisk sugar, coffee crystals and cornstarch in heavy medium saucepan until no cornstarch lumps remain. Gradually whisk in cream, then cream of coconut (coffee crystals will not dissolve completely). Mix in yolks. Whisk over medium heat until mixture thickens and boils, about 8 minutes. Cool in pan, whisking occasionally. Whisk in vanilla. Spread filling in crust. Chill until cold, about 2 hours. (Can be made 1 day ahead; cover and keep chilled.)

*For topping:* Beat whipping cream, cream of coconut and powdered sugar in medium bowl until firm peaks form. Spoon into pastry bag fitted with medium star tip. Pipe decoratively over filling.

Sprinkle with shredded coconut. Refrigerate until cold about 1 hour

_Bon Appetit, October 1995_
