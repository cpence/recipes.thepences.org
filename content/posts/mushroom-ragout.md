---
title: "Mushroom Ragout"
author: "juliaphilip"
date: "2010-04-26"
categories:
  - side dish
tags:
  - mushroom
  - vegetarian
---

## Ingredients

*   2 tbl olive oil
*   1 onion, finely chopped
*   1 lb fresh mushrooms, thickly sliced
*   2 garlic cloves, finely chopped
*   1 tsp chopped fresh rosemary or 1/4 tsp dried rosemary
*   salt and pepper
*   1 tbl tomato paste
*   1 1/2 c chicken or vegetable stock or water
*   2 tbl butter, cut in pieces
*   1 1/2 tsp balsamic vinegar
*   grated parmesan

## Preparation

Heat 1 tbl of olive oil in a large saucepan. Add the onion and cook until golden, about 6 min. Remove the onion to a bowl. Add another tbl of oil to the same pan and heat. Add the mushrooms and cook until the begin to release their liquid. Add the onion along with the garlic, rosemary, and salt and pepper to taste. Cook until the mushrooms begin to brown, 3 to 4 min. Add tomato paste and cook on high for 1 to 2 min. Add broth and reduce heat and simmer for 10 min. Gradually stir in butter. Stir in vinegar and serve over pasta or polenta, garnished with parmesan cheese.

_Joy of Cooking calendar_
