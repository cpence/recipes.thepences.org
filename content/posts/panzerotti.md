---
title: "Panzerotti"
date: 2021-07-10T11:11:09+02:00
categories:
  - main course
tags:
  - italian
---

## Ingredients

- 1 recipe of [Mommawater's crazy dough]({{<relref "crazy-dough.md">}}); 3 oz or
  85g worth for each
- frying oil

_for classic tomato and mozzarella:_

- 9 oz mozzarella cheese
- 4 oz peeled tomatoes
- fresh basil leaves

## Preparation

Take the dough and divide it into balls of around 3 oz (= 85g). If the dough has
been frozen, you can let the balls rise a bit, or not.

Cut the mozzarella into cubes (or buy the fresh mozzarella in small balls). Take
the peeled tomatoes, cook them for ten minutes in a pan with a little oil, salt
to taste and once you've removed them from the heat, add the fresh basil leaves.

Roll out the balls into a circle. Place a tablespoon of sauce and a few cubes of
cheese, then fold shut, forming a crescent. Press the edges well, then fold the
edge over inward and seal it again with a fork.

In a pan, heat the oil to around 350F and fry two panzerotti at a time, waiting
for them to turn golden before you flip them over. Drain and serve hot.

_La Cucina Italiana, adapted_
