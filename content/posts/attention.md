---
title: "Attention"
author: "juliaphilip"
date: "2016-09-13"
categories:
  - cocktails
tags:
  - gin
  - violette
---

## Ingredients

*   2 oz. gin
*   ¼ oz. dry vermouth
*   ¼ oz. créme de violette
*   ¼ oz. absinthe or absinthe substitute
*   2 dashes orange bitters
*   Lemon twist

## Preparation

Combine ingredients in a mixing glass, fill with cracked ice, stir briskly for 30 seconds, strain into a chilled glass and garnish.
