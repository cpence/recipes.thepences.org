---
title: "Couscous"
author: "pencechp"
date: "2010-04-02"
categories:
  - main course
tags:
  - lamb
  - mediterranean
  - pence
---

## Ingredients

*   3 tbsp. olive oil
*   2lb. cubed lamb
*   1 chopped onion
*   1 tsp. ground coriander
*   1 tsp. red pepper flakes
*   1 tsp. ground cumin
*   1/4 tsp. saffron
*   2 tsp. salt
*   2 quarts chicken broth
*   1/2 c. onion, chopped
*   1/2 c. turnip, chopped
*   1/2 c. zucchini, chopped
*   1/2 c. eggplant, chopped
*   1/2 c. leeks, chopped
*   1/2 c. bell pepper, chopped
*   1/2 c. celery, chopped
*   1/2 c. raisins
*   1/2 c. chick peas

## Preparation

Sauté the lamb, onion, coriander, red pepper, cumin, saffron, and salt in the olive oil.  Add the chicken broth, cover, and simmer for about an hour, or until the meat is nearly tender.  Add all the vegetables, the raisins, and the chick peas, and simmer for 30 minutes.

Serve over couscous, and top with harissa.

## Harissa

Put 1/2 cup chili powder (preferably New Mexico) in sauce pan.  Gradually add broth from couscous and a little salt and cook to thicken.
