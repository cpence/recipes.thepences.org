---
title: "White Bean and Red Onion Salad"
author: "pencechp"
date: "2011-12-30"
categories:
  - side dish
tags:
  - bean
  - salad
---

## Ingredients

*   1 1/2 cup dried Great Northern or other white bean, picked over
*   1 1/2 teaspoons salt
*   2 bay leaves
*   1 tablespoon coriander seeds, crushed coarse

*For dressing:*

*   2 garlic cloves if desired
*   2 to 3 tablespoons fresh lemon juice
*   1/4 cup extra-virgin olive oil, or to taste

*   1 cup thinly sliced red onion
*   1/2 cup chopped fresh coriander sprigs or parsley leaves

## Preparation

In a large saucepan combine beans with water to cover by 2 inches and add salt, bay leaves, and coriander seeds. Simmer beans, uncovered, stirring occasionally and adding more hot water if necessary to keep beans covered, 1 to 1 1/4 hours, or until beans are just tender but not mushy.

Make dressing while beans are cooking: Mince garlic fine and in a large bowl stir together well with lemon juice and oil.

In a colander drain beans, discarding bay leaves, and add to dressing. Toss salad and season with salt and pepper. Cool salad and stir in onion and herbs. Salad may be made 1 day ahead and chilled, covered.

Serve salad at room temperature, thinning if necessary with 1 to 2 tablespoons water.

_Gourmet, July 1995_
