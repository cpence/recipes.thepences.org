---
title: "Lemon-Dressed Farro, Tuna and Chickpea Salad"
author: "pencechp"
date: "2013-05-28"
categories:
  - main course
tags:
  - farro
  - tuna
  - untested
---

## Ingredients

*   Grated zest of 1 large lemon
*   5 tablespoons fresh lemon juice (from 2 large lemons)
*   1/4 cup extra-virgin olive oil
*   Salt
*   Freshly ground black pepper
*   1 cup pearled or semi-pearled farro, cooked according to the package directions, then cooled (about 3 cups cooked)
*   1 3/4 cups cooked or canned no-salt-added chickpeas (if using canned, drain and rinse)
*   6 1/2 to 7 ounces canned, oil-packed light tuna, flaked (may substitute water-packed light tuna)
*   4 ounces sweet onion, such as Maui, Vidalia or Walla Walla, cut into 1/4-inch dice (about 1 cup)
*   1/4 cup chopped, loosely packed parsley

## Preparation

Whisk together the lemon zest and juice and the oil in a small bowl to form an emulsified dressing. Season with salt and pepper to taste.

Combine the cooled farro, chickpeas, tuna, onion, parsley and the lemon dressing in a large bowl. Toss to incorporate. Taste, and adjust the seasoning as needed. For the best flavor, refrigerate for at least 2 hours before serving.

_Serves 6-8._

_Stephanie Witt Sedgwick, Washington Post, April 3, 2013_
