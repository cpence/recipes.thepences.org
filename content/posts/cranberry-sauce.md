---
title: "Cranberry Sauce"
author: "pencechp"
date: "2018-01-01"
categories:
  - miscellaneous
tags:
  - cranberry
  - sauce
---

## Ingredients

*   1/4 cup freshly squeezed orange juice
*   1/4 cup 100 percent cranberry juice, not cocktail
*   1 cup honey
*   1 pound fresh cranberries, approximately 4 cups

## Preparation

Wash the cranberries and discard any soft or wrinkled ones.

Combine the orange juice, cranberry juice and honey in a 2-quart saucepan over medium-high heat. Bring to a boil and then reduce the heat to medium-low and simmer for 5 minutes. Add the cranberries and cook for 15 minutes, stirring occasionally, until the cranberries burst and the mixture thickens. Do not cook for more than 15 minutes as the pectin will start to break down and the sauce will not set as well. Remove from the heat and allow to cool for 5 minutes.

Carefully spoon the cranberry sauce into a 3 cup mold. Place in the refrigerator for at least 6 hours and up to overnight.

Remove from the refrigerator, overturn the mold and slide out the sauce. Slice and serve.

_Alton Brown, Food Network_
