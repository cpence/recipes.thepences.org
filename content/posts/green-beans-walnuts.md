---
title: "Green Beans with Walnuts and Walnut Oil"
date: 2021-01-09T21:30:55+01:00
categories:
  - side dish
tags:
  - greenbean
---

## Ingredients

- 2 lbs. green beans, trimmed
- 2 tbs butter (1/4 stick)
- 2 tbs walnut oil
- 1 cup walnuts, chopped and toasted (about 3.75 oz)
- 2 tbs parsley, fresh, minced

## Preparation

Cook beans in a large pot of boiling salted water until just tender, about 5 minutes. Rinse beans with cold water and drain well. (Can be prepared 6 hours ahead. Let stand at room temperature.) Melt butter with oil in heavy large skillet over high heat. Add beans and toss until heated through, about 4 minutes. Season with salt and pepper. Add walnuts and parsley and toss. Transfer to bowl and serve.

_Bon Appetit, Nov. 1994_
