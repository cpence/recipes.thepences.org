---
title: "Gyros"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - main course
tags:
  - greek
  - lamb
---

## Ingredients

*   2 lb ground lamb
*   1 finely chopped medium onion
*   1 tbsp finely chopped garlic
*   1 tbsp dried oregano
*   1 tbsp ground rosemary
*   1 tbsp ground fenugreek
*   2 tsp salt
*   1/2 tsp ground pepper

## Preparation

Mix together as for meatloaf.  Place in loaf pan and bake at 325 for 1 to 11/2 hours.  Drain off fat and let stand 15 minutes before slicing.
