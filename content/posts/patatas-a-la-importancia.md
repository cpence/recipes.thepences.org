---
title: "Patatas a la Importancia"
author: "pencechp"
date: "2013-07-14"
categories:
  - side dish
tags:
  - potato
  - spanish
  - tapas
  - untested
---

## Ingredients

*   6 medium potatoes
*   3 cloves Garlic
*   1 large yellow onions or 2 medium
*   Olive Oil
*   2 cups chicken broth
*   3-4 threads Spanish saffron
*   2 Tbsp. flour for sauce
*   2/3 cup flour (approximately) to coat potatoes
*   2 eggs, lightly beaten
*   salt and pepper to taste

## Preparation

Finely chop the onions and garlic. Pour olive oil to cover entire bottom of a large frying pan. Place pan on medium heat. When oil is hot, add onions and garlic and sauté until onions are translucent and soft. Add about 2 Tbsp. flour and stir.

Add chicken broth and simmer for about 10 minutes. After 10 minutes of simmering, remove broth from heat and set pan with sauce aside for later. While broth simmers, peel and cut potatoes. Slice potatoes – 1/3 to 1/2-inch thick.

Spread flour onto a large dinner plate. Dredge the potatoes slices in the flour and coat both sides thoroughly. Remove the potatoes when coated and set aside.

Pour olive oil approximately 1 inch deep in a large frying pan. Place pan on medium to medium-high heat.

Beat 2 eggs in a small mixing bowl. Dip each piece of potato into the egg to coat and remove quickly, allowing excess egg to run back into the bowl. Then, place in the hot oil. Carefully monitor the heat so that the oil does not burn.

As the potatoes cook and turn a golden color on one side, turn them over and brown the other side. Remove the pieces as they brown and allow to drain on a paper towel.

When most potatoes are fried, place them in a single layer in the frying pan with the onion-garlic sauce. Place pan back on stove. Cover and gently simmer on low until potatoes are cooked. Potatoes should be soft, but not mushy. If the broth evaporates and potatoes are not completely covered, add more broth or water. Salt to taste.

_Serves 4_

_Lisa and Tony Sierra, About.com_
