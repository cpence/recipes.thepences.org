---
title: "Strawberry and Orange Fools"
author: "pencechp"
date: "2010-03-25"
categories:
  - dessert
tags:
  - strawberry
  - untested
---

## Ingredients

*   1 12-ounce basket fresh strawberries, hulled, coarsely chopped
*   3/4 cup chilled whipping cream
*   1/3 cup plain yogurt (do not use low-fat or nonfat)
*   5 tablespoons sugar
*   3 tablespoons fresh orange juice
*   1 teaspoons grated orange peel
*   2 teaspoons Cointreau or other orange liqueur (optional)
*   Additional strawberries, halved

## Preparation

Place chopped strawberries in medium bowl. Mash coarsely with fork. Place in colander and let drain 15 minutes.

Beat cream in large bowl to stiff peaks. Gently fold in yogurt, sugar, orange juice and peel. Fold in Cointreau, if desired. Gently fold in strawberries. Divide among 4 wineglasses or coupes. (Can be made 4 hours ahead. Cover; chill.)

Garnish fools with additional halved strawberries and serve.

_Bon Appetit, May 1996_
