---
title: "Insanely Good Gin and Tonic"
date: 2021-12-23T01:52:36+01:00
categories:
  - cocktails
tags:
  - gin
  - suze
---

## Ingredients

- 1 1/2 ounces gin, preferably Brooklyn
- 1/2 ounce Suze
- 1/4 ounce lime cordial (see Editor's Note), or lime juice
- 1 dash Angostura bitters
- tonic, to top (preferably Q)

## Preparation

Add gin, Suze, lime cordial and bitters to a chilled Collins glass. Top with ice
and tonic. Garnish with a lime wheel.

_Punch_
