---
title: "Spicy Cashew Shrimp"
date: 2022-03-04T10:24:20+01:00
categories:
  - main course
tags:
  - chinese
  - shrimp
---

## Ingredients

- 3 tablespoons olive oil, divided, plus more as needed
- 1 cup cashews, dry roasted
- 2 tablespoons chili garlic sauce, such as Huy Foy brand, divided
- 1 teaspoon finely grated orange zest (from 1 large orange)
- 1 teaspoon white sesame seeds, plus more for serving
- 1 pound peeled, deveined medium (41-50 count) shrimp, defrosted if frozen
- 1/4 cup fine white cornmeal
- 1/4 teaspoon fine black pepper
- 1/4 cup low-sodium soy sauce
- 3 tablespoons rice vinegar
- 3 tablespoons ketchup
- 1 teaspoon sesame oil
- 1 teaspoon minced or grated garlic
- 1/2 teaspoon minced or grated fresh ginger
- Steamed white rice, for serving
- 1/4 cup sliced scallions, for serving

## Preparation

In a large cast-iron or nonstick skillet over medium heat, heat 1 tablespoon of the olive oil until shimmering. Add the cashews and 1 tablespoon of the chili sauce. Cook, stirring frequently, until lightly toasted, 3 to 4 minutes. Move the cashews to a plate, and sprinkle with the orange zest and sesame seeds. Toss to distribute the zest and seeds.

Pat the shrimp dry. In a large bowl, whisk together the cornmeal and pepper until combined. Toss the shrimp with the seasoned cornmeal to coat. Shake off any excess flour before cooking.

Line a platter with a paper towel or a kitchen towel. In the same skillet over medium heat, heat 2 tablespoons of olive oil until hot, about 1 minute. Drop a bit of cornmeal into the oil: If it sizzles vigorously, the oil is hot enough. Working in batches, fry the shrimp until golden and cooked through, about 2 minutes on each side, adjusting the heat and adding more oil, as needed. Transfer the cooked shrimp to the prepared platter when they are done.

In a small bowl, whisk together the soy sauce, vinegar, ketchup, sesame oil, garlic, ginger and remaining 1 tablespoon of chili sauce until well combined.

When all of the shrimp are fried, reduce the heat to low. Return the cooked shrimp to the skillet and pour the sauce mixture over. Toss the shrimp with the sauce and simmer until the sauce thickens and coats the shrimp, 2 to 3 minutes. Remove from the heat.

Serve the shrimp over steamed rice, topped with cashews, scallions and more sesame seeds, if desired.

_WaPo_
