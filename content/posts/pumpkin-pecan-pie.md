---
title: "Pumpkin Pecan Pie"
author: "pencechp"
date: "2010-05-08"
categories:
  - dessert
tags:
  - pecan
  - pie
  - pumpkin
---

## Ingredients

*   1 9 inch unbaked pie shell
*   1 15 oz. Can solid pack pumpkin
*   1/3 Cup brown sugar
*   1/3 Cup granulated sugar
*   3/4 tsp. cinnamon
*   3/4 tsp. powdered ginger
*   1/8 tsp. salt
*   1 tsp. vanilla extract
*   2 eggs, well beaten
*   1/2 Cup milk

*Topping:*

*   1/4 Cup butter
*   1/2 Cup brown sugar
*   3/4 Cup coarsely chopped pecans

## Preparation

Preheat oven to 350°. In a mixing bowl combine the pumpkin, brown sugar, granulated sugar, CINNAMON, GINGER, salt, VANILLA, eggs, and milk. Mix until well blended. Pour into the unbaked pie shell. Bake at 350° for 40 minutes. Meanwhile mix the topping ingredients until crumbly. Sprinkle the topping on the pie and bake an additional 25 minutes. Cool and refrigerate at least a few hours for easiest slicing.

_Penzey's Spices_
