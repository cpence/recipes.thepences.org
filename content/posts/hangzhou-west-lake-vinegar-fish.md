---
title: "Hangzhou West Lake Vinegar Fish"
author: "pencechp"
date: "2010-07-03"
categories:
  - main course
tags:
  - chinese
  - fish
  - untested
---

## Ingredients

*   1 whole fish (800 g, or about 1 3/4 lb.)
*   1 tbsp shao xin wine (or rice wine, or sherry)
*   2 tbsp. fresh ginger, finely sliced in strips
*   2 tbsp. green onion strips
*   3 tbsp. light soy sauce
*   4 tbsp. black vinegar
*   4 tbsp. brown sugar
*   1/2 tsp. salt
*   1 c. chicken or fish stock
*   1/2 tbsp. corn starch and 2 tbsp. water, mixed well
*   sesame oil (optional)

## Preparation

1\. Remove the scales & clean the fish. If the fish is thick, butterfly it. Make a couple of slashes on the thickest part of the body to help it cook faster. Put the fish in a skillet with high sides and scatter ginger strips under and over the fish. Pour the wine all over the fish.

2\. Steam the fish for 10-12 minutes depending on the thickness of the body. Test with a fork by flaking the thickest part of the body. If it flakes off easily from the bone, it is done. Do not over cook.

3\. While fish is steaming, put the soy sauce, black vinegar, brown sugar, and salt into a small pot and cook over low heat, stirring to dissolve the sugar. When sugar is dissolved, add 1 cup stock to the sauce. When sauce boils, add the corn starch solution and stir well until sauce becomes shiny and thickened. Taste and adjust with more sugar, vinegar, or salt to taste.

4\. Add a splash of sesame oil (if using) to the thickened sauce and pour it over the steamed fish. Sprinkle the green onion strips on the fish. Serve hot.

_Adapted from Terri at Hunger Hunger (A Daily Obsession)_
