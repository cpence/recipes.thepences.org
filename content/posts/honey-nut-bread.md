---
title: "Honey Nut Bread"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - bread
  - breakfast
tags:
  - untested
---

## Ingredients

*   2 1/2 cups flour
*   2 tsp baking powder
*   1 tsp salt
*   1/2 tsp baking soda
*   1 large egg beaten until light yellow
*   1 cup milk
*   3/4 cup honey
*   2 tbsp butter, melted
*   1/2 tsp vanilla
*   1 1/2 cups chopped walnuts or pecans

## Preparation

Preheat oven to 350, grease 9 x 5 inch loaf pan.

Whisk first 4 ingredients until blended. Mix the next 5 ingredients in a bowl. Beat the liquid ingredients into the dry ones until just blended. Fold in the nuts. Pour the batter into the greased pan. Bake until a toothpick inserted in the center comes out clean, about 40 minutes. Cool slightly then unmold.

_Joy of Cooking calendar_
