---
title: "Hungarian Székely Gulyás"
author: "juliaphilip"
date: "2010-12-29"
categories:
  - main course
tags:
  - hungarian
  - pence
  - pork
  - stew
---

## Ingredients

*   2 lb pork, cut in 1-inch cubes
*   2 tbsp. butter
*   1 lg. onion, chopped (1 cup)
*   1 tbsp. paprika
*   2 c. chicken broth
*   1 c. water
*   1 tsp. caraway seeds
*   2 tsp. salt
*   dash pepper
*   1 can (1 lb., 11 fl. oz.) sauerkraut
*   2 tbsp. flour
*   1/4 c. water
*   1 carton (8 oz.) sour cream
*   parsley

## Preparation

1\. Brown pork, part at a time (removing pieces to a bowl as a brown), in butter in a heavy kettle or Dutch oven.  Sauté oven until golden, about 5 minutes, in same pan, adding more butter if needed.  Stir in paprika; cook 1 minute longer. Return all meat.

2\. Stir in chicken broth, the 1 cup of water, caraway seeds, salt and pepper.  Bring to boiling; lower the heat and cover.  Simmer for 1 hour and 15 minutes.

3\. Drain and rinse sauerkraut; stir into the stew.  Simmer 30 minutes longer, or until meat is tender.

4\. Blend flour and the 1/4 cup water in a small cup; stir into simmering stew.  Cook and stir until the gravy thickens and bubbles 3 minutes.

5\. Lower heat; stir 1 cup hot sauce into sour cream in a small bowl until well blended; stir back into the kettle.  Heat just until heated through.  Do not boil.  Sprinkle with parsley.  Makes 6 servings.

_Family Circle Cookbook_
