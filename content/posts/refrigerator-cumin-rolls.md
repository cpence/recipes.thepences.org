---
title: "Refrigerator Cumin Rolls"
author: "pencechp"
date: "2011-11-14"
categories:
  - bread
tags:
---

## Ingredients

*   1 cup milk
*   1/4 cup warm water (110°F to 115°F)
*   1 package dry yeast
*   1 teaspoon plus 1/4 cup sugar
*   3 tablespoons unsalted butter, room temperature
*   1 teaspoon salt
*   2 large eggs
*   3 3/4 cups (about) all purpose flour
*   1/2 cup (1 stick) unsalted butter, melted
*   1 large egg, beaten to blend (for glaze)
*   2 tablespoons cumin seeds, toasted

## Preparation

Scald milk in heavy medium saucepan. Cool to 105°F.

Meanwhile, mix 1/4 cup warm water, yeast and 1 teaspoon sugar in small bowl. Let stand until foamy, about 10 minutes.

Pour milk into large bowl. Add 1/4 cup sugar, 3 tablespoons butter and salt. Stir until butter melts and sugar dissolves. Add yeast mixture and 2 eggs; whisk to blend. Mix in 1 1/2 cups flour. Gradually mix in enough remaining flour to form stiff dough. Turn out onto lightly floured surface. Knead until smooth and elastic, about 5 minutes.

Lightly oil large bowl. Add dough, turning to coat. Cover; chill overnight. (Can be made 2 days ahead. Keep chilled.)

Remove dough from refrigerator and let stand 2 hours at room temperature.

Preheat oven to 400°F. Lightly butter twenty-four 1/3-cup muffin cups. Punch dough down. Divide in half. Cut each half into 12 equal pieces. Roll each piece into ball. Dip ball into melted butter. Pull and stretch ball into 6- to 8-inch-long rope. Tie into knot. Place in muffin cup. Repeat with remaining dough and butter. Brush rolls with egg glaze. Sprinkle with cumin. Let rise 30 minutes at room temperature.

Bake until light brown, about 14 minutes. Remove from muffin cups and serve.

_Bon Appetit, November 1997_
