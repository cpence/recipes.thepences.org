---
title: "One-Pot Sticky Chicken Wings"
author: "pencechp"
date: "2012-07-25"
categories:
  - main course
tags:
  - chicken
  - chinese
---

## Ingredients

*   3 lb. chicken wings, wing tips removed and cut into 2 pieces
*   2 tbsp. minced fresh ginger
*   4 small dried red chiles
*   2 whole star anise
*   one 3" cinnamon stick
*   1/3 c. soy sauce
*   1/3 c. sake \[or sherry\]
*   3 tbsp. oyster sauce
*   3 tbsp. mirin
*   3 tbsp. sugar
*   2 scallions, thinly sliced
*   lime wedges for garnish

## Preparation

In a very large nonstick skillet (or in batches), cook the chicken wings over moderate heat, turning once, until golden, about 8 minutes. Add the giner, chiles, star anise, and cinnamon and cook over moderately low heat, stirring, until fragrant, about 1 minute. Add the soy sauce, sake, oyster sauce, mirin, sugar, and 1/3 c. of water and bring to a simmer over moderate heat. Cover and simmer for 10 minutes.  Uncover and cook over moderately high heat, stirring occasionally, until the wings are cooked through and the sauce has reduced to a thick glaze, about 8 minutes \[CP: this may/will be longer, Don't Panic.\]. Discard the chiles, star anise, and cinnamon. Transfer the chicken wings to a platter, scatter the scallions on top and serve with lime wedges.

_Andrew Zimmern, via Austin-American Statesman_
