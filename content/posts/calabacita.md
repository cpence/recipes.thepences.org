---
title: "Calabacita"
author: "pencechp"
date: "2010-09-11"
categories:
  - side dish
tags:
  - mexican
  - pence
  - squash
  - zucchini
---

## Ingredients

*   2 tbsp. butter
*   1 med onion
*   2 zucchini
*   2 yellow squash
*   1 small can of lima beans
*   1 small can of corn kernels
*   1 small can mild green chile

## Preparation

Melt butter and cook zucchini, squash, and onion until softened.  Add limas, corn, and green chile and cook until heated.  Serve.
