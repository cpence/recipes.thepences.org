---
title: "Zucchini Cornbread"
author: "pencechp"
date: "2011-10-05"
categories:
  - bread
tags:
  - cornbread
---

## Ingredients

*   1/2 c (1 stick) unsalted butter, plus more for pan
*   2 large eggs, lightly beaten
*   1/2 c. buttermilk
*   1 lg. zuchini (10 oz.)
*   1 c. all-purpose flour
*   1/2 c. whole wheat flour
*   1/2 c. sugar
*   1 tsp. baking powder
*   3/4 tsp. fine sea salt
*   1/2 tsp. baking soda
*   3/4 c. medium-grind cornmeal

## Preparation

Position a rack in the middle of oven and preheat to 350°. Butter a 9 x 5 x 3" loaf pan.

Melt 1/2 cup butter in a small saucepan over medium-high heat. Continue cooking until butter solids at bottom of pan turn golden brown, about 3 minutes. Scrape butter into a medium bowl. Set aside and let cool. Whisk in eggs and buttermilk.

Trim zucchini ends. Thinly slice five 1/8" rounds from 1 end of zucchini and reserve for garnish. Coarsely grate remaining zucchini. Add to bowl with butter mixture and stir until well blended.

Sift both flours, sugar, baking powder, salt, and baking soda into a large bowl. Whisk in cornmeal. Add zucchini mixture; fold just to blend (mixture will be very thick). Transfer batter to prepared pan and smooth top. Place reserved zucchini slices atop batter down center in a single layer.

Bake bread until golden and a tester inserted into center comes out clean, 55-65 minutes. Let cool in pan 10 minutes. Remove from pan; let cool completely on a wire rack.

_Bon Appetit, 7/2011_
