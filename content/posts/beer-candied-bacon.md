---
title: "Beer Candied Bacon"
author: "pencechp"
date: "2015-01-21"
categories:
  - breakfast
  - side dish
tags:
  - bacon
  - beer
  - untested
---

## Ingredients

*   1 lb thick cut bacon
*   3/4 c. lager
*   1 c. maple syrup
*   2 tbsp. black pepper
*   1 tsp. salt

## Preparation

Preheat the oven to 375. Put the bacon in a single layer on a rimmed baking sheet. Bake until crispy, about 20 minutes. Meanwhile, combine the rest of the ingredients in a saucepan and reduce over medium heat until reduced by half and thicker than syrup, about 20 minutes. Brush the glaze on the bacon and bake until caramelized, 8-10 minutes. Cool on a rake for 5 minutes.

_Thrillist_
