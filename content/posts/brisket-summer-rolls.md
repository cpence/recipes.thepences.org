---
title: "Brisket Summer Rolls"
author: "pencechp"
date: "2014-11-18"
categories:
  - appetizer
  - main course
  - side dish
tags:
  - beef
  - untested
  - vietnamese
---

## Ingredients

_FOR THE BARBECUE MAYONNAISE:_

*   ½ cup mayonnaise
*   ¼ cup barbecue sauce
*   Lime juice, to taste
*   Sriracha, to taste

_FOR THE ROLLS:_

*   1 block (about 7 ounces) rice vermicelli
*   ¼ teaspoon salt
*   8 to 12 rice-paper wrappers for summer rolls, about 8 1/2 inches wide
*   8 sprigs mint, leaves picked off
*   8 sprigs Thai basil or basil, leaves picked off
*   Pickled carrots and daikon (do chua), homemade or fresh or jarred from Vietnamese markets
*   ¾ to 1 pound barbecued beef brisket, thickly sliced
*   ½ cup thinly sliced red cabbage
*   16 sprigs cilantro
*   Whole lettuce leaves

## Preparation

_MAKE THE BARBECUE MAYONNAISE:_ Combine mayonnaise and barbecue sauce, and stir until smooth. Season to taste with lime juice and sriracha, and set aside. (Can be made 1 day ahead and refrigerated.)

MAKE THE ROLLS: Put the rice vermicelli in a large bowl. Cover with boiling water, add the salt and let soak, swishing occasionally, until soft but still resilient, 3 to 5 minutes. Drain in a colander and rinse under cold water until the water runs clear. Let drain for at least 30 minutes, or spread on a plate and refrigerate for up to 2 hours.

When ready to serve, set out all the ingredients on a clean surface. Half-fill a bowl wide enough to fit the wrappers with lukewarm water. Place 1 wrapper in the water and pat it, gently bending to test, until pliable but not completely soft. Shake off excess water and lay the wrapper down.

Place a straight line of mint and basil leaves across the circle, about 2 inches up from the bottom edge. Plump up that line with a small clump of vermicelli, a pinch of do chua and 1/8 of the brisket. Drizzle a scant teaspoon of barbecue mayonnaise over the meat. Finish with red cabbage and 2 cilantro sprigs. Bring bottom edge of wrapper tightly up over the filling, then fold the sides in to cover. Continue to roll upward, tightly but gently, and place finished rolls on a plate, seam-side down. Repeat with remaining rolls. Cover rolls with lettuce leaves to keep them fresh. Serve as soon as possible.

_Julia Moskin, The New York Times_
