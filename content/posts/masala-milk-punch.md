---
title: "Masala Milk Punch"
author: "pencechp"
date: "2014-01-15"
categories:
  - cocktails
tags:
  - brandy
  - rum
  - untested
  - whiskey
---

## Ingredients

*   9 black tea bags
*   2 oz. ground masala spice blend
*   2 oz. freshly grated ginger
*   3/4 oz. rye
*   3/4 oz. cognac
*   3/4 oz. rum
*   2 oz. whole milk

## Preparation

Simmer the first three ingredients in 4 cups of water for 20 minutes.  Strain into a liquid measuring cup, then add an equal amount of sugar to make a 1:1 syrup.  Stir until dissolved.

For each drink, fill a 9-oz mug with boiling water.  In a separate glass, combine 1/2 oz. masala tea syrup with the rye, cognac, rum, and milk.  When the mug is hot, discard the water, pour the contents of the glass into the bug, and top with more boiling water.  Garnish with freshly grated cinnamon and nutmeg.

_Sugar House Detroit, via Spirit Magazine_
