---
title: "Raspberry Bars"
date: 2021-01-09T22:32:01+01:00
categories:
  - dessert
tags:
  - raspberry
  - untested
---

## Ingredients

- 1 cup butter, melted
- 1/2 cup sugar
- 2 teaspoons almond or vanilla extract or 1 teaspoon of each
- 1/2 teaspoon salt
- 2 cup flour
- 1 cup raspberry preserves
- 1 cup fresh raspberries divided
- 8 tablespoons butter, chilled
- 1/2 cup brown sugar
- 1/2 cup old fashioned rolled oats
- 3/4 cup flour
- 1/4 teaspoon salt
- 1 cup powdered sugar
- 1/2 teaspoon vanilla or almond extract
- 3-6 tablespoons milk, as needed

## Preparation

Preheat oven to 350 degrees and line an 8x8 inch square baking pan with foil or parchment paper.

In a large bowl combine melted butter, sugar, and extract. Stir together.
Add salt and flour and stir. Press dough into prepared pan and bake for 10 minutes.

Stir and gently mash half of the fresh raspberries into the raspberry preserves. Spread filling onto prepared crust. Top with remaining fresh raspberries.

Dice the chilled butter into small cubes. In a medium bowl stir together brown sugar, oats, flour, and salt. Add butter and mash with a fork (or pulse in a food processor!) until coarse crumbs form. Sprinkle over filling.

Bake for 25 minutes. Remove from oven and allow to cool before cutting into bars. While cooling, prepare the glaze by stirring together the powdered sugar, vanilla, and enough milk to make the glaze easily pourable. Drizzle over bars and serve.

_Creme de la Crumb_
