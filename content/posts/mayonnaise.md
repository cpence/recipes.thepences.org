---
title: "Mayonnaise"
author: "pencechp"
date: "2018-12-26"
categories:
  - side dish
tags:
  - belgian
  - sauce
  - untested
---

## Ingredients

*   4 eggs
*   1 tbsp. white vinegar
*   1 tbsp. lukewarm water
*   2 tbsp. sharp mustard
*   4 dl peanut oil or fine salad oil
*   salt and pepper

## Preparation

Put egg yolks in a bowl and beat them for a moment. Add the vinegar, water, and mustard. Whisk (or use a mixer) briefly. Whisk in the oil in a stream. Taste and season with salt and pepper. If the mayo is too thick, you can add extra vinegar, lemon juice, or water.

_Dagelijkse kost, translated_
