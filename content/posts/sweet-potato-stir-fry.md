---
title: "Sweet Potato Stir Fry"
author: "pencechp"
date: "2017-10-30"
categories:
  - main course
  - side dish
tags:
  - chinese
  - pork
  - sweetpotato
  - untested
---

## Ingredients

*   2 or 3 large cloves garlic
*   2-inch piece fresh ginger root
*   3 scallions
*   1 pound sweet potato
*   1 tablespoon Shaoxing wine or dry sherry
*   1 tablespoon low-sodium soy sauce or tamari
*   1 teaspoon Worcestershire sauce
*   ½ teaspoon sugar
*   6 ounces ground pork
*   1 tablespoon canola or grapeseed oil
*   Pinch crushed red pepper flakes

## Preparation

Mince enough of the garlic to yield 1 tablespoon. Peel and mince the ginger to yield 1 tablespoon. Separate the scallion whites and greens; chop each. Peel the sweet potato, and either grate it in a food processor (cut into chunks first), cut it into matchsticks or use a spiralizer.

Whisk together the wine or dry sherry, the soy sauce or tamari, the Worcestershire sauce and sugar in a medium bowl. Add the pork and stir with a fork to incorporate.

Heat a wok or large, well­-seasoned cast-iron skillet over high heat. Drizzle in the oil so that it coats the sides of the wok. Working quickly, add the garlic, ginger and scallion whites; stir-fry for 5 seconds, then add the pork mixture and crushed red pepper flakes. Stir-fry for 3 to 4 minutes, until the pork is cooked through.

Add the sweet potato; stir-fry for about 6 minutes and try to create some crisped edges on it, if possible. Some of the sweet potato pieces will still be somewhat firm. Remove from the heat.

Divide among individual bowls. Garnish each portion with the scallion greens. Serve hot.

_Washington Post_
