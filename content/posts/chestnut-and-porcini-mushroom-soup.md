---
title: "Chestnut and Porcini Mushroom Soup"
author: "pencechp"
date: "2018-05-02"
categories:
  - main course
  - side dish
tags:
  - chestnut
  - mushroom
  - soup
---

## Ingredients

*   400 g. chestnuts
*   2 lg shallots, minced
*   1 carrot, diced
*   1 celery stalk, diced
*   2 tbsp. butter
*   1/2 c. dried porcini mushrooms, rinsed 
*   4 c. chicken broth
*   1/2 c. heavy cream
*   salt and pepper

## Preparation

Place the dried porcinis in hot water to steep. In a 2-qt sauce pan, melt the butter over medium heat. Add the carrot, celery, and shallot. Cook vegetables until soft. Coarsely chop the chestnuts and add them to the pan. Add the porcini and the steeping liquid (being careful not to add any dirt at the bottom of the bowl).

Add broth and bring to a simmer. Simmer until all ingredients are fork-tender, 15–25 minutes. Remove from heat and homogenize with an immersion blender. Return to low heat, add cream, and season to taste.

_D'Artagnan_
