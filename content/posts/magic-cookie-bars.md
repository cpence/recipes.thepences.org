---
title: "Magic Cookie Bars"
author: "pencechp"
date: "2010-05-08"
categories:
  - dessert
tags:
  - pence
---

## Ingredients

*   1 1/2 c. graham cracker crumbs
*   1/2 c. (1 stick) melted butter
*   1 14-oz can. sweetened condensed milk
*   1--2 c. (6--12 oz.) semi-sweet chocolate chips
*   1 1/3 c. flaked coconut
*   1 c. chopped nuts

## Preparation

Preheat oven to 350 (or to 325 for a glass baking dish).  In a small bowl, combine graham cracker crumbs and butter; mix well.  Press crumb mixture firmly on bottom of 13x9-inch baking pan.  Pour sweetened condensed milk evenly over crumb mixture.  Layer evenly with remaining ingredients; press down firmly with fork.  Bake 25 minutes or until lightly browned.  Cool and cut into bars.

_Eagle Brand Sweetened Condensed Milk_
