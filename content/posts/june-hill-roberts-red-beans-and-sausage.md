---
title: "June Hill Robert's Red Beans and Sausage"
author: "pencechp"
date: "2010-04-02"
categories:
  - main course
tags:
  - bean
  - cajun
  - pence
  - sausage
  - vegan
  - vegetarian
---

## Ingredients

*   1 lb. kidney beans
*   1 lg. onion
*   1 green bell pepper
*   2 cloves garlic
*   2 bay leaves
*   1/2 tsp. dried thyme
*   1 lb. hot cured sausage
*   salt and pepper
*   28 oz. can diced tomatoes
*   rice, for serving

## Preparation

"Fast-soak" kidney beans. Chop onion, bell pepper, garlic.

Drain beans, and refill pot with hot water to cover beans by 2". Bring to a low
boil.

Add 1/3 each of onion, bell pepper and garlic. Also add bay leaves and thyme.
Simmer until beans are almost soft, about an hour.

Meanwhile, back at the ranch, cook sausage and slice, or slice and cook,
depending on what kind of sausage you have.

When beans are tender, add salt (it will take a lot - I add it until the broth
tastes slightly too salty, and then often have to add more later), about 1/2 of
the a can of tomatoes (with juice), ground pepper, and the remaining veggies.

Simmer until thick, adjusting seasonings and adding more tomato, if desired.

When nearly done, add sausage and cook about 10 minutes.

Serve over rice.

You could make this without adding the sausage (and serve it as a garnish for
those who might want to add it) and it would be vegan.
