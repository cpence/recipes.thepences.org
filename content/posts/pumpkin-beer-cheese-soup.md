---
title: "Pumpkin Beer Cheese Soup"
author: "pencechp"
date: "2014-12-02"
categories:
  - main course
tags:
  - beer
  - cheese
  - pumpkin
  - soup
  - untested
---

## Ingredients

*   2 pumpkins, about 2 pounds each
*   Cooking oil spray
*   2 tablespoons olive oil
*   2 large carrots, peeled and thickly sliced
*   2 celery stalks, thickly sliced
*   3 shallots, chopped
*   1/2 teaspoon kosher salt
*   1/2 teaspoon ground black pepper
*   2 garlic cloves, minced
*   1 tablespoon minced thyme
*   2 teaspoons minced fresh sage
*   1 teaspoon minced fresh ginger
*   1/2 teaspoon smoked paprika
*   1/4 teaspoon ground nutmeg
*   3 ½ cups chicken broth
*   2 (12-ounce) bottles pale ale
*   4 tablespoons unsalted butter
*   1/3 cup all-purpose flour
*   2 cups whole milk
*   4 cups shredded sharp cheddar cheese
*   Croutons or pumpkin seeds for garnish (optional)

## Preparation

Position 1 rack in the upper third of an oven and 1 rack in the lower third; preheat to 425°F. Line 2 rimmed baking sheets with foil and set aside.

Quarter the pumpkins, scrape out the fibers and the seeds, and spray with cooking oil. Place pumpkins, cut side down, on baking sheets. Roast until tender and the rinds pull away from the flesh, 45 minutes.

Remove from oven and peel rinds off the flesh with tongs. Discard rinds, and transfer flesh to a bowl.

In a large stockpot over medium-high heat, warm olive oil until shimmering. Add carrots, celery, shallots, salt, and pepper. Sauté until vegetables begin to soften, about 7 minutes. Add garlic, thyme, sage, ginger, paprika, and nutmeg; cook, stirring frequently, for 1 minute. Add pumpkin flesh, broth, and 1 ½ bottles of Denver Pale Ale (drink the rest). Bring to a boil, cover, reduce heat to medium-low, and simmer for 15 minutes.

In a medium pot, melt the butter over medium heat. Whisk in flour and continue to whisk constantly for 1 minute. Gradually whisk in milk until smooth and thick. Add cheese 1 cup at a time, stirring until smooth each time. Reduce heat to low until pumpkin mixture is ready.

Pour cheese mixture into pumpkin mixture. Using a stick blender, puree the soup until smooth. Or puree in batches in a blender.

Ladle 2 cups soup into 6 bowls and garnish with croutons and pumpkin seeds. Serve with more beer.

_Thrillist_
