---
title: "Build-a-Spritz"
date: 2020-06-06T13:57:50+02:00
categories:
    - cocktails
tags:
    - champagne
    - suze
    - orange
---

## Ingredients

*   1 1/2 ounces Suze
*   1 ounce club soda
*   1 ounce fresh orange juice
*   3 ounces champagne or dry sparkling wine
*   Orange wheel and sage sprig (for serving)

## Preparation

Combine Suze, club soda, and orange juice in an ice-filled rocks glass. Stir
gently to combine. Top with champagne; garnish with an orange wheel and a sage
sprig.

*Epicurious*

