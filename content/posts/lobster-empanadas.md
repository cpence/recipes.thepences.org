---
title: "Lobster Empanadas"
author: "pencechp"
date: "2013-05-12"
categories:
  - appetizer
  - main course
tags:
  - argentinian
  - crawfish
  - lobster
  - untested
---

## Ingredients

_Pastry_

*   1/4 pound unsalted butter, at room temperature
*   4 ounces cream cheese, at room temperature
*   2 cups unbleached flour
*   1/2 teaspoon baking powder
*   1 teaspoon ground cumin
*   1/4 teaspoon cider vinegar
*   6 to 8 tablespoons very cold water
*   1 tablespoon vegetable oil

_Filling_

*   6 ounces fresh lobster or langostino meat, cooked, shelled and roughly chopped (or saltwater or fresh water crayfish)
*   1/2 cup alioli or homemade mayonnaise
*   1 tablespoon tomato purée
*   1 tablespoon cognac
*   1 teaspoon Dijon mustard
*   1/3 cup finely chopped scallions, including green part
*   1 teaspoon coarse kosher or sea salt
*   1/4 teaspoon white pepper
*   2 to 3 hard-boiled eggs, cut lengthwise into 8 pieces
*   16 to 20 pimiento-stuffed green olives
*   1 large egg, beaten
*   2 tablespoons water

## Preparation

_Pastry Directions_

Using a fork, combine the butter and cream cheese. Sift the flour over the mixture. Add baking powder, cumin, vinegar, and water and combine with a fork. Flour hands generously and work the dough until it becomes a smooth, elastic ball, 3 to 4 minutes. Flour both the work surface and rolling pin. Roll out the dough to about 1/8 inch thick and cut it into 6-inch circles for main course-sized empanadas. For appetizer-sized empanaditas, cut the dough into 3-inch circles. Cover the pastries with a lightly floured towel until you are ready to fill them.

_Filling Directions_

Combine the lobster, mayonnaise, tomato purée, cognac, mustard, scallions, salt, and pepper and mix thoroughly. Set the filling aside and keep it cool until you are ready to fill the empanadas.

_Assembly Directions_

Preheat the oven to 400°F. Oil a large cookie sheet or line it with Silpat. Spoon the filling in the center of the pastry circles — about 2 tablespoons of filling for each main course empanada or 1 heaping teaspoon for empanaditas. Stuff an egg slice and 1 or 2 olives in the filling and fold the dough over the filling forming a half-moon. Beat the egg with the water to make a glaze. Moisten the open edges with the glaze and crimp the edges with a fork or your fingers to seal.

Place the empanadas on the cookie sheet and brush the top of each with the glaze. Bake them for 20 minutes, turn the heat down to 350°F and continue baking for about 5 minutes more or until the empanadas are golden. When the empanadas are done, remove them from the oven and cool them on racks for several minutes. Serve warm or at room temperature.

To freeze and reheat, place frozen cooked empanadas in a preheated 350°F oven for approximately 20 minutes.

_Shirley Lomax Brooks, Epicurious_
