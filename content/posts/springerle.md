---
title: "Springerle"
author: "pencechp"
date: "2010-05-08"
categories:
  - dessert
tags:
  - cookie
  - philip
---

## Ingredients

*   5 eggs
*   1 lb powdered sugar
*   1/2 tsp anise oil
*   1/2 tsp lemon extract
*   1 lb flour (plus more for rolling)
*   2 tsp baking powder

## Preparation

Beat 5 eggs until smooth.  Add 1 lb. of sugar, and beat in mixer approximately 25 minutes (until you can't feel the sugar).  Add 1/2 tsp. anise oil and 1/2 tsp. lemon extract.

Fold in 1 lb. flour (4 c. sifted) with 2 tsp. baking powder.  Roll out 1/2" thick and press with a springerlie board or roller.  Let stand overnight.  Bake for 15 minutes at 350 degrees.  Makes 1 lb, about 6 dozen.
