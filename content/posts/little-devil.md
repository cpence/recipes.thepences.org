---
title: "Little Devil"
author: "pencechp"
date: "2010-05-09"
categories:
  - cocktails
tags:
  - gin
  - rum
---

## Ingredients

*   3/4 oz. gin
*   3/4 oz. light rum
*   1/4 oz. Cointreau or Triple Sec
*   1/4 oz. lemon juice
*   3 or 4 ice cubes

## Preparation

Combine, stir, and strain.
