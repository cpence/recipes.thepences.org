---
title: "Vol au vent"
author: "pencechp"
date: "2018-12-26"
categories:
  - main course
tags:
  - belgian
  - chicken
  - stew
---

## Ingredients

*   1 chicken
*   100g celery (4 or 5 ribs), in large pieces
*   1 onion, quartered
*   1 tbsp thyme (or a handful of fresh thyme branches)
*   1 bay leaf
*   5 tbsp butter, divided
*   200g (1/2 lb.) white mushrooms, in pieces (around 1cm or 1/2")
*   1 leek, minced
*   1 or 2 tbsp. fresh lemon juice
*   200g (1/2 lb.) ground veal
*   5 tbsp all-purpose flour
*   200 mL (1 c.) cream
*   2 egg yolks
*   Freshly ground pepper and salt
*   4 round puff pastries (bouchées à la reine)
*   Parsley

## Preparation

Place the chicken in water with the celery, onion, thyme, and bay leaf. Season
heavily with salt and pepper. Let it simmer for around an hour, removing the
foam regularly.

In a separate pan, cook the mushrooms and the leek with 1 tbsp. of the butter
and the lemon juice. Season heavily with salt and pepper (this mixture should be
a bit overseasoned, by itself), and add some of the stock from the chicken pot
if they start to dry out.

Form the ground veal into meatballs about the size of marbles (around 1/2" or
1cm, maybe a little bigger), and cook them for two or three minutes in boiling
salted water. Plunge them into cold water to stop their cooking.

When the chicken is done, bone it and cut it into pieces. Sieve the chicken
broth and discard the solids. Make a roux with the remaining 4 tbsp. of butter
and the flour. Slowly add the chicken broth until you get a slightly thick,
smooth sauce, the consistency of a quite thick gravy. (It should take around 3
or 4 cups, 75 or 100 cL; it should also be a bit too thick, as you'll thin it
with the cream in a moment.) Cook the sauce for a bit.

Add the chicken, the meatballs, and the mushroom mixture. Let that simmer for a
second to blend flavors. Add half of the cream and bring back to a simmer. Mix
the egg yolks with the remaining cream, and add this to the sauce. Warm it just
until it thickens, but do not let it boil.

Serve over the puff-pastry rounds, garnishing with parsley.

_visit.brussels, edited heavily_
