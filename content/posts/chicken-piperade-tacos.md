---
title: "Chicken Piperade Tacos"
author: "pencechp"
date: "2017-03-30"
categories:
  - main course
tags:
  - chicken
  - mexican
  - spanish
---

## Ingredients

_PIPERADE PEPPER STEW_

- 4 red bell peppers
- 3 green bell peppers
- 1 yellow pepper
- 2 tablespoons canned chipotle peppers in adobo sauce
- 1 jalapeño, seeded and diced

_CHICKEN TINGA_

- 1 chicken breast
- 1/2 onion
- 1 garlic clove
- Bay leaves
- 8 tablespoons extra virgin olive oil
- 2 garlic cloves, minced
- 1 large yellow onion, julienned
- 1 (16-ounce) can diced tomatoes
- fresh thyme
- salt and pepper

## Preparation

Pre-heat oven to 425F. Cut peppers in half, place on foil lined baking sheet.
Roast until blackened, about 15 minutes. Wrap up in foil and lets steam for 15
minutes. Peel and slice.

Bring water to a boil, add chicken breast, ½ onion, 1 garlic clove & couple bay
leafs. Cook 15 minutes or until chicken is done. Cool and shred. Add olive oil
to a large skillet. Add yellow onions & garlic let it slowly sweat for 20
minutes. Add peppers, canned tomatoes, thyme, bay leaves & chipotle pepper (to
taste). Add chicken, jalapeno and salt and pepper. Serve as taco filling.

_Edible Nashville_
