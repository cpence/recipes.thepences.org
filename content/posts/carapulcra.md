---
title: "Carapulcra"
author: "pencechp"
date: "2012-08-12"
categories:
  - main course
tags:
  - chicken
  - peruvian
  - pork
  - potato
  - stew
---

## Ingredients

*   1/2 kg. (~1 lb.) dried yellow potatoes (amber in color)
*   3 tbsp. butter or oil
*   1 red onion, chopped finely
*   2 tbsp. minced garlic
*   pepper (to taste)
*   cumin (to taste)
*   1/2 kg. (~1 lb.) pork (shoulder or belly), cubed
*   1/2 kg. (~1 lb.) chicken, cubed
*   2 tbsp. aji panca paste (or much less of rocoto paste)
*   1 tbsp. aji amarillo paste
*   2 small cloves
*   1 cinnamon stick
*   2 tbsp. port
*   2 tbsp. Pisco \[CP: can sub tequila, in a pinch\]
*   1/2 c. unsalted, roasted peanuts, ground medium-fine in a spice grinder
*   1 c. chicken broth
*   6 butter or vanilla cookies, crushed
*   1 piece dark chocolate (or drinking chocolate), grated

If you don't have aji panca paste, it is much milder than either aji amarillo or
rocoto; for instance, a good replacement for 1 tbsp. aji amarillo + 2 tbsp. aji
panca is around 1 1/2 tbsp. aji amarillo, or perhaps 2 tbsp. for a spicier
dish. (I haven't tried the rocoto substitution yet.)

## Preparation

Wipe off the dried potatoes with a damp cloth, and toast them in a pan without
oil for several minutes until they change to a dark golden color. Cover them
with water and let stand overnight (or at least two hours). Drain, and wash them
several times, until the water runs clear.

Fry the red onion in the oil/butter, until it is almost golden. Add the garlic,
pepper, cumin, pork, chicken, aji pastes, cloves, and cinnamon stick. Let all
brown and then cover with water and cook for around 10 minutes. Add the drained
dried potatoes and boil until the potatoes are almost tender, about 40-60
minutes.

Add the port, Pisco, peanuts, broth, and butter/vanilla cookies (in pieces small
enough that they dissolve when cooked). Cook for 15 or 20 minutes over a low
heat. Stir occasionally until the potatoes are cooked. Add the chocolate, and
let stand before serving. Serve with white rice.

_Translated by me from Bibliotecas Virtuales_
