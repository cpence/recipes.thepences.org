---
title: "Broccolini with Smoked Paprika, Almonds, and Garlic"
author: "pencechp"
date: "2011-03-07"
categories:
  - side dish
tags:
  - broccoli
  - spanish
---

## Ingredients

*   3 tablespoons extra-virgin olive oil, divided
*   1/2 cup whole almonds, coarsely chopped
*   3 large garlic cloves, chopped
*   1 1/2 teaspoons smoked paprika (or pimentón de la vera)
*   Coarse kosher salt
*   2 pounds broccolini, rinsed, stalks cut into 2- to 3-inch lengths
*   1/3 cup water
*   1 to 2 teaspoons Sherry wine vinegar

## Preparation

Heat 1 tablespoon oil in heavy large skillet over medium-high heat. Add almonds. Stir until lightly browned, 2 to 3 minutes. Add garlic and paprika. Sprinkle with coarse salt; sauté 1 minute. Transfer to small bowl. Add remaining 2 tablespoons oil to skillet. Add broccolini; sprinkle with coarse salt. Add 1/3 cup water. Cover and boil until crisp-tender and still bright green, about 4 minutes. Pour off any water. Stir in almond mixture. Season to taste with coarse salt and pepper. Mix in 1 to 2 teaspoons vinegar. Transfer broccolini to bowl and serve.

_Bon Appétit, November 2009_
