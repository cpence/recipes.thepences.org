---
title: "Hot Cocoa"
author: "pencechp"
date: "2010-07-13"
categories:
  - dessert
tags:
  - chocolate
  - dutch
---

## Ingredients

*   1 quart milk
*   5 teaspoons sugar
*   1/2 teaspoon red pepper
*   3.5 ounces cocoa powder

## Preparation

Pour the milk, sugar, and pepper in a medium-size saucepan. Keep stirring to avoid burning the bottom until almost cooked. Put the cocoa powder in a big bowl and add one cup of the hot milk. Mix the milk and cocoa powder. Add the mix to the rest of the milk in the pan and keep stirring. Stir until it starts cooking and thickens. Pour the hot chocolate into cups and enjoy.

Makes 4 mugs.

_Kees Raat, via National Geographic_
