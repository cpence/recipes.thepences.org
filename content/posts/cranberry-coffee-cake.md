---
title: "Cranberry Coffee Cake"
author: "pencechp"
date: "2010-12-17"
categories:
  - breakfast
tags:
  - cake
  - cranberry
  - pecan
  - untested
---

## Ingredients

*   8 oz. cream cheese
*   1 1/2 c. sugar
*   1 c. butter, softened
*   1 1/2 tsp. vanilla
*   4 eggs
*   2 1/4 c. flour, divided
*   1 1/2 tsp. baking powder
*   1/2 tsp. salt
*   1/2 tsp. cinnamon
*   1/4 tsp. nutmeg
*   2 1/2 c. fresh or thawed frozen cranberries
*   1/2 c. pecans, coarsely chopped
*   1/4-1/2 c. powdered sugar

## Preparation

Preheat the oven to 350.  Grease a 10-inch tube or large fluted tube pan and set aside.  In a large mixing bowl, beat together the cream cheese, sugar, butter, and vanilla until well mixed.  Beat in the eggs, one at a time.  In a small bowl, sift together 2 cups of the flour, the baking powder, salt, cinnamon, and nutmeg.  Add the dry ingredients to the batter, one third at a time, mixing well after each addition.  In another small bowl, toss the cranberries and pecans with the remaining flour until well coated.  Stir gently into the batter.  Pour into the pan and bake at 350 for 55-65 minutes, until the cake is lightly browned.  Do not over-bake.  Let stand about 15 minutes before removing from the pan.  Sift powdered sugar lightly over the warm cake.

_Penzey's Spices_
