---
title: "Cheese on Crackers"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - main course
tags:
  - cheese
  - philip
---

## Ingredients

*   ½ c butter (1 stick)
*   ½ c flour
*   8 c milk
*   2 small packages American cheese, torn into pieces
*   saltines

## Preparation

Melt the butter, add flour and cook for a few minutes until light brown. Gradually add the milk, stirring continuously to avoid the formation of lumps. Bring to a boil. Add the cheese in increments, allowing one addition to melt before adding the next. Cook until all cheese has melted and the cheese sauce is smooth. Place saltines on a plate and ladle cheese sauce over them. Serve immediately.
