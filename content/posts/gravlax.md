---
title: "Gravlax"
author: "pencechp"
date: "2017-09-17"
categories:
  - main course
tags:
  - salmon
---

## Ingredients

*   2 tbsp. white peppercorns
*   1 tbsp. fennel seeds
*   1 tbsp. caraway seeds
*   2⁄3 cup kosher salt
*   1⁄3 cup sugar
*   2 lb. center-cut, skin-on salmon filet
*   1 cup dill sprigs, plus 1⁄3 cup chopped dill
*   1⁄4 cup aquavit (optional)

## Preparation

In a small food processor, pulse peppercorns, fennel seeds, and caraway seeds until coarsely ground; combine with salt and sugar. Stretch plastic wrap over a plate; sprinkle with half the salt mixture. Place salmon filet on top, flesh side up. Cover with remaining salt mixture, dill sprigs, and aquavit.

Fold plastic wrap ends around salmon; wrap tightly with more plastic wrap. Refrigerate the fish on the plate for 48-72 hours, turning the package every 12 hours and using your fingers to redistribute the herb-and-spice-infused brine that accumulates as the salt pulls moisture from the salmon. The gravlax should be firm to the touch at the thickest part when fully cured.

Unwrap salmon, discarding the spices, dill, and brine. Rinse the filet under cold running water and pat dry with paper towels. Cover a large plate with the chopped dill. Firmly press the flesh side of the gravlax into the dill to coat it evenly.

Place gravlax skin side down on a board. With a long, narrow-bladed knife (use a granton slicer if you have one; the divots along the blade make for smoother, more uniform slices), slice gravlax against grain, on the diagonal, into thin pieces. Serve with mustard-dill sauce or on knackebrod with minced onion. Refrigerate any remaining gravlax, wrapped in plastic wrap, for up to 2 weeks.

_Saveur_
