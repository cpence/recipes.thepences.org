---
title: "Cantonese Lettuceburgers"
author: "pencechp"
date: "2011-05-18"
categories:
  - main course
tags:
  - chinese
  - rice
---

## Ingredients

*   Iceberg, red or romaine lettuce leaves, washed and chilled
*   Fried rice \[Mom: I've been making Yangzhou Fried Rice (Epicurious, 7/08)\]
*   Bamboo shoots
*   Snow peas
*   One large pork chop
*   1 tbsp. soy sauce
*   1 tbsp. mirin, rice wine, sherry, etc.
*   1 tsp. sugar
*   2 cloves minced garlic
*   6 oz. mushrooms, sliced thinly

## Preparation

Julienne bamboo shoots and snow peas. Thinly slice pork and marinate in soy, wine, and sugar. Stirfry all together with bamboo shoots and mushrooms. Serve in wraps with fried rice.
