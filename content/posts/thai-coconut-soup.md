---
title: "Thai Coconut Soup"
author: "pencechp"
date: "2017-05-02"
categories:
  - main course
tags:
  - coconut
  - pork
  - soup
  - thai
  - untested
---

## Ingredients

*   1 tsp. vegetable oil
*   3 stalks lemon grass, sliced thin
*   3 large shallots, chopped
*   8 sprigs fresh cilantro leaves, chopped
*   3 tbsp. fish sauce
*   4 c. chicken broth
*   2 14 oz. cans coconut milk
*   1 tbsp. sugar
*   ½ lb. white mushrooms, cut into 1/4" slices
*   1 lb. pork tenderloin, halved lengthwise, sliced into 1/8" thick pieces
*   3 tbsp. fresh lime juice
*   2 tsp. red curry paste

## Preparation

Heat oil in large saucepan over medium heat. Add lemon grass, shallots, cilantro, and 1 tbsp. fish sauce; cook, stirring frequently, until just softened, 2 to 5 minutes. Stir in chicken broth and 1 can coconut milk; bring to simmer. Cover, reduce heat to low, and simmer 10 minutes. Strain broth, return to pan.

Return pan to medium-high heat. Stir remaining can coconut milk and sugar into broth mixture and bring to simmer. Reduce heat to medium, add mushrooms, and cook until just tender, 2 to 3 minutes. Add pork and cook, stirring constantly, until no longer pink, 1 to 3 minutes. Remove soup from heat.

Combine lime juice, curry paste, and remaining 2 tablespoons fish sauce in small bowl; stir into soup.
