---
title: "Grilled Lobster Paella"
author: "pencechp"
date: "2011-09-03"
categories:
  - main course
tags:
  - spanish
---

## Ingredients

*   1/2 cup olive oil
*   3/4 lb. Spanish chorizo, sliced into 1/2"-thick rounds
*   6 stalks green garlic, thinly sliced
*   2 leeks, chopped white and light-green parts
*   1 tbsp. smoked paprika
*   2 1/2 c. short-grain rice (bomba, Valencia, calasparra)
*   1/4 tsp. saffron threads
*   7 c. hot seafood or chicken stock
*   kosher salt
*   3 1-1 1/4 lb. lobsters, halved lengthwise, claws cracked
*   2 c. shelled peas or frozen peas, thawed
*   1/2 c. parsley lemons

## Preparation

Prepare a hot fire in a charcoal grill.  Let burn down to red\-hot coals; rake to edge of grill.  (For backup, start a second round of coals in a charcoal chimney on pavement nearby.)  Put a 16"-18" paella pan on grill grate; heat olive oil.  Add chorizo, green garlic, and leeks; cook until golden, 3-4 minutes.

Add smoked paprika and rice; cook, stirring often, until rice is coated, 2 minutes.  Add saffron to the stock, and add the stock to the pan and season to taste with salt; stir to distribute ingredients.  Let cook, undisturbed, until stock simmers and rice beings to absorb liquid, about 10 minutes.  Rotate pan every 2-3 minutes to cook evenly.

Arrange lobsters over the rice.  Continue cooking, rotating the pan often, as the rice swells and absorbs the stock.  Add more coals from the chimney to maintain even heat under the pan.  Cook until the rice is almost tender and the lobster is cooked through, about 10 more minutes.

Scatter peas on top.  (If the liquid evaporates before the rice is tender, add more hot stock.)  Cook without stirring, allowing the rice to absorb all of the liquid, so that a crust develops on the bottom and the edges begin to dry out and get crusty, 5-10 minutes, for a total cooking time of about 40 minutes.

Remove pan from grill.  Cover with large clean kitchen towels and let rest for 5 minutes.  Garnish with parsley and serve with halved lemons, making sure to scrape some of the crust from the bottom of the pan onto each plate.

_Bon Appetit, July 2011_
