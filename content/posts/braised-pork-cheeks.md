---
title: "Braised Pork Cheeks"
author: "pencechp"
date: "2014-02-24"
categories:
  - main course
tags:
  - italian
  - pasta
  - pork
---

## Ingredients

*   1/4 cup olive oil
*   3 pounds pork cheeks, silver skin, fat, and cartilage removed
*   4 celery stalks, roughly chopped
*   2 medium carrots, roughly chopped
*   1 medium yellow onion, quartered
*   1 leek, white part only, sliced
*   1 head garlic, cut in half
*   3 tablespoons honey
*   2 cups red wine
*   1 bay leaf
*   4 sprigs fresh thyme
*   6 cups veal or pork stock
*   Salt and pepper to taste
*   2 bunches baby mâche _or_ 28 oz. can diced tomatoes

## Preparation

Preheat the oven to 350ºF. Heat the olive oil in a large Dutch oven over medium heat. Add the pork cheeks and sear until well caramelized on all sides, about 4 minutes each side. Remove from the pot and set aside. Add the celery, carrots, onions, leeks, and garlic and sauté until tender and browned, about 10 minutes. Add the honey and mix well. Return the pork to the pot and stir. Add the red wine, bay leaf, and thyme. Bring the liquid to a simmer and reduce by half, about 15 minutes. Add the stock and return to a gentle simmer. Cover the pot, transfer to the oven, and braise until the meat is very tender, about 2 1/2 hours.

Remove the pot from the oven and let stand at room temperature for 20 minutes. Remove the pork from the pot and set aside. Strain the braising liquid into a medium saucepan and bring to a simmer. Simmer until the liquid has reduced by a little more than half, about 10 minutes. Skim the fat from the top. Return the pork to the liquid and season with salt and pepper to taste.

_Alternatively:_ After removing the pork from the pot, do not strain the braising liquid (remove the thyme sprigs and bay leaves). Add a 28-oz. can of diced tomatoes and reduce until it forms a proper tomato sauce. Shred the pork cheeks and return them to the sauce, and serve over pasta.

_Adapted from James Beard Foundation_
