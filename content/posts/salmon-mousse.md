---
title: "Salmon Mousse"
author: "pencechp"
date: "2019-08-09"
categories:
  - appetizer
  - main course
tags:
  - fish
  - pence
  - salmon
---

## Ingredients

(*amounts in brackets are scaled for a 515 mL European can of tomato soup*)

*   1 lb. [750 g] fresh salmon (or canned)
*   1 can concentrated tomato soup (10.75 oz) [515 mL]
*   8 oz. [360 g] cream cheese
*   2 envelopes gelatine (1/2 oz., around 14g) [26g, 2 European packets]
*   1 [2] onion, chopped fine
*   1 [2] celery stalk, chopped fine
*   1 tbsp. [5 tsp.] Worcestershire sauce
*   1 c. [350 g] mayonnaise
*   2 [4] dashes Tabasco

## Preparation

If using fresh salmon, bake the salmon until it flakes easily; cool.

Heat the tomato soup (undiluted) in a large pot, melt in the cream
cheese. Dissolve the gelatine in 1/2 c. of cold water. Add to the soup, and
cool.

Add in the remainder of the ingredients, pour into a serving dish, and
refrigerate until set. Serve with black or French bread.
