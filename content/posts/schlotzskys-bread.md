---
title: "Schlotzskys Bread"
date: 2020-04-11T13:52:17+02:00
categories:
    - bread
---

## Ingredients

*   1/2 cup warm water
*   1 packet quick rise active dry yeast (2 1/4 teaspoons)
*   1 tablespoon granulated sugar
*   3/4 cup warm milk
*   1/2 teaspoon salt
*   1/4 teaspoon baking soda
*   2 1/2 cups all-purpose flour
*   1 tablespoon cornmeal
*   1 tablespoon melted butter

## Preparation

In a medium-sized bowl, gently whisk together the yeast, warm water, and sugar. Let it stand for 5 minutes until bubbly.

Whisk in the warm milk, baking soda, salt, and 1 cup of the flour until smooth. Then mix in the remaining flour.

Spray two 9-inch cake pans (or pie pans) with nonstick cooking spray and dust the bottoms with cornmeal. Divide the dough between the pans and spread it out a little. The yeast will do the rest. Spray plastic wrap with nonstick cooking spray and lightly cover each pan. Proof (let rise) in a 150-175 degree oven (or a warm, draft-free place) for 1 hour.

Preheat the oven to 375 degrees F. Remove the plastic wrap and bake for 20 minutes, or until golden brown. Brush the tops of bread lightly with melted butter and cool in pans completely.

Dust off the loose cornmeal. Slice the bread loaves in half horizontally.

The Schlotzsky's "original" has yellow mustard, garlic/Caesar dressing, cheddar cheese, grated Parmesan, ham, salami, lettuce, tomato, and lots of sliced black olives.

*A Spicy Perspective*

