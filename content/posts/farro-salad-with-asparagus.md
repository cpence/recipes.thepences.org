---
title: "Farro Salad with Asparagus"
author: "pencechp"
date: "2015-11-01"
categories:
  - main course
  - side dish
tags:
  - asparagus
  - farro
  - salad
  - untested
---

## Ingredients

*   6 oz. asparagus, trimmed and cut into 1-inch lengths
*   6 oz. sugar snap peas, strings removed, cut into 1-inch lengths
*   Salt and pepper
*   3 tbsp. extra-virgin olive oil
*   2 tbsp. lemon juice
*   2 tbsp. minced shallot
*   1 tsp. Dijon mustard
*   1 1/2 c. farro, cooked to package directions, cooled
*   6 oz. cherry tomatoes, halved
*   3 tbsp. chopped fresh dill
*   2 oz. feta cheese, crumbled (½ cup)

## Preparation

1\. Bring 2 quarts water to boil in large saucepan. Add asparagus, snap peas, and 1 tablespoon salt. Cook until vegetables are crisp-tender, 2 to 3 minutes. Using slotted spoon, transfer vegetables to rimmed baking sheet and let cool for 15 minutes.

2\. Whisk oil, lemon juice, shallot, mustard, 1/4 teaspoon salt, and 1/4 teaspoon pepper together in large bowl. Add cooled vegetables, farro, tomatoes, dill, and 1/4 cup feta to dressing and toss to combine. Season with salt and pepper to taste and transfer to serving bowl. Sprinkle salad with remaining 1/4 cup feta and serve.
