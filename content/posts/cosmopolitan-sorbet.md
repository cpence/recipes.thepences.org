---
title: "Cosmopolitan Sorbet"
author: "pencechp"
date: "2010-03-25"
categories:
  - dessert
tags:
  - icecream
  - untested
---

## Preparation

*   2 cups sugar
*   2 cups water
*   5 1/3 cups fresh or frozen cranberries (not thawed; 24 oz)
*   1/4 cup vodka
*   1/4 cup fresh lime juice
*   2 tablespoons Cointreau or other orange liqueur

## Preparation

Bring sugar and water to a boil in a 3-quart heavy saucepan, stirring until sugar is dissolved. Add cranberries and simmer, uncovered, until berries are collapsed, 8 to 10 minutes. Pour through a large sieve into a bowl, gently pressing on solids to extract liquid (without forcing pulp through), and discard solids. Cool syrup, then chill, covered, until cold.

Stir in vodka, lime juice, and Cointreau and freeze in ice cream maker. Transfer to an airtight container and put in freezer to harden.

_Gourmet, November 2001_
