---
title: "Waffles and Waffle Iron"
author: "pencechp"
date: "2011-01-30"
categories:
  - breakfast
tags:
  - waffle
---

## Basic Waffle Batter

*   2 c. flour
*   1 1/4 c. milk
*   3/4 stick melted butter
*   2 eggs
*   2 tbsp. sugar
*   1/4 tsp. salt

Mix dry ingredients together.  Separate the egg and whisk the yolk into the dry ingredients, along with the milk, until batter has a uniform (not lumpy) consistency.  Add melted butter to batter.  Whisk egg whites until stiff and mix into batter.

Preheat waffle iron over medium heat, spray, and slowly pour batter into one side of the waffle iron, using a wooden spatula if needed to get the batter evenly distributed.  Although there should be enough butter in the recipe to keep the waffles from sticking, you should apply a spray canola oil to the waffle iron before making the first waffle.  Cook for about 3 minutes and then check for doneness.

## Belgian Waffles

*   2 c. flour
*   2 c. buttermilk
*   3/4 stick butter, melted
*   2 eggs
*   2 tbsp. sugar
*   1/2 tsp. salt
*   2 tsp. baking powder
*   1 tsp. baking soda

Whisk dry ingredients together. Add eggs, buttermilk, and butter into dry ingredients and mix until blended.  Cook as per the basic waffle recipe above.

_Rome Old Fashioned Waffle Iron_
