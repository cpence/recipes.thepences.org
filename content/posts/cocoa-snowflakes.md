---
title: "Cocoa Snowflakes"
author: "pencechp"
date: "2010-05-08"
categories:
  - dessert
tags:
  - chocolate
  - cookie
  - untested
---

## Ingredients

*   1 Cup all purpose flour
*   1 tsp. baking powder
*   1/4 tsp. salt
*   5 TB. butter
*   6 TB. cocoa powder
*   1 Cup sugar
*   1 tsp. vanilla extract
*   2 extra large eggs
*   1 Cup finely chopped nuts (optional)
*   1/2 Cup powdered sugar (for rolling cookies in)

## Preparation

In a medium bowl, sift flour, baking powder and salt, set aside. In a small heavy saucepan, melt butter over low heat, add cocoa powder, blend well with a fork or a small whisk until smooth. Remove pan from heat, stir in sugar until combined (it will be dark brown at this point). Transfer to a large mixing bowl, add vanilla extract, then eggs one at a time, stirring well with a wooden spoon or hand mixer after each addition. Add flour mixture and nuts if desired (skip the nuts if baking for children, as most don't seem to enjoy them), mix well. Cover the dough with plastic wrap, refrigerate until chilled (at least 2 hours). The dough never gets really stiff, but it won't roll into nice balls if it's warm. Preheat oven to 400°. Grab a handful of dough, enough for a cookie sheet, leaving the rest in the fridge. Roll each hunk of dough into a 3/4" ball, then roll in powdered sugar. It is easier to roll the dough into balls if you coat your hands with powdered sugar (kids love doing this). Place the sugar coated balls onto a greased cookie sheet, 2" apart. Bake cookies for 8 minutes at 400°, let cool a minute, then remove from pan. Store in an airtight container to maintain the soft and chewy texture of these cookies. If you'd like a crisper cookie, cook a minute longer (be careful not to burn the bottoms).

_Penzey's Spices_
