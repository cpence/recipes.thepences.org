---
title: "Curried Chana Dal"
author: "juliaphilip"
date: "2010-04-30"
categories:
  - side dish
tags:
  - curry
  - lentil
  - vegan
  - vegetarian
---

## Ingredients

*   1 1/2 cup (12 oz.) chana dal, washed and drained
*   5 cups water
*   1 t. ground turmeric
*   1/2 t. ground cumin
*   1 t. ground coriander
*   1/2 t. cayenne pepper (optional)
*   3 T. butter or margarine
*   1 t. cumin seed
*   1 medium onion, chopped
*   1 clove garlic, chopped
*   1 T. grated fresh ginger
*   1 t. garam masala
*   2 T. chopped fresh coriander leaves

## Preparation

Put chana dal in a bowl. Add enough cold water to cover and soak overnight.

To cook, drain chana dal. Place chana dal, water, turmeric, cumin, coriander, and cayenne in a heavy saucepan and bring to a boil over medium-high heat. Reduce heat to low, cover pan, and simmer for about 1 hour.

In a large saucepan, melt butter over medium heat. Add cumin seed and cook for 1 minute. Add the onion, garlic, and ginger and cook for about 5 minutes, stirring frequently, or until onion turns golden brown.

Add chana dal and cooking liquid to onion mixture. Turn heat to high and bring to a boil, stirring constantly. Cover pan, reduce heat to low, and simmer 30 minutes or until chana dal is tender but not mushy.

Add garam masala and mix well.

Place chana dal in a serving dish and sprinkle with chopped coriander leaves.
