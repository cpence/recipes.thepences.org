---
title: "Spicy Supercrunchy Fried Chicken"
author: "pencechp"
date: "2017-10-29"
categories:
  - main course
tags:
  - chicken
---

## Ingredients

*   1 chicken, cut into serving pieces (or 8-10 drumstick and thigh parts), trimmed
*   salt and pepper to taste
*   1 tbsp. curry powder
*   1/2 tsp. ground allspice
*   2 tbsp. minced garlic
*   1 habanero pepper, stemmed, seeded, and minced, optional
*   1 egg
*   1 c. flour lard and butter combined, or vegetable oil
*   lemon or lime wedges for garnish

## Preparation

In a bowl, toss chicken with salt, pepper, curry, allspice, garlic, chili, egg, and 2 tbsp. water. When combined, blend in flour with your hands. Keep mixing until most of the flour is blended with other ingredients and chicken is coated (add more water or flour if mixture is too thin or too dry; it should be dry but not powdery). Let sit while you heat fat; at this point chicken can marinate, refrigerated, for up to a day.

Choose a skillet or casserole at least 12" in diameter that can be covered. Add 1/2" of fat and turn heat to medium-high. If using butter, skim foam as it rises. When oil is hot, raise heat to high. Slowly add chicken to skillet. Cover, reduce to medium-high, and cook for 7 minutes.

Uncover, turn chicken, and cook uncovered for another 7 minutes. Turn again and cook for about 5 minutes more, turning as needed to make both sides golden brown.

Remove chicken from skillet and drain on paper towels. Serve chicken at any temperature, with lemon or lime wedges.

_New York Times, 2003_
