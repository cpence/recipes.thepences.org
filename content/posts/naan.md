---
title: "Naan"
date: "2020-07-12"
categories:
  - bread
tags:
  - indian
---

## Ingredients

*   3 cups (426 grams) unbleached all-purpose flour, plus more for dusting
*   2 teaspoons baking powder
*   1 1/2 teaspoons kosher salt
*   3/4 cup (180 milliliters) whole or low-fat buttermilk, at room temperature
*   About 3/4 cup warm water
*   Canola oil, for greasing the baking sheet
*   1 to 2 tablespoons melted ghee or unsalted butter, for brushing

## Preparation

In a large bowl, whisk together the flour, baking powder and salt. Pour the
buttermilk over the flour mixture and quickly stir it in. The flour will still
be fairly dry, with some wet clumps.

Pour a few tablespoons of the warm water over the flour, stirring it in with a
spatula or wooden spoon. Repeat until the flour comes together to form a soft
ball. You will use about 3/4 cup of the warm water in total, but it may need a
little more or less depending on your exact measurements or the weather. You
want the dough to be very soft, close to being slightly sticky, so if you add an
extra tablespoon or so, it won’t hurt. Using your hands, gather the ball,
picking up any dry flour in the bottom of the bowl, and knead it to form a
smooth, soft ball of dough, 1 to 2 minutes. If it’s a little too sticky to
handle, dust your hands with flour, but do not add any more flour to the dough,
if possible.

Lightly grease a rimmed baking sheet with the canola oil. Cut the dough into 6
equal portions (the dough will be roughly 700 grams, so aim for about 116 grams
each). Shape each portion into a round, cupping and tucking the edges underneath
as best you can to make it smooth. (Don’t sweat this too much, as the dough is
pretty forgiving and you’re going to roll it out anyway.) Place on the baking
sheet. Brush the rounds with the melted ghee or butter and cover with plastic
wrap or a slightly dampened clean dish towel. Let sit at room temperature for 30
minutes. The dough needs to rest, but will not rise or change much in
appearance.

With about 10 minutes left in the dough resting time, preheat a 12-inch
cast-iron skillet over medium heat.

Lightly dust a work surface with flour. Place one of the dough rounds on the
surface and then turn it over so that both sides are floured. (Keep the
remaining dough rounds covered.) Using a lightly floured rolling pin, roll the
dough into an 8- to 9-inch circle, rotating the dough 90 degrees after each
motion to create an even round. Dust the work surface and rolling pin with just
enough flour to keep things from sticking; you don’t want to overdo it.

When the skillet is sizzling-hot (check by sprinkling a few small drops of
water; if they bounce and quickly evaporate, it’s ready), add the first portion
of rolled dough. Cook for 2 to 3 minutes, until lots of bubbles appear on the
top and the bottom dries out and is freckled with brown spots.

Using tongs — or your fingers if you’re careful — flip the dough and cover the
skillet with a lid or large, rimmed baking sheet if you don’t have a top that
fits the pan. Cook for another 2 minutes, until the dough is cooked through and
there are plenty of very dark, almost charred spots on what was the top and now
the bottom of the naan. You may find you need to reduce the heat or cook time
slightly as the skillet gets very hot throughout the batch. While the first naan
cooks, roll out the second.

Remove the finished naan from the skillet, transfer to a baking sheet or serving
platter, and brush with more of the melted ghee or butter. Cover with foil or a
clean dish towel to keep warm. Transfer the second naan to the skillet, and
continue to roll and cook the remaining dough. Serve warm.

_WaPo_
