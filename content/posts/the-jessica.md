---
title: "The Jessica"
author: "pencechp"
date: "2012-05-04"
categories:
  - cocktails
tags:
  - gin
  - stgermain
---

## Ingredients

*   2 oz. gin
*   2 oz. grapefruit juice
*   1/2 oz. St. Germain
*   1 squeeze lime juice

## Preparation

Shake, strain, garnish with a sage leaf.
