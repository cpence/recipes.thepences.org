---
title: "Soy-Syrup Roasted Duck"
author: "pencechp"
date: "2014-11-19"
categories:
  - main course
tags:
  - duck
  - untested
---

## Ingredients

*   grapeseed or other neutral oil
*   1 whole (4 ½-pound) Muscovy duck, excess fat removed, trussed
*   Kosher salt and freshly ground black pepper
*   ½ cup maple syrup
*   ½ cup unseasoned rice vinegar
*   ½ cup soy sauce

## Preparation

Preheat oven to 450°F.

Lightly cost the bottom of a heavy roasting pan with oil.

Season the duck generously with salt and pepper and place in the pan, breast side up.

Roast for 10 minutes, then carefully turn the duck back side up. Roast for another 10 minutes and then carefully turn on one side. To get the duck to sit upright on a leg, rest the back of the duck against the side of the pan. Roast for another 10 minutes, then carefully turn onto the other side and roast for 10 minutes.

Meanwhile, stir together the maple syrup, vinegar, and soy sauce.

Transfer the duck to a dish and drain the fat through a fine mesh sieve into a measuring cup. Reserve for another use.

Reduce oven to 375°F.

Return duck to the roasting pan breast side up. Pour the syrup mixture over, letting it run down the sides. Return to the oven. Roast, basting every 5 minutes, for 20 minutes. Tilt the duck to let its juices run out of the cavity; the juices should be pale pink. Cook for another 5 minutes if needed. Remove from the oven and let rest for 10 minutes, basting frequently.

Carve. Arrange the sliced duck on a serving platter. Spoon the glaze from the pan all over.

_Home Cooking with Jean-Georges_
