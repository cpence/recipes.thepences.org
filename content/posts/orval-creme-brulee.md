---
title: "Orval Creme Brulee"
date: 2022-05-29T10:53:32+02:00
categories:
  - dessert
tags:
  - beer
---

## Ingredients

- 1 bottle of Orval
- 150 g fine sugar
- 5 egg yolks
- 500 mL cream
- 25 g brown sugar (for top)

## Preparation

(Serves four; original recipe was doubled.)

Reduce the beer by half and let it cool. Mix the fine sugar with the egg yolks until the mixture turns white. Add the cream and the beer. Mix well with a whisk. Divide the mixture into the appropriate ramekins. Cook for one hour in an oven at 100C (212F) in a water bath. Let cool in the fridge. Before serving, cover with brown sugar and caramelize it with a small torch.

_À l'Ange Gardien, Villers-devant-Orval_
