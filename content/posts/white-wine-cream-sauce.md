---
title: "White Wine Cream Sauce"
author: "pencechp"
date: "2011-10-17"
categories:
  - miscellaneous
tags:
  - pasta
  - sauce
---

## Ingredients

*   2 tablespoons butter
*   4 teaspoons minced shallots
*   1/2 cup white wine
*   1 pint heavy cream
*   2 tablespoons chopped fresh parsley
*   1/4 teaspoon salt
*   Salt and pepper to taste

## Preparation

Melt butter in large sauté pan over medium-high heat. Add shallots and sauté 1 minute.

Add wine and cream; cook 12 minutes over medium heat, stirring frequently.

Add salt and parsley. Season with more salt and pepper, if needed. Transfer to serving bowl.

Note: If you prepare sauce ahead and refrigerate, it will need to be reheated. Warm 2 ounces of cream in a saucepan and bring to simmer. Then add refrigerated cream sauce by whisking in slowly. Continue whisking until hot and bubbly; add 1 tablespoon butter to finish.

_Organic Authority_
