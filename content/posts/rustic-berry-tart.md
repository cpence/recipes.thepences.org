---
title: "Rustic Berry Tart"
author: "pencechp"
date: "2012-07-06"
categories:
  - dessert
tags:
  - blueberry
  - pie
  - raspberry
  - strawberry
---

## Ingredients

_Crust_

*   3/4 cup whole-wheat pastry flour
*   3/4 cup all-purpose flour
*   2 tablespoons sugar, plus 1 teaspoon for sprinkling
*   1/4 teaspoon salt
*   4 tablespoons cold butter (1/2 stick), cut into small pieces
*   1 tablespoon canola oil
*   1/4 cup ice water, plus more as needed
*   1 large egg, separated (save the white to glaze the pastry)
*   1 teaspoon lemon juice, or white vinegar

_Filling & glaze_

*   1/4 cup slivered almonds (1 ounce)
*   1/4 cup whole-wheat flour (regular or pastry flour)
*   1/4 cup plus 3 tablespoons sugar
*   4 cups mixed berries, such as blackberries, raspberries and blueberries
*   2 teaspoons lemon juice
*   1 tablespoon water
*   2 tablespoons raspberry, blueberry or blackberry jam

## Preparation

To prepare crust: Whisk whole-wheat flour, all-purpose flour, 2 tablespoons sugar and salt in a medium bowl. Cut in butter with a pastry blender or your fingers until the mixture resembles coarse crumbs with a few larger pieces. Add oil and stir with a fork to blend. Mix 1/4 cup water, egg yolk and 1 teaspoon lemon juice (or vinegar) in a measuring cup. Add just enough of the egg yolk mixture to the flour mixture, stirring with a fork, until the dough clumps together. (Add a little water if the dough seems too dry.) Turn the dough out onto a lightly floured surface and knead several times. Form the dough into a ball, then flatten into a disk. Wrap in plastic wrap and refrigerate for at least 1 hour.

Preheat oven to 425°F. Line a baking sheet with parchment paper or foil and coat with cooking spray.

To prepare filling &amp; assemble tart: Spread almonds in a small baking pan. Bake until light golden and fragrant, about 5 minutes. Let cool. Combine whole-wheat flour, 1/4 cup sugar (or Splenda) and the toasted almonds in a food processor or blender; process until the almonds are ground.

Roll the dough into a rough 13- to 14-inch circle on a lightly floured surface, about 1/4 inch thick. Roll it back over the rolling pin, brush off excess flour, and transfer to the prepared baking sheet. Spread the almond mixture over the pastry, leaving a 2-inch border all around. Toss berries with the remaining 3 tablespoons sugar (or Splenda) and 2 teaspoons lemon juice in a large bowl; spoon over the almond mixture. Fold the border up and over the filling, pleating as necessary. Blend the reserved egg white and 1 tablespoon water with a fork; brush lightly over the tart rim. Sprinkle with the remaining 1 teaspoon sugar.

Bake the tart for 15 minutes. Reduce oven temperature to 350°F and bake until the crust is golden and the juices are bubbling, 30 to 40 minutes. Leaving the tart on the parchment (or foil), carefully slide it onto a wire rack. Let cool.

Shortly before serving, melt jam in a small saucepan over low heat; brush over the berries. Cut the tart into wedges.

_Eating Well_
