---
title: "Moules Frites"
author: "pencechp"
date: "2012-02-23"
categories:
  - main course
tags:
  - belgian
  - mussels
  - potato
---

## Ingredients

*   1 tsp. Dijon mustard
*   1 egg yolk
*   1 cup canola oil, plus more for frying
*   2 tsp. white wine vinegar
*   2 tsp. fresh lemon juice
*   1/2 tsp. kosher salt, plus more to taste
*   Freshly ground black pepper, to taste
*   2 lb. Yukon Gold potatoes, peeled and cut into 1/4″-thick sticks
*   2 1/2 lb. mussels, debearded and scrubbed
*   2/3 cup dry white wine
*   2 tbsp. unsalted butter, cubed
*   3 ribs celery, finely chopped
*   1 1/2 leeks, light green and white parts, cut into 1/4″-thick slices
*   1/2 large yellow onion, finely chopped

## Preparation

Make the mayonnaise: In a large bowl, whisk mustard and egg yolk. Whisking constantly, slowly drizzle in oil in a thin stream until it begins to emulsify; whisk in vinegar, lemon juice, salt, and pepper. Set aside.

Make the fries: Pour oil into a 6-qt. Dutch oven to a depth of 2″, and heat over medium-high heat until a deep-fry thermometer reads 375°. Add potatoes and cook until tender, about 8 minutes. Using a slotted spoon, transfer fries to a rack set over a rimmed baking sheet; chill.

Increase oil temperature to 385°. Working in batches, add chilled potatoes and cook until golden brown and crisp, about 4 minutes. Using a slotted spoon, return fries to rack; season with salt.

Meanwhile, make the mussels: Heat a 12", high-sided skillet over high heat. Add mussels, wine, butter, celery, leeks, and onions; season with salt and pepper, and cover skillet. Cook, occasionally shaking skillet, until all mussels are opened, about 5 minutes. Divide mussels between 2 large bowls. Serve with fries and mayonnaise.

**Variations**

To take the flavor of moules frites in different directions, omit the wine and add one of the following ingredients to the pan with the mussels, butter, celery, leeks, and onion: 1 tablespoon blanched, minced garlic and 3/4 cup heavy cream; 1 tablespoon curry powder; 1 tablespoon piment d'Espelette; 1 tbsp. Pernod; 2/3 cup wheat beer.

_Saveur_
