---
title: "Roast Turkey in 45 Minutes"
author: "juliaphilip"
date: "2009-12-06"
categories:
  - main course
tags:
  - turkey
---

## Ingredients

*   1 8- to 12-pound turkey
*   10 garlic cloves, peeled and lightly crushed, more to taste
*   1 branch fresh tarragon or thyme separated into sprigs, or 1/2 teaspoon
    dried thyme or tarragon
*   1/3 cup extra virgin olive oil or butter
*   Salt and pepper to taste

## Preparation

Heat oven to 450 degrees. Put turkey on a stable cutting board breast side down
and cut out backbone. Turn turkey over, and press on it to flatten. Put it,
breast side up, in a roasting pan. Wings should partly cover breasts, and legs
should protrude a bit.

Tuck garlic and tarragon under the bird and in the nooks of the wings and
legs. Drizzle with olive oil, and sprinkle liberally with salt and pepper.

Roast for 20 minutes, undisturbed. Turkey should be browning. Remove from oven,
baste with pan juices, and return to oven. Reduce heat to 400 degrees (if turkey
browns too quickly, reduce temperature to 350 degrees).

Begin to check turkey's temperature about 15 minutes later (10 minutes if bird
is on the small side). It is done when thigh meat registers 165 degrees on an
instant-read meat thermometer. Check it in a couple of places. [An 18-lb. turkey
cooked in a total of about two hours.]

Let turkey rest for a few minutes before carving, then serve with garlic cloves
and pan juices.

_Mark Bittman, New York Times_
