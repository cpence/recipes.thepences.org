---
title: "Whole Wheat French Bread"
author: "pencechp"
date: "2010-10-26"
categories:
  - bread
tags:
---

## Ingredients

*   1 cup of water
*   1 1/2 tsp salt
*   2 cups (240 g) of whole wheat flour
*   1 cup (120 g) of whole wheat pastry flour
*   2 1/2 tsp of yeast
*   1 teaspoon sugar
*   about 1/3 cup corn meal on baking sheet

## Preparation

Add all ingredients (except cornmeal) to a bread maker and set it on the dough cycle. After the dough cycle concludes (about 1 hour and 20 minutes), remove the dough and divide into two balls. Roll each ball in a cylinder about 12 inches long. Make sure the cylinder is consistently the same width. Pour corn meal in two strips on a cookie sheet. Cut slits in the baguettes with a sharp knife. Brush the tops of each baguette with water to keep the dough from drying out. Cover with a towel and place somewhere warm for about 45 minutes. After 45 minutes, preheat the oven to 450º. Brush the baguettes with more water and place in the oven. Bake for 20 minutes. Cool on a rack. Bread will be very crisp on the outside, but still soft inside.

_Adapted from healthy vegan blog_
