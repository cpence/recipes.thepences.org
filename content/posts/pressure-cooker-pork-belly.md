---
title: "Pressure Cooker Pork Belly"
author: "pencechp"
date: "2017-02-21"
categories:
  - main course
tags:
  - instantpot
  - japanese
  - pork
---

## Ingredients

*   3 green onions (we’ll use only the green parts)
*   1 inch ginger
*   1 Tbsp. vegetable oil
*   2 lb. (907 g) pork belly block
*   Water for cooking pork belly
*   4 boiled eggs
*   ¼ cup (60 ml) sake
*   ½ cup (120 ml) water
*   ½ cup (120 ml) mirin
*   ½ cup (120 ml) soy sauce
*   ¼ cup (50 g) sugar

## Preparation

We will only use the green parts of the green onions (use the white parts in miso soup to go with this dish). Cut the green parts in half. Peel the ginger and slice it thinly.

On the pressure cooker, press the “Sauté” button on your Instant Pot and heat the oil. Cook the pork belly. You can skip this part to cut down the cooking time, but this process will render more fat and make the dish tastier.

Pour water to cover the meat, then add the green onions and sliced ginger. Cover and lock the lid. Make sure the steam release handle points at “sealing” and not “venting”. Press the “Keep Warm/Cancel” button on the Instant Pot to stop cooking. Press the “meat/Stew” button to switch to the pressure cooking mode. Cook under pressure for 35 minutes. If you’re using a stove-top pressure cooker, you won’t have the buttons to press. Just cook on high heat until high pressure is reached. Then reduce the heat to low to maintain high pressure for about 30 minutes.

When it is finished cooking, the Instant Pot will switch automatically to a “Keep Warm” mode. Slide the steam release handle to the "Venting" position to let out steam until the float valve drops down, OR let the pressure release naturally (takes about 15 mins). Unlock the lid and drain the cooking water and discard the green onion and ginger. Rinse the pork belly under warm water.

Put the pork belly back in the Instant Pot and add water, sake, mirin, soy sauce, and sugar. Mix the seasonings a little bit and add the boiled eggs. Press the “Sauté” button on the Instant Pot and press “Adjust” once to increase the heat. Bring it to simmer to let the alcohol evaporate. Once the alcohol smell is gone, press “Keep Warm/Cancel” button to turn off the Sauté mode. Cover and lock the lid. Make sure the steam release handle points at “sealing” and not “venting”. Press the “meat/Stew” button to turn on the pressure cooking mode. Press the “minus” button to decrease the cooking time from the preset 35 minutes to 10 minutes.

When it is finished cooking, the Instant Pot will switch automatically to a “Keep Warm” mode. Slide the steam release handle to the "Venting" position to let out steam until the float valve drops down and unlock the lid. If you have time (this is optional), press the “Sauté” button and simmer on low heat until the liquid in the cooker has reduced by half.

Serve the rice in a (donburi) bowl and pour the sauce on top. Place the pork belly and egg (add blanched green vegetable if you have any). Pour additional sauce over the meat and serve immediately.

_Just One Cookbook_
