---
title: "Pan de Tres Puntas"
author: "pencechp"
date: "2010-03-07"
categories:
  - bread
tags:
  - peruvian
---

## Ingredients

*   7 cups all purpose flour
*   1 tablespoon instant yeast
*   1 1/2 teaspoons salt
*   1/4 cup light brown sugar
*   2 tablespoons butter, melted
*   2-3 cups of water

## Preparation

Dissolve the yeast in 1 cup of the water.

Add the flour, salt, brown sugar, and melted butter to the bowl of a standing mixer, or to a large mixing bowl. Stir to mix ingredients lightly.

While stirring, gradually add the 1 cup of water with the dissolved yeast. Add a second cup of water gradually, mixing dough at the same time. Continue to add small amounts of water, until the dough comes together and can be kneaded.

Knead dough vigorously (with dough hook attachment if using a standing mixer) until it is smooth and stretchy and does not stick to the counter or bowl. Add more water as necessary if dough seems dry. It should take about 8 minutes to knead the dough in a mixer, and about 20 minutes by hand.

Cover bowl with plastic wrap and let dough rise until double in bulk (or overnight in the refrigerator).

Divide dough into 16-18 equal pieces. Roll each piece into a ball and let rest for 5 minutes.

Preheat the oven to 425 degrees. If you have a pizza stone, place it in the oven.

Flatten each ball into a circle on the counter (you can work with half of the balls at a time if you don't have much space). Use a rolling pin to roll the dough circles into flat, 6-inch diameter (approximate) circles. Let dough circles rest for 5 minutes, so the gluten can relax slightly.

Using a dough scraper or knife, firmly score a triangle in the circle of dough. Roll the edges of the circle in towards the lines of the triangle and press edges down slightly. You should have a triangle piece of dough with raised edges. Repeat with remaining circles of dough.

Place triangles onto a baking sheet (or directly onto the pizza stone, if using) and place into the oven, throwing some ice cubes onto the floor of the oven at the same time to create steam.

Bake breads (in batches if necessary) for about 15 minutes, or until they are golden brown on the edges.

Remove from oven and let cool. Serve warm with butter.
