---
title: "Hearty Ethiopian Soup (Mereq)"
author: "pencechp"
date: "2012-12-02"
categories:
  - main course
tags:
  - ethiopian
  - ribs
  - stew
---

## Ingredients

*   2 lg. red onions, chopped
*   2 lg. tomatoes, finely diced
*   1 slab ribs (~3 lb.) washed and cut apart (any variety)
*   3 tb. [qibe]( {{< relref "qibe-spiced-butter" >}} ) or regular butter
*   1/4 c. fresh ginger, finely diced (or 1 tbsp. dried ginger)
*   2 cloves fresh garlic, finely diced
*   1 jalapeño or other hot fresh pepper, minced (optional)
*   1/4 to 1 tsp. salt, to taste
*   1/4 to 1 tsp. pepper, to taste
*   1 1/2 to 2 1/2 c. water (or more, for a more "brothy" dish)

## Preparation

Put the onions and tomatoes in a pot over medium heat.  Cook, stirring, about 5 minutes -- do not let them dry out or brown.  Add the ribs to the pot and let them steep out their juices, turning a few times.  This takes maybe 20 minutes, depending on your ribs.  Once the pot is saturated with the juices of the meat (again, don't let it dry out), add the qibe or butter, ginger, garlic, jalapeño, salt, and pepper.  Mix to combine and give it a few minutes for the flavors to meld.  Then add the water to the pot -- the ribs don't have to be submerged but should be at least partially covered.  Let it simmer for at least 30 minutes, more for thicker ribs.  When the pot's volume is considerably lower and the meat looks tender and cooked, take your pot off the stove.

_Penzey's Spices_
