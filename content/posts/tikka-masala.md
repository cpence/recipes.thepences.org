---
title: "Tikka Masala"
author: "pencechp"
date: "2018-02-07"
categories:
  - main course
tags:
  - curry
  - indian
  - instantpot
---

## Ingredients

_For marinating the chicken:_

*   1 pound boneless skinless chicken breasts, chopped into bite-sized pieces
*   1 cup greek yogurt (7 oz.)
*   1 tablespoon garam masala
*   1 tablespoon lemon juice
*   1 teaspoon black pepper
*   1/4 teaspoon ground ginger

_For the sauce:_

*   15 ounces canned tomato sauce or puree
*   5 cloves garlic, minced
*   4 teaspoons garam masala
*   1/2 teaspoon paprika
*   1/2 teaspoon turmeric
*   1/2 teaspoon salt
*   1/4 teaspoon cayenne
*   1 cup heavy whipping cream

_For serving:_

*   basmati rice
*   naan
*   freshly chopped cilantro

## Preparation

Combine all marinade ingredients (minus the chicken) in a bowl and mix well. Add chicken chunks and coat with the marinade. Let sit in the refrigerator for at least 1 hour.

Select the saute mode on the pressure cooker for medium heat. When it has reached temperature, add chicken chunks (along with any marinade sticking to them) to the pressure cooker. Saute until the chicken is cooked on all sides, about 5 minutes, stirring occasionally. Turn off the saute mode.

Add all of the sauce ingredients except the cream to the pressure cooker, over the chicken, and stir. Secure and seal the lid. Select the manual mode to cook for 10 minutes at high pressure. Use the quick steam release handle to release pressure.

Select the saute mode on the pressure cooker for low heat. When it has reached temperature, add cream to the pot, stirring with the other ingredients. Simmer until the sauce is thickened to your liking, a few minutes.

Serve with basmati rice or naan. Garnish with cilantro.

_Savory Tooth_
