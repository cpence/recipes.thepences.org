---
title: "Pecan Bars"
author: "pencechp"
date: "2015-12-28"
categories:
  - dessert
tags:
  - pecan
---

## Ingredients

_Crust:_

*   1 3/4 cups all-purpose flour
*   6 tbsp. sugar
*   1/2 tsp. salt
*   8 tbsp. melted butter

_Topping:_

*   3/4 c. packed light brown sugar
*   1/2 c. light corn syrup
*   7 tbsp. melted hot butter
*   1 tsp. vanilla extract
*   1/2 tsp. salt
*   4 c. (1 lb.) pecan halves, toasted
*   1/2 tsp. sea salt (optional)

## Preparation

Adjust oven rack to lowest position and heat oven to 350 degrees. Make foil sling for 13 by 9-inch baking pan by folding 2 long sheets of aluminum foil; first sheet should be 13 inches wide and second sheet should be 9 inches wide. Lay sheets of foil in pan perpendicular to each other, with extra foil hanging over edges of pan. Push foil into corners and up sides of pan, smoothing foil flush to pan. Lightly spray foil with vegetable oil spray.

Whisk flour, sugar, and salt together in medium bowl. Add melted butter and stir with wooden spoon until dough begins to form. Using your hands, continue to combine until no dry flour remains and small portion of dough holds together when squeezed in palm of your hand. Evenly scatter tablespoon-size pieces of dough over surface of pan. Using your fingertips and palm of your hand, press and smooth dough into even thickness in bottom of pan.

Whisk sugar, corn syrup, melted butter, vanilla, and salt together in medium bowl until smooth (mixture will look separated at first but will become homogeneous), about 20 seconds. Fold pecans into sugar mixture until nuts are evenly coated.

Pour topping over crust. Using spatula, spread topping over crust, pushing to edges and into corners (there will be bare patches). Bake until topping is evenly distributed and rapidly bubbling across entire surface, 23 to 25 minutes.

Transfer pan to wire rack and lightly sprinkle with flake sea salt, if using. Let bars cool completely in pan on rack, about 1 1/2 hours. Using foil overhang, lift bars out of pan and transfer to cutting board. Cut into 24 bars. (Bars can be stored at room temperature for up to 5 days.)
