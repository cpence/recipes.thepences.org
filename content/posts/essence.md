---
title: "Essence"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - miscellaneous
tags:
  - seasoning
---

## Ingredients

*   2 ½ tablespoons paprika
*   2 tablespoons salt
*   2 tablespoons garlic powder
*   1 tablespoon black pepper
*   1 tablespoon onion powder
*   1 tablespoon cayenne pepper
*   1 tablespoon dried leaf oregano
*   1 tablespoon dried thyme

## Preparation

Mix together all ingredients and store in an airtight container.

Makes about 2/3 cup

_Emeril_
