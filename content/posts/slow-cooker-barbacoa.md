---
title: "Slow Cooker Barbacoa"
author: "pencechp"
date: "2015-05-06"
categories:
  - main course
tags:
  - beef
  - mexican
  - untested
---

## Ingredients

*   3 lbs chuck roast (fat trimmed), cut into 2-inch chunks
*   4 cloves garlic, minced
*   2 chiptoles in adobo sauce, chopped (or more to taste)
*   1 (4-ounce) can chopped green chiles
*   1 small white onion, finely chopped (about 1 cup)
*   1/4 cup fresh lime juice
*   2 tablespoons apple cider vinegar
*   3 bay leaves
*   1 Tablespoon ground cumin
*   1 Tablespoon dried Mexican oregano (or regular oregano)
*   2 teaspoons salt
*   1 teaspoon black pepper
*   1/4 tsp ground cloves
*   1/2 cup beef broth or water

## Preparation

Combine all ingredients in the bowl of a slow cooker. Toss gently to combine. Cover and cook on low for 6-8 hours, or on high for 3-4 hours, or until the beef is tender and falls apart easily when shredded with a fork.

Using two forks, shred the beef into bite-sized pieces inside of the slow cooker. Toss the beef with the juices, then cover and let the barbacoa beef soak up the juices for an extra 10 minutes. Remove the bay leaves.  Use a pair of tongs or a slotted spoon to serve the barbacoa beef.

If not using immediately, refrigerate the barbacoa beef with its juices in a sealed container for up to 5 days. Or freeze it for up to 3 months.

_Gimme Some Oven_
