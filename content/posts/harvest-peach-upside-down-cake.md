---
title: "Harvest Peach Upside-Down Cake"
author: "pencechp"
date: "2010-05-08"
categories:
  - dessert
tags:
  - cake
  - peach
  - untested
---

## Ingredients

*   1 Cup butter
*   3 Cups sugar
*   6 eggs
*   2 Cups peeled, chopped peaches (4 medium)
*   2 TB. vanilla sugar
*   3 Cups all-purpose flour
*   1 tsp. baking soda
*   1/4 tsp. salt
*   1 tsp. cinnamon

## Preparation

Preheat oven to 350°, have all ingredients at room temperature. Grease and flour tube pan. Peel and chop peaches, mix with VANILLA SUGAR in a small bowl, set aside. Sift together flour, baking soda, salt, and CINNAMON, set aside. In a large bowl, cream butter and sugar until well blended, light and fluffy. Add eggs 2 at a time, blending well after each addition. This is the most important part, as the cake will rise much higher if the sugar and butter are well beaten and the eggs are gradually added and well beaten. Starting and ending with dry ingredients, gently blend in flour mixture and peaches, one third at a time. Use a large spoon or blend on lowest speed to just combine these ingredients. Pour into the pan. Make sure not to get batter on the upper part of the tube, which will prevent the cake from sliding out of the pan. Place on middle rack in oven, bake about 70-75 minutes, until cake is browned and springy to the touch, or a thin skewer inserted in the middle comes out clean. Remove from oven, let cool 5-10 minutes, run a butter knife between the cake and the outside wall of the pan, turn pan over onto a rack and let the cake slide out. Turn the cake over at this point if you so desire, and let cool completely. Place the cake on a serving plate. The cake is easiest to cut when cool. That is also the time to sprinkle with powdered sugar if you'd like-though serving the slices warm with a thin pat of butter is quite tasty too.

_Penzey's Spices_
