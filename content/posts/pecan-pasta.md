---
title: "Pecan Pasta"
author: "pencechp"
date: "2010-04-02"
categories:
  - main course
tags:
  - pasta
  - vegan
  - vegetarian
---

## Ingredients

*   4 TBSP olive oil
*   2 large garlic cloves, finely chopped
*   3 to 4 New Mexico red or red jalapeno chiles, seeded and julienned
*   1 cup pecans, very roughly crushed (may substitute other nuts)
*   handful flat-leaf parsley, finely chopped
*   1 lb. whole wheat spaghetti
*   salt & pepper
*   20 fresh basil leaves, cut in strips

## Preparation

Bring water for pasta to a boil. Meanwhile, heat half the oil over low heat in a small sauce pan and add garlic, chiles, and nuts. Stir-fry 3 minutes. And parsley and stir-fry 2 more minutes. Remove from heat and stir in remaining oil. Cook pasta, reserve about 1/2 cup pasta water, drain and return to pot. Add nuts & pasta water as needed to keep it from sticking together. Season with salt & pepper and stir in basil. Drizzle oil over individual servings. Serves 4 as main course.
