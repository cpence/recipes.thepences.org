---
title: "Chen Kenichi's Chili Prawns"
author: "juliaphilip"
date: "2010-04-26"
categories:
  - main course
tags:
  - chinese
  - shrimp
---

## Ingredients

*   18-20 peeled jumbo shrimp, about 12 oz
*   1 tbl salt
*   1 tbl minced garlic
*   1 tbl minced fresh ginger
*   1/4 tsp dried red pepper flakes
*   2 tbl Heinz chili sauce or 1 tbl tomato paste
*   2 tbl hoisin sauce
*   2 tbl shaoshing rice wine or sherry
*   1 tbl soy sauce
*   1/2 tsp sugar
*   1/4 tsp Asian chili garlic sauce or Sriracha hot sauce
*   1/4 tsp sesame oil
*   1 tbl peanut oil
*   1/4 c chopped scallions

**Preparations**

Dissolve 1 tbl salt in 4 c warm water and put the shrimp in to brine for 10 to 15 min.

Peel and mince the garlic and ginger and set them aside in a small bowl with the dried red pepper flakes. Mix all the remaining ingredients except the peanut oil and scallions in a bowl.

Heat a wok or saute pan over high heat until it's very hot, then put in the peanut oil and swirl to cover the bottom of the pan. When it sizzles, put in the garlic, ginger and red pepper mix and stir fry briefly, until the vegetables are translucent. Add the shrimp and chili sauce and stir fry just until shrimp are cooked through and pink, adding a small amount of water if the sauce becomes too thick and dry. Stir in the scallions and serve with plenty of steaming white rice.

_Iron Chef Chen Kenichi_
