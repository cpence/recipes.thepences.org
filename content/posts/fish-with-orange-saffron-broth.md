---
title: "Fish with Orange-Saffron Broth"
author: "pencechp"
date: "2012-05-12"
categories:
  - main course
tags:
  - fish
  - orange
---

## Ingredients

*   5 8 oz. bottles clam juice
*   1/2 c. dry white wine
*   2 whole star anise
*   1 bay leaf
*   1 pinch saffron threads
*   3/4 c. fresh orange juice
*   1 c. 1/3 inch cubes seeded, peeled chayote
*   1 large carrot, peeled and thinly sliced crosswise
*   1 celery stalk, thinly sliced
*   1/2 lg. leek, thinly sliced
*   6 oz. black cod, cut into 1" cubes
*   6 oz. halibut, cut into 1" cubes
*   1 tsp. fresh tarragon, chopped

## Preparation

Boil clam juice, wine, star anise, bay leaf, and saffron in large saucepan until reduced to 2 1/2 c., about 25 minutes.  Fish out anise and bay leaf.  Boil orange juice in small saucepan until reduced to 1/4 c., about 7 minutes.  Add to clam juice broth.

Bring broth mixture to simmer. Add chayote, carrot, celery, and leek.  Simmer until vegetables are tender, about 4 minutes.  Remove pan from heat.  Add fish and tarragon; cover and let stand until fish is cooked through, about 8 minutes.  Season w/ salt and pepper.

Divide fish and vegetables between two bowls, top with broth, and serve.

_Bon Appetit, Calabash Hotel, Grenada_
