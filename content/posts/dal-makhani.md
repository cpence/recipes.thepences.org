---
title: "Dal Makhani"
date: 2021-01-09T21:45:09+01:00
categories:
  - main course
tags:
  - indian
  - bean
  - lentil
  - untested
---

## Ingredients

- 1 cup (7 ounces) whole urad beans with skin
- 4 cups water, plus more for soaking
- 1/8 teaspoon baking soda
- 1 medium (10 ounces) white onion, cut into chunks
- 6 cloves garlic, peeled and smashed
- One (2-inch) piece fresh ginger, peeled and cut in half
- 4 tablespoons ghee or unsalted butter, divided (may substitute extra-virgin olive oil)
- 1 teaspoon garam masala
- 1/2 teaspoon ground turmeric
- 1/4 cup tomato paste
- 1 teaspoon fine sea salt, plus more to taste
- 1/4 teaspoon cayenne powder
- 2 tablespoons heavy cream or creme fraiche (may substitute nondairy yogurt)
- 2 tablespoons loosely packed chopped fresh cilantro leaves, for garnish (optional)

## Preparation

Pick through the beans and discard any dirt or stones, then transfer to a medium bowl and rinse under running water. Add enough water to cover the beans by 1 inch and soak overnight (8 to 12 hours). Drain.

In a medium saucepan or Dutch oven over high heat, combine the beans with the 4 cups water and baking soda. Bring to a rolling boil, then lower the heat to a simmer, cover, and cook the beans until they are tender and almost falling apart, 30 to 45 minutes. Transfer the beans with their liquid to a large bowl. Rinse the saucepan and wipe it dry.

Combine the onion and garlic in a blender. Chop half the ginger, add it to the blender and pulse until the vegetables form a smooth paste. (If needed, add a bit of the liquid from the dal to the blender to help things move around.)

Return the saucepan or Dutch oven to medium-high heat, and melt 2 tablespoons of the ghee. Add the garam masala and turmeric and cook, stirring, until the spices release their aroma, 30 to 45 seconds. Add the tomato paste and cook until darkened, 1 to 2 minutes. Reduce the heat to medium-low, stir in the onion mixture and cook, stirring occasionally, until most of the liquid has cooked away and the ghee has started pooling in spots, 10 to 15 minutes. Add the cooked beans and their liquid and stir in the salt and cayenne. Taste, and add more salt if needed.

Increase the heat to high and bring the mixture to a boil. Cook at a boil for a few minutes, stirring occasionally to prevent the beans from sticking to the bottom of the pan. Reduce the heat to a gentle simmer and cook an additional 1 to 2 minutes. Stir in the cream and remove from the heat.

Cut the remaining ginger into matchsticks. In a small saucepan over medium-high heat, melt the remaining 2 tablespoons of ghee. Add the remaining ginger and fry until the ginger starts to turn golden brown, about 1 minute. Pour the fried ginger and ghee over the dal. Garnish with the cilantro, if using, and serve hot, with plain rice or flatbread, and yogurt or raita on the side.

_WaPo_
