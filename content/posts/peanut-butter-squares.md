---
title: "Peanut Butter Squares"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - dessert
tags:
  - cookie
  - peanutbutter
  - philip
---

## Ingredients

*   3 sticks butter, melted
*   2 cups peanut butter
*   1 lb confectioner's sugar
*   1 1/2 cups graham cracker crumbs
*   12 oz chocolate chips

## Preparation

Mix 2 sticks melted butter, peanut butter, sugar, and graham cracker crumbs together and spread in 15 x 10 1/2 in cookie sheet.

Melt chocolate chips and butter together, pour over peanut butter mixture.

Chill in refrigerator for 2 hours, then let stand at room temperature before cutting.

_Jackie Ott_
