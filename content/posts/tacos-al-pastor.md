---
title: "Tacos al Pastor"
author: "pencechp"
date: "2017-09-17"
categories:
  - main course
tags:
  - mexican
  - pork
---

## Ingredients

*   16 tortillas de maiz
*   800g (1.5 lbs) pork butt
*   1/2 pineapple
*   1 lg. onion
*   1 bunch cilantro
*   8 limes
*   salt

_For the axiote marinade:_

*   juice of 2 oranges
*   300g ground axiote (powder)
*   10 dry chiles guajillos
*   1 medium size onion
*   3 cloves of garlic
*   1 tbsp white pepper
*   1 cup white vinegar
*   1 stick of cinnamon

1 recipe [salsa taquera]( {{< relref "salsa-taquera" >}} )

## Preparation

1\. Make a cut into the side of each chile guajillo, and devein them (rip out the stem and the guts) and soak them in hot water. Once they’ve hydrated and they’re soft, remove from water and place in blender. Add the vinegar, the juice of two oranges, chop up the medium sized onion and add that too. Add your garlic cloves, the cinnamon, and blend it a little. Then add the axiote, the white pepper, then blend. Then add salt to taste. Once the mix is ready, pour into a big bowl you’ll use to marinate the meat.

2\. Cut your pork hiney into half steak pieces. Try to make them uniformly thick, but they don’t need to look pretty at all because we’re going to chop them all up before the end to serve in the tacos.

3\. Cover each “steak” in the axiote marinade and then refrigerate them all for as long as you can afford to (at least three hours).

4\. When it’s time to make dinner, start be preheating the grill.

5\. Slice your 1/2 pineapple into “disks” just a bit over 1cm thick (about 1/2″). You can just cut the slices across the entire pineapple, and you don’t have to peel the outside of the pineapple off before you grill it, either.

6\. With a hot grill, throw on the now-marinated pork steaks along with your pineapple pieces until both have been cooked. This should take about eight minutes, maybe a touch more. You know you’ve cooked the pineapple enough when it has become soft throughout. It’s alright if the pineapple slices get charred a bit, but do your best to keep them from burning, otherwise they will not taste good.

7\. Now peel the pineapple rings off and chop the pineapple into small little cooked pieces.

8\. Mince the onions, chop up the cilantro, then mix them together with the pineapple in a bowl. Take a lime and squeeze its juice into the mix and add salt to taste. Sample it to see if you need to add more lime juice.

9\. Chop up the pork into little strips and pieces for filling your tacos with.

10\. Make the tacos: add the meat to the tortillas first, then your pineapple-cilantro-onion mix, and then top off with your delicious authentic salsa taquera.

_Authentic Mexican Recipes_
