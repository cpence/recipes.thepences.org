---
title: "Collard Sandwich"
date: 2020-06-06T13:54:21+02:00
categories:
    - main course
tags:
    - greens
    - sandwich
    - untested
---

## Ingredients

*for the cornbread:*

*   1 cup cornmeal
*   1 heaping tbsp. self-rising flour
*   1/2 tsp. kosher salt
*   2 tbsp. peanut oil

*for the greens:*

*   1 bunch collards, stemmed and cut into strips
*   2 tbsp peanut oil
*   1 tsp. kosher salt, plus more to taste
*   2 tsp. sugar, plus more to taste

## Preparation

*for the cornbread:*

Combine all dry ingredients in a bowl. Add ⅔ cup water, plus more as necessary
to thin the mixture to the consistency of pancake batter. Heat peanut oil in a
cast-iron skillet over medium-high heat. When it shimmers, add the batter in
softball-sized circles. Cook for 1-2 minutes on each side, or until browned.

*for the collard greens:*

Heat peanut oil in a large saucepan over medium-high heat. Add
collards. Stir. When they begin to sizzle, add a splash of water, and then cover
the saucepan and cook 10-12 minutes, stirring occasionally. Season with salt and
sugar, adding more to taste.

*for the sandwiches:*

Cover two pieces of cornbread with collard greens. Top with two more pieces, and
then cover each sandwich with 1-2 strips of cooked bacon. Serve immediately.

*JoLynn's Concessions, NC, via Garden & Gun*

