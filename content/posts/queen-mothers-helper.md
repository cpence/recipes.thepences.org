---
title: "Queen Mother's Little Helper"
date: 2022-03-04T10:50:26+01:00
categories:
  - cocktails
tags:
  - tea
  - gin
  - untested
---

## Ingredients

_For the tisane:_

- 1/4 cup juniper berries
- 1 whole star anise
- 1 teaspoon whole coriander seeds
- 1/2 teaspoon allspice berries
- 1/2 teaspoon white peppercorns
- 4 cups water
- Peel of 1 grapefruit, stripped with a peeler, avoiding the white pith
- Peel of 1 navel orange, stripped with a peeler, avoiding the white pith
- 1/4 cup granulated sugar

_For the drinks:_

- 1 cup juniper tisane
- 3 ounces dry gin
- 4 dashes Angostura bitters
- 2 twists orange peel, for garnish

## Preparation



Make the juniper tisane: In a medium saucepan over medium heat, toast the juniper berries, star anise, coriander, allspice and peppercorns until fragrant (the juniper berries will begin to look oily).

Add the water, then the citrus peels, twisting them over the surface of the water to express the oils.

Add the sugar and bring the mixture to a boil. Reduce the heat to low and simmer to infuse, about 5 minutes. Remove from the heat and let steep for 10 minutes. Strain out (and discard) the solids and reserve the liquid. The yield is about 3 3/4 cups (enough for 6 to 7 drinks).

Make the drinks: In a small saucepan over medium-low heat, bring the tisane to a simmer. Add the gin and let rewarm for 30 seconds, then divide equally between two teacups. Add 2 dashes bitters to each drink, then garnish with the orange peel — twisted over the surface of each drink, then dropped in — stir and serve.

_WaPo_
