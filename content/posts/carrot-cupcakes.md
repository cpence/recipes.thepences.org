---
title: "Carrot Cupcakes"
author: "pencechp"
date: "2010-05-08"
categories:
  - dessert
tags:
  - cupcake
---

## Ingredients

*   3 Cups peeled, grated carrots, ends removed (about 1 lb.)
*   2 Cups all-purpose flour
*   1 tsp. cinnamon
*   1 tsp. salt
*   1 tsp. baking soda
*   1 tsp. baking powder
*   4 large eggs
*   2 Cups granulated white sugar
*   3/4 Cup applesauce
*   1/4 Cup vegetable oil
*   1 tsp. vanilla extract
*   1 Cup chopped nuts or raisins (optional)

*Frosting:*

*   1/3 Cup cream cheese
*   1/4 Cup butter (1/2 stick)
*   1 tsp. vanilla extract
*   2 Cups powdered sugar

## Preparation

Preheat oven to 325°. Line muffin tins with paper cupcake cups. Peel carrots, chop off ends, and grate finely, which is easiest using a food processor. Sift flour, CINNAMON, salt, baking soda and baking powder together, set aside. In a large mixing bowl, beat eggs until well blended. Add sugar, oil, carrots, and PURE VANILLA EXTRACT, mix well at medium speed. Add flour mixture and nuts or raisins (if desired). Blend on low speed until just mixed, spoon into cupcake cups, about 3/4 full. Place the pans in the preheated oven, bake for 12 minutes. Switch the pans so the pan from the lower rack is now on the upper rack, and bake for 12 more minutes. Check for doneness–cupcakes should be browned and springy to the touch. Let cool completely before frosting. To prepare frosting: let cream cheese and butter come to room temperature. Blend thoroughly with PURE VANILLA EXTRACT, then beat in the powdered sugar at low speed. Add food coloring if desired; it gives the frosting a nice spreading consistency. If you don’t use food coloring, add a drizzle of milk to make the frosting spread more easily if desired.

_Penzey's Spices_
