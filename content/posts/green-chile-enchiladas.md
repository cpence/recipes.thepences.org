---
title: "Green Chile Enchiladas"
author: "pencechp"
date: "2011-01-26"
categories:
  - main course
tags:
  - greenchile
  - newmexican
  - pence
  - shrimp
---

## Ingredients

*   chicken (breasts, thighs, or whole) or 3/4-1 lb. frozen cocktail shrimp
*   salt
*   pepper
*   1 bay leaf
*   water
*   1 1/2 lb. grated monterrey jack
*   1 onion
*   1 doz. green chiles, chopped, or 3 small cans green chile
*   1 lb. container sour cream
*   2 doz. corn tortillas

## Preparation

Boil chicken breasts, thighs, or a whole chicken (however much you want) with salt, pepper, and a bay leaf until fully cooked.  Remove from liquid, cool, and shred.  Place the chicken in a large bowl.  Add all but 1 cup of the cheese to the bowl, reserving for the top.  Add onion, green chiles, half to 3/4 of the container of sour cream, and salt and pepper.  Mix together until it forms a filling.  Roll (or layer) corn tortillas filled with filling, softened in oil.  If you have extra filling, spread it thinly over the top.  Cover with tin foil and bake for 45 minutes at 350.  Take the foil off, top with the reserved cheese, and bake for another 15 minutes or so until the cheese bubbles.

Can substitute 3/4-1 lb. of frozen baby cocktail shrimp for the chicken, if you want.
