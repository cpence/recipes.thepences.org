---
title: "Holy Crap Coleslaw"
author: "pencechp"
date: "2010-04-07"
categories:
  - side dish
tags:
  - apple
  - bellpepper
  - cabbage
  - untested
  - vegan
  - vegetarian
---

## Ingredients

*   2 Granny Smith apples
*   1/4 whole cabbage
*   1 orange bell pepper
*   1 red bell pepper
*   2 lemons
*   1/4 cup extra virgin olive oil
*   1 tablespoon poppy seeds
*   1 teaspoon sea salt

## Preparation

Juice lemons. Peel apples.

Julienne the cabbage, the apples, and the peppers.

Add lemon juice, oil, salt, and poppies.

Mix. Chill. Eat!

_Richard Kent_
