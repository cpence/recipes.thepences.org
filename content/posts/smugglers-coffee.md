---
title: "Smuggler's Coffee"
author: "pencechp"
date: "2017-03-21"
categories:
  - cocktails
tags:
  - coffee
  - rum
  - untested
---

## Ingredients

*   1/2 cup hot strong coffee
*   2 teaspoons sugar
*   1 ounce gold rum
*   1 ounce dark rum
*   1 small cinnamon stick
*   1 long strip of orange zest, plus finely grated zest, for garnish
*   Sweetened whipped cream and grated Mexican chocolate, for garnish

## Preparation

In a heatproof glass, stir the coffee and sugar until the sugar dissolves. Stir in both rums. Add the cinnamon and the strip of orange zest. Garnish with whipped cream, grated Mexican chocolate and grated orange zest.

_Food & Wine_
