---
title: "Imam Bayildi"
date: 2021-01-17T15:33:12+01:00
categories:
  - main course
tags:
  - turkish
  - eggplant
---

## Ingredients

- 4 aubergines, long, slim ones, ideally
- 1 lemon, halved
- Salt and black pepper
- 120ml olive oil
- 1 onion, peeled and thinly sliced
- 2 red peppers, core and seeds removed, cut into long 1cm-wide strips
- 2 big garlic cloves, peeled and sliced thin
- 1½ tsp ground cumin
- 1 tsp paprika
- 400g canned tomatoes
- ½ tsp sugar
- 2 sprigs fresh oregano
- ¾ tsp dried oregano (Greek, ideally; or fresh oregano, picked and chopped)

## Preparation

Shave long, alternate strips of peel off the aubergines, top to bottom, so they end up striped, like zebras. Starting 2cm from the top, make an incision halfway into the flesh and cut down to 2cm shy of the bottom.

Put the aubergines in a large bowl and cover with two and a half litres of cold water. Squeeze in the lemon, drop in the skins and stir in two teaspoons of salt. Put a plate on top, to keep the aubergines immersed, and leave to soak for 45 minutes. Drain, then dry in a clean tea towel.

Heat the oil in a large saute pan on a medium-high flame. Fry the aubergines for eight to 10 minutes (take care, because the oil may spit), turning regularly, until nicely browned on all sides. Remove from the pan, turn down the heat to medium-low, add the onion and peppers, and cook for 10 minutes, stirring often, until soft but not coloured. Add the garlic and spices, cook for a minute, then stir in the tomatoes, two tablespoons of water, the sugar, fresh oregano, half a teaspoon of salt and a good grind of pepper. Turn the heat to low, put the aubergines on top of the veg, cover the pan and cook for 45 minutes, until the aubergines are steamed through.

In the meantime, heat the oven to 180C/350F/gas mark 4. Carefully lift out the cooked aubergines and place them cut side up in a 20cm x 30cm ceramic baking dish; they should be nice and snug. Prise open the aubergines, so they look like long canoes, then sprinkle the insides with a generous pinch of salt. Discard the oregano from the sauce, then spoon it into the aubergines, filling them as much as you can; don't worry if some sauce spills out around them -- it's kind of unavoidable. Cover with foil and bake for 35 minutes. Remove the foil and leave the aubergines to cool to room temperature. Serve topped with a sprinkling of dried oregano, and with soft Turkish bread and Greek yogurt. Best eaten the day after, at room temperature.

_Ottolenghi, via Twitter_
