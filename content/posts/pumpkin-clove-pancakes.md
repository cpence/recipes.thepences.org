---
title: "Pumpkin Clove Pancakes"
author: "juliaphilip"
date: "2010-12-27"
categories:
  - breakfast
tags:
  - pancake
  - pumpkin
---

## Ingredients

*   2 cups all purpose flour
*   6 tablespoons (packed) brown sugar
*   1 1/2 teaspoons baking powder
*   1/2 teaspoon baking soda
*   1/2 teaspoon ground cloves
*   1/2 teaspoon salt
*   1 2/3 cups buttermilk
*   3/4 cup canned solid pack pumpkin
*   3 large eggs
*   2 tablespoons (1/4 stick) butter, melted

## Preparation

Preheat oven to 250°F. Mix first 6 ingredients in large bowl to blend. Whisk buttermilk, pumpkin, eggs and melted butter in medium bowl until well blended. Add to dry ingredients and whisk until smooth.

Melt 1 tablespoon butter in large nonstick skillet over medium heat. Working in batches, drop batter by 1/4 cupfuls into skillet. Cook pancakes until bubbles form on top and bottoms are golden brown, about 2 1/2 minutes. Turn pancakes over. Cook until bottoms are golden brown, about 2 minutes. Transfer to baking sheet; place in oven to keep warm up to 20 minutes. Repeat with remaining batter, adding more butter to skillet as necessary for each batch.

Serve pancakes hot with maple syrup.

_Bon Appetit, December 1996_
