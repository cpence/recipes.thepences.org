---
title: "Kabocha no Nimono (Simmered Pumpkin)"
author: "pencechp"
date: "2016-12-29"
categories:
  - side dish
tags:
  - japanese
  - pumpkin
---

## Ingredients

*   3 cups (720ml) dashi
*   2 1/2lb kabocha (pumpkin)
*   2 Tbsp sugar
*   1 1/2 Tbsp soy sauce
*   1/4-1/2 tsp salt
*   1 Tbsp sake

## Preparation

Remove seeds and cut kabocha into 3" square pieces. Cut off sharp edges of the kabocha pieces. Boil dashi in a pot and put kabocha in it. Cook kabocha covered until tender at medium heat, about 20-30 minutes depending on how hard your kabocha is. Add sugar, soy sauce, salt and Sake to the kabocha. Reduce heat to medium low, and cook another 15 -20 minutes to reduce the broth a little. Remove from heat and let it sit covered until cool. As it cools, kabocha will absorb more of the flavor from the broth. If you like it warm, reheat before serving.

_Japanese Cooking 101_
