---
title: "Blue Moon"
author: "juliaphilip"
date: "2016-09-06"
categories:
  - cocktails
tags:
  - gin
  - violette
---

## Ingredients

*   2 oz gin
*   1/2 oz creme de violette
*   1/2 oz fresh lemon juice
*   lemon twist

## Preparation

Combine all ingredients in cocktail shaker with ice. Shake, strain into a coupe or martini glass, garnish with lemon twist.
