---
title: "French Lamb Stew"
author: "pencechp"
date: "2016-04-28"
categories:
  - main course
tags:
  - lamb
  - stew
---

## Ingredients

*   6 shallots (3 whole, 3 chopped)
*   3 whole cloves, stuck into 3 shallots
*   1 carrot, peeled
*   2 celery stalks
*   6 peppercorns
*   3 bay leaves
*   3 cups chicken stock
*   2 pounds lamb steaks, cubed
*   2 tablespoons butter
*   ¼ teaspoon sugar
*   1 tablespoon flour
*   ½ cup Crème Fraiche
*   ½ cup dill, chopped
*   6 ounces white corn, cooked

## Preparation

Place 3 clove studded shallots, carrot, celery, peppercorns, bay leaves and stock in heavy saucepan. Simmer 10 minutes, add the lamb and cook for 2 hours. Discard vegetables and spices. Reserve lamb and stock. In saucepan on low heat, sauté 3 chopped shallots in butter. Stir in sugar and flour. Remove from heat, add reserved stock and stir. Return to heat and stir constantly while adding Crème Fraiche. Fold in lamb cubes and ½ pkg. chopped dill. Season with salt and pepper. Serve over corn. Garnish with remaining sprigs of dill.

_Fresh Herbs_
