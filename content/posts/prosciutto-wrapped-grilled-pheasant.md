---
title: "Prosciutto-Wrapped Grilled Pheasant"
author: "pencechp"
date: "2012-11-28"
categories:
  - main course
tags:
  - game
---

## Ingredients

*   1 pheasant (or grouse, etc.)
*   3 oz. prosciutto, thinly sliced
*   honey
*   soy sauce
*   butter
*   spices (e.g., tarragon, oregano, basil) (optional)

## Preparation

Prepare the pheasant, cutting into legs and (two) breast tenders.  Wrap each piece completely with prosciutto, attaching with toothpicks.  Make a baste by melting some butter, adding honey and a very slight amount of soy sauce.  Some spices can be added to the baste if desired.

Over medium fire on a grill, cook the pieces almost all the way through, noting that the legs will take longer to cook than the breast pieces (should be around 10 minutes, max).  On the last few turns, baste.
