---
title: "Sausage and Wild Rice Casserole"
author: "pencechp"
date: "2019-12-25"
categories:
  - main course
tags:
  - casserole
  - pence
  - rice
  - sausage
---

## Ingredients

*   Brown & wild rice combo (to make ~6 cups)
*   fresh sausage (e.g., bratwurst)
*   butter
*   1 lg. onion
*   1-2 cloves garlic
*   8 oz box sliced mushrooms
*   a fresh spice that matches the sausage (e.g., thyme)
*   2–3 tbsp. plus 1/4 c. parmesan

## Preparation

Preheat the oven to 350 degrees. Cook the rice mix according to the package
directions. Either cut the sausage into rounds and sautée, or sautée first and
then slice, depending on the kind of sausage. Reserve. In the same skillet, add
butter, onions, garlic, mushrooms, and spice. Sautée. Mix with the sausage,
rice, and 2–3 tbsp. parmesan, and pour into a buttered 13x9" casserole. Dot with
more butter. Bake for 45 minutes, covered. Sprinkle top with the remaining
parmesan, and bake for another 15 minutes, uncovered.
