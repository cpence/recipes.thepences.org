---
title: "Moroccan Wheat Bread"
author: "pencechp"
date: "2010-05-28"
categories:
  - bread
tags:
  - moroccan
---

## Ingredients

*   2 cups whole wheat flour
*   2 cups white flour
*   2 teaspoons salt
*   1 tablespoon yeast
*   2 tablespoons vegetable oil
*   1 or 2 tablespoons honey
*   1 1/4 cup warm water
*   additional flour for kneading
*   cornmeal, semolina, barley grits or oil for the pan

## Preparation

Prepare two baking sheets by oiling the centers, or by dusting the pans with a little cornmeal, semolina or the barley grits.

Mix the flour and salt in a bowl. Make a large well in the center of the flour mixture, and add the yeast.

Add the oil, honey and the water to the well, mixing to dissolve the yeast first, and then stirring the entire contents of the bowl to incorporate the water into the flour.

Turn the dough out onto a floured surface, and begin kneading the dough. If necessary, add flour or water in very small amounts to make the dough soft and pliable, but not sticky. Continue kneading for 10 minutes, or until the dough is very smooth and elastic.

Divide the dough in half, and shape each portion into a smooth circular mound. Place onto the prepared pans, and cover with a towel. Allow to rest for 10 minutes.

After the dough has rested, use the palm of your hand to flatten the dough into circles about 1/4" thick. Cover with a towel, and leave to rise about one hour, or until the dough springs back when pressed lightly with a finger.

Preheat an oven to 435°F (225°C).

Score the top of the bread with a very sharp knife, or poke the dough with a fork in several places. Bake the bread for about 20 minutes – rotate the pans about halfway through the baking time – or until the loaves are richly colored and sound hollow when tapped. Transfer the bread to a rack or towel-lined basket to cool.

_Christine Benlafquih, About.com_
