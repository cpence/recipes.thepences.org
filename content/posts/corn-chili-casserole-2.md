---
title: "Corn Chili Casserole"
author: "pencechp"
date: "2010-05-08"
categories:
  - side dish
tags:
  - corn
  - greenchile
  - pence
---

## Ingredients

*   2 cans kernel corn
*   2 cans creamed corn
*   2 eggs
*   1/2 stick melted butter
*   1 c. chopped onion
*   green chilies to taste
*   salt
*   1 1/2 c. cheddar cheese (optional)

## Preparation

Mix and bake at 350 until bubbly.
