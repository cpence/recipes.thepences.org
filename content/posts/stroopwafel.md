---
title: "Stroopwafel"
author: "pencechp"
date: "2011-07-07"
categories:
  - breakfast
  - dessert
tags:
  - cookie
  - dutch
  - untested
  - waffle
---

## Ingredients

_Waffles:_

*   300 g caster/superfine sugar
*   450 g butter
*   3 eggs
*   3 tbsp. milk
*   600 g flour
*   cinnamon
*   salt

_Syrup:_

*   600 g cane-sugar syrup (or light corn syrup)
*   300 g butter

## Preparation

Mix the sugar with the eggs, milk, flower, cinnamon, salt and the butter sliced in pieces. Make 12 small balls.

Preheat the waffle iron \[CP: this needs to be an old-fashioned, cast iron waffle iron that makes relatively thin waffles, not a Belgian/American waffle iron\]. Squeeze a paste ball in the iron. Bake the waffle in about 30 seconds.

Cut the waffle in two thin waffles and spread the waffle with the mix of syrup and butter.

_www.stroopwafelshop.com_
