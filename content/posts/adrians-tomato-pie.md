---
title: "Adrian's Tomato Pie"
author: "juliaphilip"
date: "2010-04-26"
categories:
  - main course
tags:
  - tomato
---

## Ingredients

*   1 1/2 c fresh bread crumbs
*   1 tbsp pasta sprinkle (Penzey's)
*   3 c sliced ripe tomatoes (about 6 large)
*   3 tb dry shallots or 1 small onion thinly sliced
*   1/2 tsp salt
*   1/4 tsp ground black pepper
*   2 shredded swiss cheese
*   3 large eggs, slightly beaten
*   1/4 tsp ground or freshly grated nutmeg
*   5 slices bacon, dried

## Preparation

Preheat oven to 325 F. Butter a 9" pie dish. Mix the bread crumbs with 1 tbsp pasta sprinkle. Spread half of the bread crumbs evenly on the bottom of the dish. Arrange half the tomatoes, overlapping, on bread crumbs. Top with half the shallots, salt, and black pepper, the sprinkle on half the swiss cheese. Make a second layer with the rest of the tomato slices, shallots, salt, pepper, and cheese. Pour the eggs over the cheese (don't worry if the eggs don't cover the entire top). Spread on the remaining bread crumbs. Sprinkle on nutmeg, top with bacon pieces. It is important to dice the bacon or the pie will be hard to cut and eat. Bake in the middle of the oven for 45-50 min, until the bacon is crisp. Remove from the oven, cool, at least 10 min, slice and serve.

_Penzeys Spices_
