---
title: "Broiled Lamb Chops with Mint Chimichurri"
author: "pencechp"
date: "2016-07-15"
categories:
  - main course
tags:
  - lamb
  - mint
---

## Ingredients

*   3/4 tsp. cinnamon
*   4 (1" thick) lamb shoulder chops
*   1 to 2 garlic cloves, pressed
*   2 c. flat-leaf parsley including trimmed stems
*   2 c. mint including trimmed stems
*   1/3 c. distilled white vinegar
*   1/2 c. extra-virgin olive oil
*   1 small package frozen peas
*   3 tbsp. water
*   2 tbsp. unsalted butter

## Preparation

Add sauce ingredients and 1/2 teaspoon salt and pulse until herbs are finely chopped. Transfer to a bowl. Cook peas in water and butter in a small saucepan over medium-high heat, covered, stirring once or twice, until just tender, about 3 minutes.

Preheat broiler. Stir together cinnamon and 1 1/2 teaspoon each of salt and pepper in a bowl, then rub over chops. Broil in a 4-sided sheet pan 3 to 4 inches from heat, turning once or twice, 8 to 10 minutes total for medium-rare.

Serve chops drizzled with a little chimichurri and serve peas and remaining chimichurri on the side.

_Gourmet, November 2009_
