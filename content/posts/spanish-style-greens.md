---
title: "Spanish Style Greens"
author: "pencechp"
date: "2011-12-30"
categories:
  - side dish
tags:
  - greens
  - spanish
  - vegan
  - vegetarian
---

## Ingredients

*   1 small bunch of young, tender greens (kale, collards, or turnip greens)
*   1/4 cup of olive oil
*   2 cloves of garlic, chopped
*   kosher salt
*   1/4 teaspoon of hot red pepper flakes or 1/2 teaspoon of pimentón de La Vera
*   a splash of wine vinegar

## Preparation

First wash the greens in several changes of water. Remove the harder, tougher parts of the stem. (With really tender greens, much of the stem can be eaten: it has a nice crunch). Chop the greens very coarsely in ribbons.

In a heavy bottom pot or pan, fry the garlic in the olive oil until fragrant, but not browned. Remove the pan from the heat and let the oil cool some. Add a pinch of salt, the pepper flakes, or the pimentón de La Vera. Stir. Add the chopped greens, stir them until well coated with oil, and return the pot to the burner. It's usually not necessary to add any liquid. The water clinging to the leaves is enough.

Cook the greens on medium, stirring until they wilt. Cover the pot and let them steam on low heat until tender. This usually takes 10 minutes but it may take more or less time, depending on the tenderness of the greens.

When they're done, toss them with a splash of vinegar, if you like.

_Simple Spanish Food_
