---
title: "Instant Pot Curried Cream of Broccoli Soup"
author: "pencechp"
date: "2017-03-21"
categories:
  - main course
tags:
  - broccoli
  - instantpot
  - soup
---

## Ingredients

*   2 tablespoons ghee, coconut oil, olive oil, or fat of choice
*   3 medium leeks, white parts only, cleaned and trimmed and roughly chopped
*   2 medium shallots, roughly chopped
*   1 tablespoon Indian curry powder
*   Kosher salt
*   1½ pounds broccoli, chopped into uniform florets
*   ¼ cup peeled and diced apple (I like Fuji)
*   4 cups bone broth or chicken stock
*   Freshly-ground black pepper
*   1 cup full-fat coconut milk
*   Leftover pork, crisped in a pan (optional)
*   Chives (optional garnish)

## Preparation

Chop broccoli (including stems) and leeks. Turn on sautee function of the Instant Pot, and add fat, leeks, shallots, curry, and salt. Cook until leeks are soft (5 minutes) and curry is fragrant. Add broccoli and apple. Stir to mix, and add broth, plus water to make sure veggies are mostly submerged if needed. Switch off sautee mode, seal the pot, and set it for five minutes at high pressure (Manual, minus to 5 minutes, adjust to high pressure.) Release pressure manually when done. Blenderize with an immersion blender, add coconut milk, adjust seasoning, and serve.

_Nom Nom Paleo_
