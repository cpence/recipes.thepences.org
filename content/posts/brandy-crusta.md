---
title: "Brandy Crusta"
author: "pencechp"
date: "2014-01-15"
categories:
  - cocktails
tags:
  - pisco
  - untested
---

## Ingredients

*   1 1/2 oz. pisco
*   1/2 oz. lime juice
*   1/2 oz. Clement Creole Shrubb (or Grand Marnier, Cointreau)
*   1/4 oz. Cherry Heering
*   2 dashes bitters (pref. orange)

## Preparation

Combine ingredients in an ice-filled shaker.  Shake, then strain into a sugar-rimmed cocktail glass.  Garnish with a lime wheel.

_Arnaud's French 75 Bar, via Spirit Magazine_
