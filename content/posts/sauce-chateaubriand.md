---
title: "Sauce Chateaubriand"
date: 2024-01-10T17:52:26-06:00
categories:
  - miscellaneous
tags:
  - french
  - sauce
---

## Ingredients


- 2 dl de vin blanc
- échalote émincée
- une branche de persil
- un brin de thym
- feuille de laurier
- 3 dl de fonds de veau lié réduit
- 100 g de beurre à la maître d'hotel
- une petite cuillerée d'estragon haché

## Preparation

1. Réduire des deux tiers 2 décilitres de vin blanc additionné d'une échalote
   émincée, d'une branche de persil, d'un brin de thym, d'un menu fragment de
   feuille de laurier.
2. Mouiller de 3 décilitres de fonds de veau lié réduit. Faire bouillir quelques
   instants. Passer à l'étamine.
3. Ajouter, au dernier moment, 100 grammes de beurre à la maître d'hôtel et une
   petite cuillerée d'estragon haché.

_MC_
