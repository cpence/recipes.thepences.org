---
title: "Mediterranean Spinach Pies"
author: "juliaphilip"
date: "2010-04-03"
categories:
  - main course
  - side dish
tags:
  - mediterranean
  - untested
  - vegan
  - vegetarian
---

## Ingredients

*   [Mediterranean pie dough]( {{< relref "mediterranean-pie-dough" >}} )
*   1 lb spinach
*   1 tsp salt
*   2 tbl olive oil
*   1 c finely chopped onion
*   1 tsp ground allspice
*   6 tbl pine nuts, toasted
*   1/4 c fresh lemon juice
*   3 tbl chopped fresh mint
*   1/2 tsp freshly ground black pepper

## Preparation

Prepare the dough according to directions.

Preheat the oven to 400 F. Lightly oil a baking sheet.

Stem the spinach, chop coarsely, and wash well to remove traces of sand. Drain well, place in a colander and sprinkle with 1 tsp of salt. Let stand 1 h. Squeeze all moisture out of the spinach and then chop coarsely.

In a large saute pan over medium heat warm the oil. Add the onion and saute until tender and translucent, 8-10 min. Add the allspice, stir well and cook for 1 min longer. Stir in the chopped spinach and cook, stirring occasionally, until the spinach wilts, a few minutes. Stir in the pine nuts, lemon juice, mint, pepper, and salt to taste. Remove from heat.

Divide dough into 12 equal portions and roll out each portion into a 6 in round. Spoon about 1/3 c of the spinach mixture into the center of each round. Bring up the three edges of the round to meet in the center and press together to seal. The pastries will be triangular.

Place on the prepared baking sheet. Brush pastries with oil. Bake until golden, 15-20 min. Remove from the oven and brush the tops with olive oil. Serve warm.
