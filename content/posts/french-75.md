---
title: "French 75"
author: "pencechp"
date: "2013-07-22"
categories:
  - cocktails
tags:
  - champagne
  - gin
---

## Ingredients

*   2 oz. gin
*   1/2 tbsp. simple syrup
*   1/2 oz. lemon juice
*   champagne

## Preparation

Mix the first three ingredients (or shake with ice), pour into champagne flute.  Top with champagne.

_Esquire_
