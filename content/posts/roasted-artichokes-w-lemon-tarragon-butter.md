---
title: "Roasted Artichokes w/ Lemon Tarragon Butter"
author: "pencechp"
date: "2013-09-29"
categories:
  - side dish
tags:
  - artichoke
---

## Ingredients

*   4 artichokes
*   olive oil
*   salt and pepper
*   4 tbsp. butter
*   2 tbsp. fresh lemon juice
*   1 tbsp. fresh tarragon, chopped

## Preparation

Wash your artichokes and cut in half lengthwise. Using a spoon, carve out the hairy “choke” in the center along with the tiny, pointy leaves. Place them in lemon water as you go to prevent oxidation. Place them on a baking sheet and drizzle them with olive oil, salt and pepper. Roast in the oven for about 45 minutes to an hour (depending on size of artichoke, large ones could be longer) at 375 degrees (F) or until the heart is fork tender.

Meanwhile, gently melt the butter in a small saucepan, add the lemon juice and cook for one minute. Remove from the heat and add the chopped tarragon. To serve, ladle a few teaspoons of the lemon butter over each artichoke. Use the center “cup” of the artichoke for dipping.

_I Breathe... I'm Hungry..._
