---
title: "Apple-Bacon Corn Bread"
author: "pencechp"
date: "2011-12-30"
categories:
  - bread
tags:
  - cornbread
---

## Ingredients

*   4 strips bacon
*   2 Granny Smith apples, peeled, cored, and finely chopped
*   1 cup all-purpose flour
*   3⁄4 cup yellow cornmeal
*   3 tablespoons sugar
*   1 1⁄2 teaspoons baking powder
*   1⁄2 teaspoon baking soda
*   1⁄2 teaspoon ground black pepper
*   1⁄4 teaspoon salt
*   1 1⁄2 cups buttermilk
*   2 eggs
*   5 tablespoons unsalted butter, melted

## Preparation

Preheat oven to 425°F. Butter a 9-inch square baking pan; set aside.

In a medium skillet, fry the bacon until crisp. Drain on a paper-towel-lined plate, then crumble the bacon into small pieces. Set aside.

Drain all but 1 tablespoon bacon drippings from the pan. Cook the apples in hot drippings over medium-high heat for 5 minutes, stirring frequently. Set aside.

In a large bowl, whisk together the fl our, cornmeal, sugar, baking powder, baking soda, pepper, and salt. In a medium bowl, beat together the buttermilk, eggs, and butter. Fold the buttermilk mixture into the flour mixture. Fold in the bacon and apples. Pour the batter into the prepared pan.

Bake for 25 to 30 minutes or until golden and a knife inserted in the center comes out clean. Cool for 10 minutes before serving.

_Deen Bros. Blog_
