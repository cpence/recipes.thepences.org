---
title: "Pea and Radish Salad"
date: 2021-09-08T17:03:43+02:00
categories:
  - side dish
tags:
  - peas
  - radish
  - vegetarian
  - salad
  - untested
---

## Ingredients

- Frozen peas, thawed
- Radishes, thinly sliced
- Spring onion, minced
- 1/2 c. heavy cream
- 1 tbsp. cider vinegar
- 1/4 tsp. honey
- salt and pepper

## Preparation

Combine the cream, vinegar, honey, salt, and pepper, and put in a jar. Shake for
about one minute. Leave on the counter for an hour to thicken. Toss with peas
and radishes.

_someone on Facebook, I think?_
