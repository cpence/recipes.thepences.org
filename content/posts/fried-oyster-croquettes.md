---
title: "Fried Oyster Croquettes"
author: "pencechp"
date: "2014-01-02"
categories:
  - side dish
tags:
  - oyster
  - untested
---

## Ingredients

*   vegetable oil
*   2 eggs, slightly beaten
*   1 cup milk
*   2 cups flour, sifted
*   2 teaspoons baking powder
*   1/2 teaspoon salt
*   1 1/2 cups oysters, drained and chopped

## Preparation

Heat about 1/2 to 1 inch of oil in a deep heavy skillet to about 365°. Combine eggs, milk, flour, baking powder, and salt; blend until smooth. Stir oysters into the batter and drop by spoonful into the hot fat. Brown on both sides. Drain on paper towels.

_Diana Rattray, About.com_
