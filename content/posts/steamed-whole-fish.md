---
title: "Steamed Whole Fish"
author: "pencechp"
date: "2015-07-31"
categories:
  - main course
tags:
  - chinese
  - fish
  - untested
---

## Ingredients

*   1 whole striped bass or sea bass (about 1 ½ lbs), cleaned (see instructions)
*   3 tablespoons fresh ginger, finely julienned
*   2 scallions, finely julienned with green and white parts separated
*   8 sprigs fresh cilantro, roughly chopped
*   ¼ cup plus 2 tablespoons canola oil
*   ¼ cup water
*   ¼ teaspoon salt
*   ¼ cup light soy sauce
*   Fresh ground white pepper to taste

## Preparation

Preparing the fish:

1\. Remove any scales from your fish using a serrated steak knife. The areas to look for are the belly and the edges of the fish including the top, near the dorsal fins, and the head. There is nothing worse than having to pick out scales while you’re having dinner.

2\. Cut off any fins with kitchen shears. They are pretty tough, so be careful with this step. Leave the tail and head in tact for presentation.

3\. Look at the cavity, and you should see the backbone. You may also see a membrane that you should pierce and cut, revealing a blood line near the bone. Run your finger or a spoon across it to clean it thoroughly.

4\. Check the head and gills. You should not see any gills left, and if there are, remove them with the kitchen shears and rinse the area clean. Older Chinese folks who like dining on the fish head will appreciate this step.

5\. Give the fish a final rinse, shake off the excess water (no need to pat it dry) and transfer to a heat-proof plate for steaming. No salt, seasoning, or wine should be used on the fish before steaming. Repeat. Nothing on the fresh fish before steaming!

Assembling the dish:

For steaming, I used an elongated heat-proof plate. To accommodate that, I needed to MacGyver a steaming apparatus that would fit said plate. It's simple enough. I used a wok and metal steam rack. If you need more height to keep the plate above the water in the wok, set a rack on top of a metal can with both ends removed. It’s a handy and cheap addition to your kitchen arsenal!

Steam for 9 minutes and turn off the heat. Use a butter knife to peek at the meat and confirm the fish is cooked through. The meat should be opaque down to the bone, but the bone should be slightly translucent and not fully cooked (remember, you will not be eating the bone. Trust me on this one!).

Next, carefully pour off all of the liquid accumulated on the plate from steaming and spread half of the ginger, the green portions of the scallion, and the cilantro over the fish.

Heat 2 tablespoons oil and the other half of the ginger in a saucepan until the ginger begins to sizzle. Next, add the water, salt, soy sauce, and fresh ground white pepper and heat the mixture until simmering.

Once simmering, add the rest of the oil and white portions of the scallion and stir until the liquid begins to simmer and sizzle once again. Spoon the entire mixture evenly over the fish and serve hot!

_The Woks of Life_
