---
title: "Roasted Fennel and Potato Soup"
author: "pencechp"
date: "2011-02-07"
categories:
  - main course
  - side dish
tags:
  - fennel
  - soup
---

## Ingredients

*   1-2 TBSP butter
*   3 TBSP olive oil
*   3 small Yukon gold potatoes, halved and sliced
*   1/4 cup dry white wine
*   3 large fennel bulbs, halved, cored and sliced
*   3 cups chicken stock
*   3 TBSP shallot, chopped
*   1/4 cup onion, chopped
*   4 cloves garlic, chopped
*   1/4 cup cream
*   1/4 cup milk
*   1/4 cup grated white cheddar or other white cheese
*   S & P

## Preparation

Heat 1 TBSP each butter and oil in heavy soup pot. Add potatoes, toss to coat, and season with S & P. Cook over medium-low heat until tender and beginning to caramelize, adding more butter or oil if they seem dry. (Aim for some browned potato bits on bottom of pan.) Add wine, and stir to deglaze.

Meanwhile, put fennel on cookie sheet, lightly salt, and toss with 1 TBSP olive oil. Spread in a single layer and bake at 375 for about 15 minutes, or until tender and brown around the edges. Add to soup pot with stock and cook over low heat.

Meanwhile - again - saute onion and shallot in remaining olive oil until edges start to brown. Stir in garlic and cook a few minutes more. Add to soup pot.

Add milk, cream and cheese and continue cooking for about 10 minutes. Adjust seasoning. Puree either in batches in blender or with boat motor. Thin with water or stock to desired consistency. Serve hot garnished with fennel fronds.

Variations: Top each bowl with 1 TBSP cooked, crumbled spicy Italian sausage (we did this -- sooo good) or chiffonade of raw baby kale. For vegan, omit milk, cream, and cheese and use vegetable stock.
