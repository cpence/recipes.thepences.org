---
title: "Moroccan Kefta-Stuffed Rolls"
author: "pencechp"
date: "2016-06-08"
categories:
  - main course
tags:
  - beef
  - moroccan
  - untested
---

## Ingredients

_For filling_

*   1 lb. ground beef
*   1 to 2 tbsp. vegetable oil or olive oil
*   1 onion, finely chopped
*   1 bell pepper (any color), finely chopped
*   1 1/2 teaspoons paprika
*   1 1/2 teaspoons cumin
*   1/2 teaspoon salt, or to taste
*   1/2 teaspoon cinnamon
*   cayenne or black pepper, to taste
*   1 generous tablespoon tomato paste
*   1 to 2 handfuls pitted green olives, chopped or sliced

_For the dough_

*   1 tablespoon yeast
*   1 1/2 cups (236 ml) warm milk
*   4 1/2 cups (or 600g) flour
*   4 tablespoons sugar
*   1 teaspoon salt
*   4 tablespoons butter, very soft or melted
*   vegetable oil, for brushing the tops of the baked rolls

## Preparation

_Make the Filling_

1. In a large frying pan or skillet, saute the onion and pepper in the oil over
   medium heat for several minutes.

2. Add the ground beef and cook until no longer pink, stirring frequently and
   breaking up any large pieces of meat with your spoon.

3. Stir in the spices, then thoroughly blend in the tomato paste. Stir in the
   olives, remove the filling from the heat and set aside to cool.

_Make the Dough_

1. Dissolve the yeast in the warm milk and set aside.

2. In a large bowl, combine the flour, sugar and salt. Add the butter and milk
   with yeast, and stir the mixture to form a sticky dough barely firm enough
   for kneading. (Use small additions of flour or milk if necessary to achieve
   this consistency.)

3. Knead the dough by hand for 10 minutes, or in a mixer with a dough hook for 5
   minutes, until very smooth but still a bit sticky. (The dough will lose its
   tacky quality after rising.)

4. Transfer the kneaded dough to an oiled bowl, turning the dough over once to
   coat it with oil. Cover the bowl with a towel and leave the dough to rise in
   a warm, draft free area for an hour or longer, until doubled in bulk.

_Shape and Stuff the Rolls_

1. After the dough has risen, turn it out onto your work surface. Divide the
   dough into smooth 1 1/2" balls.

   [CAP: The recipe says that it makes 20–24 appetizer-sized buns. You can make
   twelve instead, and they come out hamburger-bun size.]

2. Take a ball of dough and pat it out into a flat circle (approx. 3" in
   diameter) which is thinner around the edges and a little thicker to the touch
   in the middle.

3. Add a generous tablespoon of filling to the middle of the dough, then gather
   the edges up around the filling, pinching to seal and fully enclose the
   dough. Turn the stuffed ball of dough over and gently roll it under your palm
   against your work surface to smooth its appearance.

4. Transfer the stuffed dough to an oiled baking sheet (or pan lined with oiled
   parchment paper) and repeat with the remaining dough and filling.

5. When all of the rolls have been shaped, cover the tray with a towel and leave
   the stuffed dough to rest for 30 to 60 minutes, until light and puffy.

_Bake the Stuffed Rolls_

1. Preheat your oven to 425° F (220° C).

2. Bake the rolls in the middle of the oven for about 20 minutes, or until
   medium golden brown.

3. Remove the pan from the oven and, if desired, lightly brush the tops of the
   hot rolls with vegetable oil or butter. (This is an optional step if a soft
   crust is desired.) Transfer the rolls to a rack, cover loosely with a towel
   and allow to cool at least 10 minutes before serving.

_About Food, Christine Benlafquih_
