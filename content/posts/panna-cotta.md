---
title: "Panna Cotta"
author: "pencechp"
date: "2016-08-18"
categories:
  - dessert
tags:
  - custard
  - untested
---

## Ingredients

*   4 cups (1l) heavy cream or half-and-half
*   1/2 cup (100g) sugar
*   2 teaspoons of vanilla extract, or 1 vanilla bean, split lengthwise
*   2 packets powdered unflavored gelatin (about 4 1/2 teaspoons)
*   6 tablespoons (90ml) cold water

## Preparation

1\. Heat the heavy cream or half-and-half and sugar in a saucepan . Once the sugar is dissolved, remove from heat and stir in the vanilla extract. If using a vanilla bean, scrape the seeds from the bean into the cream and add the bean pod. Cover, and let infuse for 30 minutes. Remove the bean then rewarm the mixture before continuing.

2\. Lightly oil eight custard cups with a neutral-tasting oil.

3\. Sprinkle the gelatin over the cold water in a medium-sized bowl and let stand 5 to 10 minutes.

4\. Pour the very warm Panna Cotta mixture over the gelatin and stir until the gelatin is completely dissolved.

5\. Divide the Panna Cotta mixture into the prepared cups, then chill them until firm, which will take at least two hours but I let them stand at least four hours. If you’re pressed for time, pour the Panna Cotta mixture into wine goblets so you can serve them in the glasses, without unmolding.

6\. Run a sharp knife around the edge of each Panna Cotta and unmold each onto a serving plate, and garnish as desired.

_David Lebovitz_
