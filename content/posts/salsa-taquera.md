---
title: "Salsa Taquera"
author: "pencechp"
date: "2017-09-17"
categories:
  - miscellaneous
tags:
  - mexican
  - salsa
  - sauce
---

## Ingredients

*   10 tomatillos
*   2 small roma tomatoes
*   3-6 chipotle chiles in adobo (based on spice)
*   1 chile serrano
*   2 cups water (maybe less, control thickness here)
*   1 tbsp dry oregano
*   1 tbsp salt
*   1 tbsp black pepper
*   1/8 cup white vinegar
*   2 cloves of garlic

## Preparation

Boil the tomatillos and the tomatoes together. Once boiled, drain them and place in blender. Add the rest of the ingredients, and blend it all.

_Authentic Mexican Recipes_
