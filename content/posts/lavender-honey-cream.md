---
title: "Lavender Honey Cream"
author: "pencechp"
date: "2015-10-13"
categories:
  - cocktails
tags:
  - honey
  - untested
  - vodka
---

## Ingredients

*   1 1/2 oz. vodka
*   1 oz. heavy cream
*   1 oz. egg white
*   1 oz. [honey syrup]( {{< relref "honey-simple-syrup" >}} )
*   Fresh lavender

## Preparation

Shake over ice gently for 30 seconds to make a thick, creamy cocktail. Strain and garnish with lavender.

_Adapted from H. Ehrmann, A Guide to Honey Cocktails_
