---
title: "Saucisses et Stoemp aux Poireaux"
date: 2021-02-19T09:18:37+01:00
categories:
  - main course
tags:
  - belgian
  - sausage
  - potato
---

## Ingredients

- 600g de saucisses
- 1 kg pomme de terre farineuses
- 300g de poireaux émincés
- 1 échalote
- 5 cl de lait
- 25 cl d'eau
- 1/2 cube de bouillon de bœuf
- 1 c à s de moutarde
- 70g de beurre
- 1 feuille de laurier
- 2 branches de thym
- noix de muscade moulue
- poivre et sel

## Preparation

Faites revenir l'échalote hachée 5 min à feu doux dans une casserole avec 20g de beurre. Ajoutez les poireaux émincés. Faites revenir 5 minutes à feu doux. Joignez les pommes de terre pelées et coupées en morceaux réguliers, ainsi que le laurier et le thym. Couvrez d'eau, portez à ébullition et laissez mijoter 20 min.

Pendant ce temps, faites fondre 20g de beurre dans une poêle. Faites-y cuire les saucisses à feu doux, en les retournant régulièrement pour qu'elles soient dorées de partout. Retirez-les et tenez les au chaud. Jetez le gras de la poêle, ajoutez-y l'eau et le 1/2 cube de bouillon émietté. Grattez et faites réduire un peu. Ajoutez la moutarde et rectifiez l'assaisonnement.

Égouttez les poireaux et les pommes de terre. Remettez-les dans la casserole, sur feu doux. Ajoutez le lait, le reste du beurre, du poivre, du sel et de la noix de muscade, selon vos goûts. Écrasez au presse-purée jusqu'à obtention d'un stoemp onctueux.

Servez le stoemp avec les saucisses et la sauce.

_Delhaize_
