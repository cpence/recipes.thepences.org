---
title: "Cashew Pork Tenderloin"
author: "juliaphilip"
date: "2010-04-26"
categories:
  - main course
tags:
  - pence
  - pork
---

## Ingredients

*   1 tbsp vegetable oil
*   3/4 tsp crushed red pepper
*   1/4 c soy sauce + 2 tbl
*   1 tbsp dry sherry
*   1 1/4 tsp sugar
*   1 clove garlic, pressed
*   1/2 c finely chopped salted, roasted cashews
*   2 pork tenderloins, 3/4 lb each
*   1/2 tsp grated ginger

## Preparation

Combine vegetable oil and crushed red pepper in a small bowl. Let stand 5 min.

Stir in soy sauce, sherry, sugar,  and garlic until sugar dissolves. Add cashews.

Place pork tenderloins in a shallow, foil-lined baking pan. Pour cashew mixture over pork, roll to coat on all sides. Let stand 30 min, turning occasionally. Bake pork in sauce at 325 F for 50-55 min. Remove from oven and let stand 10 min. Cut across the grain into small slices. Mix together 2 tbl soy sauce and grated ginger and serve over pork.
