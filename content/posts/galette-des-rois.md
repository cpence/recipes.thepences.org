---
title: "Galette des rois"
author: "pencechp"
date: "2019-01-06"
categories:
  - dessert
tags:
  - almond
  - cake
  - french
  - untested
---

## Ingredients

*   1 œuf a température ambiante \[1 egg at room temperature\]
*   60 gr beurre mou \[60g butter, softened\]
*   60 gr amandes en poudre \[60gr ground almonds\]
*   60 gr sucre poudre/semoule \[60g superfine sugar\]
*   1 jaune d œuf pour décorer \[1 egg yolk for decoration\]
*   Crème fraîche si besoin \[Crème fraîche, if needed\]
*   500 gr pâte feuilletée \[500gr or 1/2 lb. puff pastry\]
*   2-3 gouttes d'arôme amande amère ou eau de rose \[2-3 drops of bitter almond extract, optional, can substitute rose water\]

## Preparation

Battre l’œuf entier par fouet électrique jusqu'à obtenir une mousse. Ajouter le sucre jusqu'à ce qu il se dissolve. Ajouter le beurre continuer avec le fouet. Ajouter les amandes, bien mélanger. Si trop épais, ajout de crème fraîche. \[Beat the whole egg with an electric mixer just until a mousse is formed. Add the sugar and beat just until it dissolves. Add the butter and continue beating. Add almonds, mix well.\]

Découper 2 ronds (assiette comme aide), 1 rond sur du papier cuisson/plaque du four. Étaler la frangipane sur rond de pâte feuilletée, laisser au moins 1 cm au bord sans frangipane Cacher la fève. Mouiller le bord libre. Mettre le 2ème rond de pâte feuilletée, bien appuyer les bords. Battre jaune d'œuf avec un peu de sucre ou sel. Enduire la galette. Avec une fourchette, décorer. Four préchauffe environ 170 chaleur tournante. Attendre patiemment environ 30 min. \[Cut two rounds of puff pastry (using a plate as a guide), and place one on top of parchement paper on a cookie sheet. Spread the frangipane on the round, leaving 1/2" around the outside. Hide the bean/baby/etc. Moisten the edge you left free. Put the second round on top, seal the edges well. Beat the egg yolk with a little sugar or salt. Coat the top of the cake. With a fork, decorate (traditional is a kinda wavy-lines pattern across the top). Bake in a preheated oven around 170C (325-350F), convection. Wait patiently for 30 minutes.\]

On peut le faire comme ça pour 5/6 personnes ou avec seulement 1 paquet de pâte feuilletée et une plus petite assiette pour 2 personnes (même frangipane). \[The recipe like this is for 5 or 6 people, or with just 1 packet of puff pastry and a smaller plate as your guide you can make it for 2 people (with the same amount of filling).\]

_Reddit_
