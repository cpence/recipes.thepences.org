---
title: "Seared Tofu with Sugar Snap Peas and Sesame Seeds"
author: "pencechp"
date: "2013-06-13"
categories:
  - main course
tags:
  - greenbean
  - peas
  - thai
  - vegan
  - vegetarian
---

## Ingredients

*   1 14-ounce package extra-firm tofu
*   4 large garlic cloves, grated
*   2 small jalapeño chiles, seeds and veins removed if desired, thinly sliced
*   1 1/2 tablespoons soy sauce
*   1 1/2 teaspoons grated ginger root
*   1 1/2 tablespoons fresh lime juice, more to taste
*   1 1/2 teaspoons toasted (Asian) sesame oil, more for drizzling
*   1 1/2 teaspoons Asian fish sauce
*   1 teaspoon honey
*   2 tablespoons peanut oil, more if needed
*   6 ounces sugar snap peas, trimmed and thinly sliced
*   3 scallions, thinly sliced
*   Sesame seeds, for serving
*   Cooked rice, for serving
*   Chopped cilantro or basil, for serving

[CP: As it stands, this is *quite* hot. Cut the jalapeños in half to make it
merely warm, or cut them entirely for mild (the ginger still adds some heat).]

## Preparation

Drain tofu, wrap it with a clean dish towel or several layers of paper towels
and place on a rimmed plate; top with another plate and a weight (a can
works). Let drain further.

Meanwhile, make the sauce: In a small bowl, combine garlic, chiles, soy sauce,
ginger, lime juice, sesame oil, fish sauce and honey.

Unwrap tofu and cut crosswise into 3/4-inch-thick slices. Pat slices dry.

Heat a large skillet over high heat until very hot, about 5 minutes. Add peanut
oil and let heat for 30 seconds, then carefully add tofu. Don’t touch tofu for 2
to 3 minutes, letting it sear until golden brown. Flip and sear for another 2 to
3 minutes. Move tofu to one side of pan (or stack pieces on top of one another
to make room in pan), then add sugar snap peas, scallions and, if needed, a few
drops more peanut oil. Stir-fry vegetables until they start to soften, 1 to 2
minutes. Add sauce and stir well, cooking until peas are done to taste, another
minute or 2. Spoon sauce all over tofu, unstacking it if necessary.

Sprinkle sesame seeds over tofu and vegetables and serve over rice, sprinkled
with cilantro or basil.

_Serves 2-3._

_The New York Times_
