---
title: "Rum Balls"
author: "pencechp"
date: "2010-05-09"
categories:
  - dessert
tags:
  - pence
  - rum
---

## Ingredients

*   12 oz. vanilla wafers (bashed up to a fine powder)
*   3 tbsp. cocoa
*   3/4 c. light corn syrup
*   1 c. finely chopped pecans
*   3/8 c. rum

## Preparation

Mash up, make balls, dry on waxed paper, roll in powdered sugar.  Undiluted frozen tangerine juice concentrate can be used instead of rum (without cocoa).
