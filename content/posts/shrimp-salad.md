---
title: "Shrimp Salad"
author: "pencechp"
date: "2012-07-05"
categories:
  - main course
  - side dish
tags:
  - chinese
  - pence
  - salad
  - shrimp
---

## Ingredients

*   1 lb. cooked shrimp
*   1/2 lb. frozen peas
*   1 cup chopped celery [or 1 green apple, julienned]
*   1 small onion, diced
*   1 cup mayonnaise
*   1 1/2 tbsp. lemon
*   1/8 tsp. curry powder
*   1 tsp. soy sauce
*   1/8 tsp. garlic powder
*   3 oz. chow mein noodles
*   1/2 cup slivered almonds

## Preparation

Combine shrimp, peas, celery, and onion and chill.

Mix next five ingredients and chill.

Combine shrimp, sauce, noodles and almonds and let stand about 5 minutes before serving.
