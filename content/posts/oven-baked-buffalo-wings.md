---
title: "Oven-Baked Buffalo Wings"
author: "pencechp"
date: "2017-09-17"
categories:
  - main course
tags:
  - chicken
  - untested
---

## Ingredients

*   4 lb. chicken wings
*   2 tbsp. baking powder
*   3/4 tsp. salt

## Preparation

Before you begin, uncover the chicken wings and leave them in the fridge for a few hours to dry out.

Adjust oven racks to upper-middle and lower-middle positions. Preheat oven to 250F. Line a baking tray with foil, then place a rack on the foil. Coat the rack with oil.

Place wings in a large bowl or in a ziplock bag. Add the baking powder and salt, then toss to coat evenly. Place the wings on the baking tray in a single layer with the skin side up. They should just fit snugly. Place wings on the lower middle oven rack and bake for 30 minutes.

Move wings up to the upper middle rack and increase the oven temperature to 425F/220C. Bake for 40 - 50 minutes, rotating the tray halfway through. Remove baking tray from the oven and let it stand for 5 minutes. Toss wings in sauce of choice or serve it on the side to dip / drizzle on the wings, then serve.

_RecpieTin Eats/CI_
