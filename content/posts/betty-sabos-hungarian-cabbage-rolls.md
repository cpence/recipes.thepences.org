---
title: "Betty Sabo's Hungarian Cabbage Rolls"
author: "juliaphilip"
date: "2010-04-26"
categories:
  - main course
tags:
  - cabbage
  - hungarian
  - pork
---

## Ingredients

*   2 lb ground pork
*   2 chopped onion
*   1/2 c rice
*   2 medium or 1 large head of cabbage
*   bacon
*   1 lb Boarshead package sauerkraut, drained but not rinsed
*   1 can tomato sauce
*   1/2 c brown sugar
*   1 can tomato soup (or a second can tomato sauce)
*   sour cream for serving

## Preparation

Mix together the ground pork, one chopped onion and rice.

Heat a large pot of boiling water. Cut the core out of the head of cabbage and submerge it in the boiling water, cut-out up. Tease the leaves off of the outside of the cabbage as it heats, cutting out more core if you need to. You want about 12 to 18 leaves, cooked in the boiling water until wilted. Drain.

Line the bottom of a covered casserole with bacon. Add a layer of sauerkraut, chopped onion, one can tomato sauce, and 1/2 c brown sugar.

Make cabbage rolls with meat and cabbage leaves, rolling up as you would an eggroll.

Put one layer of cabbage rolls in casserole and top with a layer of sauerkrat. Repeat.

Pour one can of tomato soup over the top. Cover and bake at 325 F for several hours. Serve with sour cream.
