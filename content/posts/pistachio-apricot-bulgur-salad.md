---
title: "Pistachio-Apricot Bulgur Salad"
author: "pencechp"
date: "2011-01-30"
categories:
  - main course
  - side dish
tags:
  - apricot
  - bulgur
  - pistachio
  - salad
  - vegan
  - vegetarian
---

## Ingredients

*   1 c. fine- or medium-grain bulgur
*   1/2 c. chopped dried apricots
*   1 c. boiling water
*   1 c. chopped fresh parsley
*   3 tbsp. finely chopped fresh mint
*   1/2 c. shelled chopped pistachios
*   1/3 c. olive or pistachio oil
*   3 tbsp. orange juice or white wine vinegar
*   1/4 c. minced red onion
*   2 tbsp. thinly sliced green onion
*   3/4 tsp. coarse salt
*   1/4 tsp. fresh ground pepper

## Preparation

Combine bulgur and apricots in a bowl. Add boiling water and let stand 30 minutes or until liquid is absorbed. Add remaining ingredients and toss gently to mix.

_Eat, Drink, and Be Healthy_
