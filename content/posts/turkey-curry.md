---
title: "Turkey Curry"
author: "juliaphilip"
date: "2009-12-06"
categories:
  - main course
tags:
  - curry
  - turkey
  - untested
---

## Ingredients

*   6 cups leftover turkey, chopped
*   2 tbsp canola oil
*   2 large onions, quartered and finely sliced
*   1 tbsp sweet curry
*   3-4 pieces leftover fruit (pears, peaches, plums)
*   1 1/2-2 cups water
*   2 tbsp white vinegar
*   1 cup yogurt, drained
*   1/4 cup fresh cilantro leaves, chopped

## Preparation

In a large pan, heat the oil over medium. Add onions and cook until lightly brown, stirring frequently. Add the sweet curry powder, stir and cook another 5 minutes. While the onions are cooking, cut up fruit. Use whatever fruit has gotten a bit soft in the fridge, peel or not as desired. Add the water, vinegar and fruit to the pan, stir and get everything simmering, then add the turkey, Make sure the mixture never boils, so the turkey stays tender. Cook everything for as long as possible, 30 minutes minimum. Make rice or pasta while the curry is cooking. Drain the yogurt but placing it in a very fine strainer or a cheese cloth-lined regular strainer over a small bowl while curry is cooking. At the end of the cooking time, add the fresh cilantro leaves and cook for one final minute. Turn the heat off and stir in the yogurt. Serve.

_Penzys_
