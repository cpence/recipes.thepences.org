---
title: "Ikea Swedish Meatballs"
date: 2020-09-26T21:48:24+02:00
categories:
    - main course
tags:
    - swedish
---

## Ingredients

*for the meatballs*

*   500g ground beef
*   250g ground pork
*   1 onion, grated
*   1 clove garlic, pressed
*   100g breadcrumbs
*   1 egg
*   5 tbsp. milk
*   salt and pepper
*   dash oil

*for the sauce*

*   40g butter
*   40g plain flour
*   150 mL vegetable stock
*   150 mL beef stock
*   150 mL thick double cream
*   2 tsp. soy sauce
*   1 tsp. dijon mustard

## Preparation

*Make the meatballs:*

Combine ground meats and mix with your fingers, breaking up lumps. Add finely
chopped onion, garlic, breadcrumbs, egg, and mix. Add milk, season generously
with salt and pepper. Shape into small, round balls. Place on a clean plate,
cover, and refrigerate for two hours.

In a frying pan, heat oil over medium heat. When hot, gently add meatballs and
brown on all sides. Move to an ovenproof dish, cover, and bake in a hot oven
(180C conventional; 160C convection) and cook for 30 minutes.

*Make the sauce:*

Melt 40g of butter in a pan. Whisk in the flour and stir for 2 minutes. Add
stocks, stirring. Add cream, soy, and mustard. Bring to a simmer and allow to
thicken.

Serve with creamy mashed potatoes or boiled new potatoes.

*Twitter*

