---
title: "Ragu alla Bolognese"
author: "pencechp"
date: "2015-05-06"
categories:
  - main course
tags:
  - italian
  - pasta
  - sauce
  - untested
---

## Ingredients

*   1 ½ lb beef (oxtail, short rib, shank—something with fat, flavor, and preferably some marrow and gelatin), in one or two large pieces
*   Salt to taste
*   1 Tbsp olive oil
*   2 medium carrots, peeled and minced
*   2 ribs celery, minced
*   1 medium onion, minced
*   1 lb ground pork (preferably from the shoulder)
*   ½ cup minced pancetta
*   1 small can whole peeled tomatoes (preferably San Marzano), drained and crushed 
*  1 cup dry red wine
*  1 ½ cups chicken stock
*  1 bay leaf

## Preparation

Heat the oil in a large, heavy-bottomed pot or Dutch oven set over medium-high heat. Season the beef on all sides with salt and cook until deeply browned all over. Remove from the pan.

If the pan is dry, add another splash of oil. Sauté the carrot, celery, and onion until soft, about 5 minutes. Add the pork and pancetta and cook until lightly browned, then stir in the tomato and continue cooking for another 3 minutes. Return the beef to the pan, add the wine, stock, bay leaf and cover. Turn the heat to low and simmer for two hours, until the beef is falling apart.

Shred the beef by hand or with two forks and fold back into the sauce, discarding any bones, excess fat, or cartilage. If the sauce looks too dry, add a splash of broth or water to get the right consistency. Serve over pasta with freshly grated Parmigiano Reggiano. Makes about 8 servings.

_Matt Goulding, Roads and Kingdoms_
