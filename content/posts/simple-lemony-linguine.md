---
title: "Simple Lemony Linguine"
author: "pencechp"
date: "2017-09-17"
categories:
  - main course
  - side dish
tags:
  - italian
  - lemon
  - pasta
  - vegetarian
---

## Ingredients

*   500 g dried linguine
*   3 lemons (zest and juice)
*   6 tbsp. extra virgin olive oil
*   100 g. parmesan cheese, grated
*   salt and freshly ground black pepper

## Preparation

Cook the linguine according to the instructions on the packet - in a generous amount of boiling, salted water. Drain and return to the saucepan. Mix the lemon juice and zest with the olive oil, then stir in the parmesan so that it becomes creamy. Season and add more lemon to taste. Add the lemon pasta sauce to the linguine shaking the pan to coat each strand. Garnish with some herbs to suit yourself.

_Nudo_
