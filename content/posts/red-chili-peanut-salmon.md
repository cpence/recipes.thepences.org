---
title: "Red Chili Peanut Salmon"
author: "pencechp"
date: "2013-12-04"
categories:
  - main course
tags:
  - fish
  - thai
  - untested
---

## Ingredients

*   1/4 cup peanut butter
*   juice from 1 lime
*   1 tablespoon fresh orange juice
*   1 heaping tablespoon Sriracha, or other red chili sauce
*   1 teaspoon minced fresh ginger
*   1 tablespoon rice vinegar
*   1/4 cup chopped cilantro leaves
*   salt and pepper to taste
*   6 6-ounce salmon filets

## Preparation

Preheat oven to 450°F. In a medium bowl, whisk together the wet ingredients and cilantro.

Line a baking sheet with foil, then place salmon filets (flesh-side up) on the foil and sprinkle generously with salt and pepper. Spoon the glaze over the fish so it’s fully coated.

Bake at 450° for 12-15 minutes, or until fish feels semi-firm with you press it with your finger. If it’s not done when you initially check it, put it back in the oven at 2 minute intervals until cooked to desired level. Fish easily overcooks, so keep a close eye on it. Remove from oven, let cool 1-2 minutes, then serve each filet over a pile of rice mixed with green onions, lime zest, and cilantro.

_Amelia Winslow, HuffPo_
