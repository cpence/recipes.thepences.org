---
title: "Dark Chocolate and Pine Nut Crostata"
author: "pencechp"
date: "2010-05-08"
categories:
  - dessert
tags:
  - chocolate
  - pie
  - untested
---

## Ingredients

*Crust:*

*   1 2/3 c. all purpose flour
*   1 c. powdered sugar
*   1/2 tsp. salt
*   6 tbsp. (3/4 stick) unsalted butter, diced, room temperature
*   2 lg. egg yolks
*   1 lg. egg

*Filling:*

*   10 oz. bittersweet (70%) chocolate, finely chopped
*   2 c. heavy whipping cream
*   1/4 c. honey
*   1/4 c. pine nuts, toasted
*   whipping cream

## Preparation

Blend flour, powdered sugar, and salt in processor 5 seconds.  Add butter, yolks, and egg.  Blend until moist clumps form.  Knead dough into ball and flatten into disk.  Wrap and chill at least 30 minutes and up to 1 day.

Roll out dough on lightly floured surface to 14-inch round.  Transfer to 10-inch diameter tart pan with removable bottom.  Cut off all but 1/2-inch overhang.  Fold overhang in, pressing to form double-thick sides that extend 1/4-inch above rim.  Chill crust 30 minutes.

Preheat oven to 375F.  Bake crust 5 minutes.  Using back of fork, press up sides of crust if slipping.  Bake until golden, pressing up sides and piercing with fork if crust bubbles, about 25 minutes longer.  Cool crust completely.

Place chocolate in medium bowl.  Bring cream to simmer in saucepan.  Pour cream over chocolate; whisk until smooth.  Whisk in honey.  Pour filling into crust.  Chill until set, at least 2 hours and up to 1 day.

Sprinkle tar with pine nuts.  Serve with whipped cream.
