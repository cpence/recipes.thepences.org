---
title: "Canning"
author: "juliaphilip"
date: "2010-05-16"
categories:
  - miscellaneous
tags:
---

How to can stuff:

1. Make the stuff you want to can.
2. Clean and sterilize your jars and rings.
3. Heat the lids in hot but not boiling water for several minutes to soften the gummed surface and clean the lids.
4. Fill the hot jars within 1/4 in of the top, place lids and rings on jars.
5. Boil jars for 5 min, then remove and let stand undisturbed for 24 h.
