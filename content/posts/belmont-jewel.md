---
title: "Belmont Jewel"
author: "pencechp"
date: "2019-06-10"
categories:
  - cocktails
tags:
  - untested
  - whiskey
---

## Ingredients

*   1 oz. pomegranate juice
*   1.5 oz. Bourbon
*   2 oz. lemonade
*   lemon slice

## Preparation

Shake with ice, strain, and serve in a highball glass over more ice.
