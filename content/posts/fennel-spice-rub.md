---
title: "Fennel Spice Rub"
author: "juliaphilip"
date: "2009-12-05"
categories:
  - miscellaneous
tags:
  - fennel
  - seasoning
---

## Ingredients

*   1 cup fennel seeds
*   3 tablespoons coriander seeds
*   2 tablespoons white peppercorns
*   3 tablespoons kosher salt

## Preparation

Put the fennel seeds, coriander seeds, and peppercorns in a heavy pan over medium heat. Watch carefully, tossing frequently so the seeds toast evenly. When light brown and fragrant, pour the seeds onto a plate to cool. They must be cool before grinding, or they will gum up the blades.

Pour the seeds into a blender and add the salt. Blend to a fine powder, shaking the blender occasionally to redistribute the seeds. Store in a tightly sealed glass jar in a cool, dry place, or freeze.

_Michael Chiarello, Napa Style_
