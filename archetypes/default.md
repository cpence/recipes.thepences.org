---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
categories:
  - main course
  - side dish
  - appetizer
  - dessert
  - breakfast
  - bread
  - cocktails
  - miscellaneous
tags:
  - list here
---

## Ingredients

- list here
- and here

## Preparation

Type here.

_Source_
